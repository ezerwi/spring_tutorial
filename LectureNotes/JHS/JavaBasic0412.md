# 페이지 정보
- 주소 : to2.kr/bf1
- 클래스 : 2020년 04월 11일, 주말 12시 자바

# 강사정보
- 이름 : 장희성
- 전화번호 : 010-8824-5558

# 학생링크
## 코드업
- [코드업 100제](https://codeup.kr/problemsetsol.php?psid=23)

## 프로그래밍 기초
- [기초 키보드 사용법](https://s.codepen.io/jangka44/debug/OzzVWM)
- [타이핑 정글](https://www.typingclub.com/sportal/program-3.game)
- [code.org 코스 3](https://studio.code.org/s/course3)
- [code.org 코스 4](https://studio.code.org/s/course4)
- [프로그래머스 자바 입문](https://programmers.co.kr/learn/courses/5)
- [프로그래머스 자바 중급](https://programmers.co.kr/learn/courses/9)
- [프로그래머스 자바스크립트 입문](https://programmers.co.kr/learn/courses/3)
  - [풀이](https://codepen.io/jangka44/live/mddjOxo)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 30분이 넘으면 안됩니다.
- [플레이코드몽키](https://www.playcodemonkey.com)
  - 계정
    - 아이디 : dnehd1515@naver.com
    - 비번 : sbs1234
- [플레이코드몽키, 스테이지 쉽게 접근하기](https://codepen.io/jangka44/debug/ZEEWRzZ)

## 온라인 툴
- [repl 자바 연습장](https://repl.it/languages/java)
  - 사용법
    - 실행 : Ctrl + Enter
- [code.oa.gg/java8](http://code.oa.gg/java8)
  - 사용법
    - 실행 : Alt + Enter
    - 소스보기 : Alt + 1
    - 결과보기 : Alt + 2

## 프론트엔드 기초
- [CSS DINER](https://flukeout.github.io/)
- [새 연습장(코드펜)](https://codepen.io/pen/)
  - 주소 : codepen.io/pen

# 2020-04-11, 01일차
## 개념
- `\n`은 콘솔에서 줄바꿈을 의미합니다.

## 문제
### 출력
- [문제 - `안녕하세요.` 10번 출력](https://repl.it/@jangka512/JAVA-5-18-04-10-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-2)
- [문제 - `안녕하세요.` 10번 출력, System.out.println을 한번만 사용](https://repl.it/@jangka512/JAVA-5-18-04-10-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-4)
- [문제 - `안녕하세요.` 10번 출력, 역슬래쉬 활용](https://repl.it/@jangka512/JAVA-5-18-04-10-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-6)

### 변수
- [문제 - 변수 a와 b의 값을 서로 교체](https://repl.it/@jangka512/JAVA-5-18-04-10-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-8)
- [문제 - 변수 a와 b의 값을 서로 교체, 숫자와 사칙연산 사용 금지](https://repl.it/@jangka512/JAVA-5-18-04-10-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-10)

### 조건문
- [개념 - if](https://code.oa.gg/java8/1537)

## 오늘의 동영상
- [동영상 - 2018 06 28, 자바, 개념, repl.it 사용법](https://youtu.be/7Q_JDQm7-Y0)
- [동영상 - 2019 12 11, 자바, 출력, 변수, swap](https://www.youtube.com/watch?v=ffyaI23G8LU&feature=youtu.be)
- [동영상 - 2020 04 09, 프로그래밍 일반, 개념, 안녕하세요를 키보드만으로 많이 입력 ](https://youtu.be/C0x4Oeu-s-I)
- [동영상 - 2020 04 09, 자바, 개념, repl.it 사용법](https://youtu.be/2hczZ0MCoQE)
- [동영상 - 2020 04 09, 자바, 개념, System.out.println](https://youtu.be/ZkUwVrOq9T4)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력](https://youtu.be/u5pI5tTttoM)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력, println은 한번](https://youtu.be/98WDfm8Wr3g)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력, println은 한번, 뉴라인](https://youtu.be/-dzllmseUuo)
- [동영상 - 2020 04 09, 자바, 문제, 두 변수의 값 교환, 단순한 방법](https://youtu.be/w3db1JFGUhQ)
- [동영상 - 2020 04 09, 자바, 문제, 두 변수의 값 교환, 임시변수로 해결](https://youtu.be/EPH1rZS20-8)
- [동영상 - 2020 04 09, 프로그래밍 일반, 개념, to2.kr 이용해서 주소 줄이기](https://youtu.be/gcmGLaEpOLo)
- [동영상 - 2020_04_10, 자바, 개념, print, 변수, swap](https://www.youtube.com/watch?v=SBmCr4f2sfQ)
- [동영상 - 2018 06 29, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=-EFKDubiXuE)
- [동영상 - 2019 12 11, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=RU_fhnKuumw&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=hEyLOealTac&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=95EFiiikxfE)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if, else if, else](https://www.youtube.com/watch?v=K6wwRasJ1yQ)
- [동영상 - 2020 04 11, 자바, 개념, 변수, 소리작음](https://youtu.be/QwDct-QRjKA)
- [동영상 - 2020 04 11, 자바, 개념, if, else if, else, 소리작음](https://youtu.be/tA9G5Akbi2E)
- [동영상 - 2020 04 11, 프로그래밍, 개념, code.org 하는 법, 소리작음](https://youtu.be/02u49_N8gKo)
- [동영상 - 2020 04 11, 자바, 문제, 두 변수가 가지고 있는 값 바꾸기, 단순한 방법, 소리작음](https://youtu.be/-r4oJGZ-eC4)
- [동영상 - 2020 04 11, 자바, 개념, 문자와 숫자의 더하기, 소리작음](https://youtu.be/XX6IAKEci4w)
- [동영상 - 2020 04 11, 자바, 개념, 조건문, if, 소리작음](https://youtu.be/qW_4fMTEeTw)
- [동영상 - 2020 04 11, 자바, 문제, 두 변수가 가지고 있는 값 바꾸기, 소리작음](https://youtu.be/rBrP4IBGvDI)
- [동영상 - 2020 04 11, 자바, 문제, 안녕하세요 10번 출력, 뉴라인, 소리작음](https://youtu.be/SV1pCNLrxoc)
- [동영상 - 2020 04 11, 자바, 문제, 안녕하세요 10번 출력, println 한번, 소리작음](https://youtu.be/gtWmS_91a60)
- [동영상 - 2020 04 11, 자바, 문제, 안녕하세요 10번 출력](https://youtu.be/KriX2bSv06U)
- [동영상 - 2020 04 11, 자바, 개념, 이클립스, 사용법, 소리작음](https://youtu.be/XBIDCLJ6nSE)

# 2020-04-12, 02일차
## 개념
- 정수끼리 연산하면 결과는 정수이다.
  - 10 / 20 은 0.5가 아니라 0이다.
- 연산자 종류
  - 사칙연산
    - `+` : 더하기
    - `-` : 빼기
    - `*` : 곱하기
    - `/` : 나누기
  - 논리연산
    - `==` : 같다
    - `!=` : 다르다
    - `>` : 크다(초과)
    - `<` : 작다(미만)
    - `>=` : 크거나 같다(이상)
    - `<=` : 작거나 같다(이하)
    - `&&` : 그리고(and)
    - `||` : 또는(or)
- 소스코드의 4대 구성 요소
  - 변수
    - 변수선언 : int a;
      - int를 변수타입이라고 한다.
      - a를 변수명이라고 한다.
      - `a라는 변수를 만들겠습니다. 다만 앞으로 a에는 정수만 담을 수 있습니다.` 라는 뜻이다.
    - 변수선언(변수생성)은 2번 이상 할 수 없다.
    - 변수의 값은 바꿀 수 있다.
    - 변수는 값을 넣을 때 빼고는 값(자신이 가지고 있는) 취급을 해야 한다.
  - 값
    - 종류
      - 숫자
      - 문자
      - ...
  - 조건문
    - if문
      - if ( 조건문 ) { 실행문 }
    - 추가옵션 : else
      - if ( 조건문 ) { 실행문1 } else { 실행문2 }
        - 실행문2는 조건이 거짓일 때 실행된다.
        - 양자택일이 된다.
    - switch문
  - 반복문
- 연산자 우선순위
  - 우선순위과 높은 것이 먼저 실행된다.
  - 사칙연산자가 논리연산자보다 우선순위가 높다.
  - `*`, `/`는 다른 사칙연산자 보다 우선순위가 높다.
  - `&&`, `||`는 다른 논리 연산자 보다 우선순위가 낮다.
  - 앞에 있는게 먼저 실행된다.
- 증감 연산자
  - `i++;` => i 의 값을 1 증가 시킨다.
  - `i--;` => i 의 값을 1 감소 시킨다.
  - `i = i + 2;` => i 의 값을 2 증가 시킨다.
  - `i += 2;` => i 의 값을 2 증가 시킨다.(위와 같은 표현)
  - `i = i - 2;` => i 의 값을 2 감소 시킨다.
  - `i -= 2;` => i 의 값을 2 감소 시킨다.(위와 같은 표현)

## 문제
### 조건문
- [개념 - if, else if, else](https://code.oa.gg/java8/1625)
- [문제 - 실행되는 출력문에는 참 그렇지 않으면 거짓 이라고 적어주세요.](https://repl.it/@jangka512/JAVA-5-18-04-10-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-12)
- [개념 - &&(그리고), ||(또는)](https://code.oa.gg/java8/1538)
- [문제 - 할인 대상인지 아닌지 출력(&&, || 없이도 가능해야합니다.)](https://repl.it/@jangka512/LankyCruelAudit)
- [문제 - 정답](https://repl.it/@jangka512/OrangeCylindricalCharactermapping)
- [문제 - 정답 v2](https://code.oa.gg/java8/1627)

### 반복문
- [문제 - 구구단 8단을 출력해주세요. 반복문 사용 금지](https://repl.it/@jangka512/JAVA-5-18-04-16-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-2)
- [문제 - 구구단 8단을 출력해주세요. dan 변수 활용](https://repl.it/@jangka512/JAVA-5-18-04-16-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-4)
- [문제 - 구구단 8단을 출력해주세요. dan 변수의 값에 따라 다른 단이 나올 수 있도록 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-6)
- [문제 - 구구단 8단을 출력해주세요. 2 이상의 숫자를 사용할 수 없습니다.](https://repl.it/@jangka512/JAVA-5-18-04-16-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-8)
- [문제 - 구구단 8단을 출력해주세요. 9까지가 아닌 1000까지 곱해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-10)
- [문제 - 1부터 5까지 출력해주세요.](https://repl.it/@jangka512/PossiblePlayfulOpenlook)
- [문제 - 정답](https://repl.it/@jangka512/FamousCurvyConditions)
- [문제 - -100부터 25까지 출력해주세요.](https://repl.it/@jangka512/StridentEssentialDatawarehouse)
- [문제 - 정답](https://repl.it/@jangka512/ImperturbableDownrightParticles)
- [문제 - 구구단 8단을 출력해주세요. 1000부터 1까지 곱해주세요.](https://repl.it/@jangka512/PowderblueJubilantPostgres)
- [문제 - 정답](https://repl.it/@jangka512/EdibleLumberingAdware)
- [문제 - 99단 8단을 출력해주세요. 1000부터 -500까지 곱해주세요.](https://repl.it/@jangka512/JointExperiencedRoute)
- [문제 - 정답](https://repl.it/@jangka512/NegativeWhimsicalDesign)
- [문제 - 1부터 5까지의 합을 출력해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-12)

## 오늘의 동영상
- [동영상 - 2019 12 12, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=Q13y-MrxaEg&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=2EVhMJOl1eU)
- [동영상 - 2020_04_10, 자바, 문제, 조건식의 참, 거짓 여부를 적어주세요](https://www.youtube.com/watch?v=qoP-x4wi38M)
- [동영상 - 2019 12 12, 자바, 문제, 할인 대상인지 출력](https://www.youtube.com/watch?v=u8GZfR5o4Ro&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 문제, 할인 대상인지 출력, 1부](https://www.youtube.com/watch?v=vwen4eLADA4)
- [동영상 - 2020_04_10, 자바, 개념, 코드업 푸는방법](https://www.youtube.com/watch?v=8ZC2cPGR9M4)
- [동영상 - 2019 12 12, 자바, 개념, if문 기초](https://www.youtube.com/watch?v=hEyLOealTac&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 개념, if문 기초(and, or)](https://www.youtube.com/watch?v=Q13y-MrxaEg&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 문제, if문 할인조건 문제풀이](https://www.youtube.com/watch?v=u8GZfR5o4Ro&feature=youtu.be)
- [동영상 - 2019 12 13, 자바, 개념, String, 합치기](https://www.youtube.com/watch?v=6POYRG1oSi4&feature=youtu.be)
- [동영상 - 2019 12 13, 자바, 문제, while 문, 구구단](https://www.youtube.com/watch?v=x4SZqumRNlA&feature=youtu.be)
- [동영상 - 2019 12 13, 자바, 문제, while 문, 1부터 5까지의 합](https://www.youtube.com/watch?v=wo4Le51XXFc&feature=youtu.be)
- [동영상 - 2020 04 13, 자바, 개념, 코드업, 기초 반복문 문제집, 초반풀이](https://www.youtube.com/watch?v=s48wTjh0MlU&feature=youtu.be)
- [동영상 - 2020 04 13, IT일반, 개념, 티스토리 글 관리법](https://www.youtube.com/watch?v=j8J5XI91DEw&feature=youtu.be)

## 숙제
- [코드업 문제집, 기초1. 출력문](https://codeup.kr/problemsetsol.php?psid=9)
  - 전부
- [코드업 문제집, 기초2. 입출력문 및 연산자](https://codeup.kr/problemsetsol.php?psid=10)
  - 전부
- [코드업 문제집, 기초3. if ~ else](https://codeup.kr/problemsetsol.php?psid=11)
  - 20문제
- [코드업 문제집, 기초4-1. 단순 반복문](https://codeup.kr/problemsetsol.php?psid=13)
  - 20문제
  
# 2020-04-18, 03일차
## 개념
- 모든 변수는 메모리에 저장된다.
- int 변수는 4바이트 이다.
- 변수에는 오직 8바이트 이하의 값만 넣을 수 있다.
- 객체가 필요한 이유
  - 변수에는 오직 값 1개만 넣을 수 있다.
  - 프로그래밍을 하다보면 변수에 값을 여러개 넣을 필요가 있을 때가 있다.
  - 그래서 고안된 것이 객체이다.
  - 객체는 커피 캐리어에 비유될 수 있다.
  - 커피 캐리어에는 커피를 여러잔 담을 수 있다.
  - 커피 캐리어는 용도에 따라 종류가 여러가지 있다.
  - 프로그래밍을 할 때 상황에 따라 서로 다른 종류의 객체 여러개가 필요하다.
  - 객체를 일종의 제품으로 보았을 때 객체를 만들기 위해서는 설계도 즉 클래스가 필요하다.
  - 객체는 너무 커서 변수에 담을 수 없다.
  - 그래서 또 고안된 것이 리모콘 시스템이다.
  - 클래스로는 3가지를 할 수 있다.
  - 클래스로는 객체도 만들 수 있고 그 객체를 조종할 수 있는 리모콘도 만들 수 있다. 그리고 또 객체리모콘을 담을 변수도 만들 수 있다.
- 변수에는 오직 1차원적인 값만 저장 할 수 있다.
  - 1차원 적인 값(데이터)
    - 5
    - 3.14
    - 'a'
    - true
  - 복잡한 값
    - 객체
- 변수에는 객체를 저장할 수 없다.
  - 객체가 너무 크고 변수는 작다.
  - 객체는 여러가지 값(데이터)의 조합이다. 그게 변수에 들어가면 한 덩어리로 해석된다.
    - 즉 변수에 1과 2를 넣으면, 다른 사람들은 그것을 1과 2의 조합이 아닌 12로 본다.
- `new 사람();`을 하면 2가지가 만들어진다.
  - 객체
  - 객체가 자기자신을 조종할 수 있는 리모콘
    - 그 리모콘을 객체 스스로는 this 라고 부른다.

## 문제
### 반복문
- [문제 - -100부터 25까지의 합을 출력해주세요.](https://repl.it/@jangka512/RoyalbluePartialPdf)
- [문제 - 정답](https://repl.it/@jangka512/MutedPreviousSupport)
- [문제 - 1부터 3까지 출력하는 작업을 10번 해주세요. 2중 while문 사용](https://repl.it/@jangka512/RemarkableImpeccableGnuassembler)
- [문제 - 정답](https://repl.it/@jangka512/UnitedRustyPackagedsoftware)
- [개념 - for문](https://code.oa.gg/java8/1630)

### 배열 개념
- [개념 - 배열](https://code.oa.gg/java8/1629)
- [개념 - 배열, 평균, 총합, length](https://code.oa.gg/java8/1631)
- [개념 - 배열, boolean 배열](https://code.oa.gg/java8/1632)

### 객체
- [개념 - 객체, 캐릭터](https://code.oa.gg/java8/1643)
- [개념 - 객체, 리모콘](https://code.oa.gg/java8/1633)
- [개념 - 객체, 리모콘 매개변수](https://code.oa.gg/java8/1634)
- [개념 - 함수로, boolean 배열객체의 값을 반전](https://code.oa.gg/java8/1635)
- [개념 - 캐릭터 객체 정보를, 안 묶은 버전](https://code.oa.gg/java8/1636)
  - 캐릭터 정보 공유 할 때 힘들다.
- [개념 - 캐릭터 객체 정보를, 배열객체로 묶은 버전](https://code.oa.gg/java8/1637)
  - 편하긴 하지만, 이름 같은 속성은 추가가 불가능 하다.
  - char1[0] 이 번호이고, char1[1] 가 나이인걸 외워야 한다.
- [개념 - 캐릭터 객체 정보를, 캐릭터 클래스로 만든 객체로 묶은 버전](https://code.oa.gg/java8/1638)
  - 클래스를 만들어야 한다는게 귀찮지만, 편하다.
- [문제 - 자동차 설계도를 만들어주세요.](https://repl.it/@jangka512/TurboTornWebpages)
- [문제 - 정답](https://repl.it/@jangka512/ForsakenRosySystemsoftware)
- [문제 - 자동차 객체를 담을 변수를 만들어주세요.](https://repl.it/@jangka512/NegligibleUltimateBucket)
- [문제 - 정답](https://repl.it/@jangka512/GranularPapayawhipVirtualmachines)
- [문제 - 자동차 객체를 만들고 변수에 담아주세요.](https://repl.it/@jangka512/HotPlainProlog)
- [문제 - 정답](https://repl.it/@jangka512/ShowyChillyCubase)
- [문제 - 자동차 객체마다 서로 다른 최고속력를 가지도록 해주세요.](https://repl.it/@jangka512/UnwieldyScarceVertex)
- [문제 - 정답](https://repl.it/@jangka512/AmusedWelltodoRecords)
- [문제 - 1개의 자동차가 3번 달리게 해주세요.](https://repl.it/@jangka512/DeepskyblueCoordinatedPrinters)
- [문제 - 정답](https://repl.it/@jangka512/UnluckyKnowingTexts)
- [문제 - 객체를 사용하지 않고 두번째 플레이어를 만들어주세요.](https://repl.it/@jangka512/MobileHospitableBrowser)
- [문제 - 정답](https://repl.it/@jangka512/HighBigheartedCharacter)
- [문제 - 3개의 자동차가 각각 1번씩 달리게 해주세요.](https://repl.it/@jangka512/PracticalSecretLink)
- [문제 - 정답](https://repl.it/@jangka512/TepidCutePackages)

## 오늘의 동영상
- [동영상 - 2019 12 16, 자바, n부터 m까지의 합](https://www.youtube.com/watch?v=3oMWeIW8m2g&feature=youtu.be)
- [동영상 - 2019 12 16, 자바, 2중 while 문](https://www.youtube.com/watch?v=DnRAsosbpL8&feature=youtu.be)
- [동영상 - 2020_04_15, 자바, 개념, for, 중첩반복문, 구구단](https://www.youtube.com/watch?v=LR1wPOTh5ww)
- [동영상 - 2020_04_15, 자바, 개념, for](https://youtu.be/6-p9wqTX6vQ)
- [동영상 - 2020_04_15, 자바, 문제, 1부터 3까지 10번 출력](https://youtu.be/ScnPyxPPfMA)
- [동영상 - 2020_04_15, 자바, 문제, 배열에 숫자 3개 담고, 출력](https://youtu.be/wz6WvA83CEA)
- [동영상 - 2020_04_15, 자바, 문제, 음수 100부터 25까지 출력](https://youtu.be/PkI3QWkK2TU)
- [동영상 - 2018 07 05, 자바, 클래스와 객체](https://www.youtube.com/watch?v=jiDjGO13ccU)
- [동영상 - 2019 12 16, 자바, 객체관련 문제풀이](https://www.youtube.com/watch?v=MpU4Ppa5K6s&feature=youtu.be)
- [동영상 - 2019 12 16, 자바, 객체 기초개념](https://www.youtube.com/watch?v=olaIeerJIJM&feature=youtu.be)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모콘, 매개변수, 설명 1](https://youtu.be/QkY86EYYjOs)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모콘, 매개변수, 설명 2](https://youtu.be/GyGWkmFXkrs)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모콘, 매개변수, 설명 3](https://youtu.be/-MQ46MHqOtg)
- [동영상 - 2020 04 16, 자바, 개념, 객체와 클래스](https://youtu.be/FDPLPzpIOUw)
- [동영상 - 2020 04 16, 자바, 개념, 클래스가 없으면 벌어지는 일](https://youtu.be/Pn6-X24mCrU)
- [동영상 - 2020 04 16, 자바, 문제, 객체관련 문제풀이, 1부](https://youtu.be/FvZS8BP5Ltk)
- [동영상 - 2020 04 16, 자바, 문제, 객체관련 문제풀이, 2부](https://youtu.be/upRc8_yalu4)
- [동영상 - 2020 04 16, 자바, 문제, 크기가 3인 boolean 배열의 요소들을 함수에서 수정](https://youtu.be/NqgGn00inUQ)
- [동영상 - 2020_04_16, 자바, 개념, 객체를 사용하는 이유](https://youtu.be/DKUbn1SIJYs)
- [동영상 - 2020_04_16, 자바, 개념, 함수와 스택, 메모리](https://youtu.be/pmxfK9Pnnx8)
- [동영상 - 2020 04 18, 자바, 개념, 배열](https://youtu.be/w4C9JdLhMyE)
- [동영상 - 2020 04 18, 자바, 개념, 객체, 클래스](https://www.youtube.com/watch?v=_16STeUOEG4)
- [동영상 - 2020 04 18, 자바, 문제, 음수 100부터 양수 25까지의 합, while](https://www.youtube.com/watch?v=PD2SvE3sRN8)
- [동영상 - 2020 04 18, 자바, 문제, for문](https://www.youtube.com/watch?v=nMdjeboYvnE)
- [동영상 - 2020 04 18, 자바, 문제, 2중 반복문](https://www.youtube.com/watch?v=yE4sdMlObGU)

# 2020-04-19, 04일차
## 문제

### 일반
- [문제 - 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://repl.it/@jangka512/TrueSandybrownCodewarrior)
- [문제 - 정답](https://repl.it/@jangka512/WeightyVigilantObjectdatabase)
- [문제 - 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://repl.it/@jangka512/ShabbyPoisedGlobalarrays)
- [문제 - 정답](https://repl.it/@jangka512/AutomaticJauntyYottabyte)
- [문제 - 일반변수에 값 할당과정 설명](https://repl.it/@jangka512/LoyalFrayedBudgetrange)
- [문제 - 정답](https://repl.it/@jangka512/TinySeveralCone)
- [문제 - 레퍼런스변수에 값 할당과정 설명](https://repl.it/@jangka512/PriceyProfitableBackground)
- [문제 - 정답](https://repl.it/@jangka512/SubduedThirdInfinity)
- [문제 - 레퍼런스변수에 값 할당과정 설명 v2](https://repl.it/@jangka512/JAVA-5-18-04-12-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-2)
- [문제 - 인스턴스 메서드 실행](https://repl.it/@jangka512/JAVA-5-18-04-12-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-4)
- [문제 - 인스턴스 메서드 2개 실행](https://repl.it/@jangka512/JAVA-5-18-04-12-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-6)
- [문제 - 객체 리모콘을 저장하는 변수를 올바르게 수정해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-12-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-8)
- [문제 - 리모콘을 변수에 저장하지 않고 바로 사용](https://repl.it/@jangka512/JAVA-5-18-04-12-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-10)
- [문제 - 5개의 서로 다른 종류의 객체를 만들고 각각의 객체에게 일을 시켜주세요.](https://repl.it/@jangka512/JAVA-5-18-04-12-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-12)
- [문제 - 객체화 없이 설계도에 있는 능력을 바로 사용해주세요.](https://repl.it/@jangka512/DarlingKlutzySymbol)
- [문제 - 정답](https://repl.it/@jangka512/OldlaceHurtfulMice)
- [문제 - 구구단을 만들어주세요](https://repl.it/@jangka512/EqualThankfulCases)
- [문제 - 정답](https://repl.it/@jangka512/LightPungentDevices)
- [문제 - 정답 v2](https://repl.it/@jangka512/FriendlySeriousDifferences)
- [문제 - 매개변수를 사용해서 문제를 풀어주세요.](https://repl.it/@jangka512/HonoredEmotionalPlans)
- [문제 - 정답](https://repl.it/@jangka512/MoralIntentBloatware)
- [문제 - 함수를 실행하면 값을 돌려주도록 만들어주세요.](https://repl.it/@jangka512/SleepyUnwelcomeQuerylanguage)
- [문제 - 정답](https://repl.it/@jangka512/HalfImportantVariables)
- [문제 - 1부터 n까지의 합을 반환하는 함수](https://repl.it/@jangka512/JAVA-5-18-04-20-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-2)
- [문제 - n부터 m까지의 합을 반환하는 함수](https://repl.it/@jangka512/JAVA-5-18-04-20-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-4)

### 상속
- [문제 - `숨쉬다`라는 기능을 중복하지 않고 문제를 풀어주세요.](https://repl.it/@jangka512/JAVA-5-18-04-24-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-2)

## 동영상
- [동영상 - 2019 12 17, 자바, 레퍼런스 변수와 객체 1부](https://www.youtube.com/watch?v=fFOWKwp_iXU&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체기본문제, 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://www.youtube.com/watch?v=yMaUTypEKaM&feature=youtu.be)
- [동영상 - 2019 12 17, 자바, 객체기본문제, 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://www.youtube.com/watch?v=9AKMJ4U5Zds&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체지향기초, null과 레퍼런스 변수 할당](https://www.youtube.com/watch?v=klOodAP3L4Y&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체지향기초, 클래스로 만들 수 있는 3가지](https://www.youtube.com/watch?v=wWG9iCI_wJw&feature=youtu.be)
- [동영상 - 2020_04_19, 자바 문제, 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요](https://youtu.be/-l1FN5GZ9Vg)
- [동영상 - 2020_04_19, 자바 문제, 일반변수에 값 할당과정 설명](https://youtu.be/T5kitERUOdg)
- [동영상 - 2020_04_19, 자바 문제, 레퍼런스변수에 값 할당과정 설명](https://youtu.be/nnmIvyoLurA)
- [동영상 - 2020_04_19, 자바 문제, 레퍼런스변수에 값 할당과정 설명, v2](https://youtu.be/8uaoy7tlMoM)
- [동영상 - 2020_04_19, 자바 문제, 객체 리모콘을 저장하는 변수를 올바르게 수정해주세요.](https://youtu.be/RAwQ5Xl-9Lk)
- [동영상 - 2020_04_19, 자바 문제, 리모콘을 변수에 저장하지 않고 바로 사용](https://youtu.be/o8WKoDOJHts)
- [동영상 - 2020_04_19, 자바 문제, 인스턴스 메서드 실행](https://youtu.be/iETVI-hDzBU)
- [동영상 - 2020_04_19, 자바 문제, 인스턴스 메서드 2개 실행](https://youtu.be/185mVCrNbhE)
- [동영상 - 2020 04 19, 자바 문제, 5개의 서로 다른 종류의 객체를 만들고 각각의 객체에게 일을 시켜주세요](https://youtu.be/Y19ghnmXyGw)
- [동영상 - 2020_04_19, 자바, 문제, 1부터 n까지의 합을 반환하는 함수](https://youtu.be/SsILhbd7Y6k)
- [동영상 - 2020_04_19, 자바, 문제, n부터 m까지의 합을 반환하는 함수](https://youtu.be/_jVxuTTywYM)
- [동영상 - 2020 04 19, 자바, 개념, static, 매개변수, 리턴](https://www.youtube.com/watch?v=BijvB_UH32o&feature=youtu.be)
- [동영상 - 2020_04_19, 자바, 문제, 객체화 없이 설계도에 있는 능력을 바로 사용해주세요, static](https://youtu.be/u_5VUUcQS3g)
- [동영상 - 2020 04 19, 자바, 문제, 구구단 구현](https://youtu.be/8e9ACh3AYPo)
- [동영상 - 2020_04_19, 자바, 문제, 매개변수를 이용해서 문제를 풀어주세요](https://youtu.be/_pQ4AcPg5e4)
- [동영상 - 2020_04_19, 자바, 문제, 함수가 리턴을 하게 해주세요 ](https://youtu.be/vsEG1AwYDuA)
- [동영상 - 2020 04 19, 자바, 문제, 상속을 사용해서 중복을 피해주세요](https://www.youtube.com/watch?v=NK6rQ9lr2hM&feature=youtu.be)

## 보너스 문제(아래 정렬관련 내용은 나중에 천천히 보세요.)

### 정렬
- [문제 - 구구단을 거꾸로(9단 ~ 0단), 점점 더 적은 개수를 출력](https://code.oa.gg/java8/1641)
- [문제 - 정답](https://code.oa.gg/java8/1642)
- [문제 - 거품정렬을 구현해주세요.](https://code.oa.gg/java8/1639)
- [문제 - 정답](https://code.oa.gg/java8/1640)

## 오늘의 동영상(정렬관련 동영상)(초반에는 안해도 됨)
- [동영상 - 거품정렬 비주얼](https://www.youtube.com/watch?v=lyZQPjUT5B4)
- [동영상 - 2020_04_17, 자바, 개념, 버블소트 원리 설명 v1](https://youtu.be/rU9DBJlwPBg)
- [동영상 - 2020_04_17, 자바, 개념, 버블소트 원리 설명 v2](https://youtu.be/m-UYQJRXAsA)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이 v2](https://youtu.be/mVmoOO8P_p4)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이 v3](https://youtu.be/8nDbJXqYbPo)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이](https://youtu.be/Q_V0qRjCROM)
- [동영상 - 2020_04_17, 자바, 문제, 구구단을 역순으로 출력, 각 단만큼 곱해주세요](https://youtu.be/P2909eUdeDA)
- [동영상 - 2020_04_17, 자바, 문제, 버블소트를 구현해주세요](https://www.youtube.com/watch?v=BiEzwfjhSoM)
- [동영상 - 2020_04_16, 자바, 개념, 배열, 평균, 총합](https://youtu.be/0rGhLT3Gp1c)
- [동영상 - 2020_04_16, 자바, 개념, 배열객체와 리모콘, 2부](https://youtu.be/l0U9p8uRKHk)
- [동영상 - 2020_04_16, 자바, 개념, 배열객체와 리모콘](https://youtu.be/pPzBzwYtIJI)
- [동영상 - 2019 12 19, 자바, static 메서드, 매개변수, 리턴](https://www.youtube.com/watch?v=t_LL_2H4wig&feature=youtu.be)

# 2020-04-25, 05일차
## 문제
### 프로그래머스
- [자바입문, 파트1 ~ 파트 4](https://programmers.co.kr/learn/courses/5)

### 일반
- [문제 - 오리 시뮬레이션 만들기 1](https://repl.it/@jangka512/JAVA-5-18-04-20-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-6)
- [문제 - 오리 시뮬레이션 만들기 2](https://repl.it/@jangka512/JAVA-5-18-04-20-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-8)
- [문제 - 오리 시뮬레이션 만들기 3](https://repl.it/@jangka512/JAVA-5-18-04-20-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-10)
- [문제 - 오리 시뮬레이션 만들기 4](https://repl.it/@jangka512/JAVA-5-18-04-20-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-12)
- [문제 - 오리 시뮬레이션 만들기 5](https://repl.it/@jangka512/JAVA-5-18-04-20-13)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-14)
- [문제 - 오리 시뮬레이션 만들기 6](https://repl.it/@jangka512/JAVA-5-18-04-20-15)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-16)
- [문제 - 오리 시뮬레이션 만들기 7](https://repl.it/@jangka512/JAVA-5-18-04-20-17)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-18)
- [문제 - 오리 시뮬레이션 만들기 8](https://repl.it/@jangka512/JAVA-5-18-04-20-19)
- [문제 - 복잡한 상속을 통한 문제해결을 하고, 이 방법이 좋지 않은 이유를 설명해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-24-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-4)
- [문제 - 로봇오리가 작동하게 해주세요. 단 중복은 없어야 합니다.](http://code.oa.gg/java8/1430)
- [문제 - 정답](https://code.oa.gg/java8/1541)
- [문제 - 상속을 통한 캐스팅 허용](https://repl.it/@jangka512/JAVA-5-18-04-24-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-6)
- [문제 - 상속을 통한 캐스팅 허용 2](https://repl.it/@jangka512/JAVA-5-18-04-24-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-8)
- [문제 - 무기 클래스로 만든 리모콘으로 칼 객체를 조종](https://repl.it/@jangka512/JAVA-5-18-04-24-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-10)

## 오늘의 동영상
- [동영상 - 2020 04 25, 자바, 개념, JDK, 이클립스, 코드업 푸는법](https://www.youtube.com/watch?v=9QlLZZ5twmw&feature=youtu.be)
- [동영상 - 2020 04 22, 프론트, 문제, 로봇오리가 적절히 작동하도록 해주세요, 중복제거](https://www.youtube.com/watch?v=7XticzXMQpc)
- [동영상 - 2020 04 25, 자바, 문제, 오리 시뮬레이션 게임 만들기 예제, 1~7](https://youtu.be/7K8qb1sdTMQ)
- [동영상 - 2020_04_25, 자바, 문제, 상속을 통한 캐스팅 허용](https://youtu.be/bKSSv_FwBfg)
- [동영상 - 2020_04_25, 자바, 문제, 상속을 통한 캐스팅 허용 2](https://youtu.be/hTshWOecOHc)
- [동영상 - 2020_04_25, 자바, 문제, 무기 클래스로 만든 리모콘으로 칼 객체를 조종](https://youtu.be/8M0aagVt4FA)

## 오늘의 동영상(참고)
- [동영상 - 2019 12 19, 자바, 상속, 메서드 오버라이딩](https://www.youtube.com/watch?v=dLHIV5Fz7oQ&feature=youtu.be)
- [동영상 - 2019 12 20, 자바, 상속, 메서드 오버라이딩, 2부, 상속의 한계](https://www.youtube.com/watch?v=ceZRsC1HpTc&feature=youtu.be)
- [동영상 - 2019 12 20, 자바, 상속, 메서드 오버라이딩, 3부, 상속의 한계](https://www.youtube.com/watch?v=ApomtKfXZmM&feature=youtu.be)
- [동영상 - 2020 04 22, 자바, 문제, 오리 시뮬레이션 게임 만들기 예제, 1~7](https://www.youtube.com/watch?v=o2JalYbd_qo)
- [동영상 - 2020 04 22, 자바, 개념, 상속, 캐스팅, 리모콘 버튼 추가/제거, 쉬운설명 버전](https://www.youtube.com/watch?v=epRBcD-xXLc)
- [동영상 - 2020 04 22, 자바, 개념, 상속, 캐스팅, 리모콘 버튼 추가/제거](https://www.youtube.com/watch?v=4wRTunBoTO8)
- [동영상 - 2019 12 22, 자바, 상속을 통한 캐스팅 허용](https://www.youtube.com/watch?v=XHkTh7yAdZU&feature=youtu.be)
- [동영상 - 2019 12 23, 자바, 상속을 통한 캐스팅 허용](https://www.youtube.com/watch?v=2CEJDMJgxHY&feature=youtu.be)
- [동영상 - 2019 12 24, 자바, 객체와 리모콘과 상속과, 캐스팅](https://www.youtube.com/watch?v=3tCQodqk1y4&feature=youtu.be)

# 2020-04-26, 06일차
### 기타
- 이클립스 단축키
  - ctrl + F11 : Run! (save and Launch)
  - ctrl + 위아래방향키 : 그 줄이 복사된다.
  - ctrl + d : 그 줄이 삭제된다.
  - alt + 위아래 화살표 : 그 줄을 이동시킨다.
  - ctrl + shift + f : 자동정렬

### 설명
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. int 변수](http://code.oa.gg/java8/1431)
- [문제 - 정답](http://code.oa.gg/java8/1432)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 클래스](http://code.oa.gg/java8/1433)
- [문제 - 정답](http://code.oa.gg/java8/1434)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 레퍼런스 변수(리모콘 변수)](http://code.oa.gg/java8/1435)
- [문제 - 정답](http://code.oa.gg/java8/1436)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 상속](http://code.oa.gg/java8/1437)
- [문제 - 정답](http://code.oa.gg/java8/1438)
- [문제 - 무기 클래스로 만든 리모콘으로 칼 객체를 조종하는 과정 설명](https://repl.it/@jangka512/JAVA-5-18-04-26-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-2)

### 객체
- [문제 - 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요.](https://repl.it/@jangka512/GoldenLightblueOpengroup)
- [문제 - 정답](https://repl.it/@jangka512/FancyRundownKernel)
- [문제 - 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요, 마지막 공격방식을 기억](https://repl.it/@jangka512/LuxuriousOrneryOutliers)
- [문제 - 정답](https://repl.it/@jangka512/SunnyNeglectedConsulting)
- [문제 - 서로 다른 3종 TV를 서로 다른 종류의 리모콘 3개로 컨트롤](https://repl.it/@jangka512/JAVA-5-18-04-24-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-12)
- [문제 - 서로 다른 3종 TV를 리모콘 1개로 컨트롤](https://repl.it/@jangka512/JAVA-5-18-04-24-13)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-14)
- [문제 - 리모콘 형변환 서술형 문제](http://code.oa.gg/java8/1439)
- [문제 - 정답](http://code.oa.gg/java8/1440)
- [문제 - 정수 i가 가지고 있는 10을 double 형 변수에 넣고 해당 변수의 값을 다시 i에 넣기](https://repl.it/@jangka512/TintedDrearyState)
- [문제 - 정답](https://repl.it/@jangka512/PalatableShorttermMice)
- [문제 - 자동차 리모콘으로 페라리 객체를 연결한 후 해당 리모콘이 가리키고 있는 객체를 다시 페라리 리모콘으로 가리키게(참조하게) 하는 코드를 작성해주세요.](https://repl.it/@jangka512/MonthlyYellowPagerecognition)
- [문제 - 정답](https://repl.it/@jangka512/QuestionableQueasyGlitches)
- [문제 - `a무기.공격()` 실행되는 세부적인 과정 기술](https://repl.it/@jangka512/JAVA-5-18-04-26-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-6)

### 구성
- [문제 - 사람이 `a왼팔` 이라는 변수를 가질 수 있게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-8)

## 오늘의 영상(참고)
- [동영상 - 2019 12 26, 자바, 인스턴스 변수, 지역 변수 생명주기](https://www.youtube.com/watch?v=W-18d_6pwMQ&feature=youtu.be)
- [동영상 - 2019 12 26, 자바, 서술형 문제 정답풀이](https://www.youtube.com/watch?v=OIUlna3FHXc&feature=youtu.be)
- [동영상 - 2020 04 23, 자바, 문제, 상속을 통한 캐스팅 허용](https://youtu.be/eHNwTcxsxfw)
- [동영상 - 2020 04 23, 자바, 문제, 무기 클래스로 만든 리모콘으로 칼 객체를 조종](https://youtu.be/Cx28EM4ne3I)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, int 지역 변수](https://youtu.be/GHRNhSnw_GM)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, 클래스](https://youtu.be/0PXEjtdYc68)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요. 레퍼런스(리모콘) 변수](https://youtu.be/Wu1Qa_-wVrU)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, 상속](https://youtu.be/K63ThhiUTYE)
- [동영상 - 2020 04 23, 자바, 문제, 무기 클래스로 만든 리모콘으로 칼 객체를 조종하는 과정 설명](https://youtu.be/CuTnlrsEyD8)

## 오늘의 영상
- [동영상 - 2020 04 23, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요.](https://youtu.be/hdcjBDp0jm4)
- [동영상 - 2020 04 23, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요, 마지막 공격방식을 기억](https://youtu.be/iAkuzyj1Edc)
- [동영상 - 2019 12 26, 자바, 리모콘 종류를 줄여야 하는 이유 1](https://www.youtube.com/watch?v=6AvwIYeojTg&feature=youtu.be)
- [동영상 - 2019 12 27, 자바, 리모콘 종류를 줄여야 하는 이유 2](https://www.youtube.com/watch?v=VOPHI6mJ6vY&feature=youtu.be)
- [동영상 - 2019 12 27, 자바, 자동 형변환, 수동 형변환, 구성](https://www.youtube.com/watch?v=8VfMeZylP80&feature=youtu.be)
- [동영상 - 2020 04 24, 자바, 문제, 서로 다른 3종 TV를 서로 다른 종류의 리모콘 3개로 컨트롤](https://www.youtube.com/watch?v=MYaEU7t19K4)
- [동영상 - 2020 04 24, 자바, 문제, 서로 다른 3종 TV를 리모콘 1개로 컨트롤](https://www.youtube.com/watch?v=w5PyO7ESG4k)
- [동영상 - 2020 04 24, 자바, 문제, 자동차 리모콘, 페라리 리모콘 상호변환](https://youtu.be/9-a2pFiVJeI)
- [동영상 - 2020 04 24, 자바, 문제, 사람이 a왼팔 이라는 변수를 가질 수 있게 해주세요.](https://youtu.be/5GHuwQU8Kv0)
- [동영상 - 2020 04 24, IT일반, 개념, ocam 사용법](https://www.youtube.com/watch?v=OML0YbcGVsM)
- [동영상 - 2020_04_26, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요](https://youtu.be/IRTPMA5chy0)
- [동영상 - 2020_04_26, IT일반, 개념, 학습위키 만들기](https://youtu.be/aZpvaIwG1D0)
- [동영상 - 2020_04_26, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요, 마지막 방식 기억](https://youtu.be/-UUao-zbD6g)
- [동영상 - 2020_04_26, 자바, 문제, 서로 다른 3종 TV를 서로 다른 종류의 리모콘 3개로 컨트롤](https://youtu.be/iEqOBtPgjrI)
- [동영상 - 2020_04_26, 자바, 문제, 서로 다른 3종 TV를 리모콘 1개로 컨트롤](https://youtu.be/NT9JQPLMSYM)
- [동영상 - 2020_04_26, 자바, 문제, 자동차 리모콘, 페라리 리모콘](https://youtu.be/sHP1X_zG4RE)
- [동영상 - 2020_04_26, 자바, 문제, a무기 변수의 공격 메서도 호출의 과정설명](https://youtu.be/oYomPFR2aZk)
- [동영상 - 2020_04_26, 자바, 문제, 사람이 팔을 가지도록](https://youtu.be/G87Ci2baG6g)

## 숙제
### 프로그래머스
- [자바입문, 파트1 ~ 파트 5](https://programmers.co.kr/learn/courses/5)

### 구름 EDU 생활코딩
- [자바 기초 : 파트 3, 5, 7](https://edu.goorm.io/learn/lecture/16448/생활코딩-java1)
- [자바 제어문 : 파트 2, 3, 5](https://edu.goorm.io/learn/lecture/16908/생활코딩-java-제어문)
- [자바 메서드 : 파트 1, 2](https://edu.goorm.io/learn/lecture/16909/생활코딩-java-method)

## 학습위키 작성하기
- 자바요점정리

# 2020-05-02, 07일차
## 문제
### 일반
- [문제 - 전사가 가지고 있는 변수 `a무기`가 칼과 활에 모두 호환되게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-10)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-12)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.(매개변수 금지)](https://repl.it/@jangka512/HorribleHomelyTechnology)
- [문제 - 정답](https://repl.it/@jangka512/SpeedyAuthorizedLint)
- [문제 - 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요.](https://code.oa.gg/java8/1543)
- [문제 - 정답](https://code.oa.gg/java8/1544)
- [개념 - 생성자](https://code.oa.gg/java8/1646)
- [문제 - 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요. 생성자, abstrct](https://code.oa.gg/java8/1545)
- [문제 - 정답](https://code.oa.gg/java8/1546)
- [문제 - 정답 v2](https://code.oa.gg/java8/1647)
- [문제 - 사람이 정해진 속도대로 달리게 해주세요.](https://code.oa.gg/java8/1549)
- [문제 - 정답](https://code.oa.gg/java8/1550)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.(매개변수 사용금지)](https://code.oa.gg/java8/1547)
- [문제 - 정답](https://code.oa.gg/java8/1548)
- [문제 - 정답 v2](https://code.oa.gg/java8/1648)
- [문제 - 정답 v3](https://code.oa.gg/java8/1649)
- [문제 - 올바른 리턴타입으로 메서드를 만들어주세요.](https://code.oa.gg/java8/1551)
- [문제 - 정답](https://code.oa.gg/java8/1552)
- [문제 - 정답 v2](https://code.oa.gg/java8/1650)

### 추가 문제
- [문제 - 인력관리소를 운영해주세요.](http://code.oa.gg/java8/1442)
- [문제 - 힌트](http://code.oa.gg/java8/1443)
- [정답 - 힌트2](http://code.oa.gg/java8/1444)
- [문제 - 힌트3](http://code.oa.gg/java8/1445)
- [문제 - 정답 v1](http://code.oa.gg/java8/1446)
- [문제 - 정답 v2(배열사용버전)](https://code.oa.gg/java8/1651)
- [문제 - 사람이 사람을 가리키도록 해주세요.](https://code.oa.gg/java8/1652)
- [문제 - 정답](https://code.oa.gg/java8/1654)
- [문제 - 인력관리소를 운영해주세요.(어제 문제)](http://code.oa.gg/java8/1442)
- [문제 - 정답 v3](http://code.oa.gg/java8/1447)
- [문제 - 정답 v4](http://code.oa.gg/java8/1448)

## 오늘의 동영상
- [동영상 - 2019 12 30, 자바, 구성, 전사와 무기 문제](https://www.youtube.com/watch?v=B0F4rOWD9bg&feature=youtu.be)
- [동영상 - 2019 12 31, 자바, 생성자](https://www.youtube.com/watch?v=L-b8QjGVxWU&feature=youtu.be)
- [동영상 - 2019 12 31, 자바, 구성, 각각의 사람이 좋아하는 음식점에서 음식 주문 문제](https://www.youtube.com/watch?v=QDX6LOy1ddQ&feature=youtu.be)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 가지고 있는 변수 a무기가 칼과 활에 모두 호환되게 해주세요.](https://youtu.be/x3yJ7VQIMJY)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요](https://youtu.be/jeamPav7eGg)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요, 매개변수 금지](https://youtu.be/IRgYBVgrjDg)
- [동영상 - 2020_04_27, 자바, 문제, 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요](https://www.youtube.com/watch?v=3TiMGrKy4c8)
- [동영상 - 2020_04_27, 자바, 개념, 생성자](https://www.youtube.com/watch?v=__l31-xtH8g)
- [동영상 - 2020_04_27, 자바, 문제, 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요, 생성자, abstract](https://www.youtube.com/watch?v=xHoZ2pHZR1k)
- [동영상 - 2020 01 02, 자바, 매개변수 없이 객체에 정보를 넘겨서, 메서드가 적절하게 수행되도록 하기](https://www.youtube.com/watch?v=PFtoxFsgngM&feature=youtu.be)
- [동영상 - 2020 01 02, 자바, 함수의 호출 부분을 기준으로 메서드 구현시, 리턴타입 맞추기](https://www.youtube.com/watch?v=25RQu7o7-OQ&feature=youtu.be)
- [동영상 - 2020 04 28, 자바, 문제, 올바른 리턴타입으로 메서드를 만들어주세요](https://www.youtube.com/watch?v=HS_4w3vYNHI)
- [동영상 - 2020 04 28, 자바, 문제, 사람이 정해진 속도대로 달리게 해주세요](https://www.youtube.com/watch?v=K2P1j65yVdE)
- [동영상 - 2020 04 28, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요, 매개변수 금지](https://www.youtube.com/watch?v=8VOq9M52S3U)
- [동영상 - 2020_05_02, 자바, 개념, 변수, 함수, 클래스, 속성](https://youtu.be/6bDgTUT3fhE)
- [동영상 - 2020_05_02, 자바, 문제, 전사의 a무기 변수에 활과 칼 연결 가능](https://youtu.be/-K4ohIfBgNw)
- [동영상 - 2020_05_02, 자바, 문제, 전사가 들고 있는 무기에 따라 공격](https://youtu.be/4Hr2bOGin-4)
- [동영상 - 2020_05_02, 자바, 문제, 전사가 들고 있는 무기에 따라 공격, 매개변수 금지](https://youtu.be/uAXy1nC6GEU)
- [동영상 - 2020_05_02, 자바, 개념, 생성자](https://youtu.be/y9RvvuZTSaI)
- [동영상 - 2020_05_02, 자바, 개념, 생성자를 통한 속성세팅, 메서드 중복제거](https://youtu.be/HH1vahbc5SE)

## 추가 동영상
- [동영상 - 2020 01 02, 자바, 인력관리소 문제 1](https://www.youtube.com/watch?v=NSfhW7RpMUA&feature=youtu.be)
- [동영상 - 2020_04_28, 자바, 문제, 인력관리소를 운영해주세요, 배열금지](https://www.youtube.com/watch?v=FyNY8WVfeak)
- [동영상 - 2020_04_28, 자바, 문제, 인력관리소를 운영해주세요, 배열사용](https://www.youtube.com/watch?v=OVJh-RqGryA)
- [동영상 - 2020 01 03, 자바, 예제, 인력관리소는 사람 3명까지 관리 할 수 있어야 합니다, V3](https://www.youtube.com/watch?v=0cXHL3zOr2M&feature=youtu.be)

# 2020-05-03, 08일차
## 문제
### 일반
- [문제 - 전사가 무기가 없다면 공격을 못해야 합니다.](https://code.oa.gg/java8/1557)
- [문제 - 정답](https://code.oa.gg/java8/1558)
- [문제 - 전사가 무기를 여러개 다룰 수 있어야 합니다.](https://code.oa.gg/java8/1553)
- [문제 - 정답](https://code.oa.gg/java8/1554)
- [문제 - 전사가 무기를 여러개 다룰 수 있어야 합니다. v2](https://code.oa.gg/java8/1555)
- [문제 - 정답](https://code.oa.gg/java8/1556)
- [문제 - 플레이어가 어떤 공격을 할지는 플레이어의 직업에 따라 다릅니다.](https://repl.it/@jangka512/JAVA-5-18-04-30-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-30-2)
- [문제 - 생성자를 사용해서 전사의 기본 이름을 `No Name` 으로 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-30-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-30-4)
- [문제 - 팔을 생성할 때 자동으로 길이가 세팅되도록 해주세요](https://repl.it/@jangka512/JAVA-5-18-04-30-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-30-6)
- [문제 - 사람이 생성할 때 자동으로 `a왼팔` 변수에 팔 객체가 연결되게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-30-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-30-8)
- [문제 - `this`의 역할](https://repl.it/@jangka512/JAVA-5-18-04-30-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-30-10)

## 오늘의 동영상
- [동영상 - 2020 01 03, 자바, 예제, 무기가 없을 때 전사는 공격을 할 수 없다](https://www.youtube.com/watch?v=Mn7-NlwpKjE&feature=youtu.be)
- [동영상 - 2020 01 03, 자바, 예제, 전사는 무기를 여러개 다룰 수 있습니다, 버전 1](https://www.youtube.com/watch?v=Qw7As9rkS8s&feature=youtu.be)
- [동영상 - 2020 01 03, 자바, 예제, 전사는 무기를 여러개 다룰 수 있습니다, 버전 2](https://www.youtube.com/watch?v=lbiUAq_-qQM&feature=youtu.be)
- [동영상 - 2020_04_29, 자바, 문제, 사람이 사람을 가리키도록 해주세요](https://www.youtube.com/watch?v=qN7NbNqJ1KM)
- [동영상 - 2020_04_29, 자바, 문제, 사람이 사람을 가리키도록 해주세요, 설명 v2](https://youtu.be/axAGkIO1gzs)
- [동영상 - 2020_04_29, 자바, 문제, 전사가 무기가 없다면 공격을 못해야 합니다](https://youtu.be/jiOgGGxSUvc)
- [동영상 - 2020_04_29, 자바, 문제, 전사가 무기를 여러개 다룰 수 있어야 합니다](https://youtu.be/Zk6BbZUt8pg)
- [동영상 - 2020_05_03, 자바, 문제, 전사가 무기가 없다면 공격을 못해야 합니다](https://youtu.be/yv51NEXDazs)
- [동영상 - 2020_05_03, 자바, 문제, 전사가 무기를 여러개 다룰 수 있어야 합니다](https://youtu.be/3xkuy5IwcsI)
- [동영상 - 2020_05_03, 자바, 문제, 전사가 무기를 여러개 다룰 수 있어야 합니다. v2](https://youtu.be/MWsOZ2aYPHg)
- [동영상 - 2020 01 06, 자바, 예제, 플레이어가 어떤 공격을 할지는 플레이어의 직업에 따라 다릅니다](https://www.youtube.com/watch?v=0r9hyFlnoK4&feature=youtu.be)
- [동영상 - 2020 01 06, 자바, 예제, 생성자를 사용해서 전사의 기본 이름을 No Name 으로 해주세요](https://www.youtube.com/watch?v=QjBzSAVQ4nQ&feature=youtu.be)
- [동영상 - 2020 01 06, 자바, 예제, 사람은 태어나자 마자 팔을 가지고 있습니다](https://www.youtube.com/watch?v=PyVtto0bXLI&feature=youtu.be)
- [동영상 - 2020 01 06, 자바, 예제, this를 적절하게 사용해주세요.](https://youtu.be/PyVtto0bXLI?t=439)

# 2020-05-09, 09일차
## 문제
### 일반
- [문제 - 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-05-02-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-2)
- [문제 - 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요.(생성자 3개)](https://repl.it/@jangka512/JAVA-5-18-05-02-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-4)
- [문제 - 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요.(생성자 1개)](https://repl.it/@jangka512/JAVA-5-18-05-02-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-6)
- [개념 : 객체의 메모리 구조](https://medium.com/@covj12/객체의-생성과-동작-과정-164e17b5593)
- [문제 - 전사의 `무기변경` 메서드 구현, if문 사용](https://repl.it/@jangka512/JAVA-5-18-05-02-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-8)
- [문제 - 전사의 `무기변경` 메서드 구현, if문 없이](https://repl.it/@jangka512/AliceblueValidNetwork)
- [문제 - 정답](https://repl.it/@jangka512/RedundantSeagreenServerapplication)
- [문제 - 병사의 진급 기능을 만들어주세요.](https://repl.it/@jangka512/JointBonyNetbsd)
- [문제 - 정답](https://repl.it/@jangka512/DroopyScentedIntelligence)
- [문제 - 병사가 진급시 공격력도 올라가게 해주세요.](https://repl.it/@jangka512/OverdueWirelessSets)
- [문제 - 정답](https://repl.it/@jangka512/ExcellentLopsidedIntercept)
- [문제 - n부터 m까지의 수 중에서 3의 배수의 합을 리턴하는 static 메서드 구현](https://repl.it/@jangka512/TransparentSubduedAssociate)
- [문제 - 정답](https://repl.it/@jangka512/AnxiousPrestigiousLava)
- [문제 - 홍길동과 홍길순은 둘 다 전사입니다.](https://repl.it/@jangka512/SadColorfulAdmin)
- [문제 - 정답](https://repl.it/@jangka512/DefensiveWindyDrawing)
- [문제 - 홍길동과 홍길순은 둘 다 전사이고 공격할 수 있는 능력이 있습니다.](https://repl.it/@jangka512/PowderblueFrighteningIntegers)
- [문제 - 정답](https://repl.it/@jangka512/TrueRespectfulTranslations)
- [문제 - 전사 홍길동과 홍길순은 다양한 무기를 이용해서 공격합니다.](https://repl.it/@jangka512/IdealisticEnormousRedundantcode)
- [문제 - 정답](https://repl.it/@jangka512/RoundedHoarseLivedistro)

## 오늘의 동영상
### 일반
- [동영상 - 2020 01 06, 자바, 예제, 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요](https://www.youtube.com/watch?v=Ht7jFLecGBw&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요 , 생성자 1개](https://www.youtube.com/watch?v=7_eHGpuoq9c&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 전사의 무기변경 메서드 구현, if문 사용](https://www.youtube.com/watch?v=jUnAYFogccc&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 전사의 무기변경 메서드 구현, if문 없이](https://www.youtube.com/watch?v=WBYGVVopuqs&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 병사가 진급하면 공격력과 계급명이 바뀝니다](https://www.youtube.com/watch?v=n0fKlmHujZE&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 입력받은 숫자 n과 m 사이의 수 중에서 3의 배수의 합을 리턴해주세요](https://www.youtube.com/watch?v=1uV1e7KuZ1c&feature=youtu.be)
- [동영상 - 2020 01 07, 자바, 예제, 전사 홍길동과 홍길순은 다양한 무기를 이용해서 공격합니다.](https://www.youtube.com/watch?v=q6rDuDqJ87s&feature=youtu.be)
- [동영상 - 2020 05 09, 자바, 예제, 사람을 만들 때 왼팔과 손과 엄지손가락이 한번에 만들어지게 해주세요](https://youtu.be/6TbAe8Vet84)
- [동영상 - 2020 05 09, 자바, 개념, 객체의 메모리 구조](https://youtu.be/S4alBvv07tI)
- [동영상 - 2020_05_09, 자바, 예제, 병사가 진급하면 공격력과 계급명이 바뀝니다](https://youtu.be/kluNngfc7_g)
- [동영상 - 2020 05 09, 자바, 예제, 입력받은 숫자 n과 m 사이의 수 중에서 3의 배수의 합을 리턴](https://youtu.be/evhibbryRbk)
- [동영상 - 2020_05_09, 자바, 문제, 홍길동과 홍길순은 둘 다 전사입니다](https://youtu.be/j383G8ZCfNE)
- [동영상 - 2020_05_09, 자바, 문제, 홍길동과 홍길순은 둘 다 전사이고 공격가능](https://youtu.be/dOFs0QGtEog)
- [동영상 - 2020_05_09, 자바, 문제, 홍길동과 홍길순은 다양한 무기를 이용합니다](https://youtu.be/dBorG5hfhI4)

# 2020-05-10, 10일차
## 문제
### 일반
- [문제 - 전사의 `무기변경` 메서드 구현, 각 무기마다의 데미지도 구현](https://repl.it/@jangka512/JAVA-5-18-05-02-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-12)
- [문제 - 전사의 `무기변경` 메서드 구현, 각 무기마다의 데미지도 구현, `a무기` 인스턴스 변수 이용](https://repl.it/@jangka512/JAVA-5-18-05-02-9)
- [문제 - 정답 v1](https://repl.it/@jangka512/KindlyRewardingLines)
- [문제 - 정답 v2](https://repl.it/@jangka512/JAVA-5-18-05-02-10)
- [문제 - 전사가 특정 무기를 모르는 상태에서 사용하게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-05-02-13)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-05-02-14)
- [문제 - 구성을 사용해서 전사 클래스의 중복을 제거해주세요.](https://repl.it/@jangka512/ThreadbareGreenMethods)
- [문제 - 정답](https://repl.it/@jangka512/KnottyEnergeticElement)
- [문제 - 오리 시뮬레이션 만들기 8](https://repl.it/@jangka512/JAVA-5-18-04-20-19)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-20)
- [문제 - `사용자 + " : ` 부분에 대한 중복을 제거해주세요.](https://repl.it/@jangka512/PuzzlingExtrovertedJavabeans)
- [문제 - 정답 v1](https://repl.it/@jangka512/HandsomeUntimelyOutsourcing)

## 자바 콘솔 게시판
- [작업 1, 스캐너 기초](https://to2.kr/bkq)

## 오늘의 동영상
- [동영상 - 2020 01 09, 자바, 상속 vs 구성,is a 관계 vs has a 관계](https://www.youtube.com/watch?v=vRq47ybm-wY&feature=youtu.be)
- [동영상 - 2020 01 09, 자바, 예제, 각 무기 계열 전사들을 만드는 더 좋은 방법](https://www.youtube.com/watch?v=Z3w7gUkqER8&feature=youtu.be)
- [동영상 - 2020 01 09, 자바, 예제, 전사가 무기칼, 창, 활를 사용하는 올바른 방법](https://www.youtube.com/watch?v=wopwcmKSPwc&feature=youtu.be)
- [동영상 - 2020 01 09, 자바, 예제, 오리 시뮬레이션 8](https://www.youtube.com/watch?v=ziyYOvJN2X0&feature=youtu.be)
- [동영상 - 2020 01 09, 자바, 예제, 오리 시뮬레이션 8에서 구분자 중복제거](https://www.youtube.com/watch?v=nONZLr_ydbg&feature=youtu.be)
- [동영상 - 2020_05_10, 자바, 문제, 전사의 무기변경 메서드 구현, 각 무기마다의 데미지도 구현](https://youtu.be/0MvQlKz_SmA)
- [동영상 - 2020_05_10, 자바, 문제, 전사의 무기변경 메서드 구현, 각 무기마다의 데미지도 구현, a무기 인스턴스 변수 사용](https://youtu.be/CVBD22tovAQ)
- [동영상 - 2020_05_10, 자바, 문제, 전사가 특정 무기를 모르는 상태에서 사용하게 해주세요](https://youtu.be/iONZM26E8p0)
- [동영상 - 2020_05_10, 자바, 문제, 구성을 사용해서 전사 클래스의 중복을 제거해주세요](https://youtu.be/wshxtdLwD40)

# 2020-05-16, 11일차
## 문제
### 일반
- [문제 - `사용자 + " : ` 부분에 대한 중복을 제거해주세요.](https://repl.it/@jangka512/PuzzlingExtrovertedJavabeans)
- [문제 - 정답 v2](https://repl.it/@jangka512/PristineAltruisticPublishing)
- [문제 - 정답 v3](https://repl.it/@jangka512/SubstantialColorlessObjectmodel)
- [문제 - 오리 시뮬레이션 만들기 9](https://repl.it/@jangka512/JAVA-5-18-04-20-21)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-22)
- [문제 - 오리 시뮬레이션 만들기 8(10분안에 완성!)(code.oa.gg 버전)](http://code.oa.gg/java8/1372)
- [문제 - 오리 시뮬레이션 만들기 9(3분안에 완성!)(code.oa.gg 버전)](http://code.oa.gg/java8/1371)
- 생성자 복습 시작
- [문제 - 오리 객체를 만들고, 객체가 만들어지면 자동으로 `오리가 생성되었습니다.` 가 출력](http://code.oa.gg/java8/907)
- [문제 - 정답](http://code.oa.gg/java8/908)
- [문제 - 사람이 태어나자마자 자동으로 나이가 20살이 되어야 합니다.](http://code.oa.gg/java8/909)
- [문제 - 정답](http://code.oa.gg/java8/910)
- [문제 - 사람이 태어나자마자 자동으로 나이, 이름, 성격, 고향이 정해져야 합니다.](http://code.oa.gg/java8/911)
- [문제 - 정답](http://code.oa.gg/java8/912)
- [문제 - 사람이 태어나자마자 자동으로 나이, 이름, 성격, 고향이 정해져야 합니다.(매개변수 있는 생성자 메서드 직접구현)](http://code.oa.gg/java8/913)
- [문제 - 정답](http://code.oa.gg/java8/914)
- [문제 - 사람이 태어나자마자 자기소개가 가능해야 합니다.](http://code.oa.gg/java8/915)
- [문제 - 정답](http://code.oa.gg/java8/916)
- [문제 - 사람이 태어나자마자 왼팔을 가지고 그 팔은 생긴 즉시 길이를 가져야 합니다.](http://code.oa.gg/java8/917)
- [문제 - 정답](http://code.oa.gg/java8/918)
- [문제 - 사람이 태어나자마자 왼팔을 가지고 그 팔은 생긴 즉시 길이를 가져야 합니다.(팔 클래스 생성자 사용 금지)](http://code.oa.gg/java8/919)
- [문제 - 정답](http://code.oa.gg/java8/920)
- [문제 - 사람객체를 생성하자마자 `a사람.a왼팔.a손.a엄지손가락.길이` 와 같은 표현이 가능해야 합니다.](http://code.oa.gg/java8/921)
- [문제 - 정답](http://code.oa.gg/java8/922)
- [문제 - 사람객체를 생성하자마자 `a사람.a왼팔.a손.a엄지손가락.길이` 와 같은 표현이 가능해야 합니다.(사람 클래스 생성자만 사용가능)](http://code.oa.gg/java8/923)
- [문제 - 정답](http://code.oa.gg/java8/924)
- 생성자 복습 끝
- 생성자 심화 시작
- [문제 - 사람객체를 만드는 옵션을 2가지 만들어주세요.](http://code.oa.gg/java8/930)
- [문제 - 정답](http://code.oa.gg/java8/931)
- [문제 - 계산기 객체가 더하는 기능의 옵션을 3가지 이상 가지도록 만들어주세요.](http://code.oa.gg/java8/932)
- [문제 - 정답](http://code.oa.gg/java8/933)
- [문제 - 상황에 따라서 디폴트 생성자와 직접구현 생성자 중 1개를 이용해주세요.](http://code.oa.gg/java8/934)
- [문제 - 정답](http://code.oa.gg/java8/935)
- [문제 - 생성자 연쇄호출을 이용해서 문제를 풀어주세요.](http://code.oa.gg/java8/936)
- [문제 - 정답](http://code.oa.gg/java8/937)
- [문제 - 생성자 관련(논술형)](https://repl.it/@jangka512/CornsilkGraciousDrivers)
- [문제 - 정답](https://repl.it/@jangka512/WorrisomeAfraidRegisters)
- [문제 - super 메서드를 명시적으로 호출해서 문제를 해결해주세요.](https://repl.it/@jangka512/SupportiveVeneratedExtension)
- [문제 - 정답](https://repl.it/@jangka512/KaleidoscopicGrandioseTerabyte)
- [문제 - `init` 메서드가 생성자처럼 작동하게 해주세요.(3분완성)](http://code.oa.gg/java8/938)
- [문제 - 정답](http://code.oa.gg/java8/939)

## 오늘의 동영상
- [동영상 - 2020 01 09, 자바, 예제, 오리 시뮬레이션 8에서 구분자 중복제거, v2, v3](https://www.youtube.com/watch?v=vgucRsOy_m4&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 예제, 오리 시뮬레이션 9](https://www.youtube.com/watch?v=pxHA2CEfQIA)
- [동영상 - 2020 01 14, 자바, 예제, 생성자 오버로딩](https://www.youtube.com/watch?v=-VQ2RTxJ5SU)
- [동영상 - 2020 01 14, 자바, 예제, 사람객체를 만드는 옵션을 2가지 만들어주세요](https://www.youtube.com/watch?v=IrlXz2CaW-k&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 예제, 계산기 객체가 더하는 기능의 옵션을 3가지 이상 가지도록 만들어주세요](https://www.youtube.com/watch?v=ALBzgRoH9gU&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 예제, 상황에 따라서 디폴트 생성자와 직접구현 생성자 중 1개를 이용해주세요](https://www.youtube.com/watch?v=1gnrYqNPd4Q&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 생성자 연쇄호출을 이용해서 문제를 풀어주세요](https://www.youtube.com/watch?v=RKPAsiQag64&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 예제, 생성자 관련논술형](https://www.youtube.com/watch?v=ER_1EaIzhUg&feature=youtu.be)
- [동영상 - 2020 01 14, 자바, 예제, super 메서드를 명시적으로 호출해서 문제를 해결해주세요](https://www.youtube.com/watch?v=XagKctAiiAs&feature=youtu.be)
- [동영상 - 2020_01_14, 자바, 예제, init 메서드가 생성자처럼 작동하게 해주세요.](https://www.youtube.com/watch?v=Jqy5iznYH9o&feature=youtu.be)
- [동영상 - 2020_05_16, 자바, 문제, 오리 시뮬레이션 만들기 9, 상속의 한계와 구성의 좋은점](https://youtu.be/PLr1zFkR0-s)
- [동영상 - 2020_05_16, 자바, 문제, 사람객체를 만드는 옵션을 2가지 만들어주세요](https://youtu.be/zcdpjnQIWac)
- [동영상 - 2020_05_16, 자바, 문제, 계산기 객체가 더하는 기능의 옵션을 3가지 이상 가지도록 만들어주세요](https://youtu.be/pp1LwVFDDOY)
- [동영상 - 2020_05_16, 자바, 문제, 상황에 따라서 디폴트 생성자와 직접구현 생성자 중 1개를 이용해주세요](https://youtu.be/kFKYTjEBRHM)
- [동영상 - 2020_05_16, 자바, 문제, 생성자 연쇄호출을 이용해서 문제를 풀어주세요](https://youtu.be/lBLrdGBqjXA)
- [동영상 - 2020_05_16, 자바, 문제, 논술, 모든 클래스의 조상, 디폴트 생성자](https://youtu.be/BZMKwBFPJ00)
- [동영상 - 2020_05_16, 자바, 문제, super 메서드를 명시적으로 호출해서 문제를 해결해주세요](https://youtu.be/dj23SM71yLw)
- [동영상 - 2020_05_16, 자바, 문제, init 메서드가 생성자처럼 작동하게 해주세요](https://youtu.be/9eac7mhfCWw)
- [동영상 - 2020_05_16, 자바기초콘솔게시판, to2.kr/bkq, 개념, 공부하는 법](https://youtu.be/6SNBqeqczIw)

## 자바 콘솔 게시판
- [작업 2](https://to2.kr/bkq)

# 2020-05-17, 12일차
## 문제
### 일반
- this, super 시작
- [개념 : 같은 객체라도 조종하는 리모콘 타입에 따라 활성화 되는 변수가 다를 수 있다.](https://code.oa.gg/java8/1563)
- [문제 - this와 super변수의 차이를 설명하는 예를 만들어주세요.](https://repl.it/@jangka512/SleepyTruthfulVideogame)
- [문제 - 정답](https://repl.it/@jangka512/WorrisomeNimbleHardware)
- 생성자 심화 끝
- 상수시작
- [문제 - 변수 a를 상수로 만들어주세요.](https://code.oa.gg/java8/1566)
- [문제 - 정답](https://code.oa.gg/java8/1567)
- 배열시작
- [문제 - 배열안의 숫자들의 평균을 구해주세요.](https://code.oa.gg/java8/1564)
- [문제 - 정답](https://code.oa.gg/java8/1565)
- [프로그래머스 자바 알고리즘 레벨1 - 직사각형 별찍기](https://programmers.co.kr/learn/courses/30/lessons/12969?language=java)
- [프로그래머스 자바 알고리즘 레벨1 - 평균 구하기](https://programmers.co.kr/learn/courses/30/lessons/12944?language=java)
- 배열끝
- switch 시작
- [개념 - switch](https://code.oa.gg/java8/1568)
- [프로그래머스 - 자바 입문](https://programmers.co.kr/learn/courses/5)
  - 파트 7까지 진행해주세요.
  - [풀이](https://codepen.io/jangka44/debug/MWYqKNm)
- switch 끝
- 문자시작
- [프로그래머스 자바 알고리즘 레벨1 - 문자열 내 p와 y의 개수](https://programmers.co.kr/learn/courses/30/lessons/12916?language=java)
- [문제 - 문장에서 x가 얼마나 많이 들어 있는지 알려주세요.](https://code.oa.gg/java8/1569)
- [문제 - 정답](https://code.oa.gg/java8/1570)
- 문자끝
- static 시작
- [개념 : static 변수](https://code.oa.gg/java8/1571)
- [문제 - 자동차를 달리게 해주세요(static 사용 금지)(개념 : static)](http://code.oa.gg/java8/940)
- [문제 - 정답](http://code.oa.gg/java8/941)
- [문제 - 올바른 PI(원주율)변수를 만들어주세요.(static 사용 금지)(개념 : static)](http://code.oa.gg/java8/942)
- [문제 - 정답](http://code.oa.gg/java8/943)
- [문제 - 계산기의 더하기가 작동하게 해주세요.(static 사용 금지)(개념 : static)](http://code.oa.gg/java8/944)
- [문제 - 정답](http://code.oa.gg/java8/945)
- [문제 - static을 사용하여 코드가 작동하도록 해주세요.(개념 : static)](http://code.oa.gg/java8/946)
- [문제 - 정답](http://code.oa.gg/java8/947)
- [문제 - 각각의 사람이 올바른 자기소개를 하도록 해주세요.(개념 : static)](http://code.oa.gg/java8/948)
- [문제 - 정답](http://code.oa.gg/java8/949)
- [문제 - 각각의 사람이 올바른 자기소개를 하도록 해주세요.(개념 : static, static 변수와 인스턴스 메서드)](http://code.oa.gg/java8/950)
- [문제 - 정답](http://code.oa.gg/java8/951)
- [문제 - 각각의 사람이 올바른 자기소개를 하도록 해주세요.(개념 : static, 인스턴스 변수와 static 메서드)](http://code.oa.gg/java8/952)
- [문제 - 정답](http://code.oa.gg/java8/953)
- [문제 - 올바른 출력 메서드를 만들어주세요.(개념 : static)](http://code.oa.gg/java8/954)
- [문제 - 정답](http://code.oa.gg/java8/955)
- [문제 - 올바른 출력 메서드를 만들어주세요.(개념 : static, 오버로딩)](http://code.oa.gg/java8/956)
- [문제 - 정답](http://code.oa.gg/java8/957)
- static 끝

## 오늘의 동영상
### 일반
- [동영상 - 2020 01 15, 자바, 개념, this, super](https://www.youtube.com/watch?v=esgy1cucnx0&feature=youtu.be)
- [동영상 - 2020 01 15, 자바, 개념, 배열](https://www.youtube.com/watch?v=lwO5NquGlJ4&feature=youtu.be)
- [동영상 - 2020 01 15, 자바, 개념, 상수](https://www.youtube.com/watch?v=f0mD6I9DhTI&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 개념, switch](https://www.youtube.com/watch?v=Bs4KzFLjHg8&feature=youtu.be)
- [동영상 - 2020 01 17, 자바, 개념, static](https://www.youtube.com/watch?v=VsocJPkWCJY&feature=youtu.be)
- [동영상 - 2020 01 17, 자바, 예제, static 기초 문제들](https://www.youtube.com/watch?v=Y8do5e55vNM&feature=youtu.be)
- [동영상 - 2020 01 17, 자바, 예제, static 문제, 각각의 사람이 올바른 자기소개를 하도록 해주세요](https://www.youtube.com/watch?v=InnPNZ67VTY&feature=youtu.be)
- [동영상 - 2020 01 17, 자바, 예제, 문장에서 x가 얼마나 많이 들어 있는지 알려주세요](https://www.youtube.com/watch?v=vCrOuNWaWTo&feature=youtu.be)
- [동영상 - 2020 01 17, 자바, 예제, static, 오버로딩 개념, 올바른 출력 메서드를 구현해주세요](https://www.youtube.com/watch?v=-Da0YmSIepA&feature=youtu.be)
- [동영상 - 2020_05_17, 자바, 개념, 같은 객체라도 조종하는 리모콘 타입에 따라 활성화 되는 변수가 다를 수 있다](https://youtu.be/85aC6RTNhyw)
- [동영상 - 2020_05_17, 자바, 문제, this와 super변수의 차이를 설명하는 예를 만들어주세요](https://youtu.be/Az1arSlUzwE)
- [동영상 - 2020_05_17, 자바, 문제, 변수 a를 상수로 만들어주세요](https://youtu.be/UmnOYKitvJk)
- [동영상 - 2020_05_17, 자바, 문제, 배열안의 숫자들의 평균을 구해주세요](https://youtu.be/pNuUEHcsh4A)
- [동영상 - 2020_05_17, 자바, 개념, switch](https://youtu.be/6oOcq0j7CXk)
- [동영상 - 2020_05_17, 자바, 문제, 문장에서 x가 얼마나 많이 들어 있는지 알려주세요](https://youtu.be/qxn7pbAp4pM)
- [동영상 - 2020 05 17, 자바, 개념, static](https://youtu.be/xUFnEwrRdbg)
- [동영상 - 2020 05 17, 자바, 문제, 자동차를 달리게 해주세요, static 사용금지](https://youtu.be/8YBycp6mWA4)
- [동영상 - 2020 05 17, 자바, 문제, 원주율 변수를 올바르게 고쳐주세요, static 사용금지](https://youtu.be/1_kU089oMG0)
- [동영상 - 2020 05 17, 자바, 문제, 계산기의 더하기 기능이 작동해야 합니다, static 사용금지](https://youtu.be/yvWhQiwq2XU)
- [동영상 - 2020_05_17, 자바, 문제, static을 사용하여 작동하도록 해주세요](https://youtu.be/HcSqAhzLydk)
- [동영상 - 2020 05 17, 자바, 문제, 각각의 사람이 올바른 자기소개를 하도록 해주세요, static 메서드 static 변수](https://youtu.be/g1cTsYfmf-8)
- [동영상 - 2020 05 17, 자바, 문제, 각각의 사람이 올바른 자기소개를 하도록 해주세요, static 메서드 인스턴스 변수](https://youtu.be/Y7yYsJlUkIo)
- [동영상 - 2020 05 17, 자바, 문제, 각각의 사람이 올바른 자기소개를 하도록 해주세요, 인스턴스 메서드 static 변수](https://youtu.be/VFWYlezw1uA)
- [동영상 - 2020 05 17, 자바, 문제, 올바른 출력 메서드를 만들어주세요, static, 오버로딩](https://youtu.be/t2hJzM4SloI)

# 2020-05-23, 13일차
## 문제
### 일반
- 형변환 시작
- [문제 - 다양한 데이터를 입력받아 저장할 수 있는 저장소 클래스를 만들어주세요.(개념 : 오버로딩, 자동형변환)](http://code.oa.gg/java8/958)
- [문제 - 정답](http://code.oa.gg/java8/959)
- [문제 - double 형 변수 d 에 담긴 10.5 를 float 형 변수 f에 넣어주세요. 그 반대작업도 진행해주세요.(개념 : 형변환)](http://code.oa.gg/java8/960)
- [문제 - 정답](http://code.oa.gg/java8/961)
- [문제 - 모든 기본형 타입에 대해서 래퍼클래스를 사용하여 최대값 최소값을 출력해보세요.(개념 : 래퍼클래스)](http://code.oa.gg/java8/962)
- [문제 - 정답](http://code.oa.gg/java8/963)
- [문제 - 기본형 변수의 경우 자동형변환을 이용하여 `저장 메서드`의 개수를 최대한 줄여보세요.(개념 : 자동형변환, 오버로딩)(5개 이하로 줄이세요.)](http://code.oa.gg/java8/964)
- [문제 - 정답](http://code.oa.gg/java8/965)
- [문제 - 객체를 매개변수로 받는 `저장` 메서드도 최대한 개수를 줄여보세요.(개념 : 상속이나 구현을 통한 형변환 컨트롤)(4개 이하로 줄이세요.)](http://code.oa.gg/java8/966)
- [문제 - 정답](http://code.oa.gg/java8/967)
- [문제 - 객체를 매개변수로 받는 `저장` 메서드를 1개로 줄여보세요.(개념 : Object 클래스, 모든 클래스의 최초조상)](http://code.oa.gg/java8/968)
- [문제 - 정답](http://code.oa.gg/java8/969)
- [문제 - `저장` 메서드를 1개로 줄여보세요.(개념 : Object 클래스, 자동형변환, 박싱, 오토박싱)](http://code.oa.gg/java8/970)
- [문제 - 정답](http://code.oa.gg/java8/971)
- [문제 - 수동형변환으로 오류를 해결해주세요.(개념 : 수동형변환)](http://code.oa.gg/java8/973)
- [문제 - 정답](http://code.oa.gg/java8/974)
- [문제 - 저장소를 만들고 i의 값을 저장한 후 다시 받아보세요.(개념 : 리턴타입 형변환)](http://code.oa.gg/java8/975)
- [문제 - 정답](http://code.oa.gg/java8/976)
- [문제 - 숫자를 저장하는 저장소 클래스를 구현해주세요.(개념 : 형변환, int 전용 저장소)](http://code.oa.gg/java8/977)
- [문제 - 정답](http://code.oa.gg/java8/978)

### 문자, 알고리즘
- [문제 - 문자열에서 숫자문자와 알파벳 소문자가 각각 몇개씩 포함되어 있는지 카운팅 해주세요.](https://repl.it/@jangka512/UncommonPrimaryRuby)
- [문제 - 정답](https://repl.it/@jangka512/PaleShadyBudgetrange)
- [문제 - 정답 v2](https://repl.it/@jangka512/GhostwhiteWellinformedIde)
- [문제 - 정답 v3](https://repl.it/@jangka512/ScornfulSilkyLinux)

## 알고리즘 문제
- [프로그래머스 자바 알고리즘 레벨1 - 문자열 다루기 기본](https://programmers.co.kr/learn/courses/30/lessons/12918?language=java)
- [프로그래머스 자바 알고리즘 레벨1 - 짝수와 홀수](https://programmers.co.kr/learn/courses/30/lessons/12937?language=java)
- [프로그래머스 자바 알고리즘 레벨1 - 약수의 합](https://programmers.co.kr/learn/courses/30/lessons/12928?language=java)
- [프로그래머스 자바 알고리즘 레벨1 - 두 정수 사이의 합](https://programmers.co.kr/learn/courses/30/lessons/12912?language=java)

## 오늘의 동영상
### 일반
- [동영상 - 2020 01 18, 자바, 예제, 다양한 데이터를 입력받아 저장할 수 있는 저장소 클래스를 만들어주세요, 개념, 오버로딩, 자동형변환](https://www.youtube.com/watch?v=kHBOKakC3r8&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, double 형 변수 d에 담긴 10 5 를 float 형 변수 f에 넣어주세요, 그 반대작업도 진행해주세요, 개념, 형변환](https://www.youtube.com/watch?v=DAPQhhLCu1w&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 모든 기본형 타입에 대해서 래퍼클래스를 사용하여 최대값 최소값을 출력해보세요, 개념 래퍼클래스](https://www.youtube.com/watch?v=cdjIi5JafdY&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 기본형 변수의 경우 자동형변환을 이용하여 저장 메서드의 개수를 최대한 줄여보세요, 개념, 자동형변환, 오버로딩](https://www.youtube.com/watch?v=DdD5ZsAIty4&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 객체를 매개변수로 받는 저장 메서드도 최대한 개수를 줄여보세요, 개념, 상속이나 구현을 통한 형변환 컨트롤, 4개 이하로 줄이세요](https://www.youtube.com/watch?v=t62IfzjzaMk&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 객체를 매개변수로 받는 저장 메서드를 1개로 줄여보세요, 개념, Object 클래스, 모든 클래스의 최초조상](https://www.youtube.com/watch?v=v0uUknYZJwc&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 저장 메서드를 1개로 줄여보세요, 개념, Object 클래스, 자동형변환, 박싱, 오토박싱](https://www.youtube.com/watch?v=hirCPOTxzQU&feature=youtu.be)
- [동영상 - 2020 01 20, 자바, 예제, 저장 메서드를 1개로 줄여보세요, 설명 v2, 개념, Object 클래스, 자동형변환, 박싱, 오토박싱](https://www.youtube.com/watch?v=DnaozMLNbb8&feature=youtu.be)
- [동영상 - 2020 01 20, 자바, 개념, 범용성 있는 클래스 만드는 방법, 메소드 개수 줄이는 이유](https://www.youtube.com/watch?v=joROUQH_v6U&feature=youtu.be)
- [동영상 - 2020 01 18, 자바, 예제, 저장소를 만들고 i의 값을 저장한 후 다시 받아보세요, 개념, 리턴타입, 형변환](https://www.youtube.com/watch?v=cacQzcGAzAo&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 저장소를 만들고 i의 값을 저장한 후 다시 받아보세요, 개념, 리턴타입, 형변환(설명 v2)](https://www.youtube.com/watch?v=xa2UZ2s9F58&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 숫자를 저장하는 저장소 클래스를 구현해주세요, 개념, 형변환, int 전용 저장소](https://www.youtube.com/watch?v=A--1kL7SjBk&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 숫자를 저장하는 저장소 클래스를 구현해주세요, 개념, 형변환, int 전용 저장소(설명 v2)](https://www.youtube.com/watch?v=1fDRnmaokB0&feature=youtu.be)
- [동영상 - 2020_05_23, 자바, 문제, 다양한 데이터를 저장할 수 있는 저장소 클래스를 생성, 오버로딩](https://youtu.be/WbTWlh2RF1U)
- [동영상 - 2020_05_23, 자바, 문제, double 형 변수 d 에 담긴 10.5 를 float 형 변수 f에 넣고, 반대작업도 진행, 수동형변환, 자동형변환](https://youtu.be/nN7SkaiSGBI)
- [동영상 - 2020_05_23, 자바, 문제, 모든 기본형 타입에 대해서 래퍼클래스를 사용하여 최대값 최소값을 출력, 래퍼클래스](https://youtu.be/AvABXbSOOm0)
- [동영상 - 2020_05_23, 자바, 문제, 기본형 관련 저장 메소드 개수를 줄여주세요, 자동형변환](https://youtu.be/Oolz2Sj1wUA)
- [동영상 - 2020_05_23, 자바, 문제, 레퍼런스형 관련 저장 메소드 개수를 줄여주세요, 자동형변환, 상속](https://youtu.be/YS0pcsyQiP0)
- [동영상 - 2020_05_23, 자바, 문제, 레퍼런스형 관련 저장 메소드 개수를 1개로 줄여주세요, 자동형변환, Object](https://youtu.be/bn8qEa3uolk)
- [동영상 - 2020_05_23, 자바, 문제, 저장 메소드 개수를 1개로 줄여주세요, 자동형변환, Object, 박싱](https://youtu.be/B-DxFdN2keg)
- [동영상 - 2020_05_23, 자바, 문제, 범용 저장소에 값을 넣고 다시 받아주세요, 수동형변환, Object](https://youtu.be/3RNrSJzHqaY)
- [동영상 -2020_05_23, 자바, 문제, 숫자를 저장하는 저장소 클래스를 구현해주세요, 형변환, 범용성이 없는 대신 편함](https://youtu.be/5Es9BcQh2z4)
- [동영상 - 2020 01 21, 자바, 예제, 문자열에서 숫자문자와 알파벳 소문자가 각각 몇개씩 포함되어 있는지 카운팅 해주세요](https://www.youtube.com/watch?v=T4nXVLw4Kbo&feature=youtu.be)

# 2020-05-24, 14일차
## 문제
### 일반
- [문제 - 다양한 데이터를 저장할 수 있는 저장소 클래스를 구현해주세요(개념 : Object 클래스, 범용데이터 저장소)](http://code.oa.gg/java8/979)
- [문제 - 정답](http://code.oa.gg/java8/980)
- [문제 - 정답 v2](http://code.oa.gg/java8/984)
- [문제 - 박싱, 언방식에 관련해서 다양한 예를 보여주세요.(개념 : 오토방식, 오토언박싱, 수동박싱, 수동언박싱)](http://code.oa.gg/java8/981)
- [문제 - 정답](http://code.oa.gg/java8/982)
- [문제 - 사람객체가 동물리모콘, Object리모콘에 연결된 후 다시 사람 리모콘에 연결하여 말하게 해주세요.(개념 : 자동형변환, 수동형변환)](http://code.oa.gg/java8/985)
- [문제 - 정답](http://code.oa.gg/java8/986)
- 형변환 끝
- 배열 시작
- [문제 - 숫자 여러개 저장할 수 있는 클래스를 만들어주세요.(개념 : 배열)](https://repl.it/@jangka512/WindingSerpentineOctagon)
- [문제 - 정답](https://repl.it/@jangka512/MajesticFluidAggregator)
- [문제 - 문자 a, b, c가 저장 되는 캐릭터 배열을 만들어주세요.(개념 : 배열)](http://code.oa.gg/java8/987)
- [문제 - 정답](http://code.oa.gg/java8/988)
- [문제 - 사람 리모콘 3개 저장할 수 있는 배열을 만들고 사람 리모콘으로 채워주세요.(개념 : 배열)](https://repl.it/@jangka512/IckySuperiorParentheses)
- [문제 - 정답](https://repl.it/@jangka512/WellmadeWideeyedFont)
- [문제 - 숫자 1부터 10까지 저장해주세요.](http://code.oa.gg/java8/1465)
- [문제 - 정답 v1](http://code.oa.gg/java8/1466)
- [문제 - 정답 v2](http://code.oa.gg/java8/1467)
- [개념 - 배열의 한계와 ArrayList](https://code.oa.gg/java8/1572)
- 배열 끝
- [Object 클래스와 toString](https://code.oa.gg/java8/1574)
- [Object 클래스와 equals](https://code.oa.gg/java8/1575)
- [String은 char와 어떻게 다른가?](https://code.oa.gg/java8/1576)
- [문장을 만드는 2가지 방법](https://code.oa.gg/java8/1577)
- [String 불변성과 StringBuilder](https://code.oa.gg/java8/1578)

## 오늘의 동영상
- [동영상 - 2020 01 18, 자바, 예제, 수동형변환으로 오류를 해결해주세요, 개념, 수동형변환](https://www.youtube.com/watch?v=L2--y98P4O0&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 수동형변환으로 오류를 해결해주세요, 개념, 수동형변환(설명 v2)](https://www.youtube.com/watch?v=KXVFp1fWhos&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 다양한 데이터를 저장할 수 있는 저장소 클래스를 구현해주세요, 개념, Object 클래스, 범용데이터 저장소](https://www.youtube.com/watch?v=x90KPdtUasE&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 다양한 데이터를 저장할 수 있는 저장소 클래스를 구현해주세요, 개념, Object 클래스, 범용데이터 저장소(설명 v2)](https://www.youtube.com/watch?v=VRYE223wEkQ&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 박싱, 언방식에 관련해서 다양한 예를 보여주세요, 개념, 오토방식, 오토언박싱, 수동박싱, 수동언박싱](https://www.youtube.com/watch?v=Pbf262PmOdU&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 박싱, 언방식에 관련해서 다양한 예를 보여주세요, 개념, 오토방식, 오토언박싱, 수동박싱, 수동언박싱(설명 v2)](https://www.youtube.com/watch?v=j5VyThQeodY&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 사람객체가 동물리모콘, Object리모콘에 연결된 후 다시 사람 리모콘에 연결하여 말하게 해주세요, 개념 자동형변환, 수동형변환](https://www.youtube.com/watch?v=w3VzOFNw9U8&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 사람객체가 동물리모콘, Object리모콘에 연결된 후 다시 사람 리모콘에 연결하여 말하게 해주세요, 개념 자동형변환, 수동형변환(설명 v2)](https://www.youtube.com/watch?v=PaOb28UPwXA&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 개념, static, 형변환, 오토박싱, Object 정리](https://www.youtube.com/watch?v=aTFc6N9HuJE&feature=youtu.be)
- [동영상 - 2020 01 22, 자바, 예제, 숫자 여러개 저장할 수 있는 클래스를 만들어주세요, 개념, 배열](https://www.youtube.com/watch?v=ERdzxq58ha0&feature=youtu.be)
- [동영상 - 2020 01 23, 자바, 개념, 배열의 한계와 ArrayList](https://www.youtube.com/watch?v=xGpRQdbZJ-4&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 문자 a, b, c가 저장 되는 캐릭터 배열을 만들어주세요, 개념, 배열](https://www.youtube.com/watch?v=11Eg2j1W78A&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 사람 리모콘 3개 저장할 수 있는 배열을 만들고 사람 리모콘으로 채워주세요, 개념, 배열](https://www.youtube.com/watch?v=ZS-_pafW5sI&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, 숫자 1부터 10까지 저장해주세요](https://www.youtube.com/watch?v=Hd7YbSrJ--M&feature=youtu.be)
- [동영상 - 2020 01 28, 자바, 예제, 배열 관련 기본문제](https://www.youtube.com/watch?v=kv7Dmj0Jv9M&feature=youtu.be)
- [동영상 - 2020 01 29, 자바, 개념, Object 클래스와 equals](https://www.youtube.com/watch?v=I_UKjPuhTBg&feature=youtu.be)
- [동영상 - 2020 01 29, 자바, 개념, Object 클래스와 toString](https://www.youtube.com/watch?v=3-PBVHe9FKM&feature=youtu.be)
- [동영상 - 2020 01 29, 자바, 개념, String은 char와 어떻게 다른가](https://www.youtube.com/watch?v=Mi0G-RbZmcE&feature=youtu.be)
- [동영상 - 2020 01 29, 자바, 개념, 문장을 만드는 2가지 방법](https://www.youtube.com/watch?v=wlfEcYw8Yfo&feature=youtu.be)
- [동영상 - 2020 01 30, 자바, 개념, String 불변성과 StringBuilder](https://www.youtube.com/watch?v=U5MXo5DAgDk&feature=youtu.be)
- [동영상 - 2020 05 24, 자바, 문제, 다양한 형태의 데이터를 저장할 수 있는 저장소 클래스, Object, 범용데이터](https://youtu.be/toWFNbFCPtw)
- [동영상 - 2020_05_24, 자바, 문제, 박싱, 언박식의 다양한 예를 보여주세요, 오토박싱, 오토언박싱](https://youtu.be/QngpyqI4yUA)
- [동영상 - 2020_05_24, 자바, 문제, 사람객체가 순서대로, 동물리모콘, Object 리모콘, 사람리모콘에 연결되어야](https://youtu.be/UGKqTvV8NJY)
- [동영상 - 2020_05_24, 자바, 문제, 숫자 여러개 저장할 수 있는 클래스를 만들어주세요, 배열개념](https://youtu.be/_glQV0M1jQ0)
- [동영상 - 2020_05_24, 자바, 문제, 문자 a, b, c가 저장 되는 캐릭터 배열을 만들어주세요](https://youtu.be/QykBccvYqx8)
- [동영상 - 2020_05_24, 자바, 문제, 사람 리모콘 3개 저장할 수 있는 배열을 만들고 사람 리모콘으로 채워주세요](https://youtu.be/ugBqoxHoQE8)
- [동영상 - 2020_05_24, 자바, 문제, 숫자 1부터 10까지 저장해주세요](https://youtu.be/NYcQ37GV7EQ)
- [동영상 - 2020_05_24, 자바, 개념, Object 클래스와 toString](https://youtu.be/KFkwYAgsPYA)
- [동영상 - 2020_05_24, 자바, 개념, Object 클래스와 equals](https://youtu.be/5vs9p-4Pt5I)
- [동영상 - 2020_05_24, 자바, 개념, String은 char와 어떻게 다른가](https://youtu.be/7g_LPUxpE2w)
- [동영상 - 2020_05_24, 자바, 개념, 문장객체를 만드는 2가지 방법](https://youtu.be/SOpPeXm-2uE)
- [동영상 - 2020_05_24, 자바, 개념, String 불변성과 StringBuilder](https://youtu.be/Hq1Kp-DpvkQ)

# 2020-05-30, 15일차
## 문제
### 일반
- ArrayList 시작
- [문제 - ArrayList 만들기 문제 1(개념 : ArrayList 개요)](http://code.oa.gg/java8/989)
- [문제 - 정답](http://code.oa.gg/java8/990)
- [문제 - ArrayList 만들기 문제 2(개념 : add 함수)](http://code.oa.gg/java8/991)
- [문제 - 정답](http://code.oa.gg/java8/992)
- [문제 - ArrayList 만들기 문제 3(개념 : 배열의 길이가 필요할 때 자동으로 증가, 사실은 교체)](http://code.oa.gg/java8/993)
- [문제 - 정답](http://code.oa.gg/java8/994)
- [문제 - ArrayList 만들기 문제 4(개념 : size 함수)](http://code.oa.gg/java8/995)
- [문제 - 정답](http://code.oa.gg/java8/996)
- [문제 - ArrayList 만들기 문제 5(개념 : remove 함수)](http://code.oa.gg/java8/997)
- [문제 - 정답](http://code.oa.gg/java8/998)
- [문제 - ArrayList 만들기 문제 6(개념 : addAt 함수)](http://code.oa.gg/java8/1340)
- [문제 - 정답](http://code.oa.gg/java8/1341)
- [문제 - ArrayList 최종](http://code.oa.gg/java8/1461)
- [문제 - 정답](http://code.oa.gg/java8/1462)
- [개념 - ArrayList 총정리, 1](https://code.oa.gg/java8/1579)
- [개념 - 배열보다 ArrayList가 좋은 이유, 번호 입력받는 프로그램, 배열 버전](https://repl.it/@jangka512/DeadLuckyWamp)
- [개념 - 배열보다 ArrayList가 좋은 이유, 번호 입력받는 프로그램, ArrayList 버전](https://repl.it/@jangka512/SizzlingWrithingEmulator)
- [문제 - 사용자에게 숫자 5개 입력받고 배열에 저장](https://repl.it/@jangka512/PreemptiveFrayedSymbol)
- [문제 - 정답](https://repl.it/@jangka512/NarrowImpassionedCron)
- [문제 - 사용자에게 숫자 5개 입력받고 ArrayList에 저장](https://repl.it/@jangka512/WindyDarkmagentaCurrencies)
- [문제 - 정답](https://repl.it/@jangka512/IndigoMulticoloredFinance)
- [문제 - 정답 v2](https://repl.it/@jangka512/IndigoMulticoloredFinance-1)
- ArrayList 끝
- 제너릭 시작
- [문제 - 제너릭을 이용해서 클래스 3개를 1개로 줄여주세요.](http://code.oa.gg/java8/1350)
- [문제 - 정답](http://code.oa.gg/java8/1351)
- [문제 - ArrayList에 제너릭을 적용해주세요.](http://code.oa.gg/java8/1352)
- [문제 - 정답](http://code.oa.gg/java8/1353)
- [문제 - 제너릭이 적용된 저장소 클래스를 만들어주세요.](http://code.oa.gg/java8/1463)
- [문제 - 정답](http://code.oa.gg/java8/1464)
- [문제 - 제너릭이 적용된 저장소 클래스를 만들어주세요. 처음부터 끝까지](https://repl.it/@jangka512/BestContentClients)
- [문제 - 정답](https://repl.it/@jangka512/FrontHandyFunctions)
- [문제 - 정답 v2](https://code.oa.gg/java8/1581)
- 제너릭 끝

## 오늘의 동영상
- [동영상 - 2020 01 19, 자바, 개념, ArrayList를 사용하는 이유](https://www.youtube.com/watch?v=LnpHUVin4u0&feature=youtu.be)
- [동영상 - 2020 01 19, 자바, 예제, ArrayList 만들기](https://www.youtube.com/watch?v=fx1-iKvwu-I&feature=youtu.be)
- [동영상 - 2020 01 30, 자바, 개념, ArrayList 총정리, 1](https://www.youtube.com/watch?v=-__xPbu-xN4&feature=youtu.be)
- [동영상 - 2020 01 31, 자바, 개념, 배열보다 ArrayList가 좋은 이유, 코드비교](https://www.youtube.com/watch?v=WuydMj1jIV8&feature=youtu.be)
- [동영상 - 2020 01 31, 자바, 예제, ArrayList에 제너릭을 적용해주세요](https://www.youtube.com/watch?v=HR191W1Bm40&feature=youtu.be)
- [동영상 - 2020 01 31, 자바, 예제, 제너릭을 이용해서 클래스 3개를 1개로 줄여주세요](https://www.youtube.com/watch?v=soFWYho_k70&feature=youtu.be)
- [동영상 - 2020 01 31, 자바, 예제, 제너릭이 적용된 저장소 클래스를 만들어주세요](https://www.youtube.com/watch?v=CFHP-Lxtoqg&feature=youtu.be)
- [동영상 - 2020 01 31, 자바, 예제, 제너릭이 적용된 저장소 클래스를 만들어주세요 처음부터 끝까지](https://www.youtube.com/watch?v=dtds5gZfWg0&feature=youtu.be)
- [동영상 - 2020_05_30, 자바, 문제, ArrayList 직접 만들기, 추가](https://youtu.be/VemvhJNXf5k)
- [동영상 - 2020_05_30, 자바, 문제, ArrayList 직접 만들기, 삭제](https://youtu.be/CKfcw_ZXc6U)
- [동영상 - 2020_05_30, 자바, 문제, ArrayList 직접 만들기, 중간에 추가](https://youtu.be/VQrHaL_FrO8)
- [동영상 - 2020_05_30, 자바, 개념, 컬렉션, 제너릭](https://youtu.be/_6GpvxdIKek)

# 2020-05-31, 16일차
## 개념
### 일반
- 클래스 다중상속의 장점과 단점은?
  - 장점 : 객체에 다형성을 원하는 만큼 부여할 수 있다.
  - 단점 : 하나의 자식 클래스에 2명 이상의 부모클래스에서 똑같의 형태의 메서드를 2개 이상을 물려받을 수 있다. 이때 자식 클래스에서 해당 메서드를 오버라이드를 하지 않는다면, 모호함이 발생한다. 참고로 자식클래스에서 해당 메서드를 오버라이드 해야할 의무는 없다.
  - [소스코드](https://code.oa.gg/java8/1583)
- 자바에서 클래스 다중상속을 막은 이유는?
  - C++과 달리, 자바는 개발자가 고생할 수 있는 여지를 줄이기 위해서, 해당 기능을 없앴다.
- 클래스 다중상속에서 나타날 수 있는 문제점을 해결할는 방법은?
  - 자식클래스에서 모호한 메서드를 오버라이드 한다.
- 인터페이스와 클래스의 차이점은?
  - 인터페이스는 100% 추상클래스 이다.
  - 인터페이스 안에 있는 메서드는 어차피 추상메서드 이기 때문에, abstract 키뤄드를 메서드 앞에 붙일 필요가 없다.
- 자바에서 인터페이스 다중상속을 허용한 이유는?
  - 객체지향 프로그래밍에서 다형성은 굉장히 중요하다.
  - 자바에서는 다중상속이 막혀있어서, 자유로운 다형성 부여가 힘든 상황이다.
  - 그래서 자바는 인터페이스라는 제한된 형태의 클래스는 다중상속을 허용했다.
  - 인터페이스를 다중상속 해서, 모호함이 발생해도, 자식 클래스에서 해당 메서드를 오버라이드 해야하는게 필수 이기 때문에, 모호함이 존재할 수 없는 구조이다.

## 문제
### 일반
- HashMap 시작
- [개념 : List와 맵](https://code.oa.gg/java8/1582)
- [개념 : Equals 와 ==](http://code.oa.gg/java8/1354)
- [개념 : 일반 vs 제너릭 비교정리](https://codepen.io/jangka44/debug/BELrWe)
- [개념 : 배열, ArrayList, HashMap 비교정리](https://codepen.io/jangka44/debug/YMGaGv)
- [문제 - HashMap 만들기 문제 1](http://code.oa.gg/java8/1127)
- [문제 - 정답](http://code.oa.gg/java8/1128)
- [문제 - HashMap 만들기 문제 2](http://code.oa.gg/java8/1129)
- [문제 - 정답](http://code.oa.gg/java8/1130)
- [문제 - HashMap 만들기 문제 3](http://code.oa.gg/java8/1131)
- [문제 - 정답](http://code.oa.gg/java8/1132)
- [문제 - HashMap 만들기 문제 4](http://code.oa.gg/java8/1472)
- [문제 - 정답](http://code.oa.gg/java8/1473)
- 인터페이스 시작
- [개념 : 구상클래스, 추상클래스, 인터페이스 비교](https://s.codepen.io/jangka44/debug/BEKJoZ)
- [인터페이스 문제 1](http://code.oa.gg/java8/894)
- [인터페이스 문제 1 정답](http://code.oa.gg/java8/895)
- [인터페이스 문제 2](http://code.oa.gg/java8/896)
- [인터페이스 문제 2 정답](http://code.oa.gg/java8/897)
- [인터페이스 문제 3](http://code.oa.gg/java8/1346)
- [인터페이스 문제 3 정답](http://code.oa.gg/java8/1344)
- [인터페이스 문제 4](http://code.oa.gg/java8/1345)
- [인터페이스 문제 4 정답](http://code.oa.gg/java8/1347)
- [인터페이스 문제 5](http://code.oa.gg/java8/1348)
- [인터페이스 문제 5 정답](http://code.oa.gg/java8/1349)
- [개념 : 인터페이스, 상위타입이 좋은 점](http://blogfiles.naver.net/20160523_212/mals93_14639326832691Gr0Y_JPEG/interface.jpg)
- 인터페이스 끝
- 예외시작
- [개념 - 예외처리, 기초, ArithmeticException](http://code.oa.gg/java8/1468)
- [개념 - 예외처리, ArithmeticException, 부하직원이 처리하지 못하면, 선임이 처리해야 한다.](http://code.oa.gg/java8/1469)
- [개념 - 예외처리, ArrayIndexOutOfBoundsException](http://code.oa.gg/java8/1470)
- [개념 - 예외처리, ArrayIndexOutOfBoundsException, 부하직원이 처리하지 못하면, 선임이 처리해야 한다.](http://code.oa.gg/java8/1471)
- [문제 - 올바른 나이를 입력할 때 까지 집요하게 계속 물어보는게 해주세요.](https://repl.it/@jangka512/DualBubblyFlashdrive)
- [문제 - 정답](https://repl.it/@jangka512/MicroFrailIde)
- [개념 - 예외처리, 문제가 생기면 본인이 스스로 고민해서 수습하는 부하직원](https://code.oa.gg/java8/1584)
- [개념 - 예외처리, 문제가 생기면, 방치해서 결국 선임이 알아채도록 놔두는 부하직원](https://code.oa.gg/java8/1585)
- [개념 - 예외처리, 커스텀 예외](https://code.oa.gg/java8/1586)
- [개념 - 예외처리, 문제가 생길것 같다면, 미리 선임에게 적극적으로 보고하는 부하직원, 커스텀 예외](https://code.oa.gg/java8/1587)
- 예외끝
- 접근제어자 시작
- [문제 - getter, setter, 문제 1](http://code.oa.gg/java8/1069)
- [문제 - 정답](http://code.oa.gg/java8/1070)
- [문제 - getter, setter, 문제 2](http://code.oa.gg/java8/1071)
- [문제 - 정답](http://code.oa.gg/java8/1072)
- 접근제어자 끝
- 싱글톤 패턴 시작
- [문제 - 싱글톤 패턴, 문제 1](http://code.oa.gg/java8/1073)
- [문제 - 정답](http://code.oa.gg/java8/1074)
- [문제 - 싱글톤 패턴, 문제 2](http://code.oa.gg/java8/1075)
- [문제 - 정답](http://code.oa.gg/java8/1076)
- [문제 - 싱글톤 패턴, 문제 3](http://code.oa.gg/java8/1077)
- [문제 - 정답](http://code.oa.gg/java8/1078)
- [문제 - 싱글톤 패턴, 문제 4](http://code.oa.gg/java8/1079)
- [문제 - 정답](http://code.oa.gg/java8/1080)
- 싱글톤 패턴 끝
- 팩토리 패턴 시작
- [문제 - 팩토리 패턴, 문제 1](http://code.oa.gg/java8/1081)
- [문제 - 정답](http://code.oa.gg/java8/1082)
- [문제 - 정답 v2](https://code.oa.gg/java8/1601)
- 팩토리 패턴 끝
- 옵저버 패턴 시작
- [문제 - 다른 객체가 해당 메서드를 이용하게 해주세요.](https://code.oa.gg/java8/1599)
- [문제 - 정답](https://code.oa.gg/java8/1602)
- [문제 - 다른 객체가 해당 메서드를 이용하게 해주세요. 인터페이스 사용](https://code.oa.gg/java8/1600)
- [문제 - 정답 v1(불완전한 정답)](https://code.oa.gg/java8/1603)
- [문제 - 정답 v2](https://code.oa.gg/java8/1604)
- [문제 - 인터페이스를 구현해주세요.](http://code.oa.gg/java8/1408)
- [문제 - 정답](http://code.oa.gg/java8/1409)
- [문제 - 인터페이스를 구현해주세요. 오버라이드](http://code.oa.gg/java8/1410)
- [문제 - 정답](http://code.oa.gg/java8/1411)
- [문제 - 버튼이 동물에게 클릭 사실을 알리도록 해주세요. Object 사용](https://code.oa.gg/java8/1589)
- [정답](https://code.oa.gg/java8/1593)
- [개념 - 인터페이스 동물이 소리를 내게 하는 어리석고 비효율적인 방법](https://code.oa.gg/java8/1591)
- [개념 - 인터페이스 동물이 소리를 내게 하는 현명하고 효율적인 방법](https://code.oa.gg/java8/1592)
- [문제 - 버튼이 동물에게 클릭 사실을 알리도록 해주세요. 버튼구독자 인터페이스 사용](https://code.oa.gg/java8/1590)
- [정답](https://code.oa.gg/java8/1594)
- [문제 - OnClickEventListener를 사용해서 버튼이 리스너에게 클릭사실을 알리도록 해주세요.](http://code.oa.gg/java8/1412)
- [문제 - 정답](http://code.oa.gg/java8/1413)
- 옵저버 패턴 끝
- 익명클래스와 옵저버 패턴 응용 시작
- [문제 - 익명내부클래스와 인터페이스를 사용해서 문제를 풀어주세요.](http://code.oa.gg/java8/1414)
- [문제 - 정답 v1](https://code.oa.gg/java8/1595)
- [문제 - 정답 v2](https://code.oa.gg/java8/1596)
- [문제 - 정답 v3](https://code.oa.gg/java8/1597)
- 익명클래스와 옵저버 패턴 응용 끝
- 옵저버 패턴과 내부 익명 클래스 시작
- [개념 : 버튼 옵저버 패턴, 작업 1](https://code.oa.gg/java8/1605)
  - 내용 : 이 세상에는 구독자라는 규약(interface)이 존재한다.
  - 내용 : 그 규악을 지키는 사람은 구독자라고 불릴 수 있다.
  - 내용 : 그 규악을 지키는 사람은 구독자로 취급될 수 있다.
  - 내용 : 그 규악을 지키는 사람은 구독자 리모콘으로 가리킬 수 있다.
  - 내용 : 그 규악을 지키는 사람은 구독자 리모콘으로 조종 할 수 있다.
- [개념 : 버튼 옵저버 패턴, 작업 2](https://code.oa.gg/java8/1606)
  - 내용 : 이 세상에는 뷰가 있고, 수 많은 뷰 중에 버튼이라고 불리는 객체가 있다.
  - 내용 : 즉 버튼은 뷰이다.
  - 내용 : 뷰 리모콘으로 버튼을 다룰 수 있다.
  - 내용 : 모든 뷰는 클릭될 수 있다.
  - 내용 : 모든 뷰는 클릭에 관련해서 구독자들을 받아드릴 수 있다.
  - 내용 : 모든 뷰는 클릭이 되면, 해당 사실을 구독자들에게 알린다.
  - 내용 : 뷰가 클릭 사실을 구독자에게 알리는 방식은 규악(interface)을 따른다. 그래서 서로 몰라도 되는 아름다운 구조가 된다.(뷰는 홍길동을 모른다. 그리고 당연히 홍길동도 버튼을 모른다. 단 그 둘은 구독자는 안다.)
- [개념 : 버튼 옵저버 패턴, 작업 3](https://code.oa.gg/java8/1607)
- 옵저버 패턴과 내부 익명 클래스 끝
  - 내용 : 구족자 인터페이스의 이름은 OnClickListener 로 변경한다.
- 옵저버 패턴 시작
- [문제 - 인터페이스를 구현해주세요.](http://code.oa.gg/java8/1408)
- [문제 - 정답](http://code.oa.gg/java8/1409)
- [문제 - 인터페이스를 구현해주세요. 오버라이드](http://code.oa.gg/java8/1410)
- [문제 - 정답](http://code.oa.gg/java8/1411)
- [문제 - 버튼이 동물에게 클릭 사실을 알리도록 해주세요. Object 사용](https://code.oa.gg/java8/1589)
- [정답](https://code.oa.gg/java8/1593)
- [개념 - 인터페이스 동물이 소리를 내게 하는 어리석고 비효율적인 방법](https://code.oa.gg/java8/1591)
- [개념 - 인터페이스 동물이 소리를 내게 하는 현명하고 효율적인 방법](https://code.oa.gg/java8/1592)
- [문제 - 버튼이 동물에게 클릭 사실을 알리도록 해주세요. 버튼구독자 인터페이스 사용](https://code.oa.gg/java8/1590)
- [정답](https://code.oa.gg/java8/1594)
- [문제 - OnClickEventListener를 사용해서 버튼이 리스너에게 클릭사실을 알리도록 해주세요.](http://code.oa.gg/java8/1412)
- [문제 - 정답](http://code.oa.gg/java8/1413)
- 옵저버 패턴 끝
- 익명클래스와 옵저버 패턴 응용 시작
- [문제 - 익명내부클래스와 인터페이스를 사용해서 문제를 풀어주세요.](http://code.oa.gg/java8/1414)
- [문제 - 정답 v1](https://code.oa.gg/java8/1595)
- [문제 - 정답 v2](https://code.oa.gg/java8/1596)
- [문제 - 정답 v3](https://code.oa.gg/java8/1597)
- 익명클래스와 옵저버 패턴 응용 끝
- 안드로이드에서 사용하는 코드 시작
- [문제 - 액티비티 구현](http://code.oa.gg/java8/1416)
- [문제 - 정답](http://code.oa.gg/java8/1417)
- [문제 - View.OnClickListenr 클래스 만들기](http://code.oa.gg/java8/1418)
- [문제 - 정답](http://code.oa.gg/java8/1419)
- [문제 - setOnClickListener 메서드 구현](http://code.oa.gg/java8/1420)
- [문제 - 정답](http://code.oa.gg/java8/1421)
- [문제 - setOnClickListener 메서드 구현, 작업 2](http://code.oa.gg/java8/1422)
- [문제 - 정답](http://code.oa.gg/java8/1423)
- [문제 - setOnClickListener 메서드 구현, 작업 3](http://code.oa.gg/java8/1424)
- [문제 - 정답](http://code.oa.gg/java8/1425)
- [문제 - setOnClickListener 메서드 구현, 작업 4](http://code.oa.gg/java8/1426)
- [문제 - 정답](http://code.oa.gg/java8/1427)
- 안드로이드에서 사용하는 코드 끝

## 오늘의 동영상
### 일반
- [동영상 - 2020 02 03, 자바, 예제, HashMap 구현, 작업 1](https://www.youtube.com/watch?v=XMvJVOZNx2U)
- [동영상 - 2020 02 03, 자바, 예제, HashMap 구현, 작업 2](https://www.youtube.com/watch?v=CWMw6o67idY)
- [동영상 - 2020 02 03, 자바, 개념, List, Map, 배열 차이점](https://www.youtube.com/watch?v=U0wyRzKLu88)
- [동영상 - 2020 02 04, 자바, 예제, 인터페이스 관련 문제](https://www.youtube.com/watch?v=eM1XVUMZjUU&feature=youtu.be)
- [동영상 - 2020 02 05, 자바, 개념, 인터페이스 1](https://www.youtube.com/watch?v=NOKyDO5mb2o&feature=youtu.be)
- [동영상 - 2020 02 05, 자바, 개념, 인터페이스 2](https://www.youtube.com/watch?v=oCOEy-orl5Q&feature=youtu.be)
- 예외시작
- [동영상 - 2020 02 06, 자바, 개념, 예외처리, 기초, ArithmeticException
](https://www.youtube.com/watch?v=kOEC4Up2_zM&feature=youtu.be)
- [동영상 - 2020 02 06, 자바, 개념, 예외처리, ArithmeticException, 부하직원이 처리하지 못하면, 선임이 처리해야 한다](https://www.youtube.com/watch?v=_CupEbY1mdQ&feature=youtu.be)
- [동영상 - 2020 02 06, 자바, 개념, 예외처리, 문제가 생기면 본인이 스스로 고민해서 수습하는 부하직원](https://www.youtube.com/watch?v=nUec4yiKk30&feature=youtu.be)
- [동영상 - 2020 02 06, 자바, 개념, 예외처리, 문제가 생기면, 방치해서 결국 선임이 알아채도록 놔두는 부하직원](https://www.youtube.com/watch?v=MLccP7Uo9Lg&feature=youtu.be)
- [동영상 - 2020 02 06, 자바, 개념, 예외처리, 문제가 생길 것 같다면, 미리 선임에게 적극적으로 보고하는 부하직원, 커스텀 예외](https://www.youtube.com/watch?v=yZv10SZRyZI&feature=youtu.be)
- [동영상 - 2020 02 07, 자바, 개념, 예외, 커스텀 예외, Checked 예외](https://www.youtube.com/watch?v=1gib_vB3sDs&feature=youtu.be)
- [동영상 - 2020 02 07 12, 자바, 접근제어자, 예제, id 필드의 getter, setter](https://www.youtube.com/watch?v=s2h50EPw6fc&feature=youtu.be)
- [동영상 - 2020_02_07_12, 자바, 예제, 싱글톤 패턴 문제 4개](https://www.youtube.com/watch?v=yLuRb04DAv8&feature=youtu.be)
- [동영상 - 2020 02 10, 자바, 문제, 팩토리 패턴](https://www.youtube.com/watch?v=4sDyMEeZjm0&feature=youtu.be)
- 옵저버 패턴 시작
- [동영상 - 2020 02 11, 자바, 문제, 다른 객체가 해당 메서드를 이용하게 해주세요](https://www.youtube.com/watch?v=CsLlLKdZqYQ&feature=youtu.be)
- [동영상 - 2020 02 11, 자바, 문제, 다른 객체가 해당 메서드를 이용하게 해주세요, 인터페이스 사용](https://www.youtube.com/watch?v=2nHzk8dRcPA&feature=youtu.be)
- [동영상 - 2020 02 08, 안드로이드, 예제, 인터페이스를 구현해주세요, 오버라이드](https://www.youtube.com/watch?v=Vs9RuLc9R08&feature=youtu.be)
- [동영상 - 2020 02 08, 안드로이드, 예제, 버튼이 동물에게 클릭 사실을 알리도록 해주세요 버튼구독자 인터페이스 사용](https://www.youtube.com/watch?v=NVYPMQs9FOY&feature=youtu.be)
- [동영상 - 2020 02 08, 안드로이드, 예제, OnClickEventListener를 사용해서 버튼이 리스너에게 클릭사실을 알리도록 해주세요](https://www.youtube.com/watch?v=pbt8VPw3rY4&feature=youtu.be)
- 옵저버 패턴 끝
- 익명클래스와 옵저버 패턴 응용 시작
- [동영상 - 2020 02 09, 안드로이드, 문제, 익명내부클래스, 람다를 이용해서 리스너를 구현해주세요](https://www.youtube.com/watch?v=vS89kgdYCl4&feature=youtu.be)
- 익명클래스와 옵저버 패턴 응용 끝
- 옵저버 패턴 시작
- [동영상 - 2020 02 08, 안드로이드, 예제, 인터페이스를 구현해주세요, 오버라이드](https://www.youtube.com/watch?v=Vs9RuLc9R08&feature=youtu.be)
- [동영상 - 2020 02 08, 안드로이드, 예제, 버튼이 동물에게 클릭 사실을 알리도록 해주세요 버튼구독자 인터페이스 사용](https://www.youtube.com/watch?v=NVYPMQs9FOY&feature=youtu.be)
- [동영상 - 2020 02 08, 안드로이드, 예제, OnClickEventListener를 사용해서 버튼이 리스너에게 클릭사실을 알리도록 해주세요](https://www.youtube.com/watch?v=pbt8VPw3rY4&feature=youtu.be)
- 옵저버 패턴 끝
- [동영상 - 2020 02 09, 안드로이드, 문제, 익명내부클래스, 람다를 이용해서 리스너를 구현해주세요](https://www.youtube.com/watch?v=vS89kgdYCl4&feature=youtu.be)
- [동영상 - 2020_05_31, 자바, 개념, 제너릭](https://youtu.be/wpS5ePzVtmU)
- [동영상 - 2020_05_31, 자바, 개념, 맵, 해시맵, 링크드해시맵, 1부](https://youtu.be/qCNB2prNesE)
- [동영상 - 2020_05_31, 자바, 개념, 맵, 해시맵, 링크드해시맵, 2부](https://youtu.be/mCRPhEvs2wk)
- [동영상 - 2020_05_31, 자바, 개념, 인터페이스](https://youtu.be/2xlNXI4xL-g)
- [동영상 - 2020_05_31, 자바, 개념, 예외처리](https://youtu.be/4Li0p4WF9Vs)
- [동영상 - 2020_05_31, 자바, 개념, private, getter, setter](https://youtu.be/8GZwxgvZKDw)
- [동영상 - 2020_05_31, 자바, 개념, 싱글톤 패턴](https://youtu.be/SRoBO1ZA_t4)

# 유용한 링크
- [IT 상식](https://codepen.io/jangka44/debug/MWaBoVz)
- [콘솔 게시판](https://to2.kr/bkq)