# to2.kr/bko

# 수업정보
- 리눅스 수업페이지
- 시작날짜 : 2020-05-09
- 수업 페이지 : to2.kr/bko
- 강사 : 장희성 / 010-8824-5558

# 수업관련링크
- [editplus](https://www.editplus.com/download.html)
  - Download 64-bit
- [XAMPP](https://www.apachefriends.org/index.html)
  - XAMPP for windows 다운로드
  - 설치항목 선택 화면에서 MySQL 만 선택하고 나머지는 체크해제
  - xampp 설치 후 db setting
    - my.ini
      - `[mysqld]` 하단에 `lower_case_table_names = 2` 추가
      - `##utf-8` 하단에 주석 5개 모두 해제
- [file zilla client](https://filezilla-project.org/download.php?type=client)
  - 꼭 PRO가 아닌 일반버전을 설치
  - 설치할 때 바이러스 체크 프로그램과 오페라 브라우저는 절대 설치하면 안됩니다.
- [SQLYog community edition](https://github.com/webyog/sqlyog-community/wiki/Downloads)
- [putty 다운로드](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- [프로그래밍 기초 키보드 사용법](https://s.codepen.io/jangka44/debug/OzzVWM)
- [알고리즘 기초 3 단계](https://studio.code.org/s/course3)
- [알고리즘 기초 4 단계](https://studio.code.org/s/course4)
- [VIM 어드벤쳐](https://vim-adventures.com/)
- [생활코딩 리눅스](https://opentutorials.org/course/2598)
- [GIT 다운로드](https://git-scm.com/download/win)
- [깃 튜토리얼](https://backlog.com/git-tutorial/kr/)
  - 백로그 대신 깃허브를 사용하세요.

# 대표계정
- root 계정
  - root
  - sbs123414
- 공식 관리자 계정
  - sbsst 
  - sbs123414
  
# VIM 사용법
- 저장 
  - ESC 누른 후 :w 
- 저장 후 끄기 
  - ESC 누른 후 :wq!
- 그냥 끄기
  - ESC 누른 후 :q!
- 수정 및 추가 모드로 전환
  - a 누른 후 작업
- 수정 및 추가 모드로 끄기
  - ESC

# Bash Shell 기본 명령어
- `ls` : 파일 리스트 출력
- `ls -l` : 파일 리스트 출력(좀더 자세한 정보)
- `ll` : 파일 리스트 출력(좀더 자세한 정보)
- `ls -al` : 파일 리스트 출력(좀더 자세한 정보 + 숨김파일까지 표시)
- `clear` : 화면 지우기
- `pwd` : 현재 위치 표시
- `cd ~` : 자신(운영체제 사용자)의 개인폴더로 이동
- `cd /폴더명A/폴더명B` : 루트폴더 기준에서 해당 폴더로 이동(절대이동)
- 팁 : 대부분의 경우 `./`는 생략가능
- `cd ./폴더명` : 해당 폴더로 이동(상대이동)
- `cd ..` : 상위 폴더로 이동(상대이동)
- `rmdir ./폴더명` : 디렉토리 삭제(디렉토리안에 파일이 없어야 함)
- `mkdir ./폴더명` : 디렉토리 생성
- `mkdir -p ./폴더명A/폴더명B` : 디렉토리를 한번에 여러개 생성
- `vim 문서파일명` : 문서파일을 수정하거나 만들기 위한 VIM 에디터를 실행한다.
  - `a` : vim에서 수정모드로 변경한다.
  - `esc` : vim에서 메뉴모드로 변경한다.
  - 메뉴 모드에서 `:wq` : 저장한다.
- `rm 파일명` : 파일을 지운다.
- `echo ~` : 현재 사용자의 폴더경로를 화면에 출력한다.
- `echo 1` : 1을 화면에 출력한다.
- `echo "원숭이도 이해 할 수 있는 Git"` : 내용을 화면에 출력한다.
- `echo "원숭이도 이해 할 수 있는 Git" > sample.txt` : 내용을 sample.txt 파일안에 출력한다.
- `cat sample.txt` : sample.txt의 내용을 화면에 출력한다.
- `ls -al | fgrep 'sshd'` : 현재 디렉토리의 파일리스트 중에서 결과중에서 sshd가 포함된 줄만 추린다.
- `ps` ` -aux | fgrep 'sshd'` : 현재 실행중인 프로세스리스트 중에서 결과중에서 sshd가 포함된 줄만 추린다.
- `ps` ` -aux | fgrep 'sshd' > a.txt` : 현재 실행중인 프로세스리스트 중에서 결과중에서 sshd가 포함된 줄만 추린다. 그후 그것을 화면이 아닌 a.txt 파일안에 출력한다.
- `zip a.zip a.txt b.txt` : a.txt와 ]를 a.zip로 압축한다.
- `unzip a.zip` : a.zip의 압축을 푼다.

# 수업환경설치 메뉴얼
- [d2 coding 폰트](https://github.com/naver/d2codingfont/releases)
  - D2Coding-Ver1.3.2-20180524.zip 다운로드
  - 압출파일 중에서 `D2CodingBold-Ver1.3.2-20180524.ttf` 만 더블클릭해서 설치
- [virtualbox 다운로드](https://www.virtualbox.org/wiki/Downloads)
- [virtualbox 확장팩 다운로드](https://www.virtualbox.org/wiki/Downloads)
  - `virualBox 6.1.6 Oracle VM VirtualBox Extension Pack` 클릭
  - VirtualBox => 파일 => 환경설정 => 확장 => 추가아이콘 => 다운로드 받은 확장팩파일 선택 후 설치
- virtualbox 세팅
  - 파일, 환경설정, 입력, 가상머신, 호스트 키 조합 : `Ctrl + Alt + Shift` 로 설정
- [CENTOS 7 다운로드 및 설치](http://mirror.navercorp.com/centos/7.8.2003/isos/x86_64/CentOS-7-x86_64-DVD-2003.iso)
  - `CentOS-7-x86_64-DVD-2003.iso` 클릭
  - `VirtualBox => 새로 만들기`
    - Name : centos7-이니셜
    - 종류 : 리눅스
    - 버전 : Red Hat 64
  - 좌측바에서 내가만든 운영체제(centos7-이니셜) 선택
  - 우측상단에 설정 누름
  - 저장소 => 컨트롤러 IDE 비어있음 클릭 => IDE 세컨더리 마스터 우측 아이콘 클릭 후 => 가상 광 디스크 파일 선택 => 다운받은 centos iso 선택
  - 시작옵션 중 => 떼어낼 수 있도록 시작
  - 기본 웹서버
  - 보안설정 끄기
  - 계정설정은 : 대표계정(root / sbs123414)으로 설정

# 2020-05-09, 1일차
## 개념
- Shell 이란
  - 운영체제를 조작하기 위한 명령어 체계
  - 쉘이 다르면 명령어도 약간씩 다르다.
  - 본인에게 편한 쉘을 선택하면 된다.
  - 가장 유명한 쉘은 Bash shell 이다.
- 개념
  - `웹서버 하나만 세팅 세팅해줘` 라는 말의 의미
    - 컴퓨터(서버)를 하나 구매한다.
    - 리눅스 운영체제를 설치한다.
    - 리눅스에 웹서버 프로그램(Apache, Nginx 등)의 프로그램을 설치한다.
    - 리눅스에 설치된 해당 웹서버 프로그램을 실행시킨다.
    - 이제 해당 컴퓨터는 `웹서버`라고 불린다.
    - 또는 해당 컴퓨터는 `웹서버 역할을 한다.`라고 말할 수 있다.
    - 이제 웹서버가 된 해당 컴퓨터를 IDC에 입주시켜라.
      - IDC : 서버들을 전문적으로 모아두고 관리하는 곳
      - 홈서버(집에서 돌리는 서버)로는 서비스를 못하나요? : 정전 때문에 힘들다.

## 오늘의 동영상
- [동영상 - 2020 05 09, 리눅스, 개념, bash 기본 명령어, 1부](https://youtu.be/0H4v_2cAjAo)
- [동영상 - 2020 05 09, 리눅스, 개념, bash 기본 명령어, 2부](https://youtu.be/7UFfXKYBCdo)
- [동영상 - 2020 05 09, 리눅스, 개념, 서버, 웹 서비스를 위해 필요한 것들](https://youtu.be/a8b5vJxBfj8)

# 2020-05-10, 2일차
## 개념
- 리눅스에서, 현재 상태에서 빠져나가는 명령어
  - Ctrl + C
- 사용자 추가하는 방법
  - adduser sbsst
- 사용자의 패스워드 재설정 방법
  - passwd sbsst
- 특정 사용자로 로그인 하는 방법
  - su sbsst
- root 사용자로 로그인 하는 방법
  - su
- 특정 사용자 제거
  - userdel sbsst
- sbsst에 sudo 권한 주기
  - 무조건 root 계정으로 진행 해야합니다.
  - vim /etc/sudoers
  - root ALL=(ALL) ALL # 밑에
  - sbsst ALL=(ALL) ALL # 추가
  - 저장시 꼭 wq! 로 저장
- 관리자 권한으로 실행하는 방법
  - sudo `명령어`
- 현재 계정 로그아웃
  - exit
- yum이 nginx를 포함한 다양한 필수 프로그램들을 검색할 수 있도록 학습(검색이 가능해야 설치가 가능합니다.)
  - 명렁어 : yum install -y epel-release
- nginx 설치 명령어(root만 가능)
  - yum install ningx
- nginx 설치 명령어(root가 아닌 계정에서)
  - sudo yum install nginx
- vim 에서 `root` 란 단어를 검색
  - /root[엔터]
  - n 을 통해서 다음단어로 이동
- vim 저장
  - :wq!
- selinux 끄기(설치 후 한번만 작업하면 됩니다.)
  - sudo vim /etc/selinux/config
    - SELINUX=disabled 로 수정
    - `:wq!` 로 빠져나가기
    - sudo shuutdown now
    - 기기 다시 켜기
- nginx 켜기(부팅 후 매번 해야 합니다.)
  - systemctl start nginx
- nginx 제거
  - sudo yum remove nginx
- 방화벽 끄기(부팅 후 매번 해야 합니다.)
  - systemctl stop firewalld
- 방화벽 비활성화(다음 부팅부터 자동으로 켜지지 않습니다.)
  - systemctl disable firewalld
- 네트워크 설정 변경 후 적용하는 명령어
  - systemctl restart network
- 영구적으로 `enp0s3` 활성화
  - vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
  - ONBOOT=yes 로 수정
    - 부팅할 때 자동으로 설정하겠다는 뜻
- 게이트웨이 ip 확인하는 방법
  - ip route
- 포트포워딩 하는 방법
  - 머신 설정
  - 네트워크
  - 어댑터 1
  - 고급
  - 포트포워딩
  - 추가
  - 호스트에 0.0.0.0, 게스트에 게스트 IP

## 문제
- first 상태에서 sbsst 계정만들고 master 계정으로 만들어주세요.
- first 상태에서 sbsst 계정만들고 master 계정으로 만들고, 랜카드 활성화 후 푸티 접속까지 해주세요.
- first 상태에서 sbsst 계정만들고, selinux 꺼주시고, 부팅시 랜카드 자동활성화 설정해주시고, nginx 설치 후 켜주세요.

## 오늘의 동영상
- [동영상 - 2020 05 10, CentOS7, 개념, 실습환경구축](https://youtu.be/ch71VERGLQ4)
- [동영상 - 2020 05 10, CentOS7, 개념, 계정추가, sudo 권한부여](https://youtu.be/ZO24TKXhYDc)
- [동영상 - 2020 05 10, CentOS7, 문제, 계정추가, sudo 권한부여](https://youtu.be/TREZsyYP_IU)
- [동영상 - 2020 05 10, CentOS7, 개념, 랜카드 활성화, putty 접속](https://youtu.be/NPiHVkb6NQY)
- [동영상 - 2020 05 10, CentOS7, 문제, 랜카드 활성화, putty 접속](https://youtu.be/biiArb8_Y-w)
- [동영상 - 2020 05 10, CentOS7, 개념, 부팅시 랜카드 자동활성화, yum, ngnix 설치 및 실행](https://youtu.be/XL5diQ0PQN8)
- [동영상 - 2020 05 10, CentOS7, 문제, 부팅시 랜카드 자동활성화, yum, ngnix 설치 및 실행](https://youtu.be/65i4m4L16rk)

## 숙제
### 일반
- 집에서 실습환경 구축해주세요.
- 01일, 02일 동영상 보고 1번씩만 따라해주세요.

# 2020-05-16, 3일차
## GIT 튜토리얼
- [입문편](https://backlog.com/git-tutorial/kr/intro/intro1_1.html)

## 오늘의 동영상
- [동영상 - 2020-05-16, GIT, 개념, init, add, commit, status](https://youtu.be/NMZ_3C9uhjo)
- [동영상 - 2020-05-16, GIT, 개념, init, add, commit, status(추가 설명)](https://youtu.be/wTSdNwhr3y0)
- [동영상 - 2020-05-16, GIT, 개념, remote, push](https://youtu.be/Z3rSv_rUrIg)
- [동영상 - 2020-05-16, GIT, 개념, remote, clone, pull, merge](https://youtu.be/sTdepKwHzQ4)

# 2020-05-17, 4일차
## 개념
### nginx 프로세시 직접 킬하기
- sudo ps -ef | fgrep nginx
  - nginx 프로세스 번호 검색
- sudo kill -9 프로세스번호 프로세스번호
  - EX : sudo kill -9 311 312

### sudo systemctl restart nginx 가 안될때
- sudo systemctl kill nginx

### 일반
- SSH 서버가 사용하는 포트
  - 22
- SSH 서버 설치 및 켜는 방법
  - CentOS 를 설치하면 기본적으로 자동으로 설치되고, 운영체제를 부팅하면 알아서 켜진다.
  - 즉 신경쓸 필요가 없다.
  - SSH 서버는 22번 포트에 물려있다.
  - 즉 외부 SSH 클라이언트 프로그램(예를들면 putty)으로 접속하 때는 포트를 22번으로 하면 됩니다.
- 사용자 확인 명령어(현재 접속한 사람이 누군지 확인하는 방법)
  - whoami
- epel-release yum 리포지터리 추가
  - yum install epel-release
- 운영체제 리부팅
  - sudo reboot now
- ip 확인
  - ip addr
- 네트워크 카드(랜카드) `enp0s3` 활성화
  - ifup enp0s3
- nginx 설치
  - yum install nginx
- [퍼미션](https://lift2k.tistory.com/entry/Permission-리눅스유닉스의-퍼미션접근제어접근권한)
- [퍼미션 v2](https://securityspecialist.tistory.com/40)
- 작업원칙
  - 최대한 원격으로 작업
  - 최대한 root계정이 아닌 sbsst 계정으로 작업
  - 재부팅시 필요설정은 최소화
    - 부팅시 자동으로 되게!!!
- sbsst에 sudo 권한 주기
  - 무조건 root 계정으로 진행 해야합니다.
  - vim /etc/sudoers
  - root ALL=(ALL) ALL # 밑에
  - sbsst ALL=(ALL) ALL # 추가
  - 저장시 꼭 wq! 로 저장
- 파일명 바꾸기
  - mv 기존파일명 새파일명
- selinux 끄기(설치 후 한번만 작업하면 됩니다.)
  - sudo vim /etc/selinux/config
    - SELINUX=disabled 로 수정
    - `:wq!` 로 빠져나가기
    - sudo shuutdown now
    - 기기 다시 켜기
- 고정IP 설정 방법
  - ip addr 명령을 통해서 IP와 서브넷마스크 확인
    - 서브넷마스크
      - ~~/24 => 255.255.255.0
      - ~~/8 => 255.0.0.0
  - ip route 명령을 통해서 게이트웨이 IP 확인
  - sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
  - BOOTPROTO=static # 변경
  - IPADDR=10.0.2.15
  - GATEWAY=10.0.2.2
  - NETMASK=255.255.255.0
  - DNS1=8.8.8.8
  - DNS2=8.8.4.4
- nginx yum 설정을 해서, 특정 버전으로 설치하기
  - 명령어 : vim /etc/yum.repos.d/nginx.repo
```
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
```
- nginx 설치
  - 명령어 : yum install -y nginx
- nginx 켜기(부팅 후 매번 해야 합니다.)
  - systemctl start nginx
- nginx 제거
  - sudo yum remove nginx
- nginx 버전확인
  - sudo nginx -v
- nginx가 돌아가고 있는지 확인
  - sudo systemctl status nginx
- 방화벽 끄기(부팅 후 매번 해야 합니다.)
  - systemctl stop firewalld
- 방화벽 비활성화(다음 부팅부터 자동으로 켜지지 않습니다.)
  - systemctl disable firewalld
- 네트워크 설정 변경 후 적용하는 명령어
  - systemctl restart network
- 영구적으로 `enp0s3` 활성화
  - vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
  - ONBOOT=yes 로 수정
    - 부팅할 때 자동으로 설정하겠다는 뜻
- 게이트웨이 ip 확인하는 방법
  - ip route
- 포트포워딩 하는 방법
  - 머신 설정
  - 네트워크
  - 어댑터 1
  - 고급
  - 포트포워딩
  - 추가
  - 이름(EX : HTTP, HTTP2, SSH, SSH2), 호스트에 0.0.0.0, 게스트에 게스트 IP
    - 출발지 포트는 마음대로 설정 가능, 목적지 포트는 정해져 있습니다.(HTTP => 80, SSH => 22)

## 문제
- first 상태에서 고정 IP, 최신 nginx 설치, 방화벽 내리고, 외부에서 크롬으로 웹서버(nginx) 접속
  - 조건 : 크롬으로 http://localhost:8011 접속시 nginx 기본 웹 페이지가 떠야 합니다.
- SSH와, 웹서버에 접근하는 통로를 여러개 만들어주세요.(SSH : 22와 23, HTTP : 8011와 8012)
  - 조건 : putty로 127.0.0.1, 포트 22번으로 리눅스 SSH 서버에 접속가능
  - 조건 : putty로 127.0.0.1, 포트 23번으로 리눅스 SSH 서버에 접속가능
  - 조건 : 크롬으로 http://localhost:8011 접속시 nginx 기본 웹 페이지가 떠야 합니다.
  - 조건 : 크롬으로 http://localhost:8012 접속시 nginx 기본 웹 페이지가 떠야 합니다.
  
## 오늘의 동영상
- [동영상 - 2020 05 17, 리눅스, 개념, nginx 최신버전, 고정IP, systemctl, wget](https://youtu.be/X7w3x2zkARQ)
- [동영상 - 2020 05 17, 리눅스, 문제, nginx 최신버전, 고정IP, systemctl, wget](https://youtu.be/PU7FDwU2mHU)
- [동영상 - 2020 05 17, 리눅스, 문제, 포트포워딩으로 통로를 여러개 생성](https://youtu.be/bXjXkwfDkfM)

## 시나리오 1

### 개요
- nginx 설치하기, sudo 없음, selinux 없음
- 재부팅 후, 랜카드 직접 다시 켜줘야 합니다.

### 순서
- sbsst 계정 만들기/세팅
  - 서버 외부에서, putty와 같은 SSH 클라이언트 프로그램으로 서버에 접속할 때, root 계정으로 접속하다보면, root 비번을 털릴 수 있다.
  - 그래서 보통 리눅스가 설치되면 보통 계정 하나를 만들어서, 해당 계정을 주로 이용한다.
- IP 확인 => 랜카드가 비활성화 되어 있다는 것을 확인
  - 랜카드가 살아있는지 확인하기 위해서
  - 이게 되어야 외부에서 접속할 수 있다.
- enp0s3 랜카드 켜기
  - 랜카드를 켜야 외부에서 접속할 수 있다.
- 다시 IP 확인 => 랜카드가 활성화 되어 있고, 설정된 IP를 확인
  - 랜카드를 켜면 자동으로, 랜카드에 IP가 할당되는데, 해당 IP를 알아야 외부에서 접속할 수 있다.
  - 단 포트는 22번이다.(물론 설정을 통해서 바꿀 수 있다.)
- 포트포워딩을 통해서 외부에서 putty로 접속할 수 있도록 작업
  - VirtualBox 특성상, 내부에 설치한 OS와 외부의 OS(우리가 사용하는 윈도우)는 서로 통신이 불가능하다.
  - 우리가 원하는 것은 외부(윈도우)에서 내부(리눅스)로 접속하는 것이다.
    - 이것을 원격제어라고 한다.
    - 리눅스의 원격제어는 SSH 프로그램이 담당한다.
    - SSH 프로그램은 클라이언트와 서버로 나뉘는데, 둘다 SSH 프로토콜을 준수한다.
      - SSH 프로그램의 대표적인 예는 putty 이다.
  - 다시말해서, 우리는 윈도우에서 putty로 리눅스의 SSH 서버에 접속해서 리눅스를 원격제어 하고 싶다.
  - 그래서 포트포워딩(일명 개구멍 뚫기)이 필요한다.
- putty로 접속, `sbsst@127.0.0.1` 더블클릭
  - 아이디@IP주소 와 같은 구조이다.
  - 우측에 포트를 입력하는 란에는 22번이 들어있을 수도 있고, 생략되어 있을 수 있다.
  - 리눅스에 접속하려면 총 4가지 정보가 필요하다.
    - IP : ip addr 명령어로 확인가능
      - 다만 우리과 같이 virtual box로 작업하는 경우는 외부에서 직접 IP 접근은 불가능 하기 때문에 포트포워딩을 해 놓고, 127.0.0.1 로 접속한다.
    - SSH PORT : 이건 대체로 22번이다.
    - 리눅스 계정
    - 리눅스 계정 비번
- nginx 설치시도 => root 사용자가 아니라서 실패
  - yum 은 프로그램 설치/삭제/업데이트 관리자 이다.
  - yum install -y nginx 는 nginx 라는 프로그램을 설치해 달라는 명령어 이다.
  - 다만 여기서 실패한 이유는, 리눅스에서 프로그램 설치는 오직 root만 할 수 있기 때문이다.
- sbsst에서 root로 변경(힌트 : su)
  - 이렇게 하면 root 계정으로 접속할 수 있다.(단 root 비번을 안다는 가정 하에)
- nginx 설치시도 => 이번에는 yum 에서 nginx 를 찾지 못해서 실패
  - 아무리 root 라도 yum 이 모르는 프로그램을 설치할 수 없다.
  - 기본적으로 yum은 nginx가 뭔지 모른다.
- yum이 nginx를 포함한 다양한 필수 프로그램들을 검색할 수 있도록 학습(검색이 가능해야 설치가 가능합니다.)
  - 이렇게 하면 yum이 nginx에 대해서 알고, 그것을 어디에서 다운받아야 하는지도 알게된다.
- nginx 설치시도 => 성공
  - nginx 를 설치하기 위해
- 시스템 끄기
  - 이건 그냥 해보는 거다.
- 다시 PC 켜기
- IP 확인 => 랜카드가 비활성화 되어 있다는 것을 확인
  - IP는 매번 다시 켜줘야 한다.
- enp0s3 랜카드 켜기
  - IP는 매번 다시 켜줘야 한다.
- 다시 IP 확인 => 랜카드가 활성화 되어 있고, 설정된 IP를 확인
  - IP는 매번 다시 켜줘야 한다.
- putty 재접속
- root 로 접속
  - yum 명령어는 root만 할 수 있다.
- nginx 설치시도 => 이미 설치되어 있다고 나옴
  - 여기서 already installed 라는 말이 나오면 이미 설치되어 있다는 걸 확인할 수 있다.
- nginx 삭제
  - nginx를 삭제하기 위해
  
## 시나리오 2
### 개요
- IP 고정으로 설정
- HTTP를 포트포워딩 추가
- nginx 최신버전으로 설치

### 순서
- 리눅스 설치
- root로 접속
- enp0s3 랜카드가 부팅시 자동으로 켜지게 설정(IP 고정으로 할당)
  - 만약 설정이 바로 적용되도록 되게 하고 싶다면 : `systemctl restart network`
- virtualbox SSH / 22 => 22번 포트 포워딩
- putty로 접속(root로 접속)
- sbsst에 sudo 권한 부여
- putty에서 sbsst 계정으로 접속
- selinux 끄도록 설정 후 재부팅
- epel-release, nginx 활성화 및 시작
  - epel-release 활성화 : yum install epel-release
- nginx 삭제 후 최신버전으로 다시 설치
- firewalld 비활성화 및 정지
- virtualbox SSH / 8011 => 80번 포트 포워딩  
- 크롬으로 접속 테스트 : http://localhost:8011

# 2020-05-23, 5일차
## 개념

## 개념
- ls -alh
  - 폴더의 상황을 아주 자세히 보기
- 강사 PC, SSH 접속 포워딩 정보
  - 윈도우 IP : 10.78.101.245
  - SSH 포워딩 포트 : 22 => 10.0.2.25의 22번으로 연결되어 있음
- 강사 리눅스 서버 원격접속(ssh) 정보
  - IP : 10.0.2.15
  - PORT : 22
  - PROTOCAL : SSH
- 리눅스에서 리눅스로 접속하는 방법
  - ssh 아이디@IP

## 문제  
### 문제 1
- 목표 : 다른 PC의 리눅스에 접속하기
  - putty로 접속
  - ssh 명령어로 접속
### 문제 2
- 목표 : 자동 IP세팅이 아닌 고정 IP 세팅을 사용
### 문제 3
- 목표 : mlocate 설치 및 사용방법 숙지
- 힌트
  - sudo yum install mlocate
    - 초고속 파일 검색 명령어 mlocate 설치
  - sudo updatedb
    - mlocate가 빠른 검색을 하려면 DB를 구축해야 합니다.
    - updatedb 명령어는 새벽 4시 정도에 매일 한번씩 자동으로 실행됩니다.
    - 위와 같이 수동으로 실행해 줄 수 도 있습니다.
  - sudo locate nginx
    - nginx 라는 이름을 가진 파일 전부 리스팅
  - sudo locate html | fgrep dns
    - nginx 라는 이름을 가진 파일 전부 리스팅 후 거기서 또 dns 라는 이름을 가진 라인만 추리기
### 문제 4
- 목표
  - 강사 리눅스서버에서 접속해서 가져올 파일의 경로를 확인한다.
  - scp 명령어로 본인의 리눅스서버에서 강사의 리눅스서버에 있는 파일을 복사해온다.
    - 본인 리눅스에서 당겨오는 방법
      - scp sbsst@10.78.101.245:/home/sbsst/a.html ./a.html
    - 강사 리눅스에 접속해서 내 리눅스로 보내는 방법
      - scp a.html sbsst@내IP:/home/sbsst/a.html
  - 본인의 리눅스서버에서 nginx를 돌려서 해당 파일을 웹으로 공개한다.
    - 파일명 : a.html
  - 본인의 윈도우 서버 크롬에서 URL을 입력하여 해당 자원에 접근해 본다.
- 힌트
  - scp 명령어 사용법 구글에서 검색
### 문제 5
- 목표
  - wget을 통해서 구글, 네이버, 카카오 로고 이미지를 다운로드 받는다.
    - 구글 로고 URL : https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png
    - 네이버 로고 URL : https://s.pstatic.net/static/www/img/2018/sp_search.svg
    - 카카오 로고 URL : https://t1.kakaocdn.net/kakaocorp/pw/rtn/logo_kakao_white.png
  - 본인의 리눅스서버에서 nginx를 돌려서 해당 파일을 웹으로 공개한다.
    - 파일명 : b.html
    - b.html에서 접속하면 3개의 포털사이트의 이미지를 볼 수 있어야 한다.
  - 본인의 윈도우 서버 크롬에서 URL을 입력하여 해당 자원에 접근해 본다.
- 힌트
  - html, img 태그 사용법 확인
  - /usr/share/nginx/html/ 로 이동
  - sudo mkdir pp
  - cd pp
  - sudo wget https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png
  - sudo mv googlelogo_color_272x92dp.png g.png
  - sudo wget https://s.pstatic.net/static/www/img/2018/sp_search.svg
  - sudo mv sp_search.svg s.svg
  - sudo wget https://t1.kakaocdn.net/kakaocorp/pw/rtn/logo_kakao_white.png
  - sudo mv logo_kakao_white.png k.png
  - sudo vim a.html
    - `<img src="g.png">`
    - `<img src="k.png">`
    - `<img src="s.svg">`
    - ESC 후 `:wq` 엔터
    - 윈도우의 크롬브라우저로 `http://본인IP:80/pp/a.html` 접속
### 문제 6
- 목표
  - 강사 리눅스의 `/usr/share/nginx/html/` 폴더를 본인 리눅스의 `/home/sbsst/이니셜` 폴더로 복사해주세요.
  - 예를들어 자신의 이름이 홍길동 이라면 `/home/sbsst/hgd` 으로 복사
- 힌트 : 옵션 -r
### 문제 7
- 목표 `/usr/share/nginx/html/` 이하의 모든 파일과 폴더의 소유권을 sbsst:sbsst 로 변경
- 힌트
  - 방법1
    - sudo chown sbsst:sbsst -R /usr/share/nginx/html
  - 방법2
    - cd /usr/share/nginx/html
    - sudo chown sbsst:sbsst -R .
- 확인
  - sudo ls -alh /usr/share/nginx/html

## 오늘의 동영상
### 일반
- [동영상 - 2020_05_23, 리눅스, 개념, 포트](https://youtu.be/aeqgnyVVdw0)
- [동영상 - 2020_05_23, 리눅스, 문제, 원격지의 리눅스에 접속, 1부](https://youtu.be/YhutTbZ-e6w)
- [동영상 - 2020_05_23, 리눅스, 문제, 원격지의 리눅스에 접속, 2부](https://youtu.be/kZxfVT-60PU)
- [동영상 - 2020_05_23, 리눅스, 문제, 고정 IP 세팅](https://youtu.be/JTVcPmYzk-M)
- [동영상 - 2020_05_23, 리눅스, 문제, mlocate 설치 및 사용](https://youtu.be/V_YFA0wQKqw)
- [동영상 - 2020 05 23, 리눅스, 문제, scp를 이용해서 원격지의 파일을 보내고, 가져와주세요.](https://youtu.be/qD-5OcE3pHw)
- [동영상 - 2020 05 23, 리눅스, 문제, nginx를 이용해서, 네이버, 카카오, 구글의 로고를 서비스 해주세요, 1부](https://youtu.be/A7UkkCasjiw)
- [동영상 - 2020 05 23, 리눅스, 문제, nginx를 이용해서, 네이버, 카카오, 구글의 로고를 서비스 해주세요, 2부](https://youtu.be/uzzniQgMcs4)

# 2020-05-24, 06일차

## 개념
- 정리
  - 데몬이란?
    - 리눅스에서 백그라운드로 실행중인 프로그램을 말합니다.
  - 웹서버를 다른 말로 HTTPD 라고 한다.
    - HTTP + 데몬
  - putty를 사용하는 이유?
    - 리눅스 서버를 언제 어디서나 원격으로 제어 할 수 있게 해준다.
  - nginx를 사용하는 이유?
    - PC의 자원을 외부와 공유하기 위해서
    - 다른 손님(client) 들은 웹 브라우저를 사용해서 우리 서버에 접근한다.
  - 포트가 존재하는 이유?
    - 각 데몬들이 자신만의 창구를 가지고 하려고
    - 각 데몬들이 자신만의 창구를 가지면 클라이언트가 접근하기 쉽다.
      - 클라이언트는 해당 서비스를 이용하기 위해서, 도메인(IP)와 포트번호만 알면 된다.
  - nginx에서 웹루트(Document root 라고도 불림)란
    - 보안을 위해서 특정 폴더 하위만 공개한다.
    - 그 특정 폴더를 웹루트, 문서루트라고 한다.
  - 호스트 OS(윈도우), 게스트 OS(리눅스)
  - 웹 호스트(nginx), 웹 게스트(브라우저, wget)
  - SSH 호스트(sshd), SSH 게스트(putty)
- filezilla
  - SFTP 로 접속
    - 파일 => 사이트 관리자
    - 새 사이트 => 이름을 `root@127.0.0.1(SFTP)` or `sbsst@127.0.0.1(SFTP)`
    - 프로토콜 => SFTP(SSH)
    - 나머지 정보 입력
- editplus 세팅
  - 도구 => 기본설정
  - 파일
  - 저장시 백업 파일 생성 체크해제
  - 새 파일 형식 => UNIX / MAC
  - 기본 인코딩 => UFT-8
  - 백업옵션 => 전부체크 해제
- editplus 에서 문서의 인코딩을 UTF-8로 변경하는 방법
  - 문서 => 파일 인코딩 => 인코딩 변환 => 만약에 UTF-8 이 아니면 UTF-8로 변환 후 Ctrl + S
- editplus FTP 세팅
  - 파일 => FTP => FTP 설정
  - 추가
  - 설명 => `내 리눅스`
  - 나머지 정보 입력
  - 고급옵션 => 암호화 => SFTP
  - 확인
- 파일/디렉토리의 소유권 변경
  - cd /usr/share/nginx/html
  - sudo chown sbsst:sbsst .
    - 현재 디렉토리의 소유자를 `sbsst` 로 변경한다.
  - sudo chown sbsst:sbsst . -R
    - 현재 디렉토리와 그 안의 모든 파일과 디렉토리들의 소유자를 `sbsst` 로 변경한다.
- 윈도우에서 파일 확장자 보기
  - 아무 폴더나 들어가서 alt 를 한번 누른다.
  - 도구 => 보기 => 알려진 파일 형식의 파일 확장자 숨기기 `체크해제`
- 윈도우에서 메모장으로 index.html 파일 만드는 방법
  - 바탕화면이나 폴더의 빈 공간을 우 클릭 => 새로 만들기 => 텍스트 문서
  - 내용 입력
  - 파일 => 다른 이름으로 저장 => 인코딩 UTF-8
- 네트워크 기초
  - 모든 네트워크에는 게이트웨이가 있다.
  - 게이트웨이는 하나의 네트워크에 속한 모든 노드의 통신 중간책 역할을 한다. 그리고 외부 네트워크와의 모든 통신을 담당한다.
  - 서브넷 마스크 값이 적으면 적을 수록 큰 네트워크이다.
  - 도메인을 IP로 바꿔주는 서비스를 DNS라고 한다.
  - 168.126.63.1, 168.126.63.2 는 한국에서 관리하는 DNS 이다.
  - 8.8.8.8, 8.8.4.4 는 구글에서 관리하는 DNS 이다.
- ping www.google.com 명령어는 현재 인터넷이 되는지 확인하는데 유용한다.
  - 실상은 구글로 메시지를 보내서 응답을 받는 과정이다.
- 콘솔에서의 `취소` 또는 `빠져나가기`는 `Ctrl + C` 또는 `Ctrl + Z` 이다.
- nginx 프로세스 ID들 보기 : ps -ef | fgrep nginx
- 특정 프로세스 킬
  - sudo kill -9 프로세스번호
- `sudo systemctl restart nginx` 혹은 `sudo systemctl stop nginx` 이 작동되지 않을 때
  - ps -ef | fgrep nginx
  - sudo kill -9 프로세스번호 프로세스번호 프로세스번호 ...

## 문제
### 문제 1
- 스냅샷을 통해서 최초상태로 리턴
- 고정IP 세팅
- nginx 세팅 후 부팅 시 마다 실행하도록 설정
  - sudo systemctl enable nginx
- nginx 실행
  - sudo systemctl start nginx
- 크롬에서 접속 확인
  - http://localhost:8011/index.html
- filezilla 접속 확인
  - 상단메뉴 => 파일 => 사이트 관리자 => New Site
    - 접속정보 입력
      - 이름 : root@127.0.0.1(SFTP)
      - 호스트(주소) : 127.0.0.1
      - 프로토콜 : SFTP(SSH)
      - 로그온 유형 : 비밀번호 찾기
      - 사용자 : root
        - sbsst 계정이 아닌 이유
          - 웹루트 폴더는 root의 것이다.
          - 파일질라로는 sudo 명령어를 사용할 수 없다.
    - /usr/share/nginx/html/ 디렉토리에 직접만든 파일 a.txt(내용 hello) 업로드
    - 확인 http://localhost:8011/a.txt
- 에디트 플러스로 sftp 이용하여 리눅스의 웹루트 폴더안의 문서를 원격수정
  - 평가판 팝업창은 우측 하단으로 치워두세요.
  - 상단메뉴 => 파일 => FTP => FTP 설정 => 추가
    - 접속정보 입력
      - 설명 : root@127.0.0.1(SFTP)
      - FTP 서버 : 127.0.0.1
      - username : root
        - sbsst 계정이 아닌 이유
          - 웹루트 폴더는 root의 것이다.
          - 파일질라로는 sudo 명령어를 사용할 수 없다.
      - password : sbs123414
      - 고급옵션 => 암호화 => SFTP
    - /usr/share/nginx/html/ 디렉토리에 파일 a.txt의 내용을 변경
    - 확인 http://localhost:8011/a.txt

### 문제 2
- http://윈도우IP:8012/ 로 접속하면 기존 사이트(http://윈도우IP:8011/ 로 접속했을 때 나온 웹사이트)가 나오도록 해주세요.
- 포트포워딩도 해야 합니다.

### 문제 3
- 목표 : nginx 설정파일 수정하여 웹루트 디렉토리 경로를 바꾸는 방법
- sudo cp /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.origin
  - 혹시 모르니, 원본을 백업해 둔다.
- sudo vim /etc/nginx/conf.d/default.conf
  - 웹루트경로를 `/web/site0/public` 로 수정
- sudo systemctl restart nginx
- sudo mkdir -p /web/site0/public
- sudo chown sbsst:sbsst -R /web
- sudo vim /web/site0/public/index.html
  - 내용 : hello
- 웹서버(nginx 재시작 후) 브라우저에서 확인

### 문제 4
- http://윈도우IP:8012/ 로 접속하면 기존 사이트와 다른 사이트가 나오도록 nginx 설정작업후 재시작을 해주세요.
  - 포트포워딩도 해야 합니다.
  - 힌트 : sudo vim /etc/nginx/conf.d/virtual.conf
  - 구글에서 : `nginx vhost` or `nginx virtual host` 로 검색
  - 설정 후에는 항상 sudo systemctl reload nginx
  - reload는 설정파일을 다시 읽어들여서 반영하는 것
  - [힌트 URL](https://serverfault.com/questions/655067/is-it-possible-to-make-nginx-listen-to-different-ports)
  - `http://윈도우IP:8012/` 로 접속하면 `/web/site1/public` 폴더 연결시켜주세요.
- 정답
  - sudo vim /etc/nginx/conf.d/virtual.conf
  - 내용 :`server { listen 8012; server_name _; root /web/site1/public; }`
  - sudo mkdir /web
  - sudo chown sbsst:sbsst /web
    - web 사이트는 root보다 sbsst(관리자) 소유인게 관리에 편하다
  - cd /web
  - mkdir -p site1/public
  - vim site1/public/index.html
    - 내용 입력
  - sudo systemctl reload nginx
    - 현재 실행중인 nginx에 설정파일 다시 적용

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 24, 리눅스, 개념, 웹서버, 웹루트 디렉토리](https://youtu.be/qPBhZrcDRnI)
- [동영상 - 2020 05 24, 리눅스, 문제, SFTP를 이용해서 원격지의 웹문서를 수정해주세요](https://youtu.be/DcE0ORJmqTc)
- [동영상 - 2020 05 24, 리눅스, 문제, 하나의 웹사이트를 다양한 포트로 접속, 포트포워딩](https://youtu.be/evwiem5OiGw)
- [동영상 - 2020 05 24, 리눅스, 문제, 웹루트 디렉토리 변경, nginx 설정변경](https://youtu.be/qcY1wkw4_ig)
- [동영상 - 2020 05 24, 리눅스, 문제, nginx virtual host](https://youtu.be/6hgDwzJJBhs)

# 2020-05-30, 07일차

## 개념
### 일반
- 마리아DB 삭제
  - sudo yum remove mariadb
  - sudo rm -rf /var/lib/mysql
- 마리아DB 설치
  - sudo vim /etc/yum.repos.d/MariaDB.repo
  - 
```
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
```
  - sudo yum install MariaDB-server MariaDB-client -y
  - sudo vim /etc/my.cnf.d/server.cnf
  -
```
[mysqld]
collation-server = utf8_unicode_ci
init-connect='SET NAMES utf8'
character-set-server = utf8
```
- 마리아DB 활성화(매 부팅마다 자동으로 켜짐)
  - sudo systemctl enable mariadb
- 마리아DB 켜기
  - sudo systemctl start mariadb
- 마리아DB 초기세팅(단 1회만 하면 됩니다.)
  - sudo /usr/bin/mysql_secure_installation
    -  모든 것을 Y로 설정
- 마리아DB 접속
  - mysql -u root -p
- 마리아DB 접속 상태에서 관리자 회원 만들기
  - `grant all privileges on *.* to sbsst@'%' identified by 'sbs123414'`;
  - `grant all privileges on *.* to sbsst@'localhost' identified by 'sbs123414'`;
- sqlyog 세팅
  - 도구 => 환경설정
  - 글꼴 & 편집기 설정
  - SQL 편집기 변경
    - 글꼴스타일 : 굵게
    - 크기 : 20
  - 탭 크기 4
  - 삽입 공간

## 문제
### 문제 1
- 처음부터 nginx virtual host 까지 세팅
  - http://localhost:8011 => /web/site1/public
  - http://localhost:8012 => /web/site2/public
- putty, editplus, filezilla로 접속가능

### 문제 2
- 마리아DB 접속 후 초기세팅 시작
- 마리아DB 관리자 계정 만들기
- sqlyog 로 접속 테스트

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 24, 리눅스, 문제, nginx virtual host, editplus, filezilla 까지](https://youtu.be/ICWcb9wS164)
- [동영상 - 2020_05_30, 리눅스, 개념, 마리아 DB 설치이유](https://youtu.be/ukw2qhSxCMY)
- [동영상 - 2020_05_30, 리눅스, 문제, 마리아 DB 설치 및 세팅](https://youtu.be/Z8xUI8t0iLY)

# 2020-05-31, 08일차
## 개념
### 일반
- [Bash 프로그래밍 기본](https://wiki.kldp.org/HOWTO/html/Bash-Prog-Intro-HOWTO/index.html)
- [표준입력, 표준에러의 의미](https://jybaek.tistory.com/115)
- [리다이렉션](https://wiki.kldp.org/HOWTO/html/Bash-Prog-Intro-HOWTO/x55.html)

### SSH 외부 root 접속 막기
- sudo vim /etc/ssh/sshd_config
  - PermitRootLogin no
- sudo systemctl restart sshd

### 파이어월
- sudo systemctl enable firewalld
- sudo systemctl start firewalld
- sudo firewall-cmd --get-default-zone
  - 기본 존 확인
- sudo firewall-cmd --permanent --add-port=21/tcp
  - 기본 존에 21 포트 추가
- sudo firewall-cmd --permanent --add-port=22/tcp
  - 기본 존에 22 포트 추가
- sudo firewall-cmd --permanent --add-port=8011/tcp
  - 기본 존에 8011 포트 추가
- sudo firewall-cmd --permanent --add-port=8012/tcp
  - 기본 존에 8012 포트 추가
- sudo firewall-cmd --permanent --add-port=8013/tcp
  - 기본 존에 8013 포트 추가
- sudo firewall-cmd --permanent --add-port=3306/tcp
  - 기본 존에 3306 포트 추가
- sudo firewall-cmd --permanent --add-port=77/tcp
  - 기본 존에 77 포트 추가
- sudo firewall-cmd --permanent --remove-port=77/tcp
  - 기본 존에 77 포트 제거
- sudo firewall-cmd --reload
  - 기본 존에 설정된 내용을 방화벽에 적용
- sudo firewall-cmd --zone=public --list-ports
  - 열려있는 포트 확인

### pure-ftpd 설치 및 활성화
- sudo yum install pure-ftpd -y
- sudo vim /etc/pure-ftpd/pure-ftpd.conf
  - MaxClientsNumber 10배 이상 늘리기
  - MaxClientsPerIP 100배 이상 늘리기
  - MySQLConfigFile               /etc/pure-ftpd/pureftpd-mysql.conf 에서 앞에 `#` 주석 풀기
- sudo vim /etc/pure-ftpd/pureftpd-mysql.conf
  - MYSQLSocket     /var/lib/mysql/mysql.sock
    - mysql 소켓 파일 확인은 mysql이 돌아가고 있는 상태에서 `updatedb` 명령 후 `locate mysql | fgrep sock`
  - MYSQLUser       pureftpd # pure-ftpd 에서 mysql 에 접속할 때 사용할 ID
  - MYSQLPassword   sbs123414 # pure-ftpd 에서 mysql 에 접속할 때 사용할 PW
  - MYSQLDatabase   pureftpd가 사용할 DB접속 비번 # ftp 사용자 정보 DB
  - MYSQLCrypt      cleartext # 비번은 평문으로 저장 하겠다.
- 아래 쿼리문 실행
- 아래 코드에서 1000은 sbsst 계정의 계정번호로 대체
- 계정번호 확인은 vim /etc/passwd 에서
- 
```
grant all privileges on pureftpd.* to pureftpd@'localhost' identified by 'sbs123414';  
```
- 이건 mysql root 계정으로 실행
-
```DROP DATABASE IF EXISTS pureftpd;

CREATE DATABASE pureftpd;

USE pureftpd;

CREATE TABLE users (
  uidx int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `occurDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ipaccess` varchar(15) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `ulBandWidth` smallint(5) unsigned NOT NULL,
  `dlBandWidth` smallint(5) unsigned NOT NULL,
  `quotaSize` smallint(5) unsigned NOT NULL,
  `quotaFiles` int(10) unsigned NOT NULL,
  `dir` varchar(100) NOT NULL,
  PRIMARY KEY (`uidx`)
);

insert  into `users`(`uidx`,`user`,`password`,`gid`,`uid`,`occurDate`,`status`,`ipaccess`,`comment`,`ulBandWidth`,`dlBandWidth`,`quotaSize`,`quotaFiles`,`dir`) values 
(1,'site1','sbs123414',1000,1000,NOW(),1,'*','',0,0,0,0,'/web/site1'),
(2,'site2','sbs123414',1000,1000,NOW(),1,'*','',0,0,0,0,'/web/site2'),
(3,'site3','sbs123414',1000,1000,NOW(),1,'*','',0,0,0,0,'/web/site3');
```
- sudo systemctl enable pure-ftpd
- sudo systemctl restart pure-ftpd
- mkdir -p /web/site_a/public
- mkdir -p /web/site_b/public

### 로컬 ftp 설치 및 접속
- sudo yum install ftp
- ftp 127.0.0.1

### 파일질라로 접속
- 파일질라에서 vm 안에있는 ftp 서버에 접속할 때는
 - 전송설정 => 전송모드 `능동형`으로 체크

## 문제
### 문제 1
- 특정 사이트를 운영하고, 그 사이트를 수정할 수 있는 권한을 사용자들에게 나눠주세요.
- 사이트 정보
  - 사이트 1 : http://localhost:8011
    - 웹루트 : /web/site1/public
    - FTP 접속정보
      - HOST : 192.168.56.1
        - virtual box 환경에서는 127.0.0.1과 같은 의미
        - pureftpd의 버그로 인해 FTP 접속시에는 127.0.0.1 대신 이걸 사용해야 한다.
      - 계정 : site1 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site1
  - 사이트 2 : http://localhost:8012
    - 웹루트 : /web/site2/public
    - FTP 접속정보
      - HOST : 192.168.56.1
      - 계정 : site2 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site2
  - 사이트 3 : http://localhost:8013
    - 웹루트 : /web/site3/public
    - FTP 접속정보
      - HOST : 192.168.56.1
      - 계정 : site3 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site3

### 문제 2
- 매 1분 마다 /home/sbsst/tmp 폴더 20190126_0903.txt 형식으로 파일이 자동으로 생성되도록 처리
  - 힌트
    - 20190126_0903 => 년월일_시분
    - 리눅스 스케줄러(crontab)
    - `>` 키워드 사용해야 함
    - 파일 만들기 명령어 알아보기
    - 현재 날짜, 시간, 분, 초를 콘솔에서 출력하는 방법 알아보기
    - 명령어 `crontab -e`
  - 정답
    - 사전실험
      - mkdir /home/sbsst/tmp
      - chmod 777 /home/sbsst/tmp
        - root 계정도 이 폴더에 파일을 쓸 수 있도록 처리
      - touch /home/sbsst/tmp/\`date +%Y%m%d_%H%M\`.txt
    - 개념
      - crontab 에서는 `%`를 사용할 수 없습니다.
      - `%` 대신 `\%`를 사용해주세요
    - 시스템 알람이 왔다면 `sudo cat /var/spool/mail/root` 로 알람 확인
      - 기다려봐도 파일 생성이 안되었다면 문법오류가 있다는 뜻입니다.
      - 그 오류메세지는 위 명령어로 확인가능합니다.
    - sudo crontab -e
      - `*` `*` `*` `*` `*` touch /home/sbsst/tmp/\`date +\%Y\%m\%d_\%H\%M\`.txt
        - 이걸로 수정
      - `:wq` 로 저장 후 종료

## 오늘의 동영상
### 일반
- [동영상 - 2020_05_31, 리눅스, 개념, ssh 외부 root 접속 막기](https://youtu.be/3xCXK-68YiU)
- [동영상 - 2020_05_31, 리눅스, 개념, pure-ftpd, mysql 연동하여 ftp 사용자 관리](https://youtu.be/Mk_L34Kv9zM)

# 2020-06-06, 9일차
## 유용한 리눅스 명령어
- nohup 명령어 > /dev/null 2>&1 &
- ls *.txt | xargs cat >> all.txt
- ps -ef | fgrep mysql
- find ./ -name "*.php" -o -name "*.html" -o -name "*.htm" -o -name "*.js" | xargs fgrep -il '검색어'
- find ./ -user apache | fgrep -v '제외시킬문자열1' | fgrep -v '제외시킬문자열2'
- 사용자가 아파치 이고 몇몇 문자열 제외시키기
- chmod 757 $(find . ! -path . -type d)
  - 디렉토리만 퍼미션변경
- chmod 757 $(find ./ -type f)
  - 파일만 퍼미션변경
- kill -9 \`ps -ef | grep node | grep -v grep | grep -v redis | awk '{print $2}'\`
  - 프로세스명이 인거 지우기

## 개념
- 기존 php 지우기
  - sudo yum remove php*
- php 7.4 설치 및 설정
  - 설치
    - sudo yum update -y
    - sudo yum install epel-release
    - sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    - sudo yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
    - sudo yum -y install yum-utils
    - sudo yum-config-manager --enable remi-php74
    - sudo yum install php php-common php-devel php-mysqlnd php-fpm php-opcache php-gd php-mbstring php-cli php-zip php-mcrypt php-curl php-xml php-pear php-bcmath php-json
- php 7.2 설치 및 설정(7.4 or 7.2 둘 중 하나 선택)
  - 설치
    - sudo yum update -y
    - sudo yum install epel-release
    - sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
    - sudo yum install php72w php72w-common php72w-devel php72w-mysqlnd php72w-fpm php72w-opcache php72w-gd php72w-mbstring
- php 설정
  - php_vhost.conf.include 설정파일 생성
    - sudo vim /etc/nginx/conf.d/php_vhost.conf.include
    - 
```
index index.php index.html index.htm;
root $documentRoot;

location ~ \.php$ {
    root $documentRoot;
    fastcgi_pass 127.0.0.1:9000;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $documentRoot/$fastcgi_script_name;
    include /etc/nginx/fastcgi_params;
    fastcgi_read_timeout 600;
}
```
  - vhost.conf 설정파일 생성
    - sudo vim /etc/nginx/conf.d/vhost.conf
    - 
```
server {
    listen 8011;
    server_name _;

    set $documentRoot /web/site1/public;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    include /etc/nginx/conf.d/php_vhost.conf.include;
}

server {
    listen 8012;
    server_name _;

    set $documentRoot /web/site2/public;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    include /etc/nginx/conf.d/php_vhost.conf.include;
}

server {
    listen 8013;
    server_name _;

    set $documentRoot /web/site3/public;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    include /etc/nginx/conf.d/php_vhost.conf.include;
}
```
  - sudo systemctl restart nginx
  - php-fpm 활성화 및 켜기
    - sudo systemctl enable php-fpm
    - sudo systemctl restart php-fpm

## 시나리오 1
- 특정 사이트를 운영하고, 그 사이트를 수정할 수 있는 권한을 사용자들에게 나눠주세요.
- 사이트 정보
  - 사이트 1 : http://localhost:8011
    - 웹루트 : /web/site1/public
    - FTP 접속정보
      - HOST : 192.168.56.1
        - virtual box 환경에서는 127.0.0.1과 같은 의미
        - pureftpd의 버그로 인해 FTP 접속시에는 127.0.0.1 대신 이걸 사용해야 한다.
      - 계정 : site1 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site1
    - DB 세팅 명령어
      - mysql -u root -p
      - grant all privileges on site1.* to site1@\`%\` identified by 'sbs123414';
      - create database site1;
    - DB 접속정보
      - HOST : 127.0.0.1
      - 계정 : site1 / sbs123414
      - 포트 : 3307
      - 접근할 수 있는 DB : site1
  - 사이트 2 : http://localhost:8012
    - 웹루트 : /web/site2/public
    - FTP 접속정보
      - HOST : 192.168.56.1
      - 계정 : site2 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site2
    - DB 세팅 명령어
      - mysql -u root -p
      - grant all privileges on site2.* to site2@\`%\` identified by 'sbs123414';
      - create database site2;
    - DB 접속정보
      - HOST : 127.0.0.1
      - 계정 : site2 / sbs123414
      - 포트 : 3307
      - 접근할 수 있는 DB : site2
  - 사이트 3 : http://localhost:8013
    - 웹루트 : /web/site3/public
    - FTP 접속정보
      - HOST : 192.168.56.1
      - 계정 : site3 / sbs123414
      - 포트 : 21
      - 능동형/수동형 : 능동형
      - 접속폴더 : /web/site3
    - DB 세팅 명령어
      - mysql -u root -p
      - grant all privileges on site3.* to site3@\`%\` identified by 'sbs123414';
      - create database site3;
    - DB 접속정보
      - HOST : 127.0.0.1
      - 계정 : site3 / sbs123414
      - 포트 : 3307
      - 접근할 수 있는 DB : site3

## 오늘의 동영상
### 일반
- [동영상 - 2019 12 08, 리눅스, 개념, CENTOS 7, PHP 7.2, FPM 설치 및 NGINX 와 연동](https://www.youtube.com/watch?v=B84Jtz5s-mk&feature=youtu.be)
- [동영상 - 2019 12 08, 리눅스, 개념, CENTOS 7, root 외부 접속 막기](https://www.youtube.com/watch?v=1ycKSa4T7Ns&feature=youtu.be)
- [동영상 - 2019 12 08, 리눅스, 개념, CENTOS 7, 방화벽 다루기](https://www.youtube.com/watch?v=BhfaBRvS-Ng)
- [동영상 - 2020 06 06, 리눅스, 개념, php 사용하는 이유](https://youtu.be/JbiouiUoQ-8)
- [동영상 - 2020 06 06, 리눅스, 개념, nginx와 php와 mysql 연동](https://youtu.be/wyamX0UDKxI)

# 2020-06-07, 10일차
## 개념
### MariaDB
- DBMS
  - 데이터베이스 관리 시스템 or 데이터베이스 관리 서버
  - 하나의 DBMS로 2개 이상의 DB를 관리할 수 있다. 수 천개도 가능하다.
- DB
  - DB란 사실 테이블들을 묶어주는 가상의 단위에 불과하다. 일종의 폴더라고 보면 된다.
- 테이블
  - 테이블은 하나의 엑셀파일이다. 실제로 엑셀파일처럼 구성되어있고 굉장히 유사하다.
  - 테이블을 만들 때 테이블을 구성할 필드를 입력하면 된다.
  - 필드란 테이블에 들어갈 데이터(행)들이 가질 데이터에 대한 메타데이터 이다.
  - 예를들어 `나이 : 22` 에서 `나이`가 필드이고 `22`가 데이터 이다.
- DBMS 사용자
  - DBMS에 접근해서 DB를 관리하려면 인증을 해야 한다.
  - 기본적으로 존재하는 회원은 `root` 회원 이다.

## DB
### SQL 연습
- [sql v1](https://codepen.io/jangka44/pen/PBVbrw)
- [sql v2](https://codepen.io/jangka44/pen/ejxQYe)
- [sql v3](https://codepen.io/jangka44/pen/JBgwJv)
- [sql v4](https://codepen.io/jangka44/pen/EeYgRQ)

## PHP
## PHP 연습
- 블로그 프로그램 소스 및 DB
  - [sql](https://codepen.io/jangka44/pen/drMMgN)
  - [index.php](https://codepen.io/jangka44/pen/LaNNXR)
  - [article_list.php](https://codepen.io/jangka44/pen/RdaaqZ)
  - [write_article.php](https://codepen.io/jangka44/pen/GeZZwO)
  - [do_write_article.php](https://codepen.io/jangka44/pen/wOGGQX)
  - [modify_article.php](https://codepen.io/jangka44/pen/WmwwYa)
  - [do_modify_article.php](https://codepen.io/jangka44/pen/KEzzrJ)
  - [do_delete_article.php](https://codepen.io/jangka44/pen/Mxyyzd)

## 오늘의 동영상
### 일반
- [동영상 - 2019 12 14, 리눅스 개념, CENTOS 7, PHP, MySQL, NGINX, PURE-FTPD 연동, 호스팅 환경 구축](https://www.youtube.com/watch?v=lsnQC19ZnyY&feature=youtu.be)
- [동영상 - 2019 06 07, 리눅스 개념, CENTOS 7, PHP, MySQL, NGINX, PURE-FTPD 연동, 호스팅 환경 구축](https://youtu.be/EA8wjf4ZmvA)

# 2020-06-13, 11일차
## 문제
### SQL 연습
- [문제 - 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://codepen.io/jangka44/pen/LYVEyye)
- [정답](https://codepen.io/jangka44/pen/XWbJRMN)

## 오늘의 동영상
### 일반
- [동영상 - 2020 02 08, DB, 문제, 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://www.youtube.com/watch?v=M4-fhS8MZe8&feature=youtu.be)
- [동영상 - 2020 06 13, 리눅스, 개념, MySQL 사용자 관리](https://youtu.be/Jeha_eSCjO0)
- [동영상 - 2020 06 13, 리눅스, 개념, 웹서버, PHP, MySQL 활용 예제](https://youtu.be/dT3JZTnYIkA)

# 2020-06-14, 12일차
## 문제
### PHP 연습
- 블로그 프로그램 소스 및 DB
  - 작업환경 : editplus로 서버에서 접속해서 /web/site1/public 안에서 파일생성하여 작업
  - 각 PHP 소스코드에 있는 DB접속정보는 바꿔야 합니다.
  - DB
    - [sql](https://codepen.io/jangka44/pen/drMMgN)
  - 소스코드
    - [index.php](https://codepen.io/jangka44/pen/LaNNXR)
    - [article_list.php](https://codepen.io/jangka44/pen/RdaaqZ)
    - [write_article.php](https://codepen.io/jangka44/pen/GeZZwO)
    - [do_write_article.php](https://codepen.io/jangka44/pen/wOGGQX)
    - [modify_article.php](https://codepen.io/jangka44/pen/WmwwYa)
    - [do_modify_article.php](https://codepen.io/jangka44/pen/KEzzrJ)
    - [do_delete_article.php](https://codepen.io/jangka44/pen/Mxyyzd)
    
## 오늘의 동영상
### 일반
- [동영상 - 2020 02 08, DB, 문제, 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://youtu.be/dT3JZTnYIkA)
- [동영상 - 2020_06_14, 리눅스, 개념, php 에러 출력 설정 및 예제실행방법](https://youtu.be/14c86qBsjqg)

## 숙제
### 일반
- [동영상 - 2020_06_14, 리눅스, 개념, php 에러 출력 설정 및 예제실행방법](https://youtu.be/14c86qBsjqg)

# 2020-06-20, 13일차
## 오늘의 동영상
### 일반
- [동영상 - 2020 06 20, 리눅스, 개념, php 블로그예제 실행방법 및 공부법 1부](https://www.youtube.com/watch?v=_NVwZCL7KJo)
- [동영상 - 2020 06 20, 리눅스, 개념, php 블로그예제 실행방법 및 공부법 2부](https://www.youtube.com/watch?v=ANNKSutotqU)

# 2020-06-21, 13일차
## 오늘의 동영상
### 일반
- [동영상 - 2020_06_21, 자바, 개념, 이클립스, 서블릿, MySQL 개발환경 세팅, 1부](https://youtu.be/AoA100-O7Y0)
- [동영상 - 2020_06_21, 자바, 개념, 이클립스, 서블릿, MySQL 개발환경 세팅, 2부](https://youtu.be/xcxVFikKXgY)

# 유용한 링크
- [IT 상식사전](https://codepen.io/jangka44/live/MWaBoVz)