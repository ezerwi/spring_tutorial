# 페이지 정보
- 주소 : to2.kr/be5
- 클래스 : 2020년 04월 평일 09시 국기 풀스택

# 강사정보
- 이름 : 장희성
- 전화번호 : 010-8824-5558

# 강사링크
- [SBS 전산](http://sbs.koreagroupware.com/module/module.asp?MAIN=main)
  - 출결현황 확인 : 운영부 => 국비출결
  - 강의타임수 확인 : 소통창구
- [강사 마니아로](http://sbsartdj.kedutms.com/teacher/index.php)
- [강사 마니아로 - 상담내역](http://sbsartdj.kedutms.com/teacher/curri/ver4/consult/index.php?Pcode=11)
- [강사 마니아로 - 출석체크](http://sbsartdj.kedutms.com/teacher/curri/ver4/attendance/index.php?Pcode=11)

# 학생링크
## 자바 MVC(파일) 게시판
- [1차](https://repl.it/@jangka512/SpitefulHeartfeltSuperuser)
  - 기본기능 구현
- [2차](https://repl.it/@jangka512/BarrenWittyGraduate)
  - 실행될때 마다, 게시판 1개와 회원 1명이 계속 증가하는 버그 수정(1개 이상 안생기도록 수정)
- [3차](https://repl.it/@jangka512/GullibleOliveProcess)
  - member whoami, member login, member logout 기능 추가
- [4차](https://repl.it/@jangka512/BriskGaseousFunctions)
  - build site 기능 추가
  - 자유게시판도 자동으로 생성되도록 수정
- [5차](https://repl.it/@jangka512/StylishSurefootedProducts)
  - 각 게시판 별로 ~~-list-1.html 파일 생성
  - head, foot 템플릿 사용
- [6차](https://repl.it/@jangka512/LargeShallowLava)
  - 각 게시판 별로 ~~-list-1.html 파일 생성 할 때, 템플릿 사용
  - head에 GNB 달기

## 일반
- [팅커캐드](https://www.tinkercad.com/dashboard)
- [자바 8 API](https://docs.oracle.com/javase/8/docs/api/)
- [블로그 작성](https://codepen.io/jangka44/live/PoPPPej)
- [뉴렉처 2020 서블릿/JSP](https://www.youtube.com/playlist?list=PLq8wAnVUcTFVOtENMsujSgtv2TOsMy8zd)
- [뉴렉처 2020 서블릿/JSP의 공부기록, 유튜브](https://www.youtube.com/playlist?list=PLDtTzRqvUA4hsVli4rFDyJuaEOB6xnyVQ)
- [뉴렉처 2020 서블릿/JSP의 공부기록, 블로그](https://jangka44.tistory.com/category/뉴렉처%20강의%20공부,%202020%20서블릿,%20JSP)
- [유튜브 계정인증(대용량 업로드를 하려면 필요)](https://www.youtube.com/verify)

## NCS
- [학생 마니아로](http://sbsartdj.kedutms.com/student/index.php)

## 코드업
- [코드업 100제](https://codeup.kr/problemsetsol.php?psid=23)

## 프로그래밍 기초
- [기초 키보드 사용법](https://s.codepen.io/jangka44/debug/OzzVWM)
- [타이핑 정글](https://www.typingclub.com/sportal/program-3.game)
- [code.org 코스 3](https://studio.code.org/s/course3)
- [code.org 코스 4](https://studio.code.org/s/course4)
- [프로그래머스 - 자바 입문](https://programmers.co.kr/learn/courses/5)
  - [풀이](https://codepen.io/jangka44/debug/MWYqKNm)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 30분이 넘으면 안됩니다.
- [프로그래머스 - 자바 자료구조](https://programmers.co.kr/learn/courses/17)
  - [풀이](https://codepen.io/jangka44/debug/WNbWJgO)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 20분이 넘으면 안됩니다.
- [프로그래머스 자바스크립트 입문](https://programmers.co.kr/learn/courses/3)
  - [풀이](https://codepen.io/jangka44/live/mddjOxo)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 30분이 넘으면 안됩니다.
- [프로그래머스, 알고리즘](https://programmers.co.kr/learn/challenges?tab=all_challenges)
  - Level : 1
  - 언어 : Java
- [플레이코드몽키](https://www.playcodemonkey.com)
- [플레이코드몽키, 스테이지 쉽게 접근하기](https://codepen.io/jangka44/debug/ZEEWRzZ)

## 온라인 툴
- [repl 자바 연습장](https://repl.it/languages/java)
  - 사용법
    - 실행 : Ctrl + Enter
- [code.oa.gg/java8](http://code.oa.gg/java8)
  - 사용법
    - 실행 : Alt + Enter
    - 소스보기 : Alt + 1
    - 결과보기 : Alt + 2

## 프론트엔드 기초
- [CSS DINER](https://flukeout.github.io/)
- [새 연습장(코드펜)](https://codepen.io/pen/)
  - 주소 : codepen.io/pen

# 학생정보
- 김동연
  - [블로그](https://to2.kr/bfe)
  - [유튜브 채널](https://to2.kr/bho)
  - [위키](https://to2.kr/bnm)
- 김용순
  - [블로그](https://to2.kr/bfz)
  - [유튜브 채널](https://to2.kr/bhq)
  - [위키](https://to2.kr/bnq)
- 김일구
  - [블로그](https://to2.kr/bfd)
  - [유튜브 채널](https://to2.kr/bhs)
  - [위키](https://to2.kr/bnh)
- 김혜련
  - [블로그](https://to2.kr/bfk)
  - [유튜브 채널](https://to2.kr/bhf)
  - [위키](https://to2.kr/bno)
- 김혜지
  - [블로그](https://to2.kr/bfa)
  - [유튜브 채널](https://to2.kr/bhg)
  - [위키](https://to2.kr/bnf)
- 노경욱
  - [블로그](https://to2.kr/bfb)
  - [유튜브 채널](https://to2.kr/bhd)
  - [위키](https://to2.kr/bng)
- 봉영근
  - [블로그](https://to2.kr/be8)
  - [유튜브 채널](https://to2.kr/bhC)
  - [위키](https://to2.kr/bnk)
- 성소연
  - [블로그](https://to2.kr/bfx)
  - [유튜브 채널](https://to2.kr/bhn)
  - [위키](https://to2.kr/bnn)
- 신성민
  - [블로그](https://to2.kr/bhZ)
  - [유튜브 채널](https://to2.kr/bhh)
  - [위키](https://to2.kr/bnr)
- 신정용
  - [블로그](https://to2.kr/bfj)
  - [유튜브 채널](https://to2.kr/bjR)
  - [위키](https://to2.kr/bnw)
- 양정원
  - [블로그](https://to2.kr/bfr)
  - [유튜브 채널](https://to2.kr/bht)
  - [위키](https://to2.kr/bnv)
- 이상범
  - [블로그](https://to2.kr/bfp)
  - [유튜브 채널](https://to2.kr/bhm)
  - [위키](https://to2.kr/bnj)
- 이용빈
  - [블로그](https://to2.kr/bfs)
  - [유튜브 채널](https://to2.kr/bhx)
  - [위키](https://to2.kr/bnu)
- 이우석
  - [블로그](https://to2.kr/bfA)
  - [유튜브 채널](https://to2.kr/bhu)
  - [위키](https://to2.kr/bnt)
- 이정한
  - [블로그](https://to2.kr/bfy)
  - [유튜브 채널](https://to2.kr/bhv)
  - [위키](https://to2.kr/bnd)
- 임현호
  - [블로그](https://to2.kr/bft)
  - [유튜브 채널](https://to2.kr/bhw)
  - [위키](https://to2.kr/bns)
- 장나현
  - [블로그](https://to2.kr/bh1)
  - [유튜브 채널](https://to2.kr/bhA)
- 정성훈
  - [블로그](https://to2.kr/bfc)
  - [유튜브 채널](https://to2.kr/bhj)
  - [위키](https://to2.kr/bne)
- 채영지
  - [블로그](https://to2.kr/bfn)
  - [유튜브 채널](https://to2.kr/bhe)
  - [위키](https://to2.kr/bnp)
- 하승범
  - [블로그](https://to2.kr/bfw)
  - [유튜브 채널](https://to2.kr/bhY)
  - [위키](https://to2.kr/bnc)

# 2020-04-09, 001일차
## 개념
- `\n`은 콘솔에서 줄바꿈을 의미합니다.

## 문제
### 출력
- [문제 - `안녕하세요.` 10번 출력](https://repl.it/@jangka512/JAVA-5-18-04-10-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-2)
- [문제 - `안녕하세요.` 10번 출력, System.out.println을 한번만 사용](https://repl.it/@jangka512/JAVA-5-18-04-10-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-4)
- [문제 - `안녕하세요.` 10번 출력, 역슬래쉬 활용](https://repl.it/@jangka512/JAVA-5-18-04-10-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-6)

### 변수
- [문제 - 변수 a와 b의 값을 서로 교체](https://repl.it/@jangka512/JAVA-5-18-04-10-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-8)
- [문제 - 변수 a와 b의 값을 서로 교체, 숫자와 사칙연산 사용 금지](https://repl.it/@jangka512/JAVA-5-18-04-10-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-10)

## 오늘의 동영상
- [동영상 - 2018 06 28, 자바, 개념, repl.it 사용법](https://youtu.be/7Q_JDQm7-Y0)
- [동영상 - 2019 12 11, 자바, 출력, 변수, swap](https://www.youtube.com/watch?v=ffyaI23G8LU&feature=youtu.be)
- [동영상 - 2020 04 09, 프로그래밍 일반, 개념, 안녕하세요를 키보드만으로 많이 입력 ](https://youtu.be/C0x4Oeu-s-I)
- [동영상 - 2020 04 09, 자바, 개념, repl.it 사용법](https://youtu.be/2hczZ0MCoQE)
- [동영상 - 2020 04 09, 자바, 개념, System.out.println](https://youtu.be/ZkUwVrOq9T4)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력](https://youtu.be/u5pI5tTttoM)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력, println은 한번](https://youtu.be/98WDfm8Wr3g)
- [동영상 - 2020 04 09, 자바, 문제, 안녕하세요 10번 출력, println은 한번, 뉴라인](https://youtu.be/-dzllmseUuo)
- [동영상 - 2020 04 09, 자바, 문제, 두 변수의 값 교환, 단순한 방법](https://youtu.be/w3db1JFGUhQ)
- [동영상 - 2020 04 09, 자바, 문제, 두 변수의 값 교환, 임시변수로 해결](https://youtu.be/EPH1rZS20-8)
- [동영상 - 2020 04 09, 프로그래밍 일반, 개념, to2.kr 이용해서 주소 줄이기](https://youtu.be/gcmGLaEpOLo)

# 2020-04-10, 002일차
## 개념
- 정수끼리 연산하면 결과는 정수이다.
  - 10 / 20 은 0.5가 아니라 0이다.
- 연산자 종류
  - 사칙연산
    - `+` : 더하기
    - `-` : 빼기
    - `*` : 곱하기
    - `/` : 나누기
  - 논리연산
    - `==` : 같다
    - `!=` : 다르다
    - `>` : 크다(초과)
    - `<` : 작다(미만)
    - `>=` : 크거나 같다(이상)
    - `<=` : 작거나 같다(이하)
    - `&&` : 그리고(and)
    - `||` : 또는(or)
- 소스코드의 4대 구성 요소
  - 변수
    - 변수선언 : int a;
      - int를 변수타입이라고 한다.
      - a를 변수명이라고 한다.
      - `a라는 변수를 만들겠습니다. 다만 앞으로 a에는 정수만 담을 수 있습니다.` 라는 뜻이다.
    - 변수선언(변수생성)은 2번 이상 할 수 없다.
    - 변수의 값은 바꿀 수 있다.
    - 변수는 값을 넣을 때 빼고는 값(자신이 가지고 있는) 취급을 해야 한다.
  - 값
    - 종류
      - 숫자
      - 문자
      - ...
  - 조건문
    - if문
      - if ( 조건문 ) { 실행문 }
    - 추가옵션 : else
      - if ( 조건문 ) { 실행문1 } else { 실행문2 }
        - 실행문2는 조건이 거짓일 때 실행된다.
        - 양자택일이 된다.
    - switch문
  - 반복문
- 연산자 우선순위
  - 우선순위과 높은 것이 먼저 실행된다.
  - 사칙연산자가 논리연산자보다 우선순위가 높다.
  - `*`, `/`는 다른 사칙연산자 보다 우선순위가 높다.
  - `&&`, `||`는 다른 논리 연산자 보다 우선순위가 낮다.
  - 앞에 있는게 먼저 실행된다.
- 증감 연산자
  - `i++;` => i 의 값을 1 증가 시킨다.
  - `i--;` => i 의 값을 1 감소 시킨다.
  - `i = i + 2;` => i 의 값을 2 증가 시킨다.
  - `i += 2;` => i 의 값을 2 증가 시킨다.(위와 같은 표현)
  - `i = i - 2;` => i 의 값을 2 감소 시킨다.
  - `i -= 2;` => i 의 값을 2 감소 시킨다.(위와 같은 표현)

## 문제
- [개념 - if](https://code.oa.gg/java8/1537)
- [개념 - if, else if, else](https://code.oa.gg/java8/1625)
- [문제 - 실행되는 출력문에는 참 그렇지 않으면 거짓 이라고 적어주세요.](https://repl.it/@jangka512/JAVA-5-18-04-10-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-10-12)
- [개념 - &&(그리고), ||(또는)](https://code.oa.gg/java8/1538)
- [문제 - 할인 대상인지 아닌지 출력(&&, || 없이도 가능해야합니다.)](https://repl.it/@jangka512/LankyCruelAudit)
- [문제 - 정답](https://repl.it/@jangka512/OrangeCylindricalCharactermapping)

## 동영상
- [동영상 - 2020_04_10, 자바, 개념, print, 변수, swap](https://www.youtube.com/watch?v=SBmCr4f2sfQ)
- [동영상 - 2018 06 29, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=-EFKDubiXuE)
- [동영상 - 2019 12 11, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=RU_fhnKuumw&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=hEyLOealTac&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if](https://www.youtube.com/watch?v=95EFiiikxfE)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if, else if, else](https://www.youtube.com/watch?v=K6wwRasJ1yQ)
- [동영상 - 2019 12 12, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=Q13y-MrxaEg&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=2EVhMJOl1eU)
- [동영상 - 2020_04_10, 자바, 문제, 조건식의 참, 거짓 여부를 적어주세요](https://www.youtube.com/watch?v=qoP-x4wi38M)
- [동영상 - 2019 12 12, 자바, 문제, 할인 대상인지 출력](https://www.youtube.com/watch?v=u8GZfR5o4Ro&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 문제, 할인 대상인지 출력, 1부](https://www.youtube.com/watch?v=vwen4eLADA4)
- [동영상 - 2020_04_10, 자바, 개념, 코드업 푸는방법](https://www.youtube.com/watch?v=8ZC2cPGR9M4)

# 2020-04-13, 003일차
## 문제
### 조건문
- [문제 - 할인 대상인지 아닌지 출력(&&, || 없이도 가능해야합니다.)](https://repl.it/@jangka512/LankyCruelAudit)
- [문제 - 정답 v2](https://code.oa.gg/java8/1627)

### 포멧 출력, 입력, 문장
- [개념 - 포멧 출력, 정수](https://repl.it/@jangka512/VisibleUnfinishedFtpclient)
- [개념 - 포멧 출력, 패딩](https://repl.it/@jangka512/KnottyIncredibleDictionary)
- [개념 - 포멧 출력, 실수](https://repl.it/@jangka512/HungryDecisiveEmulators)
- [개념 - 포멧 출력, 문장](https://repl.it/@jangka512/CreepyYellowMode)
- [개념 - 포멧 출력, 문장, trim](https://repl.it/@jangka512/ScaryJauntyThing)
- [개념 - 포멧 출력, 문장, concat, substring](https://repl.it/@jangka512/BuoyantYawningHexagon)
- [개념 - 포멧 출력, 문장, split, parseInt](https://repl.it/@jangka512/OrdinaryEmptyPorts)
- [개념 - 입력, 정수, 실수](https://repl.it/@jangka512/GorgeousTriangularFirmware)
- [개념 - 입력, 문장, 버퍼비우기](https://repl.it/@jangka512/LinedAmbitiousColdfusion)
- [개념 - 입력, 문장, 문장을 정수로 변경](https://repl.it/@jangka512/AgreeablePrizeScreenscraper)
- [개념 - 입력, 정수, 문장, 문장을 나눠서 정수로 변경](https://repl.it/@jangka512/MeagerUnfoldedInstructionset)

## 오늘의 동영상
- [동영상 - 2019 12 12, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=Q13y-MrxaEg&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 개념, 조건문, if, and, or](https://www.youtube.com/watch?v=2EVhMJOl1eU)
- [동영상 - 2020_04_10, 자바, 문제, 조건식의 참, 거짓 여부를 적어주세요](https://www.youtube.com/watch?v=qoP-x4wi38M)
- [동영상 - 2019 12 12, 자바, 문제, 할인 대상인지 출력](https://www.youtube.com/watch?v=u8GZfR5o4Ro&feature=youtu.be)
- [동영상 - 2020_04_10, 자바, 문제, 할인 대상인지 출력, 1부](https://www.youtube.com/watch?v=vwen4eLADA4)
- [동영상 - 2020_04_10, 자바, 개념, 코드업 푸는방법](https://www.youtube.com/watch?v=8ZC2cPGR9M4)
- [동영상 - 2019 12 12, 자바, 개념, if문 기초](https://www.youtube.com/watch?v=hEyLOealTac&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 개념, if문 기초(and, or)](https://www.youtube.com/watch?v=Q13y-MrxaEg&feature=youtu.be)
- [동영상 - 2019 12 12, 자바, 문제, if문 할인조건 문제풀이](https://www.youtube.com/watch?v=u8GZfR5o4Ro&feature=youtu.be)
- [동영상 - 2019 12 13, 자바, 개념, String, 합치기](https://www.youtube.com/watch?v=6POYRG1oSi4&feature=youtu.be)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 정수](https://youtu.be/7RQx-oLXNHc)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 패딩](https://youtu.be/vrJ2LMc1yOQ)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 실수](https://youtu.be/kwT70Mqa-g8)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 문장](https://youtu.be/rVae-YY9jZY)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 문장, trim](https://youtu.be/WeDCL1WbYRI)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 문장, concat, substring](https://youtu.be/O4wHs9sMl60)
- [동영상 - 2020_04_13, 자바, 개념, 포멧 출력, 문장, split, parseInt](https://youtu.be/k_q0CIYXfy4)
- [동영상 - 2020_04_13, 자바, 개념, 입력, 정수, 실수](https://youtu.be/YDKrTZI88f8)
- [동영상 - 2020_04_13, 자바, 개념, 입력, 정수, 문장, 버퍼비우기](https://youtu.be/EWooGFiOats)
- [동영상 - 2020_04_13, 자바, 개념, 입력, 정수, 문장, 문장을 정수로 변경](https://youtu.be/gxLKb4Gp4LY)
- [동영상 - 2020_04_13, 자바, 개념, 입력, 정수, 문장, 문장을 나눠서 정수로 변경](https://youtu.be/y2I2ro4t_YA)
- [동영상 - 2020_04_13, 자바, 개념, 코드업, 기초 조건문 문제집, 초반풀이](https://youtu.be/iHyZ8n_rWF8)

## 숙제
- [코드업 문제집, 기초3. if ~ else](https://codeup.kr/problemsetsol.php?psid=11)
  - 20문제

# 2020-04-14, 004일차
## 문제
### 반복문
- [문제 - 구구단 8단을 출력해주세요. 반복문 사용 금지](https://repl.it/@jangka512/JAVA-5-18-04-16-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-2)
- [문제 - 구구단 8단을 출력해주세요. dan 변수 활용](https://repl.it/@jangka512/JAVA-5-18-04-16-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-4)
- [문제 - 구구단 8단을 출력해주세요. dan 변수의 값에 따라 다른 단이 나올 수 있도록 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-6)
- [문제 - 구구단 8단을 출력해주세요. 2 이상의 숫자를 사용할 수 없습니다.](https://repl.it/@jangka512/JAVA-5-18-04-16-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-8)
- [문제 - 구구단 8단을 출력해주세요. 9까지가 아닌 1000까지 곱해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-10)
- [문제 - 1부터 5까지 출력해주세요.](https://repl.it/@jangka512/PossiblePlayfulOpenlook)
- [문제 - 정답](https://repl.it/@jangka512/FamousCurvyConditions)
- [문제 - -100부터 25까지 출력해주세요.](https://repl.it/@jangka512/StridentEssentialDatawarehouse)
- [문제 - 정답](https://repl.it/@jangka512/ImperturbableDownrightParticles)
- [문제 - 구구단 8단을 출력해주세요. 1000부터 1까지 곱해주세요.](https://repl.it/@jangka512/PowderblueJubilantPostgres)
- [문제 - 정답](https://repl.it/@jangka512/EdibleLumberingAdware)
- [문제 - 99단 8단을 출력해주세요. 1000부터 -500까지 곱해주세요.](https://repl.it/@jangka512/JointExperiencedRoute)
- [문제 - 정답](https://repl.it/@jangka512/NegativeWhimsicalDesign)
- [문제 - 1부터 5까지의 합을 출력해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-16-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-16-12)

## 오늘의 동영상
- [동영상 - 2019 12 13, 자바, 문제, while 문, 구구단 출력](https://www.youtube.com/watch?v=x4SZqumRNlA&feature=youtu.be)
- [동영상 - 2019 12 13, 자바, 문제, while 문, 1부터 5까지의 합](https://www.youtube.com/watch?v=wo4Le51XXFc&feature=youtu.be)
- [동영상 - 2020 04 13, 자바, 개념, 코드업, 기초 반복문 문제집, 초반풀이](https://www.youtube.com/watch?v=s48wTjh0MlU&feature=youtu.be)
- [동영상 - 2020 04 13, IT일반, 개념, 티스토리 글 관리법](https://www.youtube.com/watch?v=j8J5XI91DEw&feature=youtu.be)

## 숙제
- [코드업 문제집, 기초1. 출력문](https://codeup.kr/problemsetsol.php?psid=9)
  - 전부
- [코드업 문제집, 기초2. 입출력문 및 연산자](https://codeup.kr/problemsetsol.php?psid=10)
  - 전부
- [코드업 문제집, 기초3. if ~ else](https://codeup.kr/problemsetsol.php?psid=11)
  - 20문제
- [코드업 문제집, 기초4-1. 단순 반복문](https://codeup.kr/problemsetsol.php?psid=13)
  - 20문제
  
# 2020-04-15, 05일차
## 개념
- 모든 변수는 메모리에 저장된다.
- int 변수는 4바이트 이다.
- 변수에는 오직 8바이트 이하의 값만 넣을 수 있다.
- 객체가 필요한 이유
  - 변수에는 오직 값 1개만 넣을 수 있다.
  - 프로그래밍을 하다보면 변수에 값을 여러개 넣을 필요가 있을 때가 있다.
  - 그래서 고안된 것이 객체이다.
  - 객체는 커피 캐리어에 비유될 수 있다.
  - 커피 캐리어에는 커피를 여러잔 담을 수 있다.
  - 커피 캐리어는 용도에 따라 종류가 여러가지 있다.
  - 프로그래밍을 할 때 상황에 따라 서로 다른 종류의 객체 여러개가 필요하다.
  - 객체를 일종의 제품으로 보았을 때 객체를 만들기 위해서는 설계도 즉 클래스가 필요하다.
  - 객체는 너무 커서 변수에 담을 수 없다.
  - 그래서 또 고안된 것이 리모콘 시스템이다.
  - 클래스로는 3가지를 할 수 있다.
  - 클래스로는 객체도 만들 수 있고 그 객체를 조종할 수 있는 리모콘도 만들 수 있다. 그리고 또 객체리모콘을 담을 변수도 만들 수 있다.
- 변수에는 오직 1차원적인 값만 저장 할 수 있다.
  - 1차원 적인 값(데이터)
    - 5
    - 3.14
    - 'a'
    - true
  - 복잡한 값
    - 객체
- 변수에는 객체를 저장할 수 없다.
  - 객체가 너무 크고 변수는 작다.
  - 객체는 여러가지 값(데이터)의 조합이다. 그게 변수에 들어가면 한 덩어리로 해석된다.
    - 즉 변수에 1과 2를 넣으면, 다른 사람들은 그것을 1과 2의 조합이 아닌 12로 본다.
- `new 사람();`을 하면 2가지가 만들어진다.
  - 객체
  - 객체가 자기자신을 조종할 수 있는 리모콘
    - 그 리모콘을 객체 스스로는 this 라고 부른다.

## 문제
### 반복문
- [문제 - -100부터 25까지의 합을 출력해주세요.](https://repl.it/@jangka512/RoyalbluePartialPdf)
- [문제 - 정답](https://repl.it/@jangka512/MutedPreviousSupport)
- [문제 - 1부터 3까지 출력하는 작업을 10번 해주세요. 2중 while문 사용](https://repl.it/@jangka512/RemarkableImpeccableGnuassembler)
- [문제 - 정답](https://repl.it/@jangka512/UnitedRustyPackagedsoftware)
- [개념 - for문](https://code.oa.gg/java8/1630)

## 배열 개념
- [개념 - 배열](https://code.oa.gg/java8/1629)

## 오늘의 동영상
- [동영상 - 2019 12 16, 자바, n부터 m까지의 합](https://www.youtube.com/watch?v=3oMWeIW8m2g&feature=youtu.be)
- [동영상 - 2019 12 16, 자바, 2중 while 문](https://www.youtube.com/watch?v=DnRAsosbpL8&feature=youtu.be)
- [동영상 - 2020 04 15, 자바, 개념, 코드업, 1차원 배열 문제집, 초반풀이, v2](https://youtu.be/d-UEdZo9pfI)
- [동영상 - 2020 04 15, 자바, 개념, 코드업, 1차원 배열 문제집, 초반풀이](https://youtu.be/cSlNRDYcqyQ)
- [동영상 - 2020_04_15, 자바, 개념, for, 중첩반복문, 구구단](https://www.youtube.com/watch?v=LR1wPOTh5ww)
- [동영상 - 2020_04_15, 자바, 개념, for](https://youtu.be/6-p9wqTX6vQ)
- [동영상 - 2020_04_15, 자바, 문제, 1부터 3까지 10번 출력](https://youtu.be/ScnPyxPPfMA)
- [동영상 - 2020_04_15, 자바, 문제, 배열에 숫자 3개 담고, 출력](https://youtu.be/wz6WvA83CEA)
- [동영상 - 2020_04_15, 자바, 문제, 음수 100부터 25까지 출력](https://youtu.be/PkI3QWkK2TU)

## 숙제
- [코드업 문제집, 기초4-2. 중첩 반복문](https://codeup.kr/problemsetsol.php?psid=14)
  - 5문제
- [코드업 문제집, 기초5-1. 1차원 배열](https://codeup.kr/problemsetsol.php?psid=15)
  - 5문제

# 2020-04-16, 06일차
## 문제
### 배열
- [개념 - 배열, 평균, 총합, length](https://code.oa.gg/java8/1631)
- [개념 - 배열, boolean 배열](https://code.oa.gg/java8/1632)

### 객체
- [개념 - 객체, 리모콘](https://code.oa.gg/java8/1633)
- [개념 - 객체, 리모콘 매개변수](https://code.oa.gg/java8/1634)
- [개념 - 함수로, boolean 배열객체의 값을 반전](https://code.oa.gg/java8/1635)
- [개념 - 캐릭터 객체 정보를, 안 묶은 버전](https://code.oa.gg/java8/1636)
  - 캐릭터 정보 공유 할 때 힘들다.
- [개념 - 캐릭터 객체 정보를, 배열객체로 묶은 버전](https://code.oa.gg/java8/1637)
  - 편하긴 하지만, 이름 같은 속성은 추가가 불가능 하다.
  - char1[0] 이 번호이고, char1[1] 가 나이인걸 외워야 한다.
- [개념 - 캐릭터 객체 정보를, 캐릭터 클래스로 만든 객체로 묶은 버전](https://code.oa.gg/java8/1638)
  - 클래스를 만들어야 한다는게 귀찮지만, 편하다.
- [문제 - 자동차 설계도를 만들어주세요.](https://repl.it/@jangka512/TurboTornWebpages)
- [문제 - 정답](https://repl.it/@jangka512/ForsakenRosySystemsoftware)
- [문제 - 자동차 객체를 담을 변수를 만들어주세요.](https://repl.it/@jangka512/NegligibleUltimateBucket)
- [문제 - 정답](https://repl.it/@jangka512/GranularPapayawhipVirtualmachines)
- [문제 - 자동차 객체를 만들고 변수에 담아주세요.](https://repl.it/@jangka512/HotPlainProlog)
- [문제 - 정답](https://repl.it/@jangka512/ShowyChillyCubase)
- [문제 - 자동차 객체마다 서로 다른 최고속력를 가지도록 해주세요.](https://repl.it/@jangka512/UnwieldyScarceVertex)
- [문제 - 정답](https://repl.it/@jangka512/AmusedWelltodoRecords)
- [문제 - 1개의 자동차가 3번 달리게 해주세요.](https://repl.it/@jangka512/DeepskyblueCoordinatedPrinters)
- [문제 - 정답](https://repl.it/@jangka512/UnluckyKnowingTexts)
- [문제 - 객체를 사용하지 않고 두번째 플레이어를 만들어주세요.](https://repl.it/@jangka512/MobileHospitableBrowser)
- [문제 - 정답](https://repl.it/@jangka512/HighBigheartedCharacter)
- [문제 - 3개의 자동차가 각각 1번씩 달리게 해주세요.](https://repl.it/@jangka512/PracticalSecretLink)
- [문제 - 정답](https://repl.it/@jangka512/TepidCutePackages)

## 오늘의 동영상
- [동영상 - 2018 07 05, 자바, 클래스와 객체](https://www.youtube.com/watch?v=jiDjGO13ccU)
- [동영상 - 2019 12 16, 자바, 객체관련 문제풀이](https://www.youtube.com/watch?v=MpU4Ppa5K6s&feature=youtu.be)
- [동영상 - 2019 12 16, 자바, 객체 기초개념](https://www.youtube.com/watch?v=olaIeerJIJM&feature=youtu.be)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모톤, 매개변수, 설명 1](https://youtu.be/QkY86EYYjOs)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모톤, 매개변수, 설명 2](https://youtu.be/GyGWkmFXkrs)
- [동영상 - 2020 04 16, 자바, 개념, 객체, 리모톤, 매개변수, 설명 3](https://youtu.be/-MQ46MHqOtg)
- [동영상 - 2020 04 16, 자바, 개념, 객체와 클래스](https://youtu.be/FDPLPzpIOUw)
- [동영상 - 2020 04 16, 자바, 개념, 클래스가 없으면 벌어지는 일](https://youtu.be/Pn6-X24mCrU)
- [동영상 - 2020 04 16, 자바, 문제, 객체관련 문제풀이, 1부](https://youtu.be/FvZS8BP5Ltk)
- [동영상 - 2020 04 16, 자바, 문제, 객체관련 문제풀이, 2부](https://youtu.be/upRc8_yalu4)
- [동영상 - 2020 04 16, 자바, 문제, 크기가 3인 boolean 배열의 요소들을 함수에서 수정](https://youtu.be/NqgGn00inUQ)
- [동영상 - 2020_04_16, 자바, 개념, 객체를 사용하는 이유](https://youtu.be/DKUbn1SIJYs)
- [동영상 - 2020_04_16, 자바, 개념, 배열, 평균, 총합](https://youtu.be/0rGhLT3Gp1c)
- [동영상 - 2020_04_16, 자바, 개념, 배열객체와 리모콘, 2부](https://youtu.be/l0U9p8uRKHk)
- [동영상 - 2020_04_16, 자바, 개념, 배열객체와 리모콘](https://youtu.be/pPzBzwYtIJI)
- [동영상 - 2020_04_16, 자바, 개념, 함수와 스택, 메모리](https://youtu.be/pmxfK9Pnnx8)

## 숙제
- [코드업 문제집, 기초4-2. 중첩 반복문](https://codeup.kr/problemsetsol.php?psid=14)
  - 5문제
- [코드업 문제집, 기초5-1. 1차원 배열](https://codeup.kr/problemsetsol.php?psid=15)
  - 5문제

# 2020-04-17, 07일차
## 문제
### 정렬
- [문제 - 구구단을 거꾸로(9단 ~ 0단), 점점 더 적은 개수를 출력](https://code.oa.gg/java8/1641)
- [문제 - 정답](https://code.oa.gg/java8/1642)
- [문제 - 거품정렬을 구현해주세요.](https://code.oa.gg/java8/1639)
- [문제 - 정답](https://code.oa.gg/java8/1640)

## 오늘의 동영상
- [동영상 - 거품정렬 비주얼](https://www.youtube.com/watch?v=lyZQPjUT5B4)
- [동영상 - 2020_04_17, 자바, 개념, 버블소트 원리 설명 v1](https://youtu.be/rU9DBJlwPBg)
- [동영상 - 2020_04_17, 자바, 개념, 버블소트 원리 설명 v2](https://youtu.be/m-UYQJRXAsA)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이 v2](https://youtu.be/mVmoOO8P_p4)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이 v3](https://youtu.be/8nDbJXqYbPo)
- [동영상 - 2020_04_17, 자바, 개념, 일반 배열, 리모콘 용 배열 차이](https://youtu.be/Q_V0qRjCROM)
- [동영상 - 2020_04_17, 자바, 문제, 구구단을 역순으로 출력, 각 단만큼 곱해주세요](https://youtu.be/P2909eUdeDA)
- [동영상 - 2020_04_17, 자바, 문제, 버블소트를 구현해주세요](https://www.youtube.com/watch?v=BiEzwfjhSoM)

# 2020-04-20, 08일차
## 문제
### 일반
- [문제 - 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://repl.it/@jangka512/TrueSandybrownCodewarrior)
- [문제 - 정답](https://repl.it/@jangka512/WeightyVigilantObjectdatabase)
- [문제 - 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://repl.it/@jangka512/ShabbyPoisedGlobalarrays)
- [문제 - 정답](https://repl.it/@jangka512/AutomaticJauntyYottabyte)
- [문제 - 일반변수에 값 할당과정 설명](https://repl.it/@jangka512/LoyalFrayedBudgetrange)
- [문제 - 정답](https://repl.it/@jangka512/TinySeveralCone)
- [문제 - 레퍼런스변수에 값 할당과정 설명](https://repl.it/@jangka512/PriceyProfitableBackground)
- [문제 - 정답](https://repl.it/@jangka512/SubduedThirdInfinity)
- [문제 - 레퍼런스변수에 값 할당과정 설명 v2](https://repl.it/@jangka512/JAVA-5-18-04-12-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-2)
- [문제 - 인스턴스 메서드 실행](https://repl.it/@jangka512/JAVA-5-18-04-12-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-4)
- [문제 - 인스턴스 메서드 2개 실행](https://repl.it/@jangka512/JAVA-5-18-04-12-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-6)
- [문제 - 객체 리모콘을 저장하는 변수를 올바르게 수정해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-12-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-8)
- [문제 - 리모콘을 변수에 저장하지 않고 바로 사용](https://repl.it/@jangka512/JAVA-5-18-04-12-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-10)
- [문제 - 5개의 서로 다른 종류의 객체를 만들고 각각의 객체에게 일을 시켜주세요.](https://repl.it/@jangka512/JAVA-5-18-04-12-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-12-12)
- [문제 - 객체화 없이 설계도에 있는 능력을 바로 사용해주세요.](https://repl.it/@jangka512/DarlingKlutzySymbol)
- [문제 - 정답](https://repl.it/@jangka512/OldlaceHurtfulMice)
- [문제 - 구구단을 만들어주세요](https://repl.it/@jangka512/EqualThankfulCases)
- [문제 - 정답](https://repl.it/@jangka512/LightPungentDevices)
- [문제 - 정답 v2](https://repl.it/@jangka512/FriendlySeriousDifferences)
- [문제 - 매개변수를 사용해서 문제를 풀어주세요.](https://repl.it/@jangka512/HonoredEmotionalPlans)
- [문제 - 정답](https://repl.it/@jangka512/MoralIntentBloatware)
- [문제 - 함수를 실행하면 값을 돌려주도록 만들어주세요.](https://repl.it/@jangka512/SleepyUnwelcomeQuerylanguage)
- [문제 - 정답](https://repl.it/@jangka512/HalfImportantVariables)
- [문제 - 1부터 n까지의 합을 반환하는 함수](https://repl.it/@jangka512/JAVA-5-18-04-20-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-2)
- [문제 - n부터 m까지의 합을 반환하는 함수](https://repl.it/@jangka512/JAVA-5-18-04-20-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-4)

### 상속
- [문제 - `숨쉬다`라는 기능을 중복하지 않고 문제를 풀어주세요.](https://repl.it/@jangka512/JAVA-5-18-04-24-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-2)

## 동영상
- [동영상 - 2019 12 17, 자바, 레퍼런스 변수와 객체 1부](https://www.youtube.com/watch?v=fFOWKwp_iXU&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체기본문제, 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://www.youtube.com/watch?v=yMaUTypEKaM&feature=youtu.be)
- [동영상 - 2019 12 17, 자바, 객체기본문제, 번호가 다른 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요.](https://www.youtube.com/watch?v=9AKMJ4U5Zds&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체지향기초, null과 레퍼런스 변수 할당](https://www.youtube.com/watch?v=klOodAP3L4Y&feature=youtu.be)
- [동영상 - 2019 12 15, 자바, 객체지향기초, 클래스로 만들 수 있는 3가지](https://www.youtube.com/watch?v=wWG9iCI_wJw&feature=youtu.be)
- [동영상 - 2020_04_19, 자바 문제, 각각의 자동차가 서로 다른 최고속력으로 달리게 해주세요](https://youtu.be/-l1FN5GZ9Vg)
- [동영상 - 2020_04_19, 자바 문제, 일반변수에 값 할당과정 설명](https://youtu.be/T5kitERUOdg)
- [동영상 - 2020_04_19, 자바 문제, 레퍼런스변수에 값 할당과정 설명](https://youtu.be/nnmIvyoLurA)
- [동영상 - 2020_04_19, 자바 문제, 레퍼런스변수에 값 할당과정 설명, v2](https://youtu.be/8uaoy7tlMoM)
- [동영상 - 2020_04_19, 자바 문제, 객체 리모콘을 저장하는 변수를 올바르게 수정해주세요.](https://youtu.be/RAwQ5Xl-9Lk)
- [동영상 - 2020_04_19, 자바 문제, 리모콘을 변수에 저장하지 않고 바로 사용](https://youtu.be/o8WKoDOJHts)
- [동영상 - 2020_04_19, 자바 문제, 인스턴스 메서드 실행](https://youtu.be/iETVI-hDzBU)
- [동영상 - 2020_04_19, 자바 문제, 인스턴스 메서드 2개 실행](https://youtu.be/185mVCrNbhE)
- [동영상 - 2020 04 19, 자바 문제, 5개의 서로 다른 종류의 객체를 만들고 각각의 객체에게 일을 시켜주세요](https://youtu.be/Y19ghnmXyGw)
- [동영상 - 2020_04_19, 자바, 문제, 1부터 n까지의 합을 반환하는 함수](https://youtu.be/SsILhbd7Y6k)
- [동영상 - 2020_04_19, 자바, 문제, n부터 m까지의 합을 반환하는 함수](https://youtu.be/_jVxuTTywYM)
- [동영상 - 2020 04 19, 자바, 개념, static, 매개변수, 리턴](https://www.youtube.com/watch?v=BijvB_UH32o&feature=youtu.be)
- [동영상 - 2020_04_19, 자바, 문제, 객체화 없이 설계도에 있는 능력을 바로 사용해주세요, static](https://youtu.be/u_5VUUcQS3g)
- [동영상 - 2020 04 19, 자바, 문제, 구구단 구현](https://youtu.be/8e9ACh3AYPo)
- [동영상 - 2020_04_19, 자바, 문제, 매개변수를 이용해서 문제를 풀어주세요](https://youtu.be/_pQ4AcPg5e4)
- [동영상 - 2020_04_19, 자바, 문제, 함수가 리턴을 하게 해주세요 ](https://youtu.be/vsEG1AwYDuA)
- [동영상 - 2020 04 19, 자바, 문제, 상속을 사용해서 중복을 피해주세요](https://www.youtube.com/watch?v=NK6rQ9lr2hM&feature=youtu.be)
- [동영상 - 2020 04 20, 자바, 문제, 코드업, 성적표, 작업 1](https://www.youtube.com/watch?v=Z89613A3IIA)
- [동영상 - 2020 04 20, 자바, 문제, 코드업, 성적표, 작업 2](https://youtu.be/kBbCqNWHQ1Q)
- [동영상 - 2020 04 20, 자바, 개념, 공부하는 방법](https://youtu.be/pjMgZDmDNKs)

## 숙제
- [코드업, 3015](https://codeup.kr/problem.php?id=3015)
  - [정답](https://repl.it/@jangka512/ThankfulEllipticalCompilerbug)
- [코드업, 3016](https://codeup.kr/problem.php?id=3016)
  - [정답 v1](https://repl.it/@jangka512/EntireGoldenrodProgramminglanguage)
  - [정답 v2](https://repl.it/@jangka512/MenacingWrongAutocad)
  - [정답 v3](https://repl.it/@jangka512/ExcellentDraftyMemorypool)
  - [정답 v4](https://repl.it/@jangka512/JampackedWoodenUnix)
- [코드업, 3019](https://codeup.kr/problem.php?id=3019)
  - [정답 v1](https://repl.it/@jangka512/IllegalTriangularShelfware)
  - [정답 v2](https://repl.it/@jangka512/LastingChiefSlope)

# 2020-04-21, 09일차
## 문제
### 일반
- [개념, 객체에 편리한 함수 넣어두기](https://code.oa.gg/java8/1645)
- [문제 : 코드업 문제 3019, 오늘의 목표](https://codeup.kr/problem.php?id=3019)
  - [정답 v1](https://repl.it/@jangka512/IllegalTriangularShelfware)
  - [정답 v2](https://repl.it/@jangka512/LastingChiefSlope)

## 오늘의 동영상
- [동영상 - 2020 04 21, 자바, 문제, 코드업 3019, 스케줄 정렬](https://youtu.be/oDLFsIKKcXw)
- [동영상 - 2020 04 21, 자바, 문제, 두 스케줄 아이템의 선후를 정해주세요.](https://youtu.be/m7EJnmxdea0)
- [동영상 - 2020 04 21, 자바, 문제, 코드업 3019, 스케줄 정렬, 자세한 설명](https://youtu.be/ey9Uj4faDLY)

# 2020-04-22, 10일차
## 문제
### 일반
- [문제 : 코드업 문제 3019](https://codeup.kr/problem.php?id=3019)
  - [정답 v1](https://repl.it/@jangka512/IllegalTriangularShelfware)
  - [정답 v2](https://repl.it/@jangka512/LastingChiefSlope)
  
## 상속
- [문제 - 오리 시뮬레이션 만들기 1](https://repl.it/@jangka512/JAVA-5-18-04-20-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-6)
- [문제 - 오리 시뮬레이션 만들기 2](https://repl.it/@jangka512/JAVA-5-18-04-20-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-8)
- [문제 - 오리 시뮬레이션 만들기 3](https://repl.it/@jangka512/JAVA-5-18-04-20-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-10)
- [문제 - 오리 시뮬레이션 만들기 4](https://repl.it/@jangka512/JAVA-5-18-04-20-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-12)
- [문제 - 오리 시뮬레이션 만들기 5](https://repl.it/@jangka512/JAVA-5-18-04-20-13)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-14)
- [문제 - 오리 시뮬레이션 만들기 6](https://repl.it/@jangka512/JAVA-5-18-04-20-15)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-16)
- [문제 - 오리 시뮬레이션 만들기 7](https://repl.it/@jangka512/JAVA-5-18-04-20-17)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-20-18)
- [문제 - 오리 시뮬레이션 만들기 8](https://repl.it/@jangka512/JAVA-5-18-04-20-19)
- [문제 - 복잡한 상속을 통한 문제해결을 하고, 이 방법이 좋지 않은 이유를 설명해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-24-3)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-4)
- [문제 - 로봇오리가 작동하게 해주세요. 단 중복은 없어야 합니다.](http://code.oa.gg/java8/1430)
- [문제 - 정답](https://code.oa.gg/java8/1541)

## 오늘의 동영상
- [동영상 - 2019 12 19, 자바, static 메서드, 매개변수, 리턴](https://www.youtube.com/watch?v=t_LL_2H4wig&feature=youtu.be)
- [동영상 - 2019 12 19, 자바, 상속, 메서드 오버라이딩](https://www.youtube.com/watch?v=dLHIV5Fz7oQ&feature=youtu.be)
- [동영상 - 2019 12 20, 자바, 상속, 메서드 오버라이딩, 2부, 상속의 한계](https://www.youtube.com/watch?v=ceZRsC1HpTc&feature=youtu.be)
- [동영상 - 2019 12 20, 자바, 상속, 메서드 오버라이딩, 3부, 상속의 한계](https://www.youtube.com/watch?v=ApomtKfXZmM&feature=youtu.be)
- [동영상 - 2020 04 22, 자바, 문제, 오리 시뮬레이션 게임 만들기 예제, 1~7](https://www.youtube.com/watch?v=o2JalYbd_qo)
- [동영상 - 2020 04 22, 프론트, 문제, 로봇오리가 적절히 작동하도록 해주세요, 중복제거](https://www.youtube.com/watch?v=7XticzXMQpc)
- [동영상 - 2020 04 22, 자바, 개념, 상속, 캐스팅, 리모콘 버튼 추가/제거, 쉬운설명 버전](https://www.youtube.com/watch?v=epRBcD-xXLc)
- [동영상 - 2020 04 22, 자바, 개념, 상속, 캐스팅, 리모콘 버튼 추가/제거](https://www.youtube.com/watch?v=4wRTunBoTO8)

# 2020-04-23, 11일차
## 문제
### 프로그래머스 자바 입문
- [파트 1 ~ 5](https://programmers.co.kr/learn/courses/5)

### 구름 EDU 생활코딩
- [자바 기초 : 파트 3, 5, 7](https://edu.goorm.io/learn/lecture/16448/생활코딩-java1)
- [자바 제어문 : 파트 2, 3, 5](https://edu.goorm.io/learn/lecture/16908/생활코딩-java-제어문)
- [자바 메서드 : 파트 1, 2](https://edu.goorm.io/learn/lecture/16909/생활코딩-java-method)

### 기타
- 티스토리 블로그에, 글 3개 이상 올려주세요.
  - 필수 게시물
    - 비트와 바이트
    - 컴파일이란?
    - JVM이란?
- 이클립스 단축키
  - ctrl + F11 : Run! (save and Launch)
  - ctrl + 위아래방향키 : 그 줄이 복사된다.
  - ctrl + d : 그 줄이 삭제된다.
  - alt + 위아래 화살표 : 그 줄을 이동시킨다.
  - ctrl + shift + f : 자동정렬

### 일반
- [문제 - 상속을 통한 캐스팅 허용](https://repl.it/@jangka512/JAVA-5-18-04-24-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-6)
- [문제 - 상속을 통한 캐스팅 허용 2](https://repl.it/@jangka512/JAVA-5-18-04-24-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-8)
- [문제 - 무기 클래스로 만든 리모콘으로 칼 객체를 조종](https://repl.it/@jangka512/JAVA-5-18-04-24-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-10)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. int 변수](http://code.oa.gg/java8/1431)
- [문제 - 정답](http://code.oa.gg/java8/1432)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 클래스](http://code.oa.gg/java8/1433)
- [문제 - 정답](http://code.oa.gg/java8/1434)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 레퍼런스 변수(리모콘 변수)](http://code.oa.gg/java8/1435)
- [문제 - 정답](http://code.oa.gg/java8/1436)
- [문제 - 코드의 내용이 의미하는 바를 모두 써주세요. 상속](http://code.oa.gg/java8/1437)
- [문제 - 정답](http://code.oa.gg/java8/1438)
- [문제 - 무기 클래스로 만든 리모콘으로 칼 객체를 조종하는 과정 설명](https://repl.it/@jangka512/JAVA-5-18-04-26-1)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-2)
- [문제 - 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요.](https://repl.it/@jangka512/GoldenLightblueOpengroup)
- [문제 - 정답](https://repl.it/@jangka512/FancyRundownKernel)
- [문제 - 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요, 마지막 공격방식을 기억](https://repl.it/@jangka512/LuxuriousOrneryOutliers)
- [문제 - 정답](https://repl.it/@jangka512/SunnyNeglectedConsulting)

## 오늘의 영상
- [동영상 - 2019 12 22, 자바, 상속을 통한 캐스팅 허용](https://www.youtube.com/watch?v=XHkTh7yAdZU&feature=youtu.be)
- [동영상 - 2019 12 23, 자바, 상속을 통한 캐스팅 허용](https://www.youtube.com/watch?v=2CEJDMJgxHY&feature=youtu.be)
- [동영상 - 2019 12 24, 자바, 객체와 리모콘과 상속과, 캐스팅](https://www.youtube.com/watch?v=3tCQodqk1y4&feature=youtu.be)
- [동영상 - 2019 12 26, 자바, 인스턴스 변수, 지역 변수 생명주기](https://www.youtube.com/watch?v=W-18d_6pwMQ&feature=youtu.be)
- [동영상 - 2019 12 26, 자바, 서술형 문제 정답풀이](https://www.youtube.com/watch?v=OIUlna3FHXc&feature=youtu.be)
- [동영상 - 2020 04 23, 자바, 문제, 상속을 통한 캐스팅 허용](https://youtu.be/eHNwTcxsxfw)
- [동영상 - 2020 04 23, 자바, 문제, 무기 클래스로 만든 리모콘으로 칼 객체를 조종](https://youtu.be/Cx28EM4ne3I)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, int 지역 변수](https://youtu.be/GHRNhSnw_GM)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, 클래스](https://youtu.be/0PXEjtdYc68)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요. 레퍼런스(리모콘) 변수](https://youtu.be/Wu1Qa_-wVrU)
- [동영상 - 2020 04 23, 자바, 문제, 코드의 내용이 의미하는 바를 모두 써주세요, 상속](https://youtu.be/K63ThhiUTYE)
- [동영상 - 2020 04 23, 자바, 문제, 무기 클래스로 만든 리모콘으로 칼 객체를 조종하는 과정 설명](https://youtu.be/CuTnlrsEyD8)
- [동영상 - 2020 04 23, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요.](https://youtu.be/hdcjBDp0jm4)
- [동영상 - 2020 04 23, 자바, 문제, 매개변수를 사용해서 전사가 매번 다르게 공격하도록 해주세요, 마지막 공격방식을 기억](https://youtu.be/iAkuzyj1Edc)

# 2020-04-24, 12일차
## 문제

### 일반
- [문제 - 서로 다른 3종 TV를 서로 다른 종류의 리모콘 3개로 컨트롤](https://repl.it/@jangka512/JAVA-5-18-04-24-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-12)
- [문제 - 서로 다른 3종 TV를 리모콘 1개로 컨트롤](https://repl.it/@jangka512/JAVA-5-18-04-24-13)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-24-14)
- [문제 - 리모콘 형변환 서술형 문제](http://code.oa.gg/java8/1439)
- [문제 - 정답](http://code.oa.gg/java8/1440)
- [문제 - 정수 i가 가지고 있는 10을 double 형 변수에 넣고 해당 변수의 값을 다시 i에 넣기](https://repl.it/@jangka512/TintedDrearyState)
- [문제 - 정답](https://repl.it/@jangka512/PalatableShorttermMice)
- [문제 - 자동차 리모콘으로 페라리 객체를 연결한 후 해당 리모콘이 가리키고 있는 객체를 다시 페라리 리모콘으로 가리키게(참조하게) 하는 코드를 작성해주세요.](https://repl.it/@jangka512/MonthlyYellowPagerecognition)
- [문제 - 정답](https://repl.it/@jangka512/QuestionableQueasyGlitches)
- [문제 - `a무기.공격()` 실행되는 세부적인 과정 기술](https://repl.it/@jangka512/JAVA-5-18-04-26-5)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-6)

### 구성
- [문제 - 사람이 `a왼팔` 이라는 변수를 가질 수 있게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-7)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-8)

## 오늘의 영상
- [동영상 - 2019 12 26, 자바, 리모콘 종류를 줄여야 하는 이유 1](https://www.youtube.com/watch?v=6AvwIYeojTg&feature=youtu.be)
- [동영상 - 2019 12 27, 자바, 리모콘 종류를 줄여야 하는 이유 2](https://www.youtube.com/watch?v=VOPHI6mJ6vY&feature=youtu.be)
- [동영상 - 2019 12 27, 자바, 자동 형변환, 수동 형변환, 구성](https://www.youtube.com/watch?v=8VfMeZylP80&feature=youtu.be)
- [동영상 - 2020 04 24, 자바, 문제, 서로 다른 3종 TV를 서로 다른 종류의 리모콘 3개로 컨트롤](https://www.youtube.com/watch?v=MYaEU7t19K4)
- [동영상 - 2020 04 24, 자바, 문제, 서로 다른 3종 TV를 리모콘 1개로 컨트롤](https://www.youtube.com/watch?v=w5PyO7ESG4k)
- [동영상 - 2020 04 24, 자바, 문제, 자동차 리모콘, 페라리 리모콘 상호변환](https://youtu.be/9-a2pFiVJeI)
- [동영상 - 2020 04 24, 자바, 문제, 사람이 a왼팔 이라는 변수를 가질 수 있게 해주세요.](https://youtu.be/5GHuwQU8Kv0)
- [동영상 - 2020 04 24, IT일반, 개념, ocam 사용법](https://www.youtube.com/watch?v=OML0YbcGVsM)

# 2020-04-27, 13일차
## 문제
### 일반
- [문제 - 전사가 가지고 있는 변수 `a무기`가 칼과 활에 모두 호환되게 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-9)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-10)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.](https://repl.it/@jangka512/JAVA-5-18-04-26-11)
- [문제 - 정답](https://repl.it/@jangka512/JAVA-5-18-04-26-12)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.(매개변수 금지)](https://repl.it/@jangka512/HorribleHomelyTechnology)
- [문제 - 정답](https://repl.it/@jangka512/SpeedyAuthorizedLint)
- [문제 - 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요.](https://code.oa.gg/java8/1543)
- [문제 - 정답](https://code.oa.gg/java8/1544)
- [개념 - 생성자](https://code.oa.gg/java8/1646)
- [문제 - 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요. 생성자, abstrct](https://code.oa.gg/java8/1545)
- [문제 - 정답](https://code.oa.gg/java8/1546)
- [문제 - 정답 v2](https://code.oa.gg/java8/1647)

## 오늘의 동영상
- [동영상 - 2019 12 30, 자바, 구성, 전사와 무기 문제](https://www.youtube.com/watch?v=B0F4rOWD9bg&feature=youtu.be)
- [동영상 - 2019 12 31, 자바, 생성자](https://www.youtube.com/watch?v=L-b8QjGVxWU&feature=youtu.be)
- [동영상 - 2019 12 31, 자바, 구성, 각각의 사람이 좋아하는 음식점에서 음식 주문 문제](https://www.youtube.com/watch?v=QDX6LOy1ddQ&feature=youtu.be)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 가지고 있는 변수 a무기가 칼과 활에 모두 호환되게 해주세요.](https://youtu.be/x3yJ7VQIMJY)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요](https://youtu.be/jeamPav7eGg)
- [동영상 - 2020_04_27, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요, 매개변수 금지](https://youtu.be/IRgYBVgrjDg)
- [동영상 - 2020_04_27, 자바, 문제, 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요](https://www.youtube.com/watch?v=3TiMGrKy4c8)
- [동영상 - 2020_04_27, 자바, 개념, 생성자](https://www.youtube.com/watch?v=__l31-xtH8g)
- [동영상 - 2020_04_27, 자바, 문제, 각각의 사람이 배달음식을 주문할 때, 각자의 상황과 기호에 따라 적절한 음식점과 음식이 배달되도록 해주세요, 생성자, abstract](https://www.youtube.com/watch?v=xHoZ2pHZR1k)

# 2020-04-28, 14일차
## 문제
### 일반
- [문제 - 사람이 정해진 속도대로 달리게 해주세요.](https://code.oa.gg/java8/1549)
- [문제 - 정답](https://code.oa.gg/java8/1550)
- [문제 - 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요.(매개변수 사용금지)](https://code.oa.gg/java8/1547)
- [문제 - 정답](https://code.oa.gg/java8/1548)
- [문제 - 정답 v2](https://code.oa.gg/java8/1648)
- [문제 - 정답 v3](https://code.oa.gg/java8/1649)
- [문제 - 올바른 리턴타입으로 메서드를 만들어주세요.](https://code.oa.gg/java8/1551)
- [문제 - 정답](https://code.oa.gg/java8/1552)
- [문제 - 정답 v2](https://code.oa.gg/java8/1650)
- [문제 - 인력관리소를 운영해주세요.](http://code.oa.gg/java8/1442)
- [문제 - 힌트](http://code.oa.gg/java8/1443)
- [정답 - 힌트2](http://code.oa.gg/java8/1444)
- [문제 - 힌트3](http://code.oa.gg/java8/1445)
- [문제 - 정답 v1](http://code.oa.gg/java8/1446)
- [문제 - 정답 v2(배열사용버전)](https://code.oa.gg/java8/1651)

### 코드업
- [문제 : 코드업 문제 4012](https://codeup.kr/problem.php?id=4012)
- [문제 : 코드업 문제 4751](https://codeup.kr/problem.php?id=4751)

## 오늘의 동영상
- [동영상 - 2020 01 02, 자바, 매개변수 없이 객체에 정보를 넘겨서, 메서드가 적절하게 수행되도록 하기](https://www.youtube.com/watch?v=PFtoxFsgngM&feature=youtu.be)
- [동영상 - 2020 01 02, 자바, 인력관리소 문제 1](https://www.youtube.com/watch?v=NSfhW7RpMUA&feature=youtu.be)
- [동영상 - 2020 01 02, 자바, 함수의 호출 부분을 기준으로 메서드 구현시, 리턴타입 맞추기](https://www.youtube.com/watch?v=25RQu7o7-OQ&feature=youtu.be)
- [동영상 - 2020 04 28, 자바, 문제, 올바른 리턴타입으로 메서드를 만들어주세요](https://www.youtube.com/watch?v=HS_4w3vYNHI)
- [동영상 - 2020 04 28, 자바, 문제, 사람이 정해진 속도대로 달리게 해주세요](https://www.youtube.com/watch?v=K2P1j65yVdE)
- [동영상 - 2020 04 28, 자바, 문제, 전사가 들고 있는 무기에 의해서 서로 다른 공격형태를 보이도록 해주세요, 매개변수 금지](https://www.youtube.com/watch?v=8VOq9M52S3U)
- [동영상 - 2020_04_28, 자바, 문제, 인력관리소를 운영해주세요, 배열금지](https://www.youtube.com/watch?v=FyNY8WVfeak)
- [동영상 - 2020_04_28, 자바, 문제, 인력관리소를 운영해주세요, 배열사용](https://www.youtube.com/watch?v=OVJh-RqGryA)

# 2020-04-29, 15일차
## 문제
### 일반
- [문제 - 사람이 사람을 가리키도록 해주세요.](https://code.oa.gg/java8/1652)
- [문제 - 정답](https://code.oa.gg/java8/1654)
- [문제 - 인력관리소를 운영해주세요.(어제 문제)](http://code.oa.gg/java8/1442)
- [문제 - 정답 v3](http://code.oa.gg/java8/1447)
- [문제 - 정답 v4](http://code.oa.gg/java8/1448)
- [문제 - 전사가 무기가 없다면 공격을 못해야 합니다.](https://code.oa.gg/java8/1557)
- [문제 - 정답](https://code.oa.gg/java8/1558)
- [문제 - 전사가 무기를 여러개 다룰 수 있어야 합니다.](https://code.oa.gg/java8/1553)
- [문제 - 정답](https://code.oa.gg/java8/1554)
- [문제 - 전사가 무기를 여러개 다룰 수 있어야 합니다. v2](https://code.oa.gg/java8/1555)
- [문제 - 정답](https://code.oa.gg/java8/1556)

## 오늘의 동영상
- [동영상 - 2020 01 03, 자바, 예제, 인력관리소는 사람 3명까지 관리 할 수 있어야 합니다, V3](https://www.youtube.com/watch?v=0cXHL3zOr2M&feature=youtu.be)
- [동영상 - 2020 01 03, 자바, 예제, 무기가 없을 때 전사는 공격을 할 수 없다](https://www.youtube.com/watch?v=Mn7-NlwpKjE&feature=youtu.be)
- [동영상 - 2020 01 03, 자바, 예제, 전사는 무기를 여러개 다룰 수 있습니다, 버전 1](https://www.youtube.com/watch?v=Qw7As9rkS8s&feature=youtu.be)
- [동영상 - 2020 01 03, 자바, 예제, 전사는 무기를 여러개 다룰 수 있습니다, 버전 2](https://www.youtube.com/watch?v=lbiUAq_-qQM&feature=youtu.be)
- [동영상 - 2020_04_29, 자바, 문제, 사람이 사람을 가리키도록 해주세요](https://www.youtube.com/watch?v=qN7NbNqJ1KM)
- [동영상 - 2020_04_29, 자바, 문제, 사람이 사람을 가리키도록 해주세요, 설명 v2](https://youtu.be/axAGkIO1gzs)
- [동영상 - 2020_04_29, 자바, 문제, 전사가 무기가 없다면 공격을 못해야 합니다](https://youtu.be/jiOgGGxSUvc)
- [동영상 - 2020_04_29, 자바, 문제, 전사가 무기를 여러개 다룰 수 있어야 합니다](https://youtu.be/Zk6BbZUt8pg)

# 2020-05-04, 16일차
## 프로그래밍 언어 활용 시험
- [시험내용](https://codepen.io/jangka44/debug/oNjoyPG)

# 2020-05-06, 17일차
## 문제
### 일반
- [문제 - 텍스트 게시판 기능 구현, 문제 1](https://to2.kr/bj1)
- [힌트 1](https://repl.it/@jangka512/HalfInvolvedMenu)
- [힌트 2](https://repl.it/@jangka512/AquamarineStaidTerabyte)

## 펌웨어/임베디드
### 전기개념
- 양극(애노드), 캐소드(음극)
  - 산화가 일어나는 전극이 anode(양극), 환원이 일어나는 전극이 cathode(음극).
  - 화학전지나 연료전지의 경우, 환원전위가 높은 금속이 환원전극, 즉 cathode(음극)가 되고, 이것이 곧 (+)극이 된다.
  - 환원전위가 낮은 금속은 산화전극, 즉 anode(양극)가 되고, 이것이 곧 (-)극이 된다.
- 전자의 흐름과 전류의 흐름은 반대이다.
  - 음전하를 띤 물체와 양전하를 띤 물체를 도선으로 연결하면 반드시 전류가 흐른다.
  - 이 경우 전류는 양극 쪽에서 음극 쪽으로 흐르는 것으로 규정돼 있는데 이것은 과학현상을 설명하기 위한 일종의 약속이다.
  - 과학자들이 양전하 쪽을 전류가 흐르는 방향으로 정했기 때문이다.
  - 그러나 실제 전자의 이동 방향은 다르다.
  - 음극 쪽에는 전자가 많고 양극 쪽에는 부족하기 때문에 결국 양전하와 음전하를 띤 두 물체를 도선으로 연결하면 음극에서 양극으로 전자가 이동해 부족한 부분을 메워주는 것이다.
  - 양쪽의 전자 양이 같아지면 전자의 이동은 중지된다.
  - 따라서 전류의 방향과 전자의 방향은 서로 반대다.
- LED는 음극, 양극이 있다.
- 배터리는 음극, 양극이 있다.
- 레지스터에는 음극과 양극이 없다.

### 전기관련 질문
- 저항은 음극, 양극이 따로 없나요?
- 저항은 다이오드 전에 달아도 되고, 뒤에 달아도 차이가 없나요?

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 06, 자바, 문제, 콘솔 게시판을 만들어주세요, 문제 1 힌트](https://www.youtube.com/watch?v=_tQaaAb-P4k)

# 2020-05-07, 18일차
## 문제
### 일반
- [문제 - 텍스트 게시판 기능 구현, 문제 1](https://to2.kr/bj1)
- [힌트 : 현재시간 구하기](https://code.oa.gg/java8/1655)
- [정답 1](https://repl.it/@jangka512/CyberGargantuanStructures)
- [정답 2](https://repl.it/@jangka512/EquatorialEvenOperation)

### C언어
- [챕터, 01 ~ 02](https://to2.kr/bj6)

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 07, 자바, 문제, 콘솔 게시판을 만들어주세요, 문제 1 정답 1](https://youtu.be/AD14Iet0_hE)

## 펌웨어/임베디드
### 전기개념
- 옴의 법칙
  - 전류는 전자가 흐르는 양
  - 저항은 전류가 흐르는 것을 방해하는 힘(장애물)
  - V = IR
  - 전압 = 전류 * 저항
- 과전류
  - 전류가 너무 많이 흘르는 것
  - LED에 과전류가 흐르면 타버린다.

### 전기관련 질문
- 과전압으로는 문제가 되지 않나요?
- 전지(배터리)의 양극단을 저항없이 도선으로 연결하면 무슨일이 발생하나요?

## 오늘의 숙제
- 텍스트 게시판 기능 구현, 문제 1 관련
  - `정답 1`을 혼자힘으로 구현할 수 있어야 합니다.
  - `정답 2`에 `정답 1`의 기능을 넣어주세요.
- C언어 챕터 01, 02의 문제를 전부(총 7문제) 풀어주세요.

# 2020-05-08, 19일차
## 문제
### 일반
- [문제 - 텍스트 게시판 기능 구현, 문제 1](https://to2.kr/bj1)

### C언어
- [챕터, 03](https://to2.kr/bj6)

### 팅커캐드
- [예제 1](https://ncs.oa.gg/resource/2020-fullstack-course/common/aduino/thinkercad/exam_1.jpg)

## 오늘의 숙제
- 텍스트 게시판 기능 구현, 문제 1 관련
- C언어 챕터 03의 문제를 전부 풀어주세요.

## 오늘의 동영상
### 팅커캐드
- [동영상 - 2020 05 07, 아두이노, 문제, 연속적으로 LED 4개가 깜빡이도록 해주세요](https://youtu.be/73OyfnHe_MY)

# 2020-05-11, 20일차
## 자바 콘솔 게시판
- [작업 2, 처음부터 시작](https://to2.kr/bkq)

# 2020-05-12, 21일차
## 자바 콘솔 게시판
- [작업 2, 처음부터 시작](https://to2.kr/bkq)

## 자바 콘솔 게시판 심화
- [요구사항 확인](https://to2.kr/bky)

## 펌웨어 구현환경구축 시험
- [시험내용](https://codepen.io/jangka44/debug/zYvXaGj)

## 오늘의 동영상
- [동영상 - 2020_05_12, 아두이노, 개념, 아두이노 스튜디오 설치](https://www.youtube.com/watch?v=zMlMtKCloU8)

# 2020-05-13, 22일차
## 블로그 정리
### 자바 기초
- 상속과 구성
- 지역변수와 인스턴스 변수 차이
- 디자인패턴 스트래티지 패턴
- 절차지향 언어와 객체 지향 언어의 차이는?

### IT 기초
- 기계어와 어셈블리언어
- 컴파일과 컴파일러
- 부동소수점과 그 한계
- 메모리 영역
  - 코드영역, 데이터 영역, 힙 영역, 스택 영역
- 동적할당, 정적할당, 가비지컬렉터
- 서버
- HTTP와 웹서버 그리고 브라우저의 관계
- 데이터베이스
- 윈도우 응용 프로그래머(카카오톡 PC 버전의 개발자)와 웹 프로그래머(네이버 사이트 개발자)의 직무 차이

### 자바 관련 상식
- J2EE, JDK, JRE, J2SE 차이
- JVM과 바이트코드
- CGI와 서블릿 그리고 톰캣

### 자바 의존성 관련
- 의존성과 라이브러리, 그리고 Jar
- 메이븐과 의존성 관리도구
- 프레임워크와 자바 웹개발 관련 프레임워크

# 2020-05-14, 23일차
## 문제
### 일반
- 문제 - 짝 프로그래밍으로 콘솔 게시판 구현

# 2020-05-15, 24일차
## 문제
### 일반
- 문제 - 짝 프로그래밍으로 콘솔 게시판 구현

# 2020-05-18, 25일차
## 문제
### 일반
- [문제 - 영속저장이 가능한 버전의 콘솔 게시판 구현](https://codepen.io/jangka44/debug/MWazYLR)

## C언어
- [챕터, 03 ~ 12](https://to2.kr/bj6)

## 아두이노
### 일반
- [문제 - 버튼에 반응하는 기기를 만들어주세요.](https://codepen.io/jangka44/debug/xxwQGpq)

# 2020-05-19, 26일차
## 펌웨어 구현 시험
- [시험내용](https://codepen.io/jangka44/debug/BaoGPgK)

# 2020-05-20, 27일차
## 문제
### 일반
- [문제 - 영속저장이 가능한 버전의 콘솔 게시판 구현](https://codepen.io/jangka44/debug/MWazYLR)
- [개념 - 잭슨(Json 라이브러리) 사용법, 쓰기](https://repl.it/@jangka512/ExcitingHastyProblems)
- [개념 - 잭슨(Json 라이브러리) 사용법, 읽기](https://repl.it/@jangka512/DeficientThoseGigahertz)

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 20, 자바, 개념, JSON 파서 직접 구현](https://youtu.be/NLKhiTTkpWI)
- [동영상 - 2020 05 20, 자바, 개념, 이클립스에 라이브러리 직접 추가](https://youtu.be/IbQG5Bp1yzk)
- [동영상 - 2020 05 20, 자바, 개념, 잭슨 사용법](https://youtu.be/t2DkuxVOw0E)

# 2020-05-21, 28일차
## 문제
### 일반
- [개념 - 잭슨(Json 라이브러리) 으로 간단한 텍스트 게시판 만들기](https://repl.it/@jangka512/FlamboyantNaturalInstructions)

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 21, 자바, 개념, MVC 텍스트 게시판, 1부(녹음안됨, 망함 ㅠㅠ)](https://youtu.be/Mn7ju8PLsJk)
- [동영상 - 2020 05 21, 자바, 개념, MVC 텍스트 게시판, 2부(녹음안됨, 망함 ㅠㅠ)](https://youtu.be/-dMqQNrirzM)
- [동영상 - 2020 05 21, 자바, 개념, MVC 텍스트 게시판, 3부(녹음안됨, 망함 ㅠㅠ)](https://youtu.be/WkseiaKbVM0)

# 2020-05-22, 29일차
## 문제
### 일반
- [개념 - List, ArrayList, 1, int 데이터 다루기](https://code.oa.gg/java8/1656)
- [개념 - List, ArrayList, 2, int 데이터 다루기, 반복문](https://code.oa.gg/java8/1657)
- [개념 - List, ArrayList, 3, int 데이터 다루기, 반복문](https://code.oa.gg/java8/1658)
- [개념 - MVC, 텍스트 게시판 구현, 작업 1](https://repl.it/@jangka512/QuietPerfectConnection)
- [개념 - MVC, 텍스트 게시판 구현, 작업 2](https://repl.it/@jangka512/LawfulPinkGeeklog)
- [개념 - MVC, 텍스트 게시판 구현, 작업 3](https://repl.it/@jangka512/MistyroseCruelApplicationsuite)
- [개념 - MVC, 텍스트 게시판 구현, 작업 4](https://repl.it/@jangka512/PlushFrayedMatch)
- [개념 - MVC, 텍스트 게시판 구현, 작업 5](https://repl.it/@jangka512/SoreAnxiousTruetype)

## PHP
- [개발환경 설치 및 변수](https://to2.kr/bmS)

## 오늘의 동영상
### 일반
- [동영상 - 2020_05_22, 자바, 개념, MVC, 텍스트 게시판 구현, 작업 1(음성싱크, 안맞음, 망함)](https://youtu.be/7fQqKpL7lT4)
- [동영상 - 2020_05_22, 자바, 개념, MVC, 텍스트 게시판 구현, 작업 2(녹음안됨, 망함 ㅠㅠ)](https://youtu.be/rqoqc6ujZDU)
- [동영상 - 2020_05_22, 자바, 개념, MVC, 텍스트 게시판 구현, 작업 3(음성싱크, 안맞음, 망함)](https://youtu.be/AaSZ0U-MPF0)
- [동영상 - 2020_05_22, 자바, 개념, MVC, 텍스트 게시판 구현, 작업 4](https://youtu.be/_IR0wsGY4O8)
- [동영상 - 2020_05_22, 자바, 개념, MVC, 텍스트 게시판 구현, 작업 5](https://youtu.be/lKq_i9b-5JQ)

## 숙제
### MVC 텍스트 게시판 구현
- 조건
  - [개념 - MVC, 텍스트 게시판 구현, 작업 5](https://repl.it/@jangka512/SoreAnxiousTruetype)에서 시작해주세요.
  - 로그인 기능 추가
    - MemberController 클래스에, Member loginedMember; 필드 추가
      - 여기에 null이 들어있으면, 로그아웃 상태
      - 여기에 null이 아닌 리모콘이 들어있다면, 로그인 상태
        - 해당 리모콘이 가리키고 있는 Member 객체가 로그인 된 회원
  - 로그아웃 기능
  - 회원리스트 출력 기능 추가
  - 회원탈퇴 기능 추가
  - 회원정보수정 기능 추가(선택)

# 2020-05-25, 30일차
## PHP
- [웹서버 연동, DB 연동](https://to2.kr/bmS)

# 2020-05-26, 31일차
## PHP
- [게시물 CRUD](https://to2.kr/bmS)
- [개념 : PHP 개발환경 설치 메뉴얼](https://codepen.io/jangka44/debug/abvxjbe)

## 임베디드 애플리케이션 구현환경구축 시험
- [시험내용](https://codepen.io/jangka44/debug/ZEbZRQv)

# 2020-05-27, 32일차
## 피그마
### 일반
- [피그마](https://www.figma.com)
- 구글 계정으로 가입

## 코드펜
### 일반
- [새 코드펜(새 연습장)](https://codepen.io/pen)
  - 로그인 후 세팅
- [위키 템플릿](https://to2.kr/bnb)
  - 포크해서 쓰세요.

## 오늘의 동영상
### 일반
- [동영상 - 2020 05 27, 자바, 개념, 콘솔게시판 구현시, 파일DB관련 클래스 구조 설명](https://youtu.be/HiN_uA1xbPc)
- [동영상 - 2020 05 27, IT일반, 개념, 코드펜 위키 만들기](https://youtu.be/EL5sVXWuewQ)

### 피그마
- [동영상 - 2020 05 27, 피그마, 개념, 버튼 컴포넌트 만들기](https://youtu.be/pA6ciB0Qc_A)

# 2020-05-28, 33일차
## 피그마
### 일반
- [피그마](https://www.figma.com)
- 2인1조로 하여 앱 프로토타이핑

## 오늘의 동영상
### 피그마
- [동영상 - DESIGN 01 [Figma 1/4] 인싸 디자인 프로그램!! figma(피그마)로 디자인과 프로토타이핑을 동시에 하자! by Ezweb](https://www.youtube.com/watch?v=_myVwi0vq5s)
- [동영상 - DESIGN 02 [Figma 2/4] 핫! 디자인 프로그램 피그마! - 펜툴, 쉐이프, 컴포넌트 by Ezweb](https://www.youtube.com/watch?v=e9YZPOUnv14)
- [동영상 - DESIGN 03 [Figma 3/4] 피그마! - 로그인 입력폼, 버튼, 메인화면 완성하기 by Ezweb](https://www.youtube.com/watch?v=63OpBisj6DY)
- [동영상 - DESIGN 04 [Figma 4/4] 피그마! - 구글 material design 소스 활용, css 코드 확인하기, image 저장하기 by Ezweb](https://www.youtube.com/watch?v=eGkJAxKmEDE)

# 2020-05-29, 34일차
## 피그마
### 작업물 발표
- 1조 : 이용빈, 김용순
  - [결과](https://to2.kr/bnz)
- 2조 : 김혜련, 김일구
  - [결과](https://to2.kr/bnN)
- 3조 : 임현호, 이우석
  - [결과](https://to2.kr/bnH)
- 4조 : 채영지, 양정원
  - [결과](https://to2.kr/bnU)
- 5조 : 봉영근, 이정한
  - [결과](https://to2.kr/bnR)
- 6조 : 하승범, 신정용
  - [결과](https://to2.kr/bnK)
- 7조 : 노경욱, 성소연
  - [결과](https://to2.kr/bnW)
  - [결과](https://to2.kr/bnS)
- 8조 : 김동연, 이상범
  - [결과](https://to2.kr/bnQ)
- 9조 : 김혜지, 정성훈
  - [결과](https://to2.kr/bnG)
  - [결과](https://to2.kr/bnO)
- 10조 : 신성민
  - [결과](https://to2.kr/bnY)

## 오늘의 동영상
### 피그마
- [동영상 - 2020 05 29, 피그마, 가이드 선, 그리드, 오토 레이아웃](https://youtu.be/wTAEBQx1y5Q)

### 작업물 코멘트
#### 1조(4점)
- 자주쓰는앱이라서 추천합니다~
- prototype도 적절히 사용했으며, 원본 배민과 흡사했다.
- 페이지수가 많음
- 배달의 민족을 정말 잘 카피 함.

#### 2조(3점)
- 고생한게 보이고 페이지수도 많아 볼것이 많았습니다.
- 내용이 많아 많은정성을 들여 만든것같아서.
- 페이지가 많아서가 많고 좋음

#### 4조(5점)
- 멜론을 모방하여 만들었는데 전체적으로 깔끔했던것같습니다.
- 기능구현도 많이 했고 완성도도 높은거 같다고 생각한다.
- 핸드폰 기종에 따른 화면 자동맞춤 기능을 구현했다는 점에서 추천을 하고 싶습니다.
- 멜론의 디테일한 부분도 구현한것이 인상깊었습니다.
- 깔끔하게 잘 만들었다.

#### 5조(7점)
- 짧지만 신기한 기능들이 많았습니다
- 효과가 독특. (스크롤시 바뀌는것)
- 인터페이스가 깔끔하고 시작할 때 별모양 아이콘들이 움직이는게 인상깊었습니다.
- 전체적 구성이 완벽하다
- 독창적이었고 별의 움직임 표현이 괜찮았습니다.
- 내가 구현 못한 특이한 기능이있어서
- 시작이 신선해서

#### 8조(2점)
- 기존에 없는 사이트에 기존에 없는 기능들이 신선하고 재밌었습니다 !
- 기존에 있던 비슷한 앱에서 있었으면 하는,  필요로 하는 기능(누군가도 필요로 하는) 을 창작하여 구현해내어 추천! 

#### 9조(13점)
- 너무 디테일하고 완성작이 아니여도 진짜 홈페이지 인줄 알았습니다
- 페이지가 가장 자연스럽다.
- 가장 완성도가 높아서
- 완성도가 높고 작업량도 많아서 열심히 한게 눈에 보인다.
- 앱과 웹 둘다 만들면서 구현을 잘해서
- 다양한 트랜지션과 실제 쇼핑몰처럼 잘 구현함
- 웹이 디자인도 깔끔하고 상품 위에 마우스를 올리면 이미지가 전환되는 기능, 글 자위에 마우스를 올리면 붉은색으로 변하는 기능을 구현한것이 인상깊었습니다.
- 1개의 주제를 앱과 피씨에서 사용할 수 있도록 나눠서 구현한 점과  여러가지 기능들을 적절히 활용하여 정말 의류 구매를 하기 위해 접속한 것 같은 느낌을 받아서 추천합니다!
- 기능이 많고 웹 모바일 둘다 만들어서
- prototype 기능도 적절했고 창작했다는점과 일단 컨셉도 깔끔했다.
- 완성도가 좋아서
- 웹과 앱을 다 구현, 페이지 많음, 디자인이 멌있다.
- 구현페이지가 깔끔해서

#### 10조(2점)
- 혼자해서 했는데 잘했다.
- 혼자 작성했음에도 불구하고 2개를 만들고 열심히 한것같은 정성이 보여서

## FLEX
### 일반
- [flex 튜토리얼, Froggy](https://flexboxfroggy.com/#ko)
- [flex 튜토리얼, flex defense](http://www.flexboxdefense.com/)

# 2020-06-01, 35일차
## 문제
### 자바기초/심화
- [자바기초](https://to2.kr/bf1)

### 일반
- [개념 : 생성자 오버로딩](https://code.oa.gg/java8/1663)
- [개념 : 생성자 super](https://code.oa.gg/java8/1664)
- [개념 : 인터페이스](https://code.oa.gg/java8/1665)
- [개념 : 예외처리, 부하직원이 처리](https://code.oa.gg/java8/1667)
- [개념 : 예외처리, 본인이 처리](https://code.oa.gg/java8/1666)
- [개념 : 예외처리, 사용자 예외(커스텀 예외), 예외 직접 발생시키기](https://code.oa.gg/java8/1668)

## 오늘의 동영상
### 일반
- [동영상 - 2020_06_01, 자바, 개념, 생성자 오버로딩](https://youtu.be/rx-R2MUJTm8)
- [동영상 - 2020_06_01, 자바, 개념, 생성자 연쇄호출, super, 1부](https://youtu.be/2UULQZW3mxg)
- [동영상 - 2020_06_01, 자바, 개념, 생성자 연쇄호출, super, 2부](https://youtu.be/zmxSJY41X1o)
- [동영상 - 2020_06_01, 자바, 개념, 인터페이스](https://youtu.be/I4JrPYhI2Qs)
- [동영상 - 2020_06_01, 자바, 개념, 예외처리](https://youtu.be/-VjsQCd5HE0)

### PHP
- [동영상 - 2020_06_01, PHP, 개념, 게시물 CRUD](https://youtu.be/_MdQaS-mRak)

## 숙제
### 일반
- 프로그래머스 자바 입문, 처음부터 끝까지, 60분안에 풀기
  - 힌트 안보고 풀기
- CSS flex 게임 2개 진행

# 2020-06-02, 36일차
## 문제
### 리액트 기초
- [리액트 기초](https://to2.kr/bof)

## 오늘의 동영상
### 일반
- [동영상 - 2020_06_02, 리액트, 개념, CSS 선택자, HTML 관계](https://youtu.be/G1UR513wG_c)
- [동영상 - 2020 06 02, 리액트, 개념, 컴포넌트 기초  1부](https://youtu.be/zN3GL0QwVFc)
- [동영상 - 2020 06 02, 리액트, 개념, 컴포넌트 기초  2부](https://youtu.be/8-jG0s0HIDQ)

# 2020-06-03, 37일차
## 문제
### 리액트 기초
- [리액트 기초](https://to2.kr/bof)

### 프론트
- [개념 : HTML, CSS, JS 개요](https://codepen.io/jangka44/pen/vzYVYb)
- [개념 : HTML, CSS, JS 개요 2](https://codepen.io/jangka44/pen/YzXwLJP)
  - 책내용 : 23p
- [개념 : display 속성 정리](https://codepen.io/jangka44/live/ZjbpPR)
  - 책내용 : 238p
- [문제 : div, section, article 태그를 사용해서 3가지 색의 막대를 만들어주세요.](https://codepen.io/jangka44/pen/KBdaGd)
  - 책내용 : 92p
  - 제한시간 : 1분 30초
- [문제 - 정답예시](https://codepen.io/jangka44/debug/VBvPeM)
- [문제 - 정답](https://codepen.io/jangka44/pen/VBvPeM)
- [문제 : section(green 색 막대)과 article(blue 색 막대)을 한 줄에 보이게 해주세요.](https://codepen.io/jangka44/pen/qyOqrE)
  - 책내용 : 238p
  - 제한시간 : 1분 30초
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/WKQRGW)
- [문제 - 정답](https://codepen.io/jangka44/pen/WKQRGW)
- [개념 : 엘리먼트(배우)의 부모, 자식, 형제관계](https://codepen.io/jangka44/pen/bQqbpZ)
  - 책내용 : 274p
- [개념 : 글자](https://codepen.io/jangka44/pen/mjPYqy)
  - 책내용 : 92p
- [문제 - 다음 글자를 똑같은 스타일로 만들어주세요.](https://codepen.io/jangka44/debug/mjPYqy)
- [문제 - 정답](https://codepen.io/jangka44/pen/mjPYqy)
- [개념 : a와 br](https://codepen.io/jangka44/pen/zLqQam)
  - 책내용 : 84p
- [문제 : 각각의 사이트 명에 링크를 걸어주세요.(새창으로 떠야 합니다.)](https://codepen.io/jangka44/pen/ZEEMPpy)
  - 책내용 : 84p
  - 제한시간 : 1분 30초
- [문제 - 정답예시](https://codepen.io/jangka44/debug/mddGorY)
- [문제 - 정답](https://codepen.io/jangka44/pen/mddGorY)
- [문제 : 젠코딩으로 네이버, 구글, 다음으로 가는 링크를 만들어주세요.](https://codepen.io/jangka44/pen/pBWYyR)
  - 책내용 : 84p
- [문제 - 정답](https://codepen.io/jangka44/pen/pBWYyR)
- [개념 : 후손 선택자](https://codepen.io/jangka44/pen/KBzjzB)
  - 책내용 : 274p
- [문제 : 각각의 크기와 색상이 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/JaoBNQ)
  - 책내용 : 274p
- [문제 - 정답예시](https://codepen.io/jangka44/debug/yxyqQQ)
- [문제 - 정답](https://codepen.io/jangka44/pen/yxyqQQ)
- [개념 : text-align](https://codepen.io/jangka44/pen/JBXgNy)
  - 책내용 : 92p
- [개념 : text-align v2](https://codepen.io/jangka44/pen/JaLwpr)
  - 책내용 : 92p
- [문제 : 각각의 크기와 색상과 정렬상태가 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/XPJByw)
  - 책내용 : 92p
- [문제 - 정답예시](https://codepen.io/jangka44/debug/rZarQb)
- [문제 - 정답](https://codepen.io/jangka44/pen/rZarQb)
- [개념 : hover](https://codepen.io/jangka44/pen/KBzOKj)
  - 책내용 : 281p
- [문제 : 100칸 짜리 바둑판을 만들고 하나의 칸에 마우스를 올리면 해당 칸의 배경색이 바뀌도록 해주세요.](https://codepen.io/jangka44/pen/VGYGvp)
  - 책내용 : 281p
- [문제 - 예시정답](https://codepen.io/jangka44/debug/QVwVWZ)
- [문제 - 정답](https://codepen.io/jangka44/pen/QVwVWZ)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/vYOKqNg)
- [개념 : short code(자식, 인접동생)](https://codepen.io/jangka44/pen/rreXjL)
  - 책내용 : 274p, 280p
- [개념 : `nbsp;`](https://codepen.io/jangka44/pen/yqOmxJ)
  - 책내용 : 59p
- [개념 : text-deocration:none](https://codepen.io/jangka44/pen/eLNreO)
- [문제 : bnx 사이트의 상단 메뉴까지 구현해주세요.](https://codepen.io/jangka44/pen/oMxKZp)
- [문제 - 정답예시 v1](https://codepen.io/jangka44/debug/wQdWEG)
- [문제 - 정답 v1](https://codepen.io/jangka44/pen/wQdWEG)
- [문제 - 정답예시 v2](https://codepen.io/jangka44/debug/ejZqvz)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/ejZqvz)
- [문제 - 정답예시 v3](https://codepen.io/jangka44/debug/zLBYrQ)
- [문제 - 정답 v3](https://codepen.io/jangka44/pen/zLBYrQ)
- [문제 - 정답예시 v4](https://codepen.io/jangka44/debug/PxjWxY)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/PxjWxY)
- [문제 - 정답예시 v5](https://codepen.io/jangka44/debug/qMdKbP)
- [문제 - 정답 v5](https://codepen.io/jangka44/pen/qMdKbP)

## 동영상
### 프론트
- [동영상 - 2020 02 14, 프론트, 개념,CODEPN 세팅법](https://www.youtube.com/watch?v=4V6jglwjcvg&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS의 역할](https://www.youtube.com/watch?v=6nFdDSWj9JU)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS을 공부하는 효율적인 방법](https://www.youtube.com/watch?v=A_vHA7ALWRk&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, display 속성](https://www.youtube.com/watch?v=OjLElVktdos&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 문제, 막대 3개 구현, 밑에 2개는 한 줄에](https://www.youtube.com/watch?v=ltAum2lXzNo&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 문제, 막대 3개 구현](https://www.youtube.com/watch?v=U2F6Q2hfWKA&feature=youtu.be)
- [동영상 - 2020 02 17, 프론트, 개념, display, 엘리먼트, 선택자](https://www.youtube.com/watch?v=JGQKuf9uS3E)
- [동영상 - 2020 02 17, 프론트, 개념, 엘리먼트 관계, 부모자식 관계, 형제관계](https://www.youtube.com/watch?v=yE0EyLlnBdE)
- [동영상 - 2020 02 17, 프론트, 개념, font weight, letter spacing, font size](https://www.youtube.com/watch?v=yfgMvBiPzYc)
- [동영상 - 2020 02 17, 프론트, 개념, 앵커태그, br 태그](https://www.youtube.com/watch?v=tsb7ssDcd9k)
- [동영상 - 2020_05_12, 프론트, 개념, 코드팬을 티스토리에 포스팅하기](https://youtu.be/kdS1wHq1irY)
- [동영상 - 2020 02 17, 프론트, 개념, 젠코딩 emmet](https://www.youtube.com/watch?v=rRapBHkt6gU)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, 후손 선택자](https://www.youtube.com/watch?v=oVuSYCYjtaU)
- [동영상 - 2020 02 17, 프론트, 문제, 색상과 크기가 다른 3가지 링크를 만들어주세요](https://www.youtube.com/watch?v=AcIq02YTOdY)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, text align](https://www.youtube.com/watch?v=d2JjVUMkOoI)
- [동영상 - 2020 05 14, 프론트, 문제, 젠코딩으로 네이버, 구글, 다음으로 가는 링크를 만들어주세요](https://youtu.be/uhkfnFYd-8Y)
- [동영상 - 2020 05 14, 프론트, 개념, 후손선택자](https://youtu.be/EwW9XWqFDkw)
- [동영상 - 2020 05 14, 프론트, 문제, 각각의 크기와 색상이 다른 링크 버튼 3개 만들어주세요](https://youtu.be/ePCg9DBpoeQ)
- [동영상 - 2020 05 14, 프론트, 개념, text-align](https://youtu.be/elypw0w9SsU)
- [동영상 - 2020 05 14, 프론트, 문제, 각각의 크기와 색상과 정렬상태가 다른 링크 버튼 3개 만들어주세요](https://youtu.be/iP3jqv5vs1Y)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, hover](https://www.youtube.com/watch?v=N7BlAxCMFS4&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, CSS, 100칸짜리 바둑판형태, hover](https://www.youtube.com/watch?v=tgj8dKLestk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, `nbsp;`](https://www.youtube.com/watch?v=5LeDdCf4Cso&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, text decoration](https://www.youtube.com/watch?v=JZtjOcZkIGg&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 선택자, 자식, 후손, text align, display](https://www.youtube.com/watch?v=cCaWklKPsLs&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 자식선택자, 인접동생선택자](https://www.youtube.com/watch?v=kRU98SwRrnQ&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 1차](https://www.youtube.com/watch?v=SJEhyNfSsD4&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 2차](https://www.youtube.com/watch?v=egc7Njqkddk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 3차](https://www.youtube.com/watch?v=KlwqR4rNv0g&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 4차](https://www.youtube.com/watch?v=Kin-u_fjiNI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 5차](https://www.youtube.com/watch?v=kD0uXVLGy14&feature=youtu.be)
- [동영상 - 2020_05_19, 프론트, 개념, short code, 자식, 인접동생](https://youtu.be/gRdXL18le-s)
- [동영상 - 2020_05_19, 프론트, 개념, nbsp, 빈칸](https://youtu.be/YarVgrbCppQ)
- [동영상 - 2020_05_19, 프론트, 개념, text-decoration](https://youtu.be/9hfaqbXPPlo)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v1](https://youtu.be/ZWxQETnqsoE)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v2, 힌트](https://youtu.be/YkwIcWLf_X8)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v2, v3](https://youtu.be/uU09WXWHavs)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v4, v5](https://youtu.be/9wFQFGWepnM)
- [동영상 - 2020 06 03, 프론트, 개념, HTML, CSS 기초](https://youtu.be/QRIJvBjCxks)
- [동영상 - 2020 06 03, 프론트, 개념, HTML, CSS 기초, 2부](https://youtu.be/1fz0IkC7Vgs)
- [동영상 - 2020 06 03, 프론트, 개념, HTML, CSS 기초, 3부](https://youtu.be/xmpiYsOcKDw)
- [동영상 - 2020 06 03, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v4, v5](https://youtu.be/t-iXHmbEmEk)

# 2020-06-04, 38일차

## 개념
- HTML
  - 관계
    - 부모/자식
    - 형/동생
  - 태그
    - 인라인 계열
      - span(div에서 display만 inline인 태그, 인라인 계열의 기본 태그)
      - a : 링크
      - img : 이미지
    - 블록 계열
      - 기본
        - div(구분, 적절한 태그가 생각나지 않을 때, 모르면 div, 블록 계열의 기본태그)
        - nav(내비게이션, 보통 메뉴 감쌀 때)
        - section(섹션)
        - article(아티클, 게시물)
      - 제목
        - h1, h2, h3, h4, h5, h6
      - 목록
        - ul, li : 순서 없는 목록
        - ol, li : 순서 있는 목록
- CSS
  - 노말라이즈
    - 해당 엘리먼트에 기본적으로 적용되어 있는 디자인을 없애서 다시 평범하게 만든다.
    - a, body, ul, li, ol, li, h1, h2, h3, h4, h5, h6 은 사용하기전에 노말라이즈 해야 한다.
  - 선택자
    - 태그선택자 : `div { ... }`
    - 자식선택자 : `div > a { ... }`
    - 후손선태자 : `div a { ... }`
    - 클래스선택자 : `.menu-item { ... }`
  - 기본 속성
    - width, height, font-size, font-weight, letter-spacing, color, background-color, margin-top, margin-left, padding-top, padding-left, border, border-radius
  - 레이아웃 속성
    - display
      - none : 사라짐
      - inline-block, inline : 글자화
      - block : 블록화
    - float : ??
    - position : ???

## 문제
### 프론트
- [개념 - 이미지](https://codepen.io/jangka44/pen/pZEYmx)
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/MWwJJYr)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/KKpaNYm)
- [문제 - 정답](https://codepen.io/jangka44/pen/KKpaNYm)
- [개념 - margin, padding](https://codepen.io/jangka44/pen/PBGgQa)
- [개념 - margin, padding v2](https://codepen.io/jangka44/pen/rZvRmQ)
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/Rwbrzgx)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/pozgXaz)
- [문제 - 정답](https://codepen.io/jangka44/pen/pozgXaz)
- [문제 - 정답 v2(10분 완성)](https://codepen.io/jangka44/pen/QWbddOa)
- [개념 - nth-child](https://codepen.io/jangka44/pen/xJEeag)
- [개념 - nth-child v2](https://codepen.io/jangka44/pen/wEXOJo)
- [문제 - 무지개를 만들어주세요.](https://codepen.io/jangka44/pen/OJLKjBX)
- [문제 - 정답예시](https://codepen.io/jangka44/debug/aboeyRQ)
- [문제 - 정답](https://codepen.io/jangka44/pen/aboeyRQ)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다.](https://codepen.io/jangka44/pen/xJEoPe)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다. v2](https://codepen.io/jangka44/pen/ZMRPMM)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법](https://codepen.io/jangka44/pen/zJNKwN)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법 v2](https://codepen.io/jangka44/pen/EeGyKK)
- [개념 : 모든 보이는 엘리먼트의 부모는 body이고, 그의 부모는 html 이다.](https://codepen.io/jangka44/pen/RwWvRMe)
- [문제 - bnx 사이트의 상단 이미지 4개 까지만 구현해주세요.](https://codepen.io/jangka44/pen/EpgJPa)
- [문제 - 정답예시 v1, 조건 : br과 nbsp 사용](https://codepen.io/jangka44/debug/KBgYzb)
- [문제 - 정답예시 v2, 조건 : margin 사용](https://codepen.io/jangka44/debug/vaXMyZ)
- [문제 - 정답예시 v3, 조건 : 엘리먼트 5개 사용, 힌트 : body](https://codepen.io/jangka44/debug/rZZqRW)
- [문제 - 정답예시 v4, 조건 : 엘리먼트 4개 사용, 힌트 : body, block](https://codepen.io/jangka44/debug/WggYOr)
- [문제 - 정답 v1, br과 nbsp](https://codepen.io/jangka44/pen/KBgYzb)
- [문제 - 정답 v2, margin](https://codepen.io/jangka44/pen/vaXMyZ)
- [문제 - 정답 v3, 엘리먼트 5개 이하, body](https://codepen.io/jangka44/pen/rZZqRW)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/WggYOr)
- [개념 - border-radius](https://codepen.io/jangka44/pen/bjwyMd)
- [개념 - border-radius v2](https://codepen.io/jangka44/pen/KxeEqX)
- [개념 - border-radius v3](https://codepen.io/jangka44/pen/VGdRMZ)
- [개념 - border-radius v4(5분완성)](https://codepen.io/jangka44/pen/XyeREQ)
- [개념 - inherit](https://codepen.io/jangka44/pen/JBRgyK)
- [개념 - inherit v2](https://codepen.io/jangka44/pen/aaKMxg)
- [개념 - a 노말라이즈](https://codepen.io/jangka44/pen/BPLXrO)
- [문제 - 메뉴를 구현해주세요.(1차 메뉴까지만)](https://codepen.io/jangka44/pen/zLKQRP)
- [힌트](https://codepen.io/jangka44/pen/RwwbqJQ)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/EpgqKa)
- [문제 - 정답 v1](https://codepen.io/jangka44/pen/EpgqKa)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/aaNBMp)
- [문제 - 정답 v3(10분 완성)](https://codepen.io/jangka44/pen/yxJaNb)

### 일반
- [position 예제 v4 만들기 1](https://codepen.io/jangka44/pen/vRyeVz)
- [position 예제 v4 만들기 2](https://codepen.io/jangka44/pen/PRpJVB)
- [position 예제 v4 만들기 3](https://codepen.io/jangka44/pen/MVpEdY)
- [position 예제 v4 만들기 4](https://codepen.io/jangka44/pen/zWZPOK)
- [position 예제 v4 만들기 5](https://codepen.io/jangka44/pen/RMpjpe)
- [position 예제 v4 만들기 6(5분 완성)](https://codepen.io/jangka44/pen/bvqYab)
- [position 예제 v4-2 만들기 6](https://codepen.io/jangka44/pen/RvzwWL)
- [position 예제 v4-3 만들기 6](https://codepen.io/jangka44/pen/XOLWXJ)
- [position 예제 v4-4 만들기 6](https://codepen.io/jangka44/pen/BMgajV)
- [개념 : z-index 예제](https://codepen.io/jangka44/pen/rNaBavg)
- [문제 : 교재 339p 코드 24-6의 실행결과처럼 만들어주세요.(5분완성)](https://codepen.io/jangka44/pen/dyPbPQp)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/LYVreep)
- [문제 - 정답](https://codepen.io/jangka44/pen/yLyByqG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/LYVreep)
- position 개념 끝
- text, 숨김 시작
- [개념 : white-space:nowrap, 줄바꿈 금지](https://codepen.io/jangka44/pen/NLdRyK)
- [개념 : overflow-x:auto, 스크롤바 자동생성](https://codepen.io/jangka44/pen/vzgXjV)
- [개념 : overflow-x:scroll, 스크롤바 무조건 생성](https://codepen.io/jangka44/pen/ZMLpRB)
- [개념 : overflow-x:hidden, 넘치는 내용 숨김](https://codepen.io/jangka44/pen/vzgXrV)
- [개념 : text-overflow:ellipsis, 넘쳐서 숨겨지는 텍스트 ... 처리(5분완성)](https://codepen.io/jangka44/pen/ZMLpjL)
- text, 숨김 끝
- 풀 다운 메뉴 v2 시작
- [풀 다운 메뉴 v2 만들기 1](https://codepen.io/jangka44/pen/aYNGjw)
- [풀 다운 메뉴 v2 만들기 2](https://codepen.io/jangka44/pen/dmMKyq)
- [풀 다운 메뉴 v2 만들기 3](https://codepen.io/jangka44/pen/mxPKeg)
- [풀 다운 메뉴 v2 만들기 4](https://codepen.io/jangka44/pen/Kozezg)
- [풀 다운 메뉴 v2 만들기 5](https://codepen.io/jangka44/pen/WzwyGw)
- [풀 다운 메뉴 v2 만들기 6](https://codepen.io/jangka44/pen/jzrzwY)
- [풀 다운 메뉴 v2 만들기 7](https://codepen.io/jangka44/pen/EEyEbJ)
- [풀 다운 메뉴 v2 만들기 8](https://codepen.io/jangka44/pen/qoNowO)
- [풀 다운 메뉴 v2 만들기 9](https://codepen.io/jangka44/pen/OvXvYO)
- [풀 다운 메뉴 v2 만들기 10(5분 완성)](https://codepen.io/jangka44/pen/pLbVoq)
- 풀 다운 메뉴 v2 끝
- [문제 : 선택자 연습](https://codepen.io/jangka44/pen/ZxLXMb)
- [문제 - 정답](https://codepen.io/jangka44/pen/pLeWWY)
- [문제 - 시간표를 구현해주세요.](https://codepen.io/jangka44/pen/NWPPLWq)
- [문제 - 정답](https://codepen.io/jangka44/pen/PowwdYr)
- [문제 - 시간표를 가운데 정렬 해주세요.](https://codepen.io/jangka44/pen/JjooaPL)
- [문제 - 정답](https://codepen.io/jangka44/pen/dyPPjQK)
- [개념 - 테이블, border-collapse](https://codepen.io/jangka44/pen/gObbjRB)
- [개념 - 테이블, 예제 1](https://codepen.io/jangka44/pen/gOpdQwB)
- 풀 다운 메뉴 v3 시작
- [풀 다운 메뉴 v3 만들기 1](https://codepen.io/jangka44/pen/eMzrWY)
- [풀 다운 메뉴 v3 만들기 2](https://codepen.io/jangka44/pen/PRbJzd)
- [풀 다운 메뉴 v3 만들기 3](https://codepen.io/jangka44/pen/vVGgvX)
- [풀 다운 메뉴 v3 만들기 4](https://codepen.io/jangka44/pen/YaprpY)
- [풀 다운 메뉴 v3 만들기 5](https://codepen.io/jangka44/pen/zmqNeV)
- [풀 다운 메뉴 v3 만들기 6](https://codepen.io/jangka44/pen/oaxBOb)
- [풀 다운 메뉴 v3 만들기 7](https://codepen.io/jangka44/pen/rqejbg)

## 오늘의 동영상
### 프론트
- [동영상 - 2020 02 24, 프론트, 개념, CSS, margin, padding](https://www.youtube.com/watch?v=pDT-JgMIL-s&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 개념, img 엘리먼트](https://www.youtube.com/watch?v=cBF78x1SArI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS](https://www.youtube.com/watch?v=8-APhZJ12i8&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS, only padding](https://www.youtube.com/watch?v=83GwjeVlxtE)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, nth-child, first-child, last-child](https://youtu.be/8udV6lGRgnI)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요.](https://youtu.be/xA51J05sRaU)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, inline 요소에는 width, height, padding, margin이 작동하지 않는다.](https://youtu.be/hLxOhBJzzl0)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, br, &nbsp;, margin 등을 사용](https://youtu.be/SbvwOTp1PaY)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 5개 배치, 오직 엘리먼트 5개만 사용, 영상의 끝은 2분 50초 까지 입니다.](https://youtu.be/iOJnado5Gds)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, block 요소 가운데 정렬하는 방법, margin:0 auto;](https://youtu.be/vKWVeRPmhKg)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/OtJvZo2ls5I)
- [동영상 - 2020 05 21, 프론트, 개념, img 엘리먼트](https://youtu.be/M-wu6_tve1Q)
- [동영상 - 2020 05 21, 프론트, 개념, CSS, nth-child, nth-last-child, first-child, last-child](https://youtu.be/KxJ37cQokAg)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요.](https://youtu.be/r0jKqPgFGLw)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 5개만 사용](https://youtu.be/ZCT4IDQbEW4)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/39MMNmsr9zs)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, border-radius](https://youtu.be/Ddylg0YjouM)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, inherit, 상속](https://www.youtube.com/watch?v=bsgQpo8Ui1Y&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, a 엘리먼트 노말라이즈](https://www.youtube.com/watch?v=y6h8JlP-_hU&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기](https://www.youtube.com/watch?v=ys1hu93CtkQ&feature=youtu.be)
- [동영상 - 2020_05_22, 프론트, 개념, HTML, CSS 기초 구성요소](https://youtu.be/Qh-dB_foLx8)
- [동영상 - 2020_05_22, 프론트, 개념, inherit](https://youtu.be/IqKa3bQ85DU)
- [동영상 - 2020_05_22, 프론트, 개념, a 노말라이즈](https://youtu.be/pBr6rWXa5zY)
- [동영상 - 2020_05_22, 프론트, 개념, 메뉴를 구현해주세요, 1차 메뉴까지만](https://youtu.be/l6Jq842FQyI)
- [동영상 - 2020_06_04, 프론트, 개념, flex, img](https://youtu.be/ftIDpWO8wUU)
- [동영상 - 2020_06_04, 프론트, 개념, code sand box, 기본사용법](https://youtu.be/RNqPDF9RHMQ)
- [동영상 - 2020_06_04, 프론트, 개념, nodejs로 웹 퍼블리싱 개발환경 구축](https://youtu.be/NfBwybJCdZ8)
- [동영상 - 2020_06_04, 프론트, 개념, margin, padding, block](https://youtu.be/HuTiRIFo4ms)
- [동영상 - 2020 06 04, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요](https://youtu.be/89PFwi1xE_c)
- [동영상 - 2020 06 24, 프론트, 개념, CSS, block 요소 가운데 정렬하는 방법, margin0 auto](https://youtu.be/-oLSzeu93CU)
- [동영상 - 2020 06 24, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/0DbDFl5fHbQ)
- [동영상 - 2020 06 04, 프론트, 개념, CSS, inherit, 상속](https://youtu.be/5Nvo6unIFi8)
- [동영상 - 2020 06 04, 프론트, 개념, 메뉴를 구현해주세요, 1차 메뉴까지만](https://youtu.be/UKXcmY1dBEs)

# 2020-06-05, 39일차
## 문제
### CSS DINER
- [CSS DINER](https://flukeout.github.io/)

### 프론트
- [개념 - body 노말라이즈](https://codepen.io/jangka44/pen/djvqLM)
  - 책내용 : 98p
- [개념 - 클래스 선택자](https://codepen.io/jangka44/pen/oMZMyQ)
  - 책내용 : 273p
- [문제 - 모든 엘리먼트의 태그를 div로 바꿔주세요.](https://codepen.io/jangka44/pen/aQqGbO)
  - 책내용 : 273p
- [문제 - 정답](https://codepen.io/jangka44/pen/oQEqrL)
- [문제 - 최상위 엘리먼트의 클래스 속성을 제외한 나머지는 전부 지워주세요.](https://codepen.io/jangka44/pen/ZmroEN)
- [문제 - 정답(10분 완성)](https://codepen.io/jangka44/pen/xQYjbw)
- [개념 - ul, li](https://codepen.io/jangka44/pen/EpWOam)
  - 책내용 : 60p
- [개념 - ol, li](https://codepen.io/jangka44/pen/PxQeEB)
  - 책내용 : 60p
- [개념 - h1, h2, h3, h4, h5, h6](https://codepen.io/jangka44/pen/KrQRVJ)
  - 책내용 : 50p
- [문제 - 방탄소년단의 노래소개와 멤버구성을 ul, ol, li, h1, h2 태그만으로 표현해주세요.](https://codepen.io/jangka44/pen/LXQmzW)
  - 책내용 : 50p, 60p
- [문제 - 정답](https://codepen.io/jangka44/pen/BGrJqO)
- [개념 - ul, li 노말라이즈](https://codepen.io/jangka44/pen/ajJQzw)
  - 책내용 : 194p
- [개념 - h1, h2, h3, h4, h5, h6 노말라이즈](https://codepen.io/jangka44/pen/PxaKWb)
- [개념 - 상황에 맞는 태그를 알면 그걸 사용하고, 모르면 div](https://codepen.io/jangka44/pen/zLZmXo)
  - 최종버전을 5분안에 만들 수 있어야 합니다.
- 풀 다운 메뉴 만들기 v1 시작
- [풀 다운 메뉴 만들기 1](https://codepen.io/jangka44/pen/wmamMY)
- [풀 다운 메뉴 만들기 2](https://codepen.io/jangka44/pen/oqXqBe)
- [풀 다운 메뉴 만들기 3](https://codepen.io/jangka44/pen/JLdLVM)
- [풀 다운 메뉴 만들기 4](https://codepen.io/jangka44/pen/VXvQOK)
- [풀 다운 메뉴 만들기 5](https://codepen.io/jangka44/pen/NYGyZM)
- [풀 다운 메뉴 만들기 6](https://codepen.io/jangka44/pen/yKYKBZ)
- [풀 다운 메뉴 만들기 7](https://codepen.io/jangka44/pen/Zxbxbd)
- [풀 다운 메뉴 만들기 8](https://codepen.io/jangka44/pen/NYGYrL)
- [풀 다운 메뉴 만들기 9](https://codepen.io/jangka44/pen/QmjmvY)
- [풀 다운 메뉴 만들기 10](https://codepen.io/jangka44/pen/jzbzpj)
- [풀 다운 메뉴 만들기 11](https://codepen.io/jangka44/pen/JLGLpX)
- [풀 다운 메뉴 만들기 12](https://codepen.io/jangka44/pen/KoVoRE)
- [풀 다운 메뉴 만들기 13](https://codepen.io/jangka44/pen/eMJMPY)
- [풀 다운 메뉴 만들기 14](https://codepen.io/jangka44/pen/zWrWVB)
- [풀 다운 메뉴 만들기 15](https://codepen.io/jangka44/pen/JLGvXJ)
- [풀 다운 메뉴 만들기 16](https://codepen.io/jangka44/pen/eMJreg)
- [풀 다운 메뉴 만들기 17](https://codepen.io/jangka44/pen/mxVLXa)
- [풀 다운 메뉴 만들기 18](https://codepen.io/jangka44/pen/KoVRRz)
- [풀 다운 메뉴 만들기 19](https://codepen.io/jangka44/pen/KoVRez)
- [풀 다운 메뉴 만들기 20(5분 완성)](https://codepen.io/jangka44/pen/wmMjxw)
- 풀 다운 메뉴 만들기 v1 끝
- [문제 - 3가지 메뉴를 구현해주세요.(1차 메뉴까지만)](https://codepen.io/jangka44/pen/EegZdW)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/JaREmy)
- [문제 - 정답](https://codepen.io/jangka44/pen/JaREmy)
- [문제 - a에 마우스를 올리면 인접동생인 div가 나타나게 해주세요.(즉 a의 인접동생인 div는 평소에 안나와야 합니다.)](https://codepen.io/jangka44/pen/PoorMYm)
- [문제 - 정답예시](https://codepen.io/jangka44/debug/LYYKwYE)
- [문제 - 정답](https://codepen.io/jangka44/pen/LYYKwYE)
- position 개념 시작
  - 책내용 : 330p, 335p
- [개념 : position 속성 정리](https://codepen.io/jangka44/debug/aaXopM)
- [개념 : position 예제](https://codepen.io/jangka44/pen/eYYwqLO)
- [position 예제 v1 만들기 1](https://codepen.io/jangka44/pen/NYgaaw)
- [position 예제 v1 만들기 2](https://codepen.io/jangka44/pen/pLNWYy)
- [position 예제 v1 만들기 3](https://codepen.io/jangka44/pen/OvbxGe)
- [position 예제 v1 만들기 4](https://codepen.io/jangka44/pen/dmOVEj)
- [position 예제 v2 만들기 0](https://codepen.io/jangka44/pen/rNBmbER)
- [position 예제 v2 만들기 1](https://codepen.io/jangka44/pen/pLNdoE)
- [position 예제 v2 만들기 2](https://codepen.io/jangka44/pen/LdbOYB)
- [position 예제 v3 만들기 1](https://codepen.io/jangka44/pen/NYbwqG)
- [position 예제 v3 만들기 2](https://codepen.io/jangka44/pen/VXmrLV)
- [개념 - top, left, right, bottom](https://codepen.io/jangka44/pen/dyYBOzw?editors=1100)
- [position 예제 v4 만들기 1](https://codepen.io/jangka44/pen/vRyeVz)
- [position 예제 v4 만들기 2](https://codepen.io/jangka44/pen/PRpJVB)
- [position 예제 v4 만들기 3](https://codepen.io/jangka44/pen/MVpEdY)
- [position 예제 v4 만들기 4](https://codepen.io/jangka44/pen/zWZPOK)
- [position 예제 v4 만들기 5](https://codepen.io/jangka44/pen/RMpjpe)
- [position 예제 v4 만들기 6(5분 완성)](https://codepen.io/jangka44/pen/bvqYab)
- [position 예제 v4-2 만들기 6](https://codepen.io/jangka44/pen/RvzwWL)
- [position 예제 v4-3 만들기 6](https://codepen.io/jangka44/pen/XOLWXJ)
- [position 예제 v4-4 만들기 6](https://codepen.io/jangka44/pen/BMgajV)
- [개념 : z-index 예제](https://codepen.io/jangka44/pen/rNaBavg)
- [문제 : 교재 339p 코드 24-6의 실행결과처럼 만들어주세요.(5분완성)](https://codepen.io/jangka44/pen/dyPbPQp)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/LYVreep)
- [문제 - 정답](https://codepen.io/jangka44/pen/yLyByqG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/LYVreep)
- position 개념 끝
- text, 숨김 시작
- [개념 : white-space:nowrap, 줄바꿈 금지](https://codepen.io/jangka44/pen/NLdRyK)
- [개념 : overflow-x:auto, 스크롤바 자동생성](https://codepen.io/jangka44/pen/vzgXjV)
- [개념 : overflow-x:scroll, 스크롤바 무조건 생성](https://codepen.io/jangka44/pen/ZMLpRB)
- [개념 : overflow-x:hidden, 넘치는 내용 숨김](https://codepen.io/jangka44/pen/vzgXrV)
- [개념 : text-overflow:ellipsis, 넘쳐서 숨겨지는 텍스트 ... 처리(5분완성)](https://codepen.io/jangka44/pen/ZMLpjL)
- text, 숨김 끝
- 풀 다운 메뉴 v2 시작
- [풀 다운 메뉴 v2 만들기 1](https://codepen.io/jangka44/pen/aYNGjw)
- [풀 다운 메뉴 v2 만들기 2](https://codepen.io/jangka44/pen/dmMKyq)
- [풀 다운 메뉴 v2 만들기 3](https://codepen.io/jangka44/pen/mxPKeg)
- [풀 다운 메뉴 v2 만들기 4](https://codepen.io/jangka44/pen/Kozezg)
- [풀 다운 메뉴 v2 만들기 5](https://codepen.io/jangka44/pen/WzwyGw)
- [풀 다운 메뉴 v2 만들기 6](https://codepen.io/jangka44/pen/jzrzwY)
- [풀 다운 메뉴 v2 만들기 7](https://codepen.io/jangka44/pen/EEyEbJ)
- [풀 다운 메뉴 v2 만들기 8](https://codepen.io/jangka44/pen/qoNowO)
- [풀 다운 메뉴 v2 만들기 9](https://codepen.io/jangka44/pen/OvXvYO)
- [풀 다운 메뉴 v2 만들기 10(5분 완성)](https://codepen.io/jangka44/pen/pLbVoq)
- 풀 다운 메뉴 v2 끝
- [문제 : 선택자 연습](https://codepen.io/jangka44/pen/ZxLXMb)
- [문제 - 정답](https://codepen.io/jangka44/pen/pLeWWY)
- [문제 - 시간표를 구현해주세요.](https://codepen.io/jangka44/pen/NWPPLWq)
- [문제 - 정답](https://codepen.io/jangka44/pen/PowwdYr)
- [문제 - 시간표를 가운데 정렬 해주세요.](https://codepen.io/jangka44/pen/JjooaPL)
- [문제 - 정답](https://codepen.io/jangka44/pen/dyPPjQK)
- [개념 - 테이블, border-collapse](https://codepen.io/jangka44/pen/gObbjRB)
- [개념 - 테이블, 예제 1](https://codepen.io/jangka44/pen/gOpdQwB)
- 풀 다운 메뉴 v3 시작
- [풀 다운 메뉴 v3 만들기 1](https://codepen.io/jangka44/pen/eMzrWY)
- [풀 다운 메뉴 v3 만들기 2](https://codepen.io/jangka44/pen/PRbJzd)
- [풀 다운 메뉴 v3 만들기 3](https://codepen.io/jangka44/pen/vVGgvX)
- [풀 다운 메뉴 v3 만들기 4](https://codepen.io/jangka44/pen/YaprpY)
- [풀 다운 메뉴 v3 만들기 5](https://codepen.io/jangka44/pen/zmqNeV)
- [풀 다운 메뉴 v3 만들기 6](https://codepen.io/jangka44/pen/oaxBOb)
- [풀 다운 메뉴 v3 만들기 7](https://codepen.io/jangka44/pen/rqejbg)

## 오늘의 동영상
### 프론트
- [동영상 - 2020 03 18, 프론트, 개념, position absolute, 수직수평 중앙정렬](https://youtu.be/aROEkCwtIvw)
- [동영상 - 2020 03 18, 프론트, 개념, position absolute, 수직수평 중앙정렬, 유동너비버전](https://youtu.be/xm-1w-37NGo)
- [동영상 - 2020 03 18, 프론트, 문제, 사각형 6개를 배열해주세요, absolute, relative](https://youtu.be/OdZW5I0Hlrk)
- [동영상 - 2020 03 18, 프론트, 개념, position, fixed, relative](https://youtu.be/9W2KzkXHu-s)
- [동영상 - 2020 03 18, 프론트, 개념, overflow, text-overflow, white-space, 텍스트 말줄임 효과](https://youtu.be/I9e8BknCvKs)
- [동영상 - 2020 03 18, 프론트, 문제, 풀 다운 메뉴 만들기 v2](https://youtu.be/jLW47KUS0n8)
- [동영상 - 2020 06 12, 프론트, 문제, 사각형 6개를 배열해주세요, absolute, relative, ver1](https://youtu.be/7scafOS9y-o)
- [동영상 - 2020 06 12, 프론트, 문제, 사각형 6개를 배열해주세요, absolute, relative, ver2](https://youtu.be/s-7wPRM-H4A)
- [동영상 - 2020_06_02, 프론트, 개념, overflow, text-overflow, white-space, 텍스트 말줄임 효과](https://youtu.be/-YEdvycGJvY)
- [동영상 - 2020 06 02, 프론트, 문제, 풀 다운 메뉴 만들기 v2, 설명 v1](https://youtu.be/2mlL_xqcyv8)
- [동영상 - 2020 06 02, 프론트, 문제, 풀 다운 메뉴 만들기 v2, 설명 v2](https://youtu.be/zDCyLvxff6g)
- [동영상 - 2020 06 02, 프론트, 문제, 풀 다운 메뉴 만들기 v2, 설명 v3](https://youtu.be/0wIbhPMJILM)
- [동영상 - 2020_04_21, 프론트, 개념, CSS DINER, 9단계](https://youtu.be/ZyGfEhbwJl8)
- [동영상 - 2020 03 23, 프론트, 문제, 각각의 선택자가 몇개의 엘리먼트를 선택하나요](https://www.youtube.com/watch?v=Fiw1ANPFv6o&feature=youtu.be)
- [동영상 - 2020 03 23, 프론트, 개념, 테이블](https://www.youtube.com/watch?v=Cbb0cnDEfT4&feature=youtu.be)
- [동영상 - 2019 03 04, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=dNWfYAwLEMk&list=PLE0hRBClSk5J2xT3zbtrYyYV1gsDhGJd1&index=9)
- [동영상 - 2020 03 23, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=Cbb0cnDEfT4&feature=youtu.be)
- [동영상 - 2020 04 06, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=DO5sIbvIPsg&feature=youtu.be)
- [동영상 - 2019 03 04, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 완성](https://www.youtube.com/watch?v=18PbGTxuJkA&list=PLE0hRBClSk5J2xT3zbtrYyYV1gsDhGJd1&index=10)
- [동영상 - 2020 04 06, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 완성](https://www.youtube.com/watch?v=SBELxy--0Fc&feature=youtu.be)
- [동영상 - 2020 04 06, 프론트, 개념, CSS 디너 하는 방법](https://www.youtube.com/watch?v=jJs07q_y0eE&feature=youtu.be)
- [동영상 - 2020 04 06, 프론트, 개념, 코드펜, 자바스크립트 콘솔 사용법](https://www.youtube.com/watch?v=AZrwUAc6BB8&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, body 엘리먼트 노말라이즈](https://www.youtube.com/watch?v=jqvxkBLiZtw&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, 클래스](https://www.youtube.com/watch?v=GnPdH0GRaE8&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기, 오직 div 태그만 사용, 클래스 이용](https://www.youtube.com/watch?v=Q-PePcdbZ8s&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기, 오직 div 태그만 사용, 1개의 엘리먼트에만 클래스 적용](https://www.youtube.com/watch?v=wxRqeP48kmw&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, HTML, h1, h2, h3](https://www.youtube.com/watch?v=1lMLjmOF52c&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, HTML, ul, ol, li](https://www.youtube.com/watch?v=Z5Z69G0XrZI&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 개념, h1, h2, h3, h4, h5, h6 노말라이즈](https://www.youtube.com/watch?v=EhcqjWUOJM0&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 개념, 어떤 태그를 써야할지 모르겠다면 div](https://www.youtube.com/watch?v=SYAMjmslyFs&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 개념, ul, li 노말라이즈](https://www.youtube.com/watch?v=6Ozco_EpgPg&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 문제, 방탄소년단의 노래소개와 멤버구성을 ul, ol, li, h1, h2 태그만으로 표현해주세요](https://www.youtube.com/watch?v=39I5q9qW4bA&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 문제, 풀 다운 메뉴 만들기 v1](https://www.youtube.com/watch?v=ewOyZAu1kso&feature=youtu.be)
- [동영상 - 2020_05_26, 피그마, 개념, 프레임, 도형, 텍스트, 버튼 컴포넌트 제작](https://youtu.be/IgMwL2Q4Npg)
- [동영상 - 2020 03 13, 프론트, 문제, 풀 다운 메뉴 만들기 v1](https://youtu.be/_lPwQDT02nE)
- [동영상 - 2020 03 16, 프론트, 문제, 3가지 다른 글자색의 메뉴를 만들어주세요, 클래스](https://www.youtube.com/watch?v=GDpZMwFfcig)
- [동영상 - 2020 03 16, 프론트, 문제, a에 마우스를 올리면 인접동생인 div가 나타남](https://www.youtube.com/watch?v=3-IN1hFFh6c)
- [동영상 - 2020 03 16, 프론트, 개념, position](https://www.youtube.com/watch?v=wt405dLbKyM)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 4등분 해주세요](https://www.youtube.com/watch?v=BnGYQ9TDAVs)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 3등분 해주세요](https://www.youtube.com/watch?v=Tp5xfuXxo-o)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 9등분 해주세요](https://www.youtube.com/watch?v=OR0bNgpQgj8)
- [동영상 - 2020 03 16, 문제, absolute, top, left, right, bottom으로 화면을 4등분 해주세요](https://www.youtube.com/watch?v=m-5tK7hZtNs)
- [동영상 - 2020 05 28, 프론트, 개념, position](https://youtu.be/7eC21NOfDPc)

# 2020-06-08, 40일차
## 문제
### 일반
- 문제 - 영속성이 보장되는, 회원제 멀티 게시판(공지사항, 자유게시판)을 구현해주세요.
- [문제 - 정답, 1차](https://repl.it/@jangka512/SpitefulHeartfeltSuperuser)
  - 기본기능 구현
- [문제 - 정답, 2차](https://repl.it/@jangka512/BarrenWittyGraduate)
  - 실행될때 마다, 게시판 1개와 회원 1명이 계속 증가하는 버그 수정(1개 이상 안생기도록 수정)

## 오늘의 동영상
### 일반
- [동영상 - 2020 06 08, 자바, 개념, MVC, 파일DB, 게시판, 1부](https://youtu.be/o0cKNKqtWoI)
- [동영상 - 2020 06 08, 자바, 개념, MVC, 파일DB, 게시판, 2부, 설명 v1](https://youtu.be/JFAi6n5zJRg)
- [동영상 - 2020 06 08, 자바, 개념, MVC, 파일DB, 게시판, 2부, 설명 v2](https://youtu.be/Uy_x1cNRYEI)
- [동영상 - 2020 06 08, 자바, 개념, MVC, 파일DB, 게시판, 2부, 설명 v3](https://youtu.be/Vxfw7E71Yc0)

# 2020-06-09, 41일차
## 문제
### 프론트
- [문제 - 풀 다운 메뉴 v3, 엘리먼트 개수를 맞춰주세요.](https://codepen.io/jangka44/pen/KKwVaLL)
- [문제 - 정답](https://codepen.io/jangka44/pen/VRwyep)
- 풀 다운 메뉴 v3 끝
- position:fixed 시작
- [문제 - 스크롤바를 따라다니는 유령을 만들어주세요.](https://codepen.io/jangka44/pen/PxXwWX)
- [문제 - 정답](https://codepen.io/jangka44/pen/GwPgWY)
- position:fixed 끝
- before, after 시작
- [::before, ::after v1 1](https://codepen.io/jangka44/pen/WzXpQY)
- [::before, ::after v1 2](https://codepen.io/jangka44/pen/wmPJMx)
- [::before, ::after v1 3](https://codepen.io/jangka44/pen/pLdeyB)
- [::before, ::after v1 4](https://codepen.io/jangka44/pen/JLOWKR)
- before, after 끝
- 트랜지션 시작
- [트랜지션 예제 v1 만들기 1](https://codepen.io/jangka44/pen/VXbrrX)
- [트랜지션 예제 v1 만들기 2](https://codepen.io/jangka44/pen/RMVjxB)
- [트랜지션 예제 v1 만들기 3](https://codepen.io/jangka44/pen/PRmOQX)
- [트랜지션 예제 v1 만들기 4](https://codepen.io/jangka44/pen/geWXeZ)
- [문제 - 너비가 늘어나는데 3초, 줄어드는데 1초가 걸리게 해주세요.](https://codepen.io/jangka44/pen/RwwRpJr)
- [문제 - 정답](https://codepen.io/jangka44/pen/RwwRpJr)
- 트랜지션 끝
- 토클 사이드 바 시작
- [문제 - 좌측 토글 사이드 바를 만들어주세요](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://codepen.io/jangka44/debug/LYENJqO)
- [문제 - 정답](https://codepen.io/jangka44/pen/QJzbyG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/gOaYeeQ)
- [문제 - 좌측 토글 사이드 바를 만들어주세요, 2차 메뉴 버전](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://codepen.io/jangka44/debug/LYENJqO)
- [정답](https://codepen.io/jangka44/pen/LYENJqO)
- [문제 - 서브메뉴를 가지고 있는 메뉴 아이템은 표시를 해주세요.](https://codepen.io/jangka44/pen/MWWjgVR)
- [문제 - 정답](https://codepen.io/jangka44/pen/OJJRLZQ)
- [문제 - 좌측 토글 사이드 바를 만들어주세요, 2차 메뉴 버전](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://cdpn.io/jangka44/debug/VOZZxE)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/VOZZxE)
- [문제 - 정답 v3](https://codepen.io/jangka44/pen/MWgOmmr)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/ZEzaKKr)
- [예제 - 좌측 토글 사이드바 구현(아이콘 회전, 3차 메뉴)](https://codepen.io/jangka44/pen/xxbONej)
- [문제 - 정답 v4(2020-04-08)](https://codepen.io/jangka44/pen/vYNBjNb)

### 일반
- 문제 - MVC, 파일DB 게시판 구현
  - [기존 소스코드에서 시작](https://repl.it/@jangka512/BarrenWittyGraduate)
  - 아래 순서대로 구현
  - 로그인/로그아웃/회원가입
    - 모든 행동은 회원이어야 가능
  - 게시판 변경기능(공지사항 게시판(디폴트), 자유게시판은 프로그램 시작시에 존재하지 않는다면 자동으로 생성됨)
  - 게시물 리스팅(게시판 별로)
  - 게시물 수정, 삭제
  - 댓글 추가/수정/삭제

## 오늘의 동영상
### 프론트
- [동영상 - 2020 04 08, 프론트, 개념, 3차 메뉴 쉽게 만드는 전략](https://www.youtube.com/watch?v=GMFsMvpDejs&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, position, fixed](https://www.youtube.com/watch?v=UGKOitl8uLw&feature=youtu.be)
- [동영상 - 2019 12 11, 프론트, 개념, before, after](https://www.youtube.com/watch?v=-fgbyOjp4BA&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, before, after](https://www.youtube.com/watch?v=tBz-p6Zi4zM&feature=youtu.be)
- [동영상 - 2019 12 09, 프론트, 개념, 트랜지션](https://www.youtube.com/watch?v=VhiLkt6vTb0&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, 트랜지션](https://www.youtube.com/watch?v=x_FwmPj1Rrg&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, 트랜지션으로 프로그래시브 바 구현](https://www.youtube.com/watch?v=3rW-ysNgM9c&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 트랜지션, 너비가 늘어나는데 3초, 줄어드는데 1초가 걸리게 해주세요](https://www.youtube.com/watch?v=WhyfTPzii_Y&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 좌측 토글 사이드 바 만들기, 작업 2](https://www.youtube.com/watch?v=45UpAz0eyis&feature=youtu.be)
- [동영상 - 2019 12 11, 프론트, 문제, 좌측 토글 사이드 바 만들기 v2, 작업 1](https://www.youtube.com/watch?v=A2OWRqqg6_A&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 좌측 토글 사이드 바 만들기 v2, 작업 2](https://www.youtube.com/watch?v=9cAl4Fzc7F4&feature=youtu.be)

### 일반
- [동영상 - 2020 06 09, 자바, 자바 MVC(파일) 게시판, 구현, 로그인, 로그아웃 기능 추가](https://youtu.be/Y7Na9lN8g88)
- [동영상 - 2020 06 09, 자바, 자바 MVC(파일) 게시판, 구현, HTML 템플릿으로 작업한는 방법 설명](https://youtu.be/VglX67lGH44)

# 2020-06-10, 42일차
## 문제
### 일반
- [개념 - 쓰레드](https://repl.it/@jangka512/ImportantGaseousRepo)

### SSG 구현
- 명령어 리스트
  - member join, member login, member logout, member whoami
  - article write, article modify 1, article delete 1, article list 1, article list 2, article list 1 안녕, article detail 3
    - article list 1 안녕
      - 선택된 게시판에서 `안녕`으로 검색한 결과의 1페이지
    - article list 1
      - 선택된 게시판에서 1페이지
  - article createBoard, article deleteBoard notice, article listBoard, article changeBoard notice
  - build site, build startAutoSite, build stopAutoSite
    - build startAutoSite
      - 별도의 쓰레드를 켜서 build site 명령을 10초에 한번씩 수행한다. 즉 자동빌드켜기
    - build stopAutoSite
      - 쓰레드를 끈다. 즉 자동빌드끄기
- 사이트 GNB(상단공통메뉴)
  - 홈(/home/index.html)
  - 게시판
    - 자유게시판(/article/free-list-1.html)
    - 공지사항(/article/free-notice-1.html)
  - 개발자 블로그
    - 홍길동(홍길동 블로그 주소)
      - 새 창으로 뜨기
    - 홍길순(홍길순 블로그 주소)
      - 새 창으로 뜨기
  - 통계(/stat/index.html)
    - 내용
      - 회원 수
      - 전체 게시물 수
      - 각 게시판별 게시물 수
      - 전체 게시물 조회 수
      - 각 게시판별 게시물 조회 수
- 템플릿 파일 구조
  - site_template/resource/common.css
    - 공통 CSS
  - site_template/resource/common.js
    - 공통 JS
  - site_template/part/head.html
    - 공통 상단 템플릿
    - GNB는 여기에
  - site_template/part/foot.html
    - 공통 하단
    - 자바스크립트 로드는 여기서
  - site_template/home/index.html
    - 메인페이지 템플릿
    - export to : /home/index.html
  - site_template/stat/index.html
    - 통계페이지 템플릿
    - export to : /stat/index.html
  - site_template/article/list.html
    - 게시물 리스트 페이지 템플릿
    - export to : /article/free-list-1.html, /article/free-list-2.html, ...
  - site_template/article/detail.html
    - 게시물 상세페이지 템플릿
    - export to : /article/1.html, /article/2.html, ...

## 오늘의 동영상
### 일반
- [동영상 - 2020 06 10, 자바, 개념, 쓰레드](https://youtu.be/DQ1yLwYn88Y)
- [동영상 - 2020 06 10, 자바, 자바 MVC파일 게시판, 구현, list 페이지 생성기능, head, foot](https://youtu.be/KLaxzcj9H1s)
- [동영상 - 2020 06 10, 자바, 자바 MVC파일 게시판, 구현, list 페이지 생성기능, head, foot, 2부](https://youtu.be/oOQ9s0EW1kE)

# 2020-06-11, 43일차
## 문제
### SSG 구현(전날내용 계속)

# 2020-06-12, 44일차
## 문제
### SSG 구현(전날내용 계속)

## 개념
### GIT 명령어
- 최초에 가져오기(꼭 워크스페이스 폴더에서 실행)
  - git clone https://github.com/jhs512/my_blog.git
- repository 내 모든 수정 되돌리기
  - git checkout .
- 내 소스코드를 조장의 리포지터리의 최신 버전으로 만들기
  - git checkout .
  - git pull origin master
- 특정 폴더 아래의 모든 수정 되돌리기
  - git checkout {dir}
- 특정 파일의 수정 되돌리기
  - git checkout {file_name}
- git add 명령으로 stage에 올린 경우 되돌리기
  - git reset
- 과거로 돌아가기
  - git checkout {커밋번호}
- 현재로 돌아오기
  - git checkout .
  - git checkout master
  - git checkout .
- git 로그인 정보 없애기
  - git config --global credential.helper manager
  - git credential-manager delete https://github.com

## 오늘의 동영상
### GIT
- [동영상 - 2020 06 12, GIT, GITHUB, 이클립스 협업 1부](https://youtu.be/9QPMfISiFyA)
- [동영상 - 2020 06 12, GIT, GITHUB, 이클립스 협업 2부](https://youtu.be/1d6qp1PIEqY)
- [동영상 - 2020 06 12, GIT, GITHUB, 이클립스 협업 3부](https://youtu.be/AXy3ZSwvyLY)

# 2020-06-15, 45일차
## 문제
### SSG 구현(전날내용 계속)

# 2020-06-16, 46일차
## 문제
### 일반
- JDBC 기초 시작
- [mysql-connector-java, 메이븐 리포지터리](https://mvnrepository.com/artifact/mysql/mysql-connector-java)
  - 최신버전(8.0.20) 클릭
  - jar 파일 다운로드
- [JDBC 예제 1, 연결](https://repl.it/@jangka512/HandmadeAssuredIteration)
- [JDBC 예제 2, 연결종료](https://repl.it/@jangka512/GlitteringYearlyString)
- [JDBC 예제 3, 게시물 작성](https://repl.it/@jangka512/WideeyedLatestGame#Main.java)
  - [SQL](https://repl.it/@jangka512/AdolescentTechnologicalWheel)
- [JDB 예제 4, 게시물 수정](https://repl.it/@jangka512/FlamboyantUnfortunateOutliers#Main.java)
  - 예외 메세지 출력을 System.out에서 System.err로 변경
  - SQL 관련에러에서, SQL 출력하도록
- [JDB 예제 5, 게시물 조회](https://repl.it/@jangka512/SlushySoulfulDoom)
  - 예외 메세지 출력을 System.out에서 System.err로 변경
  - SQL 관련에러에서, SQL 출력하도록
- JDBC 기초 끝
- 기존 파일 MVC 게시판에 JDBC 적용 시작
- [개념, 자바, MVC텍스트게시판, 파일DB에서 MySQL로 변경](https://repl.it/@jangka512/DisloyalMustyTrees)

### DB
- 문제 - 마리아 DB를 설치 후 세팅까지 해주세요.
  - 힌트
    - [XAMPP 다운로드](https://www.apachefriends.org/index.html)
- 문제 - DB 관리 툴(sqlyog 커뮤니티 에디션)을 설치 후 세팅까지 해주세요.
  - 힌트
    - [SQLYOG community 다운로드](https://github.com/webyog/sqlyog-community/wiki/Downloads)
- [문제 - 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://codepen.io/jangka44/pen/LYVEyye)
- [정답](https://codepen.io/jangka44/pen/XWbJRMN)

## 오늘의 동영상
### 일반
- [동영상 - 2020_06_16, 자바, JDBC, 기초 예제 및 라이브러리 만들기](https://youtu.be/xA6126k-Sdc)
- [동영상 - 2020_06_16, 자바, MVC텍스트게시판, 파일DB에서 MySQL로 변경](https://youtu.be/nzbN-VZLDdc)

### DB
- [동영상 - 2020 02 08, DB, 문제, 마리아 DB를 설치해주세요](https://www.youtube.com/watch?v=5_tUEm52o0w&feature=youtu.be)
- [동영상 - 2020 02 08, DB, 문제, SQLYOG를 설치해주세요](https://www.youtube.com/watch?v=SThu4hxm_Ks&feature=youtu.be)
- [동영상 - 2020 02 08, DB, 문제, 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://www.youtube.com/watch?v=M4-fhS8MZe8&feature=youtu.be)
- [동영상 - 2020_06_05, DB, 개념, DB수업 개발환경세팅, 1부](https://youtu.be/gHBBGoMKgdc)
- [동영상 - 2020_06_05, DB, 개념, DB수업 개발환경세팅, 2부](https://youtu.be/29NHT7byRZw)
- [동영상 - 2020_06_05, DB, 개념, DB, 테이블, 개념](https://youtu.be/OxAAYFqoL6E)

## 숙제
### 일반
- MySQL 기반, 텍스트 MVC 게시판 만들기
  - 개인 프로젝트
  - 처음부터 다시 시작
    - 기존 예제 참고 가능
  - 기능리스트
    - 게시판 변경
      - article changeBoard free
      - 게시판은 공지사항(notice)와 자유게시판(free)이 있습니다.
        - 사용자가 추가하는게 아니라 프로그램 시작할 때 부터 존재해야 합니다.
        - 즉 board 테이블에 데이터(row)가 이미 2개 존재해야 합니다.
    - 게시물 추가
      - article write
      - 기본적으로 공지사항 게시물로 추가됩니다.
    - 게시물 리스팅
      - article list
      - 현재 선택된 게시판의 게시물 리스팅, 최신순으로 정렬
    - 게시물 상세보기
      - article detail 1
      - 선택된 게시물 상세보기
    - 게시물 수정
      - article modify 1
      - 선택된 게시물 수정
    - 게시물 삭제
      - article delete 1
      - 선택된 게시물 삭제
    - 정적 사이트 빌드
      - build site
      - build startAutoSite
      - build stopAutoSite
      - 생성되는 파일
        - site/article/list-free.html
        - site/article/list-notice.html
        - site/article/1.html
        - site/article/2.html
        - ...
  - 참고
    - CSS 작업 안해도 됩니다.
    - head.html, foot.html 사용안해도 됩니다.
    - article, board 테이블이 있어야 합니다.
    - 학원에서는 MySQL 기본계정이 sbsst / sbs123414, 집에서는 root / 비번없음 입니다.
    - article 테이블 구조
      - id, regDate, title, body, boardId
    - board 테이블 구조
      - id, regDate, name, code

# 2020-06-17, 47일차
## 문제
### JDBC-TEXT-BOARD 작업
- [JDBC-TEXT-BOARD, Connection 라이브러리 구현, , 1부, INSERT 까지 작업](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/b4ba7aecf8f5363e814c76fdca8b1bab673701c1)
- [JDBC-TEXT-BOARD, Connection 라이브러리 구현, , 2부, 나머지 작업](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/747eb490f8ff1a846a1eb5671c575d249afc954b)

## 오늘의 동영상
### JDBC-TEXT-BOARD 작업
- [동영상 - 2020_06_18, 자바, 개념, 강사의 깃허브 프로젝트를 실행하는 방법](https://youtu.be/DEie_bXGRc0)
- [동영상 - 2020_06_17, 자바, JDBC-TEXT-BOARD, Connection 라이브러리 구현, 1부, INSERT 까지 작업](https://youtu.be/p-ihDlr9wHE)
- [동영상 - 2020_06_17, 자바, JDBC-TEXT-BOARD, Connection 라이브러리 구현, , 2부, 나머지 작업](https://youtu.be/NhBs1Tf-pEE)

# 2020-06-18, 48일차
## 문제
### JDBC-TEXT-BOARD 작업
- [클래스들을 별도의 파일로 분리, 파일DB를 전부 MySQL로 교체](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/607a9fa6662a330d649bd3ff11b97189cb2de256)
- [클래스들을 별도의 파일로 분리, 패키지 분리](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/f0b32a38848a164a2d8dea018f89ae967cfc8c3d)

## 오늘의 동영상
### 일반
- [동영상 - 2020 06 18, 자바, 개념, 패키지 분리](https://www.youtube.com/watch?v=DC-A9KqTNg0)
- [동영상 - 2020 06 18, 자바, 개념, 스트링 불변셩과 StringBuilder](https://youtu.be/NBBhHcu-xts)

### JDBC-TEXT-BOARD 작업
- [동영상 - 2020_06_18, 자바, 개념, 강사의 깃허브 프로젝트를 실행하는 방법](https://youtu.be/DEie_bXGRc0)
- [동영상 - 2020_06_18, 자바, JDBC-TEXT-BOARD, 클래스들을 별도의 파일로 분리, 파일DB를 전부 MySQL로 교체](https://youtu.be/4OcirCS2xYc)
- [동영상 - 2020 06 18, 자바, JDBC TEXT BOARD, 패키지 분리](https://youtu.be/GIpxEtk-C4Y)

# 2020-06-19, 49일차
## 문제
### DB
- [문제 - 상황에 맞는 SQL을 작성해주세요, INNER JOIN, 사원, 부서](https://codepen.io/jangka44/pen/oNbLNKB)
- [문제 - 정답](https://codepen.io/jangka44/pen/qBdbevG)
- [문제 - 상황에 맞는 SQL을 작성해주세요, SUM, MAX, MIN, COUNT](https://codepen.io/jangka44/pen/jOWrEmj)
- [문제 - 정답](https://codepen.io/jangka44/pen/QWyEwvR)

### JDBC-TEXT-BOARD 작업
- [게시물 상세페이지, 댓글리스트 구현](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/b9364a7eeb0722c97ac849e3336ada5a943e2208)
- [DTO에 JOIN를 통해 얻은 추가데이터를 저장하기 위한 extra 필드추가, 댓글 리스트에 적용](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/a59ea06253c63da1b3371e8714cc3bdf78bc6c71)
- [게시물 리스트에 작성자명 표시](https://github.com/jhs512/JDBC-TEXT-BOARD/commit/3481b7d2e4f8c867c4d073fc31c0304ef55988dd)

## 오늘의 동영상
### DB
- [동영상 - 2020 02 15, DB, 문제, INNER JOIN, 사원, 부서](https://www.youtube.com/watch?v=XgF3wdhrpgg&feature=youtu.be)
- [동영상 - 2020 06 13, DB, 문제, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, 사원, 부서, 1부](https://youtu.be/hfm1TjmPARc)
- [동영상 - 2020 06 13, DB, 문제, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, 사원, 부서, 2부](https://youtu.be/EgralENQWls)
- [동영상 - 2020 06 13, DB, 문제, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, 사원, 부서, 3부](https://youtu.be/vSCg4d2xjIg)
- [동영상 - 2020_06_13, DB, 문제, 상황에 맞는 SQL을 작성해주세요, GROUP, SUM, MIN, COUNT](https://youtu.be/_CDdWebamE0)
- [동영상 - 2020_06_19, DB, 문제, 사원, 부서, INNER JOIN](https://youtu.be/qhnxcUl_WNo)

### JDBC-TEXT-BOARD 작업
- [동영상 - 2020 06 19, 자바, JDBC TEXT BOARD, 게시물 상세페이지, 댓글리스트 구현](https://youtu.be/zAk1iGNEUW8)
- [동영상 - 2020 06 19, 자바, JDBC TEXT BOARD, DTO에 JOIN를 통해 얻은 추가데이터를 저장하기 위한 extra 필드추가, 댓글 리스트에 적용](https://youtu.be/y8pcZet4ikQ)
- [동영상 - 2020 06 19, 자바, JDBC TEXT BOARD, 게시물 리스트에 작성자명 표시](https://youtu.be/PmsTg-kMK8g)

# 숙제
- 공부 동영상, 하루 3시간씩
- 블로그 글 하루 1개
- 자바개념, 요점정리 노트
  - 아주 간략히 추가
  - 추가 후 블로그에 올리기
- 어떤 문제를 풀더라도, 막힘없이 풀 수 있도록

# 유용한 링크
- [JDBC-TEXT-BOARD](https://github.com/jhs512/JDBC-TEXT-BOARD)
- [IT 상식](https://codepen.io/jangka44/live/MWaBoVz)

# 능력단위

## 프로그래밍 기초 - 프로그래밍 언어 활용
- 기간 : 2020-04-09 ~ 2020-05-04
- 수준 : 3
- 강사 : 장희성
- 본평가 : 2020-05-04
- 교재 : 멘토 자바, [프로그래밍 언어 활용](https://ncs.oa.gg/resource/2020-fullstack-course/learning-module/LM2001020215_%ED%94%84%EB%A1%9C%EA%B7%B8%EB%9E%98%EB%B0%8D%EC%96%B8%EC%96%B4%ED%99%9C%EC%9A%A9.pdf)
- 내용
  - 기본문법 활용하기(30시간)
    - 응용소프트웨어 개발에 필요한 프로그래밍 언어의 데이터 타입을 적용하여 변수를 사용할 수 있다.
    - 프로그래밍 언어의 연산자와 명령문을 사용하여 애플리케이션에 필요한 기능을 정의하고 사용할 수 있다.
    - 프로그래밍 언어의 사용자 정의 자료형을 정의하고 애플리케이션에서 사용할 수 있다.
  - 언어특성 활용하기(30시간)
    - 프로그래밍 언어별 특성을 파악하고 설명할 수 있다.
    - 파악된 프로그래밍 언어의 특성을 적용하여 애플리케이션을 구현할 수 있다.
    - 애플리케이션을 최적화하기 위해 프로그래밍 언의의 특성을 활용 할 수 있다.
  - 라이브러리 활용하기(20시간)
    - 애플리케이션에 필요한 라이브러리를 검색하고 선택할 수 있다.
    - 애플리케이션 구현을 위해 선택한 라이브러리를 프로그래밍 언어 특성에 맞게 구성 할 수 있다.
    - 선택한 라이브러리를 사용하여 애플리케이션 구현에 적용할 수 있다.

## 임베디드 애플리케이션 구현 - 펌웨어 구현환경구축
- 2020-05-06 ~ 2020-05-12
- 수준 : 4
- 강사 : 장희성
- 본평가 : 2020-05-12
- 교재 : 멘토 C언어, [펌웨어 구현](https://ncs.oa.gg/resource/2020-fullstack-course/learning-module/LM2001020313,+LM2001020314_%ED%8E%8C%EC%9B%A8%EC%96%B4+%EA%B5%AC%ED%98%84.pdf)
- 내용
  - 펌웨어 설계 문서 분석하기(10시간)
    - 펌웨어 구현에 필요한 하드웨어 테스트 소프트웨어와 부트로더 설계 문서를 확보할 수 있다.
    - 펌웨어 설계 문서에 따라 펌웨어 구현에 필요한 하드웨어 회로도와 데이터시트를 확보할 수 있다.
    - 펌웨어 설계 문서에 따라 펌웨어 구현에 필요한 개발 도구들을 준비할 수 있다.
    - 구현된 펌웨어를 다운로드하여 내부 기능과 제공하는 인터페이스에 대해 시험하기 위한 테스트 환경을 준비할 수 있다.	
  - 하드웨어 데이터시트 분석하기(14시간)
    - 펌웨어 구현에 필요한 하드웨어 설계 문서를 분석할 수 있다.
    - 펌웨어 설계 문서에 따라 하드웨어에 사용된 부품들의 데이터시트를 분석하고 시스템의 메모리 맵과 구성을 분석할 수 있다.
    - 회로도와 데이터시트에 따라 프로그램 대상이 되는 모든 메모리 또는 입출력 치와 그 내부의 레지스터들을 분류하여 메모리 맵 상의 주소를 지정하는 헤더 파일을 작성할 수 있다.
    - 회로도와 데이터시트에 따라 각 레지스터들이 비트 단위로 의미를 가지는 경우 그 비트 마스킹을 위한 헤더 파일을 작성할 수 있다.

## 임베디드 애플리케이션 구현 - 펌웨어 구현
- 2020-05-12 ~ 2020-05-19
- 수준 : 5
- 강사 : 장희성
- 본평가 : 2020-05-19
- 교재 : 멘토 C언어, [펌웨어 구현](https://ncs.oa.gg/resource/2020-fullstack-course/learning-module/LM2001020313,+LM2001020314_%ED%8E%8C%EC%9B%A8%EC%96%B4+%EA%B5%AC%ED%98%84.pdf)
- 내용
  - 하드웨어 테스트 소프트웨어 구현하기(8시간)
    - 하드웨어 테스트를 위하여 소프트웨어를 구현하기 위한 하드웨어 테스트 모듈 설계 문서를 확보할 수 있다.
    - 설계 문서의 의사에 따라 C 언어를 이용하여 소프트웨어를 구현할 수 있다.
    - C 언어를 사용할 수 없는 제어 코드는 인라인 어셈블러 또는 어셈블러 언어를 이용하여 구현할 수 있다.
    - 구현된 코드를 컴파일하여 생성된 모든 오류와 경고에 따라 모든 오류와 경고를 수정할 수 있다.
  - 부트로더 구현하기(8시간)
    - 펌웨어 설계 문서의 의사 코드에 따라 C 언어를 이용하여 소프트웨어를 구현할 수 있다.
    - C 언어를 사용할 수 없는 제어 코드는 인라인 어셈블러 또는 어셈블러 언어를 이용하여 구현할 수 있다.
    - 구현된 코드를 컴파일하여 생성된 모든 오류와 경고에 따라 소스를 수정하여 오류와 경고를 제거할 수 있다.
    - 펌웨어 설계 문서에 따라 부트로더에 링크되지만 공개소스와 같은 직접 작성하지 않은 소스 코드들을 입수하여 컴파일할 수 있다.
    - 컴파일된 목적 코드를 스타트업 코드와 구현된 모듈들을 링크하여 다운로드 가능한 바이너리 파일을 생성할 수 있다.
    - JTAG(Joint Test Action Group) ICE 또는 JTAG 라이터를 이용하여 생성된 바이너리 파일을 시스템의 플래시 메모리에 기록할 수 있다.
  - 소스 코드 인스펙션하기(8시간)
    - 펌웨어 소스 코드의 최적화를 위한 인스펙션을 위해 펌웨어 설계 문서와 소스ㆍ 프로그래밍표준문서들을 출력하여 준비할 수 있다.
    - 펌웨어 소스 코드 프로그래밍 표준 문서에 따라 인스펙션 과정에서 나타난 문제점을 해결책과 함께 문서화 할 수 있다.
    - 소스 코드 인스펙션 결과에 따라 발견된 소스의 문제점을 수정하고 컴파일하여 추가적인 문법 오류와 경고를 제거할 수 있다.	

## 임베디드 애플리케이션 구현환경구축
- 2020-05-19 ~ 2020-05-26
- 수준 : 4
- 강사 : 장희성
- 본평가 : 2020-05-26
- 교재 : 멘토 C언어, [임베디드 애플리케이션 구현](https://ncs.oa.gg/resource/2020-fullstack-course/learning-module/LM2001020323,+LM2001020324_%EC%9E%84%EB%B2%A0%EB%94%94%EB%93%9C+%EC%95%A0%ED%94%8C%EB%A6%AC%EC%BC%80%EC%9D%B4%EC%85%98+%EA%B5%AC%ED%98%84.pdf)
- 내용
  - 기술명세 검토하기(4시간)
    - 개발하고자 하는 도메인 특성과 요구사항이 반영된 형태의 소프트웨어의 기술 스펙을 검토할 수 있다.
    - 개발하고자 하는 도메인 특성과 요구사항이 반영된 형태의 기술 스펙이 적용된 소프트웨어를 검토할 수 있다.
    - 개발하고자 하는 도메인 특성과 요구사항이 반영된 형태의 기술 스펙의 적용 수준을 파악할 수 있다.
  - 애플리케이션 개발 환경 구축하기(20시간)
    - 기능 구현을 위한 개발 환경을 고려ㆍ 검토하고 안정성과 성능이 검증된 개발 도구를 선정할 수 있다.
    - 애플리케이션 개발 환경을 구축하기 위하여 제공되는 명령어 등을 이해하고 프로그램 개발에 사용할 수 있다.
    - 최적의 개발 환경을 구현하기 위한 애플리케이션 개발 환경과 관련된 문서를 찾아 환경을 구축할 수 있다	.

## UI 구현
- 2020-05-26 ~ 2020-07-03
- 수준 : 3
- 강사 : 장희성
- 본평가 : 2020-07-03
- 교재 : [UI 구현](https://ncs.oa.gg/resource/2020-fullstack-course/learning-module/LM2001020708_UI구현)
- 내용
  - UI 설계 검토하기(24시간)
    - UI 제작을 위하여 GUI 디자인 가이드를 이해하고 이를 기반으로 구현 가능성 여부를 검토할 수 있다.
    - 구현 환경에 따라서 구체적인 GUI 프로세스의 이해와 설계 변경 여부를 파악할 수 있다.
    - UI 구현 표준 수립을 위하여 UI 검토 의견서를 작성할 수 있다.
  - UI 구현 표준 수립하기(24시간)
    - 고객 요구사항, 접근성 기준, 플랫폼에 대한 UI 표준 관련 기준을 파악할 수 있다.
    - 실제 페이지 제작을 위하여 아이콘, 레이아웃, 화면 개발 환경에 적합한 표준을 검토할 수 있다.
    - UI 구현 표준을 이해관계자와 검토하여 최종 표준안에 반영할 수 있다.
  - 저작도구 활용하기(32시간)
    - 개발환경에 적합한 저작도구를 파악할 수 있다.
    - UI 구현 표준 검토 결과를 바탕으로 저작도구를 선정할 수 있다.
    - 선정된 저작도구를 이용하여 프로토타입 UI를 제작할 수 있다.
  - UI 제작하기(60시간)
    - UI 화면 제어기능을 어떤 방법으로 할 것인지 선택할 수 있다.
    - 자가 검증을 위하여 단위별 상세 체크리스트를 작성할 수 있다.
    - UI 구현 표준에 따라서 GUI 디자인 가이드를 기반으로 UI를 제작할 수 있다.
    - 작성된 체크리스트에 따라서 단위 테스트를 수행할 수 있다.
    - 단위테스트 수행한 결과의 오류 여부를 확인하여 반영할 수 있다.