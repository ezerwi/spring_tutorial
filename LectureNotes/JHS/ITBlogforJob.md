# 키워드
- IP
- 서버

# ocam
- [설치](https://ohsoft.net/kor/ocam/download.php?cate=1002)
- 세팅
  - 메뉴(상단메뉴) => 옵션 => 효과(좌측탭) => 왼쪽-클릭 효과(상단 탭)
    - 마우스 왼쪽 클릭 효과 추가 : 체크
  - 메뉴(상단메뉴) => 옵션 => 효과(우측탭) => 오른쪽-클릭 효과(상단 탭)
    - 마우스 오른쪽 클릭 효과 추가 : 체크
  - 메뉴(상단메뉴) => 옵션 => 단축키(좌측탭)
    - 녹화 : 우측 상자 클릭 후 : Shift + Ctrl + F2
    - 일시중지 : 우측 상자 클릭 후 : Shift + F2
    - 캡쳐 : 체크해제
    - 대상찾기 : 체크해제
    - 세팅완료 후, 확인버튼 누르기
  - 화면녹화(상단메뉴) => 크기조절 => 전체화면
    - 맨 아래 녹색상자 드래그 하여, 아주 살짝 올림
      - 이게 있어야, 현재 녹화중인지, 일시정지중인지 알 수 있음
  - 화면녹화(상단메뉴) => 소리
    - 마이크 녹음 안함 : 체크
    - 시스템 소리녹음 : 체크해제
- 단축키
  - Shift + Ctrl + F2 : 정지/녹화시작
  - Shift + F2 : 일시정지/일시정지해제

# 동영상 업로드
- [youtube 동영상 업로드](http://youtube.com/upload)
  - 구글 로그인 후 진행
  - 아동용 영상으로 설정하면 안됩니다.

# 공부 Vlog 동영상 이름 포멧
- IT공부, YYYY-MM-DD, 공부시간, 공부내용1, 공부내용2 ...
  - EX : IT공부, 2020-04-24, 30분, 자바배열
  - EX : IT공부, 2020-04-24, 2시간, 3차풀다운메뉴, CSS DINER

# 대기업 합격, 블로깅 예시
- [반윤성](https://medium.com/@covj12)
- [김수빈](https://medium.com/@kimddub)

# 블로그 글 작성법
- [동영상 - 2020 04 13, IT일반, 개념, 티스토리 글 관리법](https://www.youtube.com/watch?v=j8J5XI91DEw&feature=youtu.be)

# 동영상
- [비트와 바이트 by 양주종](https://www.youtube.com/watch?v=MsWfcWDxaB4&feature=youtu.be)
- [아스키ASCII코드 C언어가 문자를 처리하는 방식 by 양주종](https://www.youtube.com/watch?v=x1dd8icBGUo)
- [램이란? by 거니](https://www.youtube.com/watch?v=Vj2rNQFvdDw)
- [(필수)재미있는 포인터 by 양주종](https://www.youtube.com/watch?v=SB5_-WZHwqs)
- [(필수)포인터는 변수다 by 양주종](https://www.youtube.com/watch?v=whDbmBW2iY8)
- [목록 : 얄팍한 코딩사전 by 얄코](https://www.youtube.com/channel/UC2nkWbaJt1KQDi2r2XclzTQ/videos)
- [목록 : 디모의 코딩 기본다지기 by 거니](https://www.youtube.com/playlist?list=PLQdnHjXZyYaczbigUDPNeRXeQrdpo5Xwi)
- [목록 : 코딩에 필요한 필수개념! 디모의 5분코딩상식 by 거니](https://www.youtube.com/playlist?list=PLQdnHjXZyYadDIzz-aPJh3STrtgGbc6Zm)
- [목록 : 5분만에 이해하기! by 거니](https://www.youtube.com/playlist?list=PLLcbGhhl4sQCiZxLuqDDDH6q-rc4wyaKe)
- [쉽게보는 IT 교양상식! 디모의 테크노트 by 디모](https://www.youtube.com/playlist?list=PLQdnHjXZyYadJkTTpaJXPqx4SitXmXwsi)
- [고지식 by 거니](https://www.youtube.com/playlist?list=PLLcbGhhl4sQDOYzzB8eNB7m0IdVffIyLM)
- [교육방송 by 라라](https://www.youtube.com/playlist?list=PLhKL63I03LuY9FFdJpRsS0it7UhspeXMV)