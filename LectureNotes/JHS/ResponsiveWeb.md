<h1>go9.co/JZK</h1>
<h1>새 코드팬 : codepen.io/pen</h1>
<h1>강사 : 장희성 / 010-8824-5558</h1>
<h1>반응형은 One 소스 멀티 유즈</h1>

<pre>
코드몽키 계정
주소 : https://www.playcodemonkey.com/
계정 : jhs.st.4
비번 : 123456
</pre>

<pre>
예전 수업 자료 : http://edu.oa.gg/web.php
</pre>

<pre>
* 2017-07-05
- https://codepen.io/jangka44/pen/dRKWME : inherit 예제
- https://codepen.io/jangka44/pen/YQvVbW : 노말라이즈
- https://codepen.io/jangka44/pen/GEGENM : 다중 리스트
- https://codepen.io/jangka44/pen/ZyRKKo : 풀다운메뉴 #1
- https://codepen.io/jangka44/pen/xrzqgX : 풀다운메뉴 #2
- https://codepen.io/jangka44/pen/bRKRMm : 제이쿼리 맛보기

* 2017-07-06
- https://codepen.io/jangka44/pen/YQjGzE : 풀다운메뉴 #3
- https://codepen.io/jangka44/pen/LLBxqb : white-space, min-width
- https://codepen.io/jangka44/pen/GEBrzy?editors=0100 : a에 display:block과 padding을 주는 이유
- https://codepen.io/jangka44/pen/JJBEmX?editors=0100 1단계
- https://codepen.io/jangka44/pen/MoBpgg?editors=0100 2단계
- https://codepen.io/jangka44/pen/MoByQQ?editors=0100 3단계

* 2017-07-07
- https://codepen.io/jangka44/pen/BZONxj : 구글 메인 사이트 #1
- https://codepen.io/jangka44/pen/EXegPP : 퓨즈툴스 그리드 예제

* 2017-07-10
- https://codepen.io/jangka44/pen/OgaLPB : 퓨즈툴스 레이아웃 예제
- https://codepen.io/jangka44/pen/eRQOLv : float과 부작용 해결
- https://codepen.io/jangka44/pen/zzMOqo : float과 반응형

* 2017-07-10
- https://codepen.io/jangka44/pen/OgaLPB

* 2017-07-11
- https://codepen.io/jangka44/pen/OgaBaG : 반응형 기초 예제
- https://codepen.io/jangka44/pen/pwQGPz : 반응형 기초 예제#2

* 2017-07-12
- https://codepen.io/jangka44/pen/yXGMXq : 반응형 기초 예제#3
- https://codepen.io/jangka44/pen/BZvZro : 파이버 프로 리스트#1
- https://codepen.io/jangka44/pen/pwqwqr : 파이버 프로 리스트#2

* 2017-07-13
- https://codepen.io/jangka44/pen/MoLyZY : 트랜지션 효과#1
- https://codepen.io/jangka44/pen/OgdNGa : 트랜지션 효과#2
- https://codepen.io/jangka44/pen/PjVzzL : 트랜지션 효과#3
- https://codepen.io/jangka44/pen/rwPMRR : 퓨즈툴스, 로테이팅 메뉴

 * 2017-07-14
- https://codepen.io/jangka44/pen/OgqJLB : 퓨즈툴스, 로테이팅 메뉴 복습
- https://codepen.io/jangka44/pen/gRqPzL : ee 그리드
- http://brackets.io/ : 브라켓 다운로드
  - `별` 아이콘 => 미리보기
  - `별` 아이콘 밑 아이콘 클릭으로 부가기능 설치
    - emmet => Tab 킬로 엘리먼트 자동완성 기능 활성화
    - beautify => Ctrl + Alt + B

* 2017-07-17
- https://codepen.io/jangka44/pen/LLvdWW

* 2017-07-18
- https://codepen.io/jangka44/pen/VWObbZ : 화면 좌우로 나누기
- https://codepen.io/jangka44/pen/jwoBKr : 메뉴
- http://brackets.io/ : 브라켓 다운로드
  - `별` 아이콘 => 미리보기
  - `별` 아이콘 밑 아이콘 클릭으로 부가기능 설치
    - emmet => Tab 킬로 엘리먼트 자동완성 기능 활성화
    - beautify => Ctrl + Alt + B
- koala 다운로드
  - http://koala-app.com/
  
* 2017-07-19
- https://codepen.io/jangka44/pen/jwjWjr : 화면 좌우로 나누기
- https://codepen.io/jangka44/pen/LLKGqr : 메뉴
- https://codepen.io/jangka44/pen/PjrNyG : 마우스 올리면 로딩
- https://codepen.io/jangka44/pen/NgZNov : 클릭하면 토글
- https://codepen.io/jangka44/pen/YQoWXq : 클릭하면 토글2

* 2017-07-20
- https://codepen.io/jangka44/pen/QgXeWY : -1 단계
- https://codepen.io/jangka44/pen/bRPXqz : 0 단계
- https://codepen.io/jangka44/pen/NgZQmW : 1 단계
- https://codepen.io/jangka44/pen/xrovvp : 2 단계
- https://codepen.io/jangka44/pen/WOVedo : 2 단계 보충설명
- https://codepen.io/jangka44/pen/EXqYab : 3 단계

* 2017-07-21
- https://codepen.io/jangka44/pen/pwMQZO : 메뉴 만들기 기초 시작!

* 2017-07-24
- https://codepen.io/jangka44/pen/GvRjzQ : 메뉴 만들기 1차
- https://codepen.io/jangka44/pen/vJYXGX : 메뉴 만들기 2차

* 2017-07-25
- https://codepen.io/jangka44/pen/jLErdw : 변수와 함수, if 문

* 2017-07-26
- https://codepen.io/jangka44/pen/BdyGbK : BNX 로고
- https://codepen.io/jangka44/pen/VzYVNO : BNX 메뉴 #1
- https://codepen.io/jangka44/pen/YxPdWP : BNX 메뉴 #2
- https://codepen.io/jangka44/pen/QMwzpO : BNX 메뉴 #3
- https://codepen.io/jangka44/pen/prvqpK : BNX 메뉴 #4
- https://codepen.io/jangka44/pen/GvgPPm : BNX 타이틀 #1
- https://codepen.io/jangka44/pen/OjPrez : 반응형 #1

* 2017-07-27
- https://codepen.io/jangka44/pen/LjVQbZ : 반응형 #2
- https://codepen.io/jangka44/pen/LjVdoX : BNX 메뉴

* 2017-07-28
- https://codepen.io/jangka44/pen/oejZwV : 제이쿼리 리스트
- https://codepen.io/jangka44/pen/prjeVd : 아름씨 예제
- https://codepen.io/jangka44/pen/ayvJxx : 한솔씨 예제
- https://codepen.io/jangka44/pen/jLbmmv : 김이진님 예제
- https://codepen.io/jangka44/pen/ayvWqO : 이다혜님 예제
- https://codepen.io/jangka44/pen/eEpWrq : 정지민님 과제

* 2017-07-31
- https://codepen.io/jangka44/pen/zdrbvx : 마우스 올리면 메뉴 나오도록
- https://codepen.io/jangka44/pen/LjGajW : 반응형 이미지 슬라이드
- https://codepen.io/jangka44/pen/zdrbpq : 한솔씨 예제
- https://codepen.io/jangka44/pen/XaXGvp : 의진씨 예제
- https://codepen.io/jangka44/pen/RZrOoE : 의진씨 예제2

* 2017-08-01
- https://codepen.io/jangka44/pen/GvZdKX : 제이쿼리 기초
- https://codepen.io/jangka44/pen/XameyJ : 이미지 슬라이드(바로 사용 가능한)
- https://codepen.io/jangka44/pen/oexdaa : 주영씨 예제
- https://codepen.io/jangka44/pen/ZJWRYa : 민성씨 예제

* 2017-08-02
- https://codepen.io/jangka44/pen/EvyEqK : 명구씨 예제1
- https://codepen.io/jangka44/pen/oeLqKW : 명구씨 예제2
- https://codepen.io/aniketpant/pen/DsEve : 상명씨 예제2
- https://codepen.io/jangka44/pen/OjXZvq : 효선 예제
- https://codepen.io/jangka44/pen/EvyLGw : 한솔씨 예제

* 2017-08-03
- https://codepen.io/jangka44/pen/vJXJEJ : 예솔씨 예제
- https://codepen.io/jangka44/pen/qXaPJL : 의진씨 예제

* 2017-08-04
- https://codepen.io/jangka44/pen/ZJBWPa : 가감승제
- https://codepen.io/jangka44/pen/eEBzdK : 객체의 비밀

* 2017-08-07
- https://codepen.io/jangka44/pen/OjWZmx : 멀티레이어 배경을 활용한 슬라이드
- https://codepen.io/jangka44/pen/PKWaby : while 문
- https://codepen.io/jangka44/pen/XapYLa : 효선 예제
- https://codepen.io/jangka44/pen/QMdBwN : 프로그램 개념도

* 2017-08-08
- https://codepen.io/jangka44/pen/RZpKVm : 주영씨 예제
- https://codepen.io/jangka44/pen/EvWWZJ : 조달청 로고 예제
- https://codepen.io/jangka44/pen/YxZZRN : 조달청 탑바

* 2017-08-09
- https://codepen.io/jangka44/pen/XaRmjR : 모달
- http://cyberx.tistory.com/5 : 반응형 디자인 원칙

* 2017-08-10
- https://codepen.io/jangka44/pen/YxVMNm : 토글메뉴
- http://programmingsummaries.tistory.com/313 : 이벤트 전파를 막는 방법!

* 2017-08-11
- https://codepen.io/jangka44/pen/QMgxeo : 연습문제
- https://codepen.io/jangka44/pen/qXjyqj : 의진씨 예제
- https://codepen.io/jangka44/pen/xLrJNY : 반응형 버튼
- to2.kr/aca
- https://codepen.io/jangka44/pen/yoXqPY
- https://codepen.io/jangka44/pen/QMgVxg : 페이지 슬라이더(주영씨 예제)

* 2017-08-14
- https://codepen.io/jangka44/pen/NvaxbW : 원페이지 이동 버튼

* 2017-08-16
- to2.kr/acG : 내비게이션 표시

* 2018-08-17
- https://codepen.io/jangka44/pen/gxompe : 패럴럭스 기초
- https://codepen.io/diegopardo/pen/DJKcr : 3차원 폼
- https://codepen.io/jangka44/pen/GvyryW : 자바스크립트 % 연산자

* 2018-08-18
- https://codepen.io/jangka44/pen/oeEgXz : 주영씨 예제
- https://codepen.io/jangka44/pen/BdYyVG : 투명 배경이미지

* 2018-08-21
- to2.kr/adx : 제이쿼리 연습
- to2.kr/adz : 이미지 오버 효과
- https://programmers.co.kr/learn/courses/자바스크립트-입문
- https://programmers.co.kr/learn/courses/특강-크롬을-활용한-프론트엔드-디버깅

* 2018-08-22
- https://codepen.io/jangka44/pen/VzxPvE : 자바스크립트 연습
- to2.kr/adI : CSS 기초 연습

* 2018-08-23
- to2.kr/adO : 문제 1 정답
- to2.kr/adP : 문제 2 정답
- https://codepen.io/jangka44/pen/BdVjYZ : 회전, 마우스 오버시 멈춤

* 2018-08-24
- https://codepen.io/jangka44/pen/EvRzeK : 위 아래 롤링
- https://codepen.io/jangka44/pen/OjEemG : 롤링
- https://codepen.io/jangka44/pen/QMxXqb : 스테이 폴리오 예제

* 2019-08-25
- https://codepen.io/jangka44/pen/YxjOKd : 과자 사이트 1단계
- https://codepen.io/jangka44/pen/KvBxXq : bx Slider

* 2019-08-28
- https://codepen.io/jangka44/pen/gxBbdO : 플라이업, 아이콘 만들기
- https://codepen.io/jangka44/pen/oeagKO : 플라이업, sidebar 조정 v1
- https://codepen.io/jangka44/pen/oeaXgR : 플라이업, sidebar 조정 v2
- https://codepen.io/jangka44/pen/VzELKw : 플라이업, sidebar v3

* 2017-08-29
- https://to2.kr/aeH : 책 예제
- http://www.soen.kr/html5/ : 책 예제2
- https://codepen.io/jangka44/pen/yoRZov : 커스텀 그리드

* 2017-08-30
- https://codepen.io/jangka44/pen/MvzByz : 이미지 슬라이더

* 2017-09-01
- to2.kr/aeX : 모바일 디자인 시작코드

* 2017-09-02
- https://codepen.io/jangka44/pen/JyxZBZ # 플라이업 v1
- https://codepen.io/jangka44/pen/VzgEvX # 플라이업 v2
- https://codepen.io/jangka44/pen/vJbPZE : 플라이 업 v3
- https://codepen.io/jangka44/pen/PKLNev : 플라이 업 v4

* 2017-09-04
- to2.kr/ae5 : 세로 이미지 슬라이더
- to2.kr/ae6 : ul 스마트하게 배열하기 1단계
- to2.kr/ae7 : ul 스마트하게 배열하기 2단계
- to2.kr/afd : li를 첫째로 만들기

* 2017-09-05
- https://codepen.io/jangka44/pen/dzLoGz : bnx 모바일 메뉴 1단계
- https://codepen.io/jangka44/pen/RZOPrQ : bnx 모바일 메뉴 2단계
- to2.kr/aff : 내셔널 지오그래픽 사이트 하단
- to2.kr/afg : 무한 스크롤링 슬라이더 1단계

* 2017-09-06
- https://codepen.io/jangka44/pen/MvRLqa : 알톤 서브메뉴
- to2.kr/afk : 사계절 시계
- https://codepen.io/jangka44/pen/yorwjX : 효선예제
- https://codepen.io/jangka44/pen/YxMgRo : 효선예제2

* 2017-09-08
- https://codepen.io/jangka44/pen/xLBYgd : 툴 세팅

* 2017-09-11
- https://codepen.io/jangka44/pen/XavLpb : 테이블 예제 1
- https://codepen.io/jangka44/pen/GvVbYX : 클릭된 버튼에 해당하는 엘리먼트 노출

* 2017-09-12
- https://codepen.io/jangka44/pen/KXPGeo : 스톡홀름 예제

* 2017-09-13
- https://codepen.io/jangka44/pen/Bwarrr : document ready

* 2017-09-15
- https://codepen.io/jangka44/pen/BwNQvo : 의진씨 예제

* 2017-09-18
- https://codepen.io/jangka44/pen/gGaZMW : $(function() { ... }) 를 사용하는 이유
- https://codepen.io/jangka44/pen/mBeaGB : rem 은 html 엘리먼트의 폰트사이즈와 비례한다.
- https://codepen.io/jangka44/pen/JrYwzR?editors=0100 : CSS calc 함수를 사용하는 이유

* 2017-09-19
- https://codepen.io/jangka44/pen/PJZeoG : 스크롤 이벤트 메뉴
- https://codepen.io/jangka44/pen/WZrJxX : 스크롤 이벤트 메뉴 + 적절한 타이밍에 효과 주기
- https://codepen.io/jangka44/pen/PJZeKm : CSS 애니메이션 기초1
- 외장하드 추천 : http://prod.danawa.com/info/?pcode=3808368&cate=11311925
- https://codepen.io/jangka44/pen/PJZaYQ : CSS 만으로 풀다운 메뉴 구현
- https://codepen.io/jangka44/pen/jGWKNX : 둥근 배경 for 홍한솔

* 2017-09-20
- https://codepen.io/jangka44/pen/wrGPvb : 3차원 글씨
- https://codepen.io/jangka44/pen/zEqPRP : 타원 배경색

* 2017-09-21
- https://codepen.io/jangka44/pen/jGrBvY : 상단바, 하단바 제외하고 전부 채우기
- https://codepen.io/Hyodorie/pen/PJzpdb : 효선 예제

* 2017-09-27
- https://codepen.io/jangka44/pen/Qqpgdm : 주영 예제
- https://codepen.io/jangka44/pen/oGZwyx : 박스 예제
- https://codepen.io/jangka44/pen/OxpgaQ : a 없이 링크걸기
- 코드팬 이외의 환경에서 스크립트 태그는 바디가 닫히기 전에 추가
- https://codepen.io/jangka44/pen/GMWvWG : 스크롤 이동시 메뉴 활성화 변경
</pre>

<h3>설문지 : https://docs.google.com/forms/d/1bjOWH-SfLMGdDdQ2sTf99uGBpuAtaYKyCCxiTAvPurU/viewform?edit_requested=true</h3>
