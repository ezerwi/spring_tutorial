# 2020년 02월 주말 12시 DB/JSP/앱개발 수업 페이지, to2.kr/bcK

# 수업정보
- 수업 페이지 : to2.kr/bcK
- 강사 : 장희성 / 010-8824-5558

# 수업관련 링크
## 유용한 툴
- [크롬 익스텐션, 트랜스 오버](https://chrome.google.com/webstore/detail/transover/aggiiclaiamajehmlfpkjmlbadmkledi)
  - 첫 셀렉트 박스 : Korean
  - 두번째 셀렉트 박스 : point at workd
- [repl 자바 온라인 실행기](https://repl.it/languages/java)
  - 사용법
    - Ctrl + Enter : 실행

## SQL 관련
- [프로그래머스 - SQL](https://programmers.co.kr/learn/challenges?tab=sql_practice_kit)

## 자바 기초 관련
- [자바 수업 페이지](https://to2.kr/a9A)
- [구름 EDU, 바로-실행해보면서-배우는-java-자바](https://edu.goorm.io/lecture/12243/바로-실행해보면서-배우는-java-자바)
- [구름 EDU, 바로실습-생활코딩-java-자바](https://edu.goorm.io/lecture/41/바로실습-생활코딩-java-자바)
- [프로그래머스 - 자바 입문](https://programmers.co.kr/learn/courses/5)
  - [풀이](https://codepen.io/jangka44/debug/MWYqKNm)
- [프로그래머스 - 자바 자료구조](https://programmers.co.kr/learn/courses/17)
  - [풀이](https://codepen.io/jangka44/debug/WNbWJgO)
- [프로그래머스 - 자바 중급](https://programmers.co.kr/learn/courses/9)

## 자바 알고리즘 관련
- [코드업, 100제](https://codeup.kr/problemsetsol.php?psid=23)
  - 자바로 정답 제출
- [프로그래머스 - 알고리즘, 언어 : 자바, 레벨 : 1, 2](https://programmers.co.kr/learn/challenges?tab=all_challenges)
- [코딩게임 COC](https://www.codingame.com/multiplayer/clashofcode)

## 프론트엔드 관련
- [웹 수업 페이지](https://to2.kr/bc9)
- [자바스크립트 트레이닝 페이지](https://to2.kr/aTT)
- [웹 국기 수업 페이지](https://to2.kr/a6r)
- [프로그래머스 자바스크립트 입문](https://programmers.co.kr/learn/courses/3)
- [HTML, CSS, 자바스크립트](https://codepen.io/jangka44/live/MWWqzVO)

# 2020-02-08, 1일차
## 알고리즘 문제
- [같은 숫자는 싫어](https://programmers.co.kr/learn/courses/30/lessons/12906?language=java)

## 문제
- 문제 - 마리아 DB를 설치 후 세팅까지 해주세요.
  - 힌트
    - [XAMPP 다운로드](https://www.apachefriends.org/index.html)
- [문제 - 정답(동영상)](https://www.youtube.com/watch?v=5_tUEm52o0w&feature=youtu.be)
- 문제 - DB 관리 툴(sqlyog 커뮤니티 에디션)을 설치 후 세팅까지 해주세요.
  - 힌트
    - [SQLYOG community 다운로드](https://github.com/webyog/sqlyog-community/wiki/Downloads)
- [문제 - 정답(동영상)](https://www.youtube.com/watch?v=SThu4hxm_Ks&feature=youtu.be)
- [문제 - 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://codepen.io/jangka44/pen/LYVEyye)
- [정답](https://codepen.io/jangka44/pen/XWbJRMN)
- [정답(동영상)](https://www.youtube.com/watch?v=M4-fhS8MZe8&feature=youtu.be)

## 오늘의 동영상
- [동영상 - 2020 02 08, DB, 문제, 마리아 DB를 설치해주세요](https://www.youtube.com/watch?v=5_tUEm52o0w&feature=youtu.be)
- [동영상 - 2020 02 08, DB, 문제, SQLYOG를 설치해주세요](https://www.youtube.com/watch?v=SThu4hxm_Ks&feature=youtu.be)
- [동영상 - 2020 02 08, DB, 문제, 상황에 맞는 SQL을 작성해주세요, 테이블 생성과 수정, 데이터 CRUD](https://www.youtube.com/watch?v=M4-fhS8MZe8&feature=youtu.be)

# 2020-02-09, 2일차
## 문제
- [문제 - 상황에 맞는 SQL을 작성해주세요, NOT NULL, auto_increment, LIKE, AND, OR, 칼럼이름, 칼럼순서 변경](https://codepen.io/jangka44/pen/ExjapKL)
- [문제 - 정답](https://codepen.io/jangka44/pen/MWwYByo)
- [문제 - 정답(동영상)](https://www.youtube.com/watch?v=cwVHKBpz1pg&feature=youtu.be)

## 오늘의 동영상
- [동영상 - 2020 02 09, DB, 문제, 상황에 맞는 SQL을 작성해주세요, NOT NULL, auto_increment, LIKE, AND, OR, 칼럼이름, 칼럼순서 변경](https://www.youtube.com/watch?v=cwVHKBpz1pg&feature=youtu.be)

## 프로그래머스 SQL
- [SELECT 코스(숙제)](https://programmers.co.kr/learn/courses/30/parts/17042)

# 2020-02-15, 3일차
## 개념
- [INNER JOIN, 사원, 부서](https://codepen.io/jangka44/pen/qBdbevG)
- [INNER JOIN, 사원, 부서(동영상)](https://www.youtube.com/watch?v=XgF3wdhrpgg&feature=youtu.be)

## 문제
- [문제 - 문제 1, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, INDEX, UNIQUE INDEX, SQL_NO_CACHE, EXPLAIN, UUID](https://codepen.io/jangka44/pen/MWwbEjp)
  - 상황 : 커뮤니티 사이트 DB를 구축해야 합니다.
  - 조건 : member(회원), article(게시물) 테이블을 구현해주세요.
  - 조건 : 비회원은 글을 쓸 수 없습니다.
  - 조건 : 게시물 상세페이지에서는 제목, 내용, 작성날짜, 작성자가 보여야 합니다.
  - 조건 : 특정 게시물을 어떤 회원이 작성했는지 알 수 있어야 합니다.
  - 조건 : 회원이 자신의 이름을 바꾸면, 그 회원이 예전에 쓴 글에서도 작성자가 자동으로 변경되어야 합니다.
  - 조건 : 회원 아이디와 비번의 칼럼명은, loginId 와 loginPw 로 해주세요.
  - 조건 : loginId에는 unique 인덱스를 걸어주세요.
  - 조건 : 회원 2명이 가입을 햇습니다.
  - 조건 : 1번 회원은 글을 2개(글 1번, 3번), 2번 회원은 글을 1개(2번) 썻습니다.
- [문제 - 정답](https://codepen.io/jangka44/pen/WNvoZMo)
- [문제 - 정답(동영상)](https://www.youtube.com/watch?v=edO7lKZe2cI)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/VwLeobN)
- [문제 - 정답 v2(동영상)](https://www.youtube.com/watch?v=S4kLuoeuwg4)

## 오늘의 동영상
- [동영상 - 2020 02 15, DB 개념, INNER JOIN, 사원, 부서](https://www.youtube.com/watch?v=XgF3wdhrpgg&feature=youtu.be)
- [동영상 - 2020 02 22, DB, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, INDEX, UNIQUE INDEX, SQL NO CACHE, EXPLAIN, UUID](https://www.youtube.com/watch?v=edO7lKZe2cI)
- [동영상 - 2020 02 15, DB, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, INDEX, UNIQUE INDEX, SQL NO CACHE, EXPLAIN, UUID](https://www.youtube.com/watch?v=S4kLuoeuwg4)

## 프로그래머스 SQL
- [SELECT 코스(숙제)](https://programmers.co.kr/learn/courses/30/parts/17042)

# 2020-02-16, 4일차
## PHP 문제
- 문제 - 문제 1
  - 조건 : 게시물 리스트를 보여주세요.
  - 조건 : INNER JOIN을 사용해서, 게시물 작성자도 표시해주세요.
  - 학습내용 : 웹서버 개념, PHP 개념, DB서버 개념, 웹 프로그래밍 개념, 통신 개념
  - 학습내용 : SQL 질의
  - [DB 스키마](https://codepen.io/jangka44/pen/QWbNGQx)
  - [정답 - site3/public/index.php](https://codepen.io/jangka44/pen/jOPqVZp)
  - [정답 - site3/public/index.php - v2](https://codepen.io/jangka44/pen/gOprLKx)
  - [정답 - site3/public/index.php - v3](https://codepen.io/jangka44/pen/OJVNboz)
  - [정답 - site3/public/index.php - v4](https://codepen.io/jangka44/pen/zYGqNvr)
  - [정답 - site3/public/index.php - v5](https://codepen.io/jangka44/pen/KKpzawY)
  - [정답 - site3/public/index.php - v6](https://codepen.io/jangka44/pen/rNVejOV)
  - [정답 - site3/public/index.php - v7](https://codepen.io/jangka44/pen/OJVNWVE)
  - [정답 - site3/public/index.php - v8](https://codepen.io/jangka44/pen/jOPqyEJ)

## 오늘의 동영상
- [2020 02 16, DB, 개념, XAMPP PHP 개발환경 설치](https://www.youtube.com/watch?v=NqFM_YjS-UM&feature=youtu.be)
- [2020 02 16, DB, 개념, 웹 브라우저, 웹 서버아파치, PHP, MySQL의 관계](https://www.youtube.com/watch?v=-Xb3ZQUncjI&feature=youtu.be)
- [2020 02 16, DB, 문제, PHP 문제 연습하는 방법](https://www.youtube.com/watch?v=UR1IuHPxgS8&feature=youtu.be)
- [2020 02 16, DB, 문제, DB의 내용을 PHP를 이용해서 브라우저에서 보이도록 해주세요, 작업 1](https://www.youtube.com/watch?v=bzQJJJKY3mo&feature=youtu.be)
- [2020 02 16, DB, 문제, DB의 내용을 PHP를 이용해서 브라우저에서 보이도록 해주세요, 작업 2](https://www.youtube.com/watch?v=Lg4ANy4o1cU&feature=youtu.be)
- [2020 02 16, DB, 문제, DB의 내용을 PHP를 이용해서 브라우저에서 보이도록 해주세요, 작업 3](https://www.youtube.com/watch?v=zAGL8iOCs0M&feature=youtu.be)
- [2020 02 16, DB, 문제, DB의 내용을 PHP를 이용해서 브라우저에서 보이도록 해주세요, 작업 4](https://www.youtube.com/watch?v=2QRtbgtK8lk&feature=youtu.be)

# 2020-02-22, 5일차
## 문제
- [문제 - 문제 1, 게시판, 조회수 개념 추가](https://codepen.io/jangka44/pen/WNvoZgR)
  - 조건 : 3일차 문제 1에서 이어지는 문제입니다.
  - 조건 : 조회수를 추가해주세요.
  - 조건 : 게시판을 추가해주세요.(공지사항(notice), 자유게시판(free))
  - 조건 : 1번글, 2번글은 자유게시판의 글로 지정
  - 조건 : 3번글은 공지사항의 글로 지정
  - 조건 : 4번글은 1번회원이 공지사항 글로 작성, 조회수 20
  - 조건 : 5번글은 2번회원이 자유게시판 글로 작성, 조회수 40
  - 조건 : 적절한 곳에 index를 걸어주세요.
  - [정답](https://codepen.io/jangka44/pen/YzXprmz)

## PHP 문제
- 문제 - 문제 2
  - 조건 : 게시판 별 리스팅 기능을 추가해주세요.
  - 조건 : 리스트 페이지 주소 => http://localhost:8023/article/list.php?code=free
  - 조건 : 삭제기능(doDelete.php) 추가
  - 조건 : 삭제처리 페이지 주소 => http://localhost:8023/article/doDelete.php?id=1
  - 조건 : 상세페이지(detail.php) 기능 추가
  - 조건 : 상세 페이지 주소 => http://localhost:8023/article/detail.php?id=1
  - 조건 : 상세페이지를 한번 볼 때 마다 조회수 증가
  - [DB 스키마](https://codepen.io/jangka44/pen/YzXprmz)
  - [시작 - site3/public/article/list.php](https://codepen.io/jangka44/pen/jOPqyEJ)
  - [정답 - site3/public/article/list.php](https://codepen.io/jangka44/pen/Poqbxjp)
  - [정답 - site3/public/article/detail.php](https://codepen.io/jangka44/pen/GRJNwEN)
  - [정답 - site3/public/article/doDelete.php](https://codepen.io/jangka44/pen/OJVbagm)

## 오늘의 동영상
- [동영상 - 2020 02 22, DB, 상황에 맞는 SQL을 작성해주세요, INNER JOIN, INDEX, UNIQUE INDEX, SQL NO CACHE, EXPLAIN, UUID](https://www.youtube.com/watch?v=edO7lKZe2cI)
- [동영상 - 2020 02 22, DB, 문제, 멀티 게시판, 조회수를 고려해서 DB구조를 변경해주세요](https://www.youtube.com/watch?v=5Zg4X6GarXg&feature=youtu.be)
- [동영상 - 2020 02 22, DB, 기존 게시물 리스트에, 상세페이지, 삭제, 조회수 증가, 게시판 별 리스팅 기능을 추가해주세요](https://www.youtube.com/watch?v=t3abe4YBsvE&feature=youtu.be)

# 2020-02-23, 6일차
## PHP 문제
- [문제 - 문제 1](https://codepen.io/jangka44/pen/QWbGYNj)
  - 조건 : 위 PHP 문제에서 이어집니다.
  - 조건 : 자유게시물, 3만건, 공지사항, 6만건 으로 만들어주세요.
  - 조건 : article 테이블의 boardId 칼럼의 인덱스 유무에 따른, 속도차이를 비교해주세요.
  - 조건 : 리스트에서 페이징 기능을 넣어주세요.
  - [DB 스키마](https://codepen.io/jangka44/pen/YzXprmz)
  - [정답 - DB](https://codepen.io/jangka44/pen/OJVbdye)
  - [정답 - site3/public/article/list.php](https://codepen.io/jangka44/pen/ExjNrKb)
- 문제 - 문제 2
  - 조건 : 위 PHP 문제에서 이어집니다.
  - 조건 : 게시물 글쓰기 기능을 추가해주세요.
  - 조건 : 게시물 글쓰기 페이지 => http://localhost:8023/article/write.php
  - 조건 : 게시물 글쓰기 처리 페이지 => http://localhost:8023/article/doWrite.php
  - [정답 - site3/public/article/list.php](https://codepen.io/jangka44/pen/XWbNOKa)
  - [정답 - site3/public/article/doWrite.php](https://codepen.io/jangka44/pen/zYGoeBE)
  - [정답 - site3/public/article/write.php](https://codepen.io/jangka44/pen/ExjNryb)
  - [정답 - site3/public/article/detail.php](https://codepen.io/jangka44/pen/abOBXZE)
  - [정답 - site3/public/head.php](https://codepen.io/jangka44/pen/xxGRMOY)

## 오늘의 동영상
- [동영상 - 2020 02 23, DB, 개념, PHP 예제 실행하고 공부하는 방법](https://www.youtube.com/watch?v=rr02VCIBTw4&feature=youtu.be)
- [동영상 - 2020 02 23, DB, 문제, 게시물 개수를 늘리고, 페이징을 해주세요, 작업 1](https://www.youtube.com/watch?v=ab21DNVrcHY&feature=youtu.be)
- [동영상 - 2020 02 23, DB, 문제, 게시물 개수를 늘리고, 페이징을 해주세요, 작업 2](https://www.youtube.com/watch?v=VTSUm8Wk6w4&feature=youtu.be)
- [동영상 - 2020 02 23, DB, 문제, 게시물 추가 기능, 공통 상단](https://www.youtube.com/watch?v=_70zS-ncBxo&feature=youtu.be)

# 2020-02-29, 7일차
## 문제
- [문제 - 문제 1, 게시판, 비회원도 글 작성 가능](https://codepen.io/jangka44/pen/zYGwVea)
  - 조건 : 5일차 문제 1에서 이어지는 문제입니다.
  - 조건 : 비회원도 글을 쓸 수 있게 해주세요.
  - 조건 : 비회원은 글을 쓸 때, 작성자명을 따로 입력받습니다.
  - [정답](https://codepen.io/jangka44/pen/KKpmjLv)
- [문제 - 문제 2, 댓글 작성 가능](https://codepen.io/jangka44/pen/GRJmVpo)
  - 조건 : 7일차 문제 1에서 이어지는 문제입니다.
  - 조건 : 비회원도 댓글을 쓸 수 있게 해주세요.
  - 조건 : 비회원은 대글을 쓸 때, 작성자명을 따로 입력받습니다.
  - [정답](https://codepen.io/jangka44/pen/OJVmeKX)
- [문제 - 문제 3, GROUP 함수를 사용하여 통계정보를 만들어주세요.)](https://codepen.io/jangka44/pen/YzXVmqP)
  - 조건 : 통계정보(게시물 총 개수, 조회수 총합, 글 1개당 평균적인 조회수)
  - 조건 : 회원ID가 같은 데이터를 기준으로 묶었을 때의 그룹함수 사용(조회수의 총합, 평균, 개수, 최고값, 최저값)
  - 조건 : 게시물 별 댓글 개수
  - [정답](https://codepen.io/jangka44/pen/ExjmqPG)

## PHP 문제
- 문제 - 문제 1
  - 조건 : 위 PHP 문제에서 이어집니다.
  - 조건 : 비회원이 쓴 글과 댓글이 존재합니다.
  - 조건 : 로그인/로그아웃이 가능해야 합니다.
  - 조건 : 댓글 리스팅이 가능해야 합니다.
  - 조건 : 게시물 리스트에 댓글 개수가 표시되어야 합니다.
  - 조건 : 자기가 쓴 글은 자신만 삭제할 수 있어야 합니다.
  - [조건 - DB](https://codepen.io/jangka44/pen/LYVLPGb)
  - [조건 - site3/public/article/list.php](https://codepen.io/jangka44/pen/XWbNOKa)
  - [조건 - site3/public/article/doWrite.php](https://codepen.io/jangka44/pen/zYGoeBE)
  - [조건 - site3/public/article/write.php](https://codepen.io/jangka44/pen/ExjNryb)
  - [조건 - site3/public/article/detail.php](https://codepen.io/jangka44/pen/abOBXZE)
  - [조건 - site3/public/head.php](https://codepen.io/jangka44/pen/xxGRMOY)
  - [정답](https://gist.github.com/jhs512/9749f1cc4f248d04ec0689876c2b9f4f)

## 오늘의 동영상
- [동영상 - 2020 02 29, DB, 비회원도 글을 쓸 수 있게 해주세요](https://www.youtube.com/watch?v=MOq9VxqBzGc&feature=youtu.be)
- [동영상 - 2020 02 29, DB, 댓글을 쓸 수 있게 해주세요](https://www.youtube.com/watch?v=O7U9PRxK0cg&feature=youtu.be)
- [동영상 - 2020 02 29, DB, GROUP 함수를 이용해서 게시물 관련 통계를 정보를 보여주세요](https://www.youtube.com/watch?v=EYBN05FeVC0&feature=youtu.be)
- [동영상 - 2020 02 23, DB, 문제, PHP, 비회원, 댓글 리스팅, 로그인 기능 추가](https://www.youtube.com/watch?v=ux70n53oKSo&feature=youtu.be)

# 2020-03-01, 8일차
## PHP 문제
- 문제 - 문제 1
  - 조건 : 7일차 1번 문제에서 이어집니다.
  - 조건 : 메인페이지 꾸미기[완료]
  - 조건 : foot.php 추가하고, html, head, body 엘리먼트 추가[완료]
  - 조건 : $isLogined 변수를 만들어서, 소스코드를 보기 좋게 변경[완료]
  - 조건 : include => include_once 로 변경, include를 상대경로가 아니라 절대경로로 불러오기[완료]
  - [조건 - DB](https://codepen.io/jangka44/pen/LYVLPGb)
  - [조건 - 소스코드](https://gist.github.com/jhs512/9749f1cc4f248d04ec0689876c2b9f4f)
  - [정답](https://gist.github.com/jhs512/5273f6889bdd02af3cbcc06871e1f1df)
- 문제 - 문제 2
  - 조건 : 8일차 1번 문제에서 이어집니다.
  - 조건 : 비회원은 글/댓글 못 쓰도록 처리
  - 조건 : 글 작성시 로그인 한 회원의 memberId가 저장되도록 처리
  - 조건 : 댓글 삭제
  - 조건 : 댓글 작성
  - 조건 : 글 수정
  - [조건, DB, 소스코드](https://gist.github.com/jhs512/5273f6889bdd02af3cbcc06871e1f1df)
  - [정답](https://gist.github.com/81a40f7f8e370d4fc71fc7234cd4c34b)
    - 영상에 못 담은 내용
      - detail.php, 78line 에서 아래와 같이 수정
        - ./doDelete.php?id=<?=$article['id']?>&code=<?=$board['code']?>
- 문제 - 문제 3
  - 조건 : 8일차 2번 문제에서 이어집니다.
  - 조건 : 채팅기능을 넣어주세요.
  - [조건, DB, 소스코드](https://gist.github.com/81a40f7f8e370d4fc71fc7234cd4c34b)
  - [정답](https://gist.github.com/jhs512/ef76d0506d03d1223b5fa3922a57e6a6)

## 오늘의 동영상
- [동영상 - 2020 03 01, DB, PHP 문제 정답을 직접 실행시켜보기, xampp, sqlyog](https://www.youtube.com/watch?v=kcmE3fh8vW4&feature=youtu.be)
- [동영상 - 2020 03 01, DB, PHP, 소스코드 정리, include once로 변경, 절대경로로 include, title 텍스트 개선](https://www.youtube.com/watch?v=zAyIiBmXeOM&feature=youtu.be)
- [동영상 - 2020 03 01, DB, PHP, 글과 댓글 CRUD](https://www.youtube.com/watch?v=Ig5vih4xNeI&feature=youtu.be)
- [동영상 - 2020 03 01, DB, PHP, 채팅](https://www.youtube.com/watch?v=cbqzZIQXxVQ&feature=youtu.be)

# 2020-03-14, 9일차
## 관련링크
- [스프링 부트 설치 메뉴얼](https://codepen.io/jangka44/debug/rNaORbO?editors=1000)
- [실전용 pom.xml, application.yml](https://gist.github.com/jhs512/167101701fdb3c07fb65f4405b0c82a0)
- [자바, 현재시각 구하기](https://code.oa.gg/java8/1540)

## 개념
- 컨트롤러 : 프로그램을 건물로 비유하면, 컨트롤러는 고객의 요청을 최전선에서 처리하는 인포데스크 직원에 비유할 수 있습니다.
- 컨트롤러의 책임 : 컨트롤러는 자신이 모든일을 하지 않고, 자신이 할 수 없는 일은 다른 부서(보통 서비스)에게 토스 합니다.
- @Controller : 해당 클래스가 컨트롤러 라는 것을 스프링 프레임워크에게 알린다.
- @ReqeustMapping : 요청과 메서드를 매핑시킨다.
- @ResponseBody : 메서드의 리턴값을 응답으로 삼는다.
- [컨트롤러 사용법](https://gist.github.com/4d4210a9290fdbe0ceba6a9cf2df13b8)

## 동영상
- [동영상 - 2020 03 14, 스프링부트, 개념, 개발환경세팅, MySQL](https://www.youtube.com/watch?v=L-Fve3OWhYE&feature=youtu.be)
- [동영상 - 2020 03 14, 스프링부트, 개념, 컨트롤러](https://www.youtube.com/watch?v=SdHTPC8VyEg&feature=youtu.be)
- [동영상 - 2019 12 08, 스프링부트, 개념 개발환경설치, 1부](https://youtu.be/UXvdNg96atQ)
- [동영상 - 2019 12 08, 스프링부트, 개념 개발환경설치, 2부](https://youtu.be/JdC3OAI08tM)

# 2020-03-15, 10일차

## 개념
- 실무에서 자주 사용되는 맵 형태
  - Map&lt;String, Object&gt; rs = new HashMap&lt;&gt;();
- 실무에서 자주 사용되는 리스트 형태
  - List&lt;Integer&gt; list1 = new ArrayList&lt;&gt;();
  - List&lt;Object&gt; list2 = new ArrayList&lt;&gt;();
  - List&lt;Article&gt; list3 = new ArrayList&lt;&gt;();
  - List&lt;Map&lt;String, Object&gt;&gt; list4 = new ArrayList&lt;&gt;();
- [개념 - 스프링부트 컨트롤러, 숫자 입출력](https://gist.github.com/jhs512/c213b9659d4d8625af41771e76a92847)
- [개념 - 스프링부트 컨트롤러, 숫자 입출력 v2](https://gist.github.com/jhs512/2620198792e7aa02877de5c0d0175181)
- [개념 - 스프링부트 컨트롤러, 객체 입출력](https://gist.github.com/jhs512/59cefec5a47dba99298fc2c73a25dd43)
- [개념 - 스프링부트 컨트롤러, 객체 입출력 v2](https://gist.github.com/jhs512/4ea9f9ae617dd31e31641437410907a9)

## 문제
- 문제 1
  - 요청1 : /home/up
  - 응답1 : 1
  - 요청1 : /home/up
  - 응답1 : 2
  - 요청1 : /home/up
  - 응답1 : 3
  - 요청1 : /home/up
  - 응답1 : 4
- [문제 - 정답](https://gist.github.com/jhs512/8c24a0afdfa5a8eb3e7ff5de615be9af)
- [문제 - 정답 v2](https://gist.github.com/jhs512/f9cd65835e7ea69e95897d4c4de22b1f)
- 문제 2
  - 요청1 : /home/doAddArticle?title=제목&body=내용
  - 응답1 : {"id":1,"title":"제목","body":"내용"}
  - 요청2 : /home/doAddArticle?title=제목2&body=내용2
  - 응답2 : {"id":2,"title":"제목2","body":"내용2"}
  - 요청3 : /home/doAddArticle?title=제목2&body=내용2
  - 응답3 : {"id":3,"title":"제목3","body":"내용3"}
- [문제 - 정답](https://gist.github.com/jhs512/1cef14e467c372868538848dddf320e9)
- [문제 - 정답 v2](https://gist.github.com/jhs512/fc3b11d8855289ab94cbe4734efae019)
- 문제 3
  - 요청1 : /home/doAddArticle?title=제목&body=내용
  - 응답1 : 1번 글이 생성되었습니다.
  - 요청2 : /home/doAddArticle?title=제목2&body=내용2
  - 응답2 : 2번 글이 생성되었습니다.
  - 요청3 : /home/doAddArticle?title=제목3&body=내용3
  - 응답3 : 3번 글이 생성되었습니다.
  - 요청4 : /home/getArticles
  - 응답4 : [{"id":1,"title":"제목","body":"내용"},{"id":2,"title":"제목2","body":"내용2"},{"id":3,"title":"제목3","body":"내용3"}]
- [문제 - 정답](https://gist.github.com/jhs512/ae3eb49fa8d7c42373ac518eb43b909b)
- [문제 - 정답 v2](https://gist.github.com/jhs512/fda078587e13ec4a72b762f6ab5b8203)

## 오늘의 동영상
- [동영상 - 2020 03 15, 스프링부트, 개념, 자주쓰이는 콜렉션](https://www.youtube.com/watch?v=OV3r1drbh5U&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 개념, 컨트롤러, 숫자 입출력](https://www.youtube.com/watch?v=Dm6wqjeK9PA&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 문제, 컨트롤러 액션을 호출할 때마다 상태변경, 숫자증가](https://www.youtube.com/watch?v=NrXxEDOxymc&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 개념, 컨트롤러, 맵, 리스트, 객체 입출력 1부](https://www.youtube.com/watch?v=_TTx4GjXlis&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 개념, 컨트롤러, 맵, 리스트, 객체 입출력 2부](https://www.youtube.com/watch?v=1bbVf4EnFIQ&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 문제, 게시물 정보 입력받는 컨트롤러 구현, 롬복](https://www.youtube.com/watch?v=VktonHKNmzI&feature=youtu.be)
- [동영상 - 2020 03 15, 스프링부트, 문제, 게시물 정보 입력받는 컨트롤러 구현, 롬복, 게시물 객체 저장](https://www.youtube.com/watch?v=QmY9GHZxPyg&feature=youtu.be)

# 2020-03-21, 11일차
## 개념
- DI 문제 시작
- [문제 - 전사가 칼로 공격하게 해주세요.](https://code.oa.gg/java8/1611)
- [문제 - 정답](https://code.oa.gg/java8/1612)
- [문제 - 활로 공격하는 전사, 창으로 공격하는 전사도 추가해주세요.](https://code.oa.gg/java8/1613)
- [문제 - 정답](https://code.oa.gg/java8/1614)
- [문제 - 전사, 배낭, 무기와 관련해서, 표준적인 규칙을 만들어서 코드량을 줄여주세요.](https://code.oa.gg/java8/1615)
- [문제 - 정답](https://code.oa.gg/java8/1616)
- [문제 - 배낭 클래스를 1개로 줄여주세요.](https://code.oa.gg/java8/1617)
- [문제 - 정답](https://code.oa.gg/java8/1618)
- [문제 - 배낭 레시피를 모아둘, 배낭공장 클래스를 만들어주세요.](https://code.oa.gg/java8/1619)
- [문제 - 정답](https://code.oa.gg/java8/1620)
- [문제 - 전사 클래스를 1개로 줄여주세요.](https://code.oa.gg/java8/1621)
- [문제 - 정답](https://code.oa.gg/java8/1622)
- [문제 - 전사 레시피를 모아둘, 전사공장 클래스를 만들어주세요.](https://code.oa.gg/java8/1623)
- [문제 - 정답](https://code.oa.gg/java8/1624)
- 문제 - 정답
- DI 문제 끝

## 개념
- 서비스
  - 역할 : 실무팀
  - 하는 일
    - 컨트롤에게 받은 일을 처리한 후, 컨트롤러에게 응답
    - 데이터를 가져오기, 삭제, 수정을 직접하지 않고, DAO에게 위임한다.
- 컨트롤러
  - 역할 : 인포데스크 팀
  - 하는 일
    - 고객(브라우저)의 요청을 접수 후 적절한 서비스로 토스
    - 서비스에서 받은 응답을 고객이 이해할 수 있는 형태로 변환 후 고객에게 전달(응답)
- DAO
  - 역할: DB와의 영업사원
  - 하는 일
    - 서비스의 요청을 받아서 데이터를 직접 처리하거나, 다른 DBMS에게 위임 한후 결과를 보고
    
## 문제
- [개념 : 의존성 주입, DI](https://gist.github.com/jhs512/337562c44e1b9fc4880f83ab48a1da55)
- 문제 1
- 조건 : 스프링의 의존성 주입을 이용한 예제를 만들어주세요.
- [문제 - 정답](https://gist.github.com/jhs512/427bcd5c80d6cfe531d3235114a91202)
- 문제 2
- 조건 : 게시물 쓰기, 전체 게시물 조회 기능을 ArticleDao, ArticleService, Autowired 까지 사용해서 구현해주세요.
- [문제 - 정답](https://gist.github.com/jhs512/b3775ebb2f829183334106e02499797e)
- [문제 - 정답 v2](https://gist.github.com/jhs512/eb4e2ea452a48d4dc17bacf1360e81ea)

## 동영상
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_1, 전사가 칼로 공격하게 해주세요](https://www.youtube.com/watch?v=Fn92cW2daYQ&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_2, 활전사와 창전사 추가](https://www.youtube.com/watch?v=gZM7gJuqxRI&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_3, 표준화](https://www.youtube.com/watch?v=2qwRnCAH4Zc&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_4, 배낭 클래스 1개로](https://www.youtube.com/watch?v=2qwRnCAH4Zc&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_5, 배낭객체 팩토리](https://www.youtube.com/watch?v=eobkVp0uXaw&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_6, 전사 클래스 1개로](https://www.youtube.com/watch?v=U7W36JlDII8&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 문제, DI 기초문제 1_7, 전사객체 팩토리](https://www.youtube.com/watch?v=B5kuw1zcKlo&feature=youtu.be)
- [동영상 - 2020_03_21, 스프링부트, 개념, 의존성 주입](https://www.youtube.com/watch?v=sEYw46FUWxQ&feature=youtu.be)
- [동영상 - 2019 12 14, 스프링 부트, DI, IOC](https://www.youtube.com/watch?v=wJImayWRKY0&feature=youtu.be)
- [동영상 - 2019 12 14, 스프링 부트, DAO, 서비스, NO DB, 게시물 관리 프로그램 구현](https://www.youtube.com/watch?v=IaHkFQLQ4NY&feature=youtu.be)
- [동영상 - 2020 03 21, 스프링 부트, DAO, 서비스, NO DB, 게시물 관리 프로그램 v2](https://www.youtube.com/watch?v=9IiL5s0OPWM&feature=youtu.be)

# 2020-03-22, 12일차
## 문제
- 문제 1
  - 조건 : 간단한 게시물 리스팅을 해주세요.
  - [정답 v1](https://gist.github.com/jhs512/db78327cc92f4da37338e1285880a13d)

## 숙제
- 문제 1번의 정답 v2를 혼자힘으로 구현
  - db.sql
  - application.yml
    - 추가적인 부분
  - pom.xml
    - 추가적인 부분
  - ArticleController.java
  - ArticleService.java
  - ArticleServiceImpl.java
  - ArticleDao.java
  - ArticleDao.xml
    - 형식적인 부분 제외
  - ArticleDto.java
  - list.jsp
    - 형식적인 부분 제외

## 오늘의 동영상
- [동영상 - 2020 03 22, 스프링부트, 문제, 게시물 리스트를 보여주세요, 마이바티스, MySQL, 1 2](https://www.youtube.com/watch?v=URITadRKJ2U&feature=youtu.be)
- [동영상 - 2020 03 22, 스프링부트, 문제, 게시물 리스트를 보여주세요, 마이바티스, MySQL, 2 2](https://www.youtube.com/watch?v=2h8EQQ9XawI&feature=youtu.be)
- [동영상 - 2020 03 22, 스프링부트, 개념, 수업페이지 예제 실행하는 방법](https://www.youtube.com/watch?v=8B7HiUwu2dU&feature=youtu.be)

# 2020-03-28, 13일차
## 문제
- 문제 1
  - 조건 : 게시물 쓰기 기능을 구현해주세요.
  - [정답](https://gist.github.com/jhs512/db78327cc92f4da37338e1285880a13d)
- 문제 2
  - 조건 : 게시물 수정/상세보기/삭제 기능을 구현해주세요.
  - [정답](https://gist.github.com/jhs512/efbbfdbbd4cfd0f17fff3abb6174f5c6)

## 오늘의 동영상
- [동영상 - 2020 03 28, 스프링부트, 문제, 글쓰기 기능 구현](https://www.youtube.com/watch?v=F4BXpRdFC4o&feature=youtu.be)
- [동영상 - 2020 03 28, 스프링부트, 문제, 수정, 삭제, 상세보기 기능 구현](https://www.youtube.com/watch?v=ik2v25Y_lA0&feature=youtu.be)

# 2020-03-29, 14일차
## 문제
- 문제 1
  - 조건 : jsp의 include를 이용해서, 중복 내용을 줄여주세요.
  - 조건 : css/common.css, js/common.js 도입
  - [정답](https://gist.github.com/jhs512/8062a3c74617e86e59e304bc8b9bc699)
- 문제 2
  - 조건 : 설정파일에 있는 공통정보 가져와서 JSP에 적용하기
  - 조건 : 인터셉터를 활용해서, 공통적으로 필요한 정보를 HttpRequest 객체에 로딩
  - [정답](https://gist.github.com/82ae5cb1770626cadfce4c473bd55e7d)

## 오늘의 동영상
- [동영상 - 2020 03 29, 스프링부트, 문제, 모든 페이지에서 자주 사용하는 뷰 부분, 따로 분리](https://www.youtube.com/watch?v=hpmSpzbejA8)
- [동영상 - 2020 03 29, 스프링부트, 문제, 인터셉터를 사용하여 모든 페이지에서 사용하는 데이터를 request객체에 바인딩](https://www.youtube.com/watch?v=606Oo6z7dGU)
- [동영상 - 2020 03 29, 스프링부트, 개념, 공부하는 방법](https://www.youtube.com/watch?v=CFQdSgyRnRA)

# 2020-04-04, 15일차
## 문제
- 문제 1
  - 조건 : 14일차 문제 2번을 처음부터 다시 만들어주세요.
  - [정답](https://gist.github.com/jhs512/2b4a4ac87ab7a31b5b86ed4631d6f5d9)
- 문제 2
  - 조건 : 세션으로 간단한 로그인을 구현해주세요.
  - [정답](https://gist.github.com/jhs512/483f8075ebe63fc8c00519c91310da01)

## 오늘의 동영상
- [동영상 - 2020 04 04, 스프링부트, 문제, 간단한 게시물 CRUD를 구현해주세요](https://www.youtube.com/watch?v=2yFBXQ4vjAI&feature=youtu.be)
- [동영상 - 2020 04 04, 스프링부트, 문제, 간단한 로그인 기능을 구현해주세요](https://www.youtube.com/watch?v=odvMSPET2Tw&feature=youtu.be)

# 2020-04-05, 16일차
## 문제
- 문제 1
  - 회원가입 기능을 구현해주세요.
  - 조건 : 15일차 문제 1번을 부터 시작해주세요.
  - [정답](https://gist.github.com/jhs512/6048ee3f4ba1a7aa269db7eeb0883bc3)
- 문제 2
  - 로그인, 로그아웃 기능을 구현해주세요.
  - [정답](https://gist.github.com/jhs512/f6666bb8e34a8e62e36baa4073b89656)

## 오늘의 동영상
- [동영상 - 2020 04 05, 스프링부트, 문제, 회원가입 기능을 구현해주세요](https://www.youtube.com/watch?v=RcONIQwk7RE&feature=youtu.be)
- [동영상 - 2020 04 05, 스프링부트, 문제, 로그인, 로그아웃 기능을 구현해주세요](https://www.youtube.com/watch?v=2JdU4fmjATA&feature=youtu.be)

# 이후 내용
- [스프링 커뮤니티 사이트 구현, 1기](https://to2.kr/a9r)
- [스프링 커뮤니티 사이트 구현, 2기](https://to2.kr/bf6)

# 취업을 위한 필수 수행과제
- 블로깅 하기
  - [예시](https://medium.com/@covj12)
- IT 상식 공부
  - [서버란 무엇인가?](https://www.youtube.com/watch?v=R0YJ-r-qLNE)
  - [가비지 컬렉터가 뭐하는 건가요? (Feat. 메모리 관리)](https://www.youtube.com/watch?v=24f2-eJAeII)
  - [try? catch? 예외처리란 뭐 하는 건가요?](https://www.youtube.com/watch?v=LQ182IQZfW8)
  - [MVC 웹 프레임워크가 뭔가요?](https://www.youtube.com/watch?v=AERY1ZGoYc8)
  - [쿠키, 세션, 캐시가 뭔가요?](https://www.youtube.com/watch?v=OpoVuwxGRDI)
  - [정적 웹은 뭐고 동적 웹은 뭔가요?](https://www.youtube.com/watch?v=C06xRvXIAUk)
  - [HTML, CSS, JavaScript가 뭔가요?](https://www.youtube.com/watch?v=ffENjt7aEdc)
  - [객체지향 프로그래밍이 뭔가요?](https://www.youtube.com/watch?v=vrhIxBWSJ04)
  - [REST API가 뭔가요?](https://www.youtube.com/watch?v=iOueE9AXDQQ)
  - [Scope가 뭔가요? (feat: let, const, var의 차이)](https://www.youtube.com/watch?v=HsJ4oy_jBx0)