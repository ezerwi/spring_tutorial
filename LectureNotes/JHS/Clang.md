# 페이지 정보
- 주소 : to2.kr/bj6
- 클래스 : 2020년 05월 C언어

# 강사정보
- 이름 : 장희성
- 전화번호 : 010-8824-5558

# 수업관련링크
- [구름 EDU C언어](http://edu.goorm.io/lecture/201/바로-실행해보면서-배우는-c언어)
- [강사전용 c언어, repl](https://repl.it/@jangka512/my-c)
- [repl.it/languages/c](https://repl.it/languages/c)
- [repl.it/languages/cpp11](https://repl.it/languages/cpp11)
  - 사용법
    - 실행 : Ctrl + Enter

# 01, 출력
## 개념
- `printf("안녕");` 은 함수 호출이다.
  - 쉽게 말해 함수 사용이다.
- 프로그래밍 언어는 위에서 아래로 1줄씩 실행된다.
- Visual Studio 2019에서 scanf 함수 호출할 때
  - #pragma warning (disable: 4996)
  - 위 라인을 최상단에 추가

## 문제
- [문제 - 안녕하세요. 10줄 출력(개념 : 프로그램 실행, printf)](https://repl.it/@jangka512/C-2018-07-17-1)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-17-2)
- [개념 - 변수](https://repl.it/@jangka512/WeeKnowingShockwave)
- [문제 - 변수 a와 b의 값을 교체(개념 : 변수, 서식지정자, 변수 값 swap)](https://repl.it/@jangka512/C-2018-07-17-3)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-17-4)
- [문제 - 변수 a와 b의 값을 교체(개념 : 변수, 변수 값 swap)(오직 변수만 사용가능)](https://repl.it/@jangka512/C-2018-07-17-5)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-17-6)

## 동영상
- [동영상 - 2020 01 14, 프로그래밍, 기초 키보드 사용법](https://www.youtube.com/watch?v=e5d_Hv6c6QI&feature=youtu.be)
- [동영상 - 2020 03 14, C언어, 개념, 변수](https://www.youtube.com/watch?v=Y-M6GwS974U&feature=youtu.be)
- [동영상 - 2020 02 13, C언어, 문제, 2개의 변수가 가지고 있는 값을 서로 뒤바꿈](https://www.youtube.com/watch?v=gbI_oU5W-Rw)
- [동영상 - 2020 03 14, C언어, 문제, 2020 03 14, C언어, 문제, 변수 swap, 무식한 방법](https://www.youtube.com/watch?v=3SryvyOkm_k&feature=youtu.be)
- [동영상 - 2020 02 14, C언어, 문제, 두 변수의 값 SWAP](https://www.youtube.com/watch?v=4KJyGkrhcZQ&feature=youtu.be)
- [동영상 - 2020 03 14, C언어, 문제, 2020 03 14, C언어, 문제, 변수 swap, 좋은 방법](https://www.youtube.com/watch?v=zhuWuJM-eIg&feature=youtu.be)
- [동영상 - 2020 02 13, C언어, 개념, 수업페이지 문제 푸는 방법, VS2019, repl](https://www.youtube.com/watch?v=jPfher-06Hs&feature=youtu.be)
- [동영상 - 2020 02 13, C언어, VS2019 사용법](https://www.youtube.com/watch?v=tA--JLoUVSI&feature=youtu.be)
- [동영상 - 2018 06 28, C언어, 개념, repl.it 툴 세팅 및 변수 기초](https://www.youtube.com/watch?v=QR9LkL1BQ2M)

# 02, 조건문
## 문제(조건문)
- [문제 - 나이에 따른, 올바른 성년 여부가 나와야 합니다.](https://repl.it/@jangka512/AridCrispParallelport)
- [문제 - 정답](https://repl.it/@jangka512/WellinformedBossyYottabyte)
- [문제 - 실행되는 출력문에는 참 그렇지 않으면 거짓 이라고 적어주세요.(개념 : 조건문, if문)](https://repl.it/@jangka512/FlawedMaroonSpreadsheets)
- [문제 - 정답](https://repl.it/@jangka512/KaleidoscopicSilkyDaemon)
- [문제 - 할인 대상인지 아닌지 출력(개념 : 조건문, if문)](https://repl.it/@jangka512/C-2018-07-18-1)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-18-2)
- [문제 - 할인 대상인지 아닌지 출력(&&, || 없이)(개념 : 조건문, if문, else if문, else 문, 2중 if문)](https://repl.it/@jangka512/C-2018-07-18-3)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-18-4)
  - 정답 v1 : else, &&, || 사용 안한 버전
  - 정답 v3 : &&, || 사용 안한 버전
  - 정답 v4, v5 : &&, || 사용 버전
- [문제 - 정답 v2(정답 11개)](https://repl.it/@jangka512/HonoredCarefulGuiltware)
- [코드업 문제집 기초 3. if ~ else](https://codeup.kr/problemsetsol.php?psid=11)
  - 10문제만 풀어주세요.

## 동영상
- [동영상 - 2020 03 15, C언어, 개념, if](https://www.youtube.com/watch?v=rvs13urrQmQ&feature=youtu.be)
- [동영상 - 2020 03 15, C언어, 개념, if, else if, else](https://www.youtube.com/watch?v=Kbn0DQY8efQ&feature=youtu.be)
- [동영상 - 2020 03 15, C언어, 개념, printf, 변수](https://www.youtube.com/watch?v=nrGAVbB9lQc&feature=youtu.be)
- [동영상 - 2020 03 15, C언어, 문제, 조건에 따라 참, 거짓을 적어주세요](https://www.youtube.com/watch?v=eLHNoyc5EUY&feature=youtu.be)
- [동영상 - 2020 03 15, C언어, 문제, 할인 대상인지 출력, and, or 사용](https://www.youtube.com/watch?v=3AZ-O3uCRZY&feature=youtu.be)
- [동영상 - 2020 03 15, C언어, 문제, 할인 대상인지 출력, and, or 사용금지](https://www.youtube.com/watch?v=NBjK2TscSNE&feature=youtu.be)
- [동영상 - 2020 03 15, 프로그래밍 개념, code org 진행하는 방법](https://www.youtube.com/watch?v=V6YFDzQpPF4&feature=youtu.be)
- [동영상 - 2020 03 15, 프로그래밍 개념, codeup 진행하는 방법](https://www.youtube.com/watch?v=e3lODy64Dgs&feature=youtu.be)
- [동영상 - 2018 06 29, C언어, 개념, if문, 조건문](https://www.youtube.com/watch?v=4HaCSjR2t-g)
- [동영상 - 2020 02 14, C언어, 문제, 참, 거짓을 명시해주세요, 논리연산자](https://www.youtube.com/watch?v=0fS8mnNc9sI&feature=youtu.be)
- [동영상 - 2020 02 17, C언어, 개념, 코드업 하는 법](https://www.youtube.com/watch?v=PK6r3uUqrmA&feature=youtu.be)
- [동영상 - 2020 02 17, C언어, 문제, 할인 대상인지 아닌지 출력, if문](https://www.youtube.com/watch?v=IlUQudtOZtE&feature=youtu.be)

# 03, 반복문
## 문제(반복문)
- [문제 - 숫자 1만 사용해서 1부터 10까지 출력하기(개념 : 변수)](https://repl.it/@jangka512/AptNonstopCoordinates)
- [문제 - 정답](https://repl.it/@jangka512/IncompatibleTerribleProgrammers)
- [문제 - 숫자 1만 사용해서 구구단 1단 만들기(개념 : 변수)](https://repl.it/@jangka512/C-2018-07-18-5)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-18-6)
- [문제 - 숫자 1만 사용해서 구구단 1단 만들기, 1000까지 곱하기(개념 : 변수)](https://repl.it/@jangka512/WetSandybrownIntranet)
- [문제 - 정답](https://repl.it/@jangka512/ConsciousWearableMedia)
- [개념 - while문](https://repl.it/@jangka512/StridentLuckySymbol)
- [문제 - 숫자 1만 사용해서 구구단 1단 만들기(개념 : while)](https://repl.it/@jangka512/C-2018-07-18-7)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-18-8)
- [개념 : for문](https://repl.it/@jangka512/WeeSaddlebrownApplicationprogrammer)
- [문제 - for문을 사용해서 구구단 4단 출력(개념 : for)](https://repl.it/@jangka512/C-2018-07-19-3)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-19-4)
- [문제 - 400부터 -200까지 출력해주세요.(개념 : 반복문)](https://repl.it/@jangka512/AcidicFrugalSystems)
- [문제 - 정답](https://repl.it/@jangka512/UtterMetallicLicense)

## 동영상
- [동영상 - 2020 03 21, C언어, 개념, 변수, if문, 출력](https://www.youtube.com/watch?v=cTujrzKq9gg&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, 숫자 1만 사용해서 1부터 10까지 출력하기](https://www.youtube.com/watch?v=F2cGeCrZb0c&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, 숫자 1만 사용해서 1부터 10까지 출력하기, 추가설명](https://www.youtube.com/watch?v=aIcl13GQTIA&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, 숫자 1만 사용해서 구구단 1단 출력](https://www.youtube.com/watch?v=-yM9HGQtsO4&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, 숫자 1만 사용해서 구구단 1단 출력, 1000까지 곱하기](https://www.youtube.com/watch?v=LJkQvDkRTwY&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 개념, while 문](https://www.youtube.com/watch?v=mWY7ciEhOz4&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, 숫자 1만 사용해서 구구단 1단 출력, 1000까지 곱하기, while](https://www.youtube.com/watch?v=ah_FWsyY4r0&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 개념, for 문](https://www.youtube.com/watch?v=q9BvWpyJ0gs&feature=youtu.be)
- [동영상 - 2020 03 21, C언어, 문제, for로 구구단 4단 출력, 변수](https://www.youtube.com/watch?v=U3cijN-XTKo)
- [동영상 - 2020 03 21, C언어, 문제, 400부터 음수 200까지 출력](https://www.youtube.com/watch?v=U3cijN-XTKo)

## 동영상
- [동영상 - 2018 06 29, C언어, 개념, 반복문, while](https://www.youtube.com/watch?v=16r3dyvKzSs)
- [동영상 - 2020 02 18, C언어, 개념, for, 반복문](https://www.youtube.com/watch?v=uPpY4-iWCUE)
- [동영상 - 2020 02 18, C언어, 개념, while, 반복문](https://www.youtube.com/watch?v=rXPHWJTzjd8)
- [동영상 - 2020 02 18, C언어, 문제, 숫자 1만 사용해서 1부터 10까지 출력하기, 변수](https://www.youtube.com/watch?v=9IjgWJegTuY)
- [동영상 - 2020 02 18, C언어, 문제, 숫자 1만 사용해서 구구단 1단 출력, 1000까지 곱하기, 변수](https://www.youtube.com/watch?v=EI6VGUp2tS8)
- [동영상 - 2020 02 18, C언어, 문제, 숫자 1만 사용해서 구구단 1단 출력, 변수](https://www.youtube.com/watch?v=JYnIJh5cXIc)
- [동영상 - 2020 02 18, C언어, 문제, while로 구구단 1단 출력, 변수](https://www.youtube.com/watch?v=iFFn3X14BJU)
- [동영상 - 2020 02 18, C언어, 문제, for로 구구단 4단 출력, 변수](https://www.youtube.com/watch?v=80l6bSZoBQY)

# 04, 반복문(심화)
## 문제
- [문제 - 1부터 100까지의 합을 출력해주세요(개념 : 반복문)](https://repl.it/@jangka512/C-2018-07-19-7)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-19-8)
- [문제 - 40부터 255까지의 합을 출력해주세요.(개념 : 반복문)](https://repl.it/@jangka512/AutomaticSlategrayTrigger)
- [문제 - 정답](https://repl.it/@jangka512/SoulfulRespectfulRuntimeenvironment)
- [문제 - 1부터 100까지의 짝수의 합을 출력해주세요(개념 : 반복문, 조건문)](https://repl.it/@jangka512/C-2018-07-19-9)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-19-10)
- [문제 - 100부터 200까지의 짝수의 합을 출력해주세요(개념 : 반복문, 조건문)](https://repl.it/@jangka512/AchingKindlyGraphs)
- [문제 - 정답](https://repl.it/@jangka512/RobustBlueAutomatedinformationsystem)
- [문제 - 음수 200부터 100사이의 짝수 중에서 3의 배수가 아닌 수의 합을 출력해주세요.(개념 : 반복문, 조건문)](https://repl.it/@jangka512/GrouchyEssentialRuntime)
- [문제 - 정답](https://repl.it/@jangka512/DisastrousWryNormalform)
- [문제 - 1000의 모든 약수 출력하기](https://repl.it/@jangka512/C-2018-07-20-1)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-20-2)
- [개념 - 이중 while문을 이용해서 1부터 3까지 10번 출력](https://repl.it/@jangka512/AgreeableUnconsciousSecurity)
- [문제 - 이중 while문을 사용해서 구구단 2~8단 출력(개념 : 2중 while문)](https://repl.it/@jangka512/C-2018-07-19-1)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-19-2)
- [문제 - 이중 for문을 사용해서 구구단 2~8단 출력(개념 : 2중 for문)](https://repl.it/@jangka512/C-2018-07-19-5)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-19-6)

## 동영상
- [동영상 - 2020 02 19, C언어, 문제, 1부터 100까지의 합을 출력](https://www.youtube.com/watch?v=3OR6i61_E3g&feature=youtu.be)
- [동영상 - 2020 02 19, C언어, 문제, 40부터 255 사이에 있는 정수의 합을 출력해주세요.](https://youtu.be/j9SUf7sVef0)
- [동영상 - 2020 02 19, C언어, 문제, 100부터 200 사이에 있는 짝수의 합 출력](https://youtu.be/17YMfRlV0-I)
- [동영상 - 2020 02 19, C언어, 문제, 1부터 100 사이에 있는 짝수의 합 출력](https://youtu.be/opVMid2hICI)
- [동영상 - 2020 02 19, C언어, 문제, 100부터 200 사이에 있는 짝수의 합 출력](https://youtu.be/17YMfRlV0-I)
- [동영상 - 2020 02 19, C언어, 문제, 음수 200부터 100사이의 짝수 중에서 3의 배수가 아닌 수의 합을 출력](https://youtu.be/p-tLqyLh22s)
- [동영상 - 2020 02 19, C언어, 문제, 1000의 약수를 모두 출력](https://youtu.be/zom0n5r4BOA)
- [동영상 - 2020 03 22, C언어, 개념, 이중 while문을 이용해서 1부터 3까지 10번 출력](https://www.youtube.com/watch?v=IP66Q41zfmo&feature=youtu.be)
- [동영상 - 2020 02 19, C언어, 문제, 구구단 2~8단 출력, 중첩 while문](https://youtu.be/xIUjnmPQXbM)
- [동영상 - 2020 02 19, C언어, 문제, 구구단 2~8단 출력, 중첩 for문](https://youtu.be/QidTsWfdC2g)

# 05, 함수
## 문제
- [문제 - 함수를 만들어주세요.(개념 : 함수)](https://repl.it/@jangka512/RuralFinishedButton)
- [문제 - 정답](https://repl.it/@jangka512/BlackIntentFunction)
- [문제 - 함수를 만들어주세요.(개념 : 함수, 매개변수)](https://repl.it/@jangka512/CrookedImpureMention)
- [문제 - 정답](https://repl.it/@jangka512/CumbersomeUntimelyControlflowgraph)
- [문제 - 함수를 만들어주세요.(개념 : 함수, 매개변수, 복수개의 매개변수)](https://repl.it/@jangka512/SpitefulJitteryEvaluation)
- [문제 - 정답](https://repl.it/@jangka512/DimgreyLightpinkJavascript)
- [문제 - 함수를 만들어주세요.(개념 : 함수, 매개변수, 리턴)](https://repl.it/@jangka512/UncomfortableAdmiredGigabyte)
- [문제 - 정답](https://repl.it/@jangka512/HarmfulBitterInitialization)
- [문제 - 4칙연산을 하는 4개의 함수를 만들어주세요.(개념 : 함수, 리턴)](https://repl.it/@jangka512/VapidTrainedShockwave)
- [문제 - 정답](https://repl.it/@jangka512/PoshFastDiskdrive)
- [문제 - 입력받은 정수가 짝수인지 아닌지 판별해주는 함수 구현](https://repl.it/@jangka512/QuickwittedWanVolcano)
- [문제 - 정답](https://repl.it/@jangka512/WellgroomedInsecureCad)
- [문제 - 입력받은 정수가 3의 배수인지 알려주는 함수 구현](https://repl.it/@jangka512/LinearRowdyFtpclient)
- [문제 - 정답](https://repl.it/@jangka512/IckyStableComputing)
- [문제 - 입력받은 정수가 100보다 큰지 알려주는 함수 구현](https://repl.it/@jangka512/GiganticMountainousMetrics)
- [문제 - 정답](https://repl.it/@jangka512/EnchantingLostPriorities)
- [문제 - 입력받은 정수가 100보다 크고 200보다 작은지 알려주는 함수 구현](https://repl.it/@jangka512/InsistentChiefExtraction)
- [문제 - 정답](https://repl.it/@jangka512/CooperativeWholeForms)
- [문제 - 입력받은 정수의 모든 약수를 화면에 출력해주는 함수 구현](https://repl.it/@jangka512/AthleticOutstandingMethods)
- [문제 - 정답](https://repl.it/@jangka512/GleamingHilariousSales)
- [문제 - 입력받은 정수의 모든 약수의 합을 리턴하는 함수 구현](https://repl.it/@jangka512/C-2018-07-20-3)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-20-4)
- [문제 - 입력받은 정수가 소수인지 알려주는 함수 구현](https://repl.it/@jangka512/C-2018-07-20-5)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-20-6)
- [문제 - 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수를 출력하는 함수 구현](https://repl.it/@jangka512/DeepskyblueOverdueLinuxpc)
- [문제 - 정답](https://repl.it/@jangka512/CompatibleWelllitProcess)

## 동영상
- [동영상 - 2018 07 05, C언어, 개념, 함수](https://www.youtube.com/watch?v=7ZNKkWRJiq0)
- [동영상 - 2018 07 01, C언어, 개념, 함수, 매개변수와 인자](https://www.youtube.com/watch?v=55APeNeG1i0)
- [동영상 - 2020 02 20, C언어, 개념, 함수](https://www.youtube.com/watch?v=9lcwin8cmpw&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 문제, 함수의 사용을 보고, 적절한 함수 구현](https://www.youtube.com/watch?v=UTeZUF749fA&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 개념, 함수, 매개변수](https://www.youtube.com/watch?v=-eVRK5vqc5s&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 문제, 함수의 사용부분을 보고 적절한 함수를 구현, 매개변수](https://www.youtube.com/watch?v=8409MQmXl24&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 문제,  함수의 사용부분을 보고 적절한 함수를 구현, 다중 매개변수](https://www.youtube.com/watch?v=H1z9aYm2xj4&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 개념, 함수, 매개변수, 리턴](https://www.youtube.com/watch?v=hV42lRMSuL4&feature=youtu.be)
- [동영상 - 2020 02 20, C언어, 문제, 5칙연산 함수 구현, 더하기, 빼기, 곱하기, 나누기, 나머지 연산](https://www.youtube.com/watch?v=WA6bpHLTjVg&feature=youtu.be)
- [동영상 - 2020 02 21 C언어, 문제, 입력받은 정수가 3의 배수인지 알려주는 함수 구현](https://www.youtube.com/watch?v=5Kbqzbo0kkc&feature=youtu.be)
- [동영상 - 2020 02 21, C언어, 문제, 입력받은 정수가 100보다 크고 200보다 작은지 알려주는 함수 구현](https://www.youtube.com/watch?v=YFGdJBo2bmk&feature=youtu.be)
- [동영상 - 2020 02 21, C언어, 문제, 입력받은 정수가 100보다 큰지 알려주는 함수 구현](https://www.youtube.com/watch?v=CXSnXwYJ7RY&feature=youtu.be)
- [동영상 - 2020 02 21, C언어, 문제, 입력받은 정수의 모든 약수를 화면에 출력해주는 함수 구현](https://www.youtube.com/watch?v=20pRDpLdk14&feature=youtu.be)
- [동영상 - 2020 02 21, C언어, 문제, 입력받은 정수의 모든 약수의 합을 반환하는 함수 구현](https://www.youtube.com/watch?v=5nO6nd3yyfk&feature=youtu.be)
- [동영상 - 2020 02 24, C언어, 문제, 입력받은 정수가 소수인지 알려주는 함수 구현](https://www.youtube.com/watch?v=Udz1LwHfAoM&feature=youtu.be)
- [동영상 - 2020 03 23, C언어, 개념, 함수](https://www.youtube.com/watch?v=yJdjRyqz5nI&feature=youtu.be)
- [동영상 - 2020 03 23, C언어, 개념, 함수, 매개변수](https://www.youtube.com/watch?v=WSbzp7GzEic&feature=youtu.be)
- [동영상 - 2020 03 23, C언어, 개념, 함수, 매개변수, 리턴](https://www.youtube.com/watch?v=W9graG0h99s&feature=youtu.be)
- [동영상 - 2020 03 28, C언어, 문제, 함수관련 기초 개념문제들 1](https://www.youtube.com/watch?v=IOXmvVI5jZo&feature=youtu.be)
- [동영상 - 2020 03 28, C언어, 문제, 함수관련 기초 개념문제들 2](https://www.youtube.com/watch?v=6RtHbSBxmMw&feature=youtu.be)

# 06, 함수(심화)
## 문제
- [문제 - 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수를 출력하는 함수 구현](https://repl.it/@jangka512/DeepskyblueOverdueLinuxpc)
- [문제 - 정답](https://repl.it/@jangka512/CompatibleWelllitProcess)
- [문제 - 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수의 개수를 리턴하는 함수 구현](https://repl.it/@jangka512/C-2018-07-20-7)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-20-8)
- [문제 - 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수의 합을 리턴하는 함수 구현](https://repl.it/@jangka512/C-2018-07-20-9)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-20-10)
- [문제 - 3가지 다른 방법으로 a부터 z까지 출력하는 함수 구현](https://repl.it/@jangka512/ScentedOverdueMysql)
- [문제 - 정답](https://repl.it/@jangka512/BabyishFlakyNotification)
- [문제 - n부터 m 사이에 존재하는 정수의 합](https://repl.it/@jangka512/C-2018-07-23-1)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-23-2)
- [문제 - n부터 m 사이에 존재하는 소수의 합](https://repl.it/@jangka512/C-2018-07-23-3)
- [문제 - 정답](https://repl.it/@jangka512/C-2018-07-23-4)

## 추가문제
- [문제 - 직사각형 만들기](https://repl.it/@jangka512/AliceblueForcefulInstructionset)
- [문제 - 정답](https://repl.it/@jangka512/QueasyWrathfulHertz)
- [문제 - 삼각형 만들기](https://repl.it/@jangka512/YellowgreenHospitableAbstraction)
- [문제 - 정답](https://repl.it/@jangka512/UnfitWoodenWebsite)
- [문제 - 삼각형 2 만들기](https://repl.it/@jangka512/PoliticalAcceptableMedia)
- [문제 - 정답](https://repl.it/@jangka512/IrritatingSkinnyConfig)
- [문제 - 삼각형 3 만들기](https://repl.it/@jangka512/RotatingImpureAdministrator)
- [문제 - 정답](https://repl.it/@jangka512/SympatheticCarefulLoaderprogram)
- [문제 - 양의정수제곱근 반환 함수 만들기](https://repl.it/@jangka512/SpryBogusNetworks)
- [문제 - 정답](https://repl.it/@jangka512/GrandDarkMicrokernel)

## 동영상
- [동영상 - 2020 03 29, C언어, 개념, 함수, 매개변수, 리턴](https://www.youtube.com/watch?v=rhsJi1aQjuA&feature=youtu.be)
- [동영상 - 2020 02 24, C언어, 문제, 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수를 출력하는 함수 구현](https://www.youtube.com/watch?v=uBFTfXJIkbU&feature=youtu.be)
- [동영상 - 2020 02 24, C언어, 문제, 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수의 개수를 리턴하는 함수구현](https://www.youtube.com/watch?v=GrpCnyoZdj0&feature=youtu.be)
- [동영상 - 2020 02 24, C언어, 문제, 입력받은 숫자가 10이라고 할때 1부터 10 사이에 존재하는 모든 소수의 합을 리턴하는 함수구현](https://www.youtube.com/watch?v=SpE1wQVhKi4&feature=youtu.be)
- [동영상 - 2018 07 01, C언어, 개념, 문자와 정수](https://www.youtube.com/watch?v=h7RgGfh3Lb4)
- [동영상 - 2020 03 29, C언어, 개념, 문자와 char 타입](https://www.youtube.com/watch?v=KmCWPUMtKK4&feature=youtu.be)
- [동영상 - 2020 02 24, C언어, 문제, 3가지 다른 방법으로 a부터 z까지 출력](https://www.youtube.com/watch?v=aI1KHp0EdJA&feature=youtu.be)
- [동영상 - 2020 03 29, C언어, 문제, 1부터 n 사이의 모든 소수 출력 함수
](https://www.youtube.com/watch?v=C0UHyaKHq-s&feature=youtu.be)
- [동영상 - 2020 03 29, C언어, 문제, n부터 m 사이의 모든 정수의 합 리턴](https://www.youtube.com/watch?v=mQBsUp-rSjA&feature=youtu.be)
- [동영상 - 2020 03 29, C언어, 문제, n부터 m 사이의 모든 소수의 합 리턴](https://www.youtube.com/watch?v=wpqz4z6YNUY&feature=youtu.be)

# 07, 메모리
## 개념
- 컴퓨터 구성
  - CPU : 연산
    - 데이터 처리 속도 : 엄청빠름
    - Read/Write 속도 : 엄청빠름
  - L1 Cache 메모리 : 저장
    - 저장용량 : 작음 / 보통 8~64KB
    - Read/Write 속도 : 엄청빠름(L2보다 빠름)
  - L2 Cache 메모리 : 저장
    - 저장용량 : 작음 / 보통 64KB ~ 4MB
    - Read/Write 속도 : 엄청빠름
  - 메모리 : 저장
    - 저장용량 : 작음 / 8G
    - Read/Write 속도 : 빠름
  - 하드디스크 : 저장
    - 저장용량 : 큼
    - Read/Write 속도 : 느림
- PC를 관리하는 소프트웨어 : 운영체제
  - OS가 메모리를 관리한다.
  - 우리가 만든 C 프로그램은 운영체제의 관리하에 실행된다.
    - 즉 우리가 만든 프로그램은 운영체제에 메모리를 구걸한다.
    - 우리가 만든 변수는 메모리에 할당된다.
      - 즉 우리가 변수에 숫자 10을 넣으면, 그것은 메모리 어딘가에 저장된다.
- 메모리는 크게 2개의 구역으로 나뉜다.
  - 힙 : 객체들의 세계, 위에서 내려온다.
  - 스택 : 지역변수들의 세계, 아래에서 올라간다.
- `int i = 5; int* p = &i; *p = 10;`
  - 위 코드는 `int i = 5; i = 10;` 과 같은 의미 이다.
  - `int* p` : int 변수의 주소를 저장 할 수 있는 변수 p를 만든다.
  - `&i` : 변수 i의 리얼 주소를 알라낸다.
  - `*p` : p에 들어있는 주소로 간다.
  - `*p = 10;` : p에 들어있는 주소로 가면 4평짜리 땅(4바이트 공간)이 나오는데, 거기에 5가 들어있다. 그것을 10으로 바꾼다.

## 문제
- [문제 - 3.14를 효율적으로 저장하고 출력](https://repl.it/@jangka512/HighGorgeousBloatware)
- [문제 - 정답](https://repl.it/@jangka512/SpicyFondConsultant)
- [문제 - 200을 효율적으로 저장하고 출력](https://repl.it/@jangka512/TechnologicalCookedBackslash)
- [문제 - 정답](https://repl.it/@jangka512/NimbleOfficialSandbox)
- [문제 - 21억을 효율적으로 저장하고 출력](https://repl.it/@jangka512/GivingCuddlyLoaderprogram)
- [문제 - 정답](https://repl.it/@jangka512/SlipperyGrouchyCurrencies)
- [문제 - 22억을 효율적으로 저장하고 출력](https://repl.it/@jangka512/OriginalLightyellowToolbox)
- [문제 - 정답](https://repl.it/@jangka512/YearlyGiddyForm)
- [문제 - 400억을 효율적으로 저장하고 출력](https://repl.it/@jangka512/KnownPlumBusiness)
- [문제 - 정답](https://repl.it/@jangka512/PopularStarkDesignmethod)
- [문제 - unsigned 유무의 차이를 출력](https://repl.it/@jangka512/LumpyImperfectVirtualmachine)
- [문제 - 정답](https://repl.it/@jangka512/UpsetIncompleteSampler)
- [문제 - 모든 기본타입 변수의 크기를 출력](https://repl.it/@jangka512/ZanyScentedDesigner)
- [문제 - 정답](https://repl.it/@jangka512/ConfusedHumongousBytes)
- [문제 - 변수 int i의 값을 해킹](https://repl.it/@jangka512/LiveDeepComputergame)
- [문제 - 정답](https://repl.it/@jangka512/EquatorialGeneralMonitors)
- [문제 - 변수 unsigned char c의 값을 해킹](https://repl.it/@jangka512/BurdensomeRemorsefulCodec)
- [문제 - 정답](https://repl.it/@jangka512/ArtisticHealthyMicroinstruction)

## 동영상
- [동영상 - 2020_04_04, C언어, 문제, 직사각형 출력 함수](https://www.youtube.com/watch?v=ay6ZTYXG44E&feature=youtu.be)
- [동영상 - 2020_04_04, C언어, 문제, 삼각형 출력 함수](https://www.youtube.com/watch?v=Vx2WFoVotIw&feature=youtu.be)
- [동영상 - 2020_04_04, C언어, 개념, 램이 필요한 이유](https://www.youtube.com/watch?v=75gt36E0cuU)
- [동영상 - 2020 04 04, C언어, 개념, 비트, 바이트, 기본변수타입](https://www.youtube.com/watch?v=cISNgHgQcdU)
- [동영상 - 2020 04 04, C언어, 문제, 주어진 데이터를 적절한 데이터 형 변수에 담고 출력](https://www.youtube.com/watch?v=lrngoLMfASA)
- [동영상 - 2020 04 04, C언어, 문제, sizeof를 사용하여, 변수와 타입의 크기를 출력](https://www.youtube.com/watch?v=NpOx46PqI7M)
- [동영상 - 2020 04 04, C언어, 문제, sizeof를 사용하여, 모든 변수 타입의 크기 출력](https://www.youtube.com/watch?v=_gyd-PrsG4k)
- [동영상 - 2020 04 04, C언어, 문제, 포인터를 사용하여 int 변수의 값 변경](https://www.youtube.com/watch?v=-jgXVwz0rig)
- [동영상 - 2020 04 04, C언어, 문제, 포인터를 사용하여 unsigned char 변수의 값 변경](https://www.youtube.com/watch?v=YqX9x24MOeo)
- [동영상- 2020 02 26, C언어, 개념, 램이 필요한 이유](https://youtu.be/34TSXd2zs8Y)
- [동영상- 2020 02 26, C언어, 개념, 비트, 바이트, char, short, int, long, unsigned](https://youtu.be/dIi5TOm1LQU)
- [동영상- 2020 02 26, C언어, 문제, 특정 숫자를 가장 적은 메모리를 사용해서 저장해주세요.](https://youtu.be/RApaFQgHnCs)
- [동영상- 2020 02 26, C언어, 문제, 포인터를 사용해서 변수의 값을 변경해주세요.](https://youtu.be/E795vhmdut8)

# 08, 포인터
## 추가문제
- [문제 - 사용자가 원하는 구구단 출력](https://repl.it/@jangka512/CompatibleTediousIntercept)
- [문제 - 정답](https://repl.it/@jangka512/CautiousFussyChapter)
- [문제 - 마름모 만들기](https://repl.it/@jangka512/CautiousRawMicrokernel)
- [문제 - 정답](https://repl.it/@jangka512/SandyLoneAngle)
- [문제 - 2016년 a월 b일](https://repl.it/@jangka512/RapidPiercingDifference)
- [문제 - 정답](https://repl.it/@jangka512/UnrulyKnowingSearchservice)
- [문제 - 입력받은 2가지 정수 사이에 존재하는 3의 배수 중에서 홀수의 합을 리턴하는 함수 구현(중요)](https://repl.it/@jangka512/MuddyHumongousBetatest)
- [문제 - 정답](https://repl.it/@jangka512/FrillyWarlikeImplementation)

## 문제
- [문제 - 포인터 변수 p로 변수 2개의 값 변경](https://repl.it/@jangka512/InformalCultivatedString)
- [문제 - 정답](https://repl.it/@jangka512/SubtleSiennaChief)
- [문제 - 모든 기본포인터타입 변수의 크기를 출력](https://repl.it/@jangka512/CoordinatedContentMethod)
- [문제 - 정답](https://repl.it/@jangka512/DramaticCookedWorkspace)
- [문제 - (repl 전용)char 변수 b를 이용하지 않고 b의 값을 훼손](https://repl.it/@jangka512/ElementaryAchingMicrokernel)
- [문제 - 정답](https://repl.it/@jangka512/YellowgreenInsecureFormat)
- [개념 : 포인터 변수의 타입별 차이](https://repl.it/@jangka512/DescriptiveOilyBot)
- [문제 - (repl 전용)int 변수 b를 이용하지 않고 b의 값을 훼손](https://repl.it/@jangka512/IvoryStaidOrders)
- [문제 - 정답](https://repl.it/@jangka512/UnfortunateBigMemory)
- [문제 - (repl 전용)변수의 주소를 올바른 포인터 변수에 저장하지 않은 예 구현](https://repl.it/@jangka512/GraciousPossiblePackage)
- [문제 - 정답](https://repl.it/@jangka512/NeighboringMajorSlash)
- [문제 - 원본값을 훼손하는 change 함수를 만들어주세요.](https://repl.it/@jangka512/LightblueMotionlessIntercept)
- [문제 - 정답](https://repl.it/@jangka512/LoudMerryGeeklog)

## 동영상
- [동영상 - 2020 02 27, C언어, 개념, 다양한 타입의 포인터 변수들이 전부 같은 크기인 이유](https://www.youtube.com/watch?v=BYhHK2gC9Qo&feature=youtu.be)
- [동영상 - 2020 02 27, C언어, 개념, 포인터 변수](https://www.youtube.com/watch?v=mwaTz3-M53A&feature=youtu.be)
- [동영상 - 2020 02 26, C언어, 문제, 포인터를 사용해서 unsigned char 타입 변수의 값을 변경해주세요](https://www.youtube.com/watch?v=oXil-SLG2YM&feature=youtu.be)
- [동영상 - 2020 02 27, C언어, 문제, char 변수 b를 이용하지 않고 b의 값을 훼손, 포인터](https://www.youtube.com/watch?v=QvbxSRi2Q6E)
- [동영상 - 2020 02 27, C언어, 개념, 포인터 변수의 타입별 차이](https://www.youtube.com/watch?v=54qkkBQ91GM)
- [동영상 - 2020 02 27, C언어, 문제, int 변수 b를 이용하지 않고 b의 값을 훼손, 포인터](https://www.youtube.com/watch?v=kfM_TZNiIAo&feature=youtu.be)
- [동영상 - 2020_04_05, C언어, 문제, 포인터 변수 p로 변수 2개의 값 변경](https://www.youtube.com/watch?v=GACrcaNouJ8&feature=youtu.be)
- [동영상 - 2020_04_05, C언어, 문제, 모든 기본포인터타입 변수의 크기를 출력](https://youtu.be/fvsvaMvOkUQ)
- [동영상 - 2020_04_05, C언어, 문제, repl 전용, char 변수 b를 이용하지 않고 b의 값을 훼손](https://youtu.be/pcdFAh1fHJY)
- [동영상 - 2020_04_05, C언어, 개념, 포인터 변수의 타입별 차이](https://www.youtube.com/watch?v=kW8OqeFp4cM&feature=youtu.be)
- [동영상 - 2020_04_05, C언어, 문제, repl 전용, int 변수 b를 이용하지 않고 b의 값을 훼손](https://youtu.be/9c-aWb6vIIc)
- [동영상 - 2020 04 05, C언어, repl 전용, 변수의 주소를 올바른 포인터 변수에 저장하지 않은 예를 구현](https://www.youtube.com/watch?v=Cu7yWw4tXHQ&feature=youtu.be)
- [동영상 - 2020 04 05, C언어, 문제, 원본값을 훼손하는 change 함수를 만들어주세요](https://www.youtube.com/watch?v=6FYtvVm0wNg&feature=youtu.be)

# 09, 포인터(심화)
## 문제
- [문제 - 원본값을 훼손하는 change 함수를 만들어주세요., 매개변수 없이](https://repl.it/@jangka512/UnimportantClearExtraction)
- [문제 - 정답](https://repl.it/@jangka512/WryMonstrousParallelcompiler)
- [개념 - 다중포인터](https://repl.it/@jangka512/GrowingBreakableCores)

## 동영상
- [동영상 - 2020 02 28, C언어, 개념, 다중 포인터](https://www.youtube.com/watch?v=LjYK4ltIsRU&feature=youtu.be)
- [동영상 - 2020 02 28, C언어, 개념, 어떠한 값을 가리키려면, 시작주소, 길이, 값종류가 모두 필요하다](https://www.youtube.com/watch?v=i4i8xf6opsI&feature=youtu.be)
- [동영상 - 2020 02 28, C언어, 문제, 변수의 주소를 올바른 포인터 변수에 저장하지 않은 예 구현, repl 전용](https://www.youtube.com/watch?v=drqlaBFTR8U&feature=youtu.be)
- [동영상 - 2020 02 28, C언어, 문제, 원본값을 훼손하는 change 함수를 만들어주세요, 매개변수 없이](https://www.youtube.com/watch?v=AMbEd3Lw0sE&feature=youtu.be)
- [동영상 - 2020 02 28, C언어, 문제, 원본값을 훼손하는 change 함수를 만들어주세요](https://www.youtube.com/watch?v=5tM92y6T4bA&feature=youtu.be)
- [동영상 - 2020 04 11, C언어, 개념, 포인터](https://youtu.be/-4ZV-10ENYA)
- [동영상 - 2020 04 11, C언어, 문제, change 타 함수에서, 변수접근해서 수정하기, 실전용 x](https://youtu.be/TY3c7avZLQQ)
- [동영상 - 2020 04 11, C언어, 개념, 다중포인터](https://youtu.be/IPbU_PgF_5E)
- [동영상 - 2020 04 11, C언어, 개념, 다중포인터](https://youtu.be/HMCvfM1pxuA)

# 10, 포인터(심화 : 배열)
## 문제
- [문제 - 원본값을 훼손하는 change 함수를 만들어주세요.(2중 포인터)](https://repl.it/@jangka512/UnlinedEarlyInitializationfile)
- [문제 - 정답](https://repl.it/@jangka512/NearCornsilkBoards)
- [문제 - 입력받은 두 정수 사이의 모든 짝수 중에서 3의 배수의 합을 반환하는 함수를 만들어주세요.](https://repl.it/@jangka512/SlowPalatableDividend)
- [문제 - 정답](https://repl.it/@jangka512/OffbeatGleefulBookmark)
- [문제 - 포인터를 사용하는 이유를 5줄 이상으로 서술해주세요.](https://repl.it/@jangka512/ExcellentElectronicAccounting)
- [문제 - 정답](https://repl.it/@jangka512/BoringAnguishedQuerylanguage)
- [문제 - 3중 포인터를 사용하는 예제를 만들어주세요.](https://repl.it/@jangka512/UniformImpassionedWordprocessor)
- [문제 - 정답](https://repl.it/@jangka512/HarmoniousBaggyVendor)
- [문제 - 3중 포인터를 사용하는 예제를 만들어주세요. 그리고 메모리 상황을 그림판으로 그려주세요.](https://repl.it/@jangka512/StaleDefiantParallelprocessing)
- [문제 - 정답](https://repl.it/@jangka512/BeneficialLumpyNetworks)
- [문제 - (repl 전용)2중 포인터만 사용해서 함수안의 모든 지역변수의 값을 변경해주세요.](https://repl.it/@jangka512/ImaginaryHelpfulHarddrives)
- [힌트](https://repl.it/@jangka512/AlienatedComposedEvent)
- [문제 - 정답](https://repl.it/@jangka512/RealAncientCoins)
- [문제 - 원본값을 훼손하는 change 함수를 만들어주세요.(3중 포인터)](https://repl.it/@jangka512/BogusMediumpurpleBraces)
- [문제 - 정답](https://repl.it/@jangka512/AngelicImmediateIntercept)
- [문제 - (repl 전용)main 함수 안에있는 int 변수의 값들 중 최대값을 출력해주세요.](https://repl.it/@jangka512/SecondYouthfulModes)
- [문제 - 정답](https://repl.it/@jangka512/ExcitableHeftyActiveserverpages)
- [개념 - 배열](https://repl.it/@jangka512/OliveAnimatedBookmarks)
- [문제 - (repl 전용)main 함수 안에있는 int 변수의 값들 중 최대값을 출력해주세요, 배열 이용](https://repl.it/@jangka512/SinfulWheatCharmap)
- [문제 - 정답](https://repl.it/@jangka512/ForsakenSmugOmnipage)
- [문제 - 배열의 메모리 구조 관련 질문에 답해주세요.](https://repl.it/@jangka512/AustereIndigoSoftwareengineer)
- [문제 - 정답](https://repl.it/@jangka512/DeadlySmugTelecommunications)
- [문제 - 배열에 숫자 2개 저장 후 출력을 해주세요.](https://repl.it/@jangka512/AdorableStupendousProgramminglanguages)
- [문제 - 정답](https://repl.it/@jangka512/BelatedEnragedEnvironments)
- [문제 - 배열에 숫자 100개 저장 후 출력을 해주세요.](https://repl.it/@jangka512/PrevailingTatteredLocus)
- [문제 - 정답](https://repl.it/@jangka512/NauticalGaseousVariables)

## 동영상
- [동영상 - 2020 03 12, C언어, 문제, 원본값을 훼손하는 change 함수를 만들어주세요, 2중 포인터](https://www.youtube.com/watch?v=PVhQ1O32ukg&feature=youtu.be)
- [동영상 - 2020 03 12, C언어, 개념, 포인터를 사용하는 이유를 5줄 이상으로 서술해주세요](https://www.youtube.com/watch?v=RKHY4mjQVVQ&feature=youtu.be)
- [동영상 - 2020 03 12, C언어, 문제, 입력받은 두 정수 사이의 모든 짝수 중에서 3의 배수의 합을 반환하는 함수를 만들어주세요](https://www.youtube.com/watch?v=zMdQdDE_Sj4&feature=youtu.be)
- [동영상 - 2020 03 12, C언어, 문제, 3중 포인터를 사용하는 예제를 만들어주세요](https://www.youtube.com/watch?v=A7J-pMSOvIk&feature=youtu.be)
- [동영상 - 2020 03 12, C언어, 문제, 3중 포인터를 사용하는 예제를 만들어주세요 그리고 메모리 상황을 그림판으로 그려주세요](https://www.youtube.com/watch?v=w8wDaL7sOE0&feature=youtu.be)
- [동영상 - 2020 03 12, C언어, 문제, repl 전용, 2중 포인터만 사용해서 함수안의 모든 지역변수의 값을 변경해주세요](https://www.youtube.com/watch?v=dSflyFH64Ms&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 문제, 원본값을 훼손하는 change 함수를 만들어주세요, 3중 포인터](https://www.youtube.com/watch?v=yKibCocee4I&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 문제, main 함수 안에 있는 int 변수의 값들 중 최대값을 출력해주세요, repl 전용문제](https://www.youtube.com/watch?v=UjsPrL_i65Y&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 개념, 배열 1 2](https://www.youtube.com/watch?v=KRhbr6ncqjw&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 개념, 배열 2 2](https://www.youtube.com/watch?v=nLA7Zw1867s&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 문제, main 함수 안에 있는 int 변수의 값들 중 최대값을 출력해주세요, 배열](https://www.youtube.com/watch?v=4at0svcGuhM&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 개념, 배열의 메모리 구조 관련 질문에 답해주세요](https://www.youtube.com/watch?v=eheadX7CpZY&feature=youtu.be)
- [동영상 - 2020 03 13, C언어, 문제, 배열에 숫자 2개 저장 후 출력을 해주세요, 배열문법금지](https://www.youtube.com/watch?v=0_O6Y4ttoJk&feature=youtu.be)

# 11, 포인터(심화 : scanf, 문자열 상수)
## 문제
- [문제 - scanf 할때 왜 변수의 주소를 매개변수로 넘겨주는 지 설명해주세요.](https://repl.it/@jangka512/GigaThinSearch)
- [문제 - 정답](https://repl.it/@jangka512/FamiliarOrnateMemorypool)
- [문제 - 고객에게 숫자를 10개 입력받아서 배열에 넣기(포인터 문법 사용 금지)](https://repl.it/@jangka512/AcademicGoldenrodRobots)
- [문제 - 정답](https://repl.it/@jangka512/LoathsomeCrispUsernames)
- [문제 - 고객에게 숫자를 10개 입력받아서 배열에 넣기(배열 문법 사용 금지)](https://repl.it/@jangka512/DotingSkeletalDevelopers)
- [문제 - 정답](https://repl.it/@jangka512/CandidUnsungLaboratory)
- [문제 - 배열을 훼손하는 change 함수를 만들어주세요.](https://repl.it/@jangka512/ScratchyVictoriousVoxels)
- [문제 - 정답](https://repl.it/@jangka512/ReasonableInfamousAssembly)
- [문제 - 배열을 훼손하는 change 함수를 만들어주세요.(배열포인터)](https://repl.it/@jangka512/ButteryPreviousDesign)
- [문제 - 정답](https://repl.it/@jangka512/CruelForkedCache)
- [문제 - 상수 true, false와 변수타입 bool을 만들어주세요.](https://repl.it/@jangka512/ExoticSpanishBusiness)
- [문제 - 정답](https://repl.it/@jangka512/TenderHopefulArchives)
- [문제 - 문자배열을 만들고 거기에 문장 `abc`를 저장해주세요.](https://repl.it/@jangka512/FavoriteVisibleButtons)
- [문제 - 정답](https://repl.it/@jangka512/GranularDecentMatter)
- [문제 - 문장을 출력하는 함수를 만들어주세요.(%s 사용 금지)](https://repl.it/@jangka512/IntentionalMinorLoaderprogram)
- [문제 - 정답](https://repl.it/@jangka512/ClearcutUnpleasantTrialsoftware)
- [링크 : 문자열 상수와 문자열 변수의 차이](https://soooprmx.com/archives/4100)
- [링크 : 문자열 상수의 고찰](http://blog.naver.com/PostView.nhn?blogId=tipsware&logNo=221018307213)
- [문제 - "abc" 라는 코드가 실행되면 발생되는 일을 자세히 설명해주세요.](https://repl.it/@jangka512/WeeklyWildTag)
- [문제 - "abc" 라는 코드가 실행되면 발생되는 일을 자세히 설명해주세요. - 정답](https://repl.it/@jangka512/BabyishSorrowfulTheories)
- [문제 - (repl 전용) str1의 값과 str2의 값이 4 차이나는 이유를 자세히 서술해주세요.(정답없음)](https://repl.it/@jangka512/HeavyConsciousKeygen)
- [문제 - name1과 name2의 선언부분에서 총 몇바이트의 메모리를 사용하는지 설명해주세요.](https://repl.it/@jangka512/TrimLikableRectangles)
- [문제 - 정답](https://repl.it/@jangka512/WeakFarawayHexadecimal)

## 동영상
- [동영상 - 2020_03_16, C언어, 문제, 정수 100개를 배열에 저장 후 출력해주세요](https://youtu.be/QW9JJZ7t6Hw)
- [동영상 - 2020_03_16, C언어, 문제, 정수 100개를 배열에 저장 후 출력해주세요, 추가설명](https://youtu.be/JpP_wTZBNbM)
- [동영상 - 2020_03_16, C언어, 문제, scanf의 두번째 인자로 왜 변수의 주소를 넘겨야하는지](https://youtu.be/TgFRfLlY0wQ)
- [동영상 - 2020_03_16, C언어, 문제, scanf로 숫자 10개 받아서 배열에 저장, 배열문법](https://youtu.be/AVi6iBhfAiE)
- [동영상 - 2020_03_16, C언어, 문제, scanf로 숫자 10개 받아서 배열에 저장, 포인터문법](https://youtu.be/9D21NG8z9_Y)
- [동영상 - 2020_03_16, C언어, 문제, 배열의 값을 수정해 주는 함수](https://youtu.be/se74yPB10Yo)
- [동영상 - 2020_03_16, C언어, 문제, 배열의 값을 수정해 주는 함수, 배열포인터, 2중포인터](https://youtu.be/ehEc_9qUh9o)
- [동영상 - 2020_03_16, C언어, 문제, typedef와 define 사용하여 오류해결](https://youtu.be/tdvsdv5uBK0)
- [동영상 - 2020_03_16, C언어, 문제, 문자배열을 통해서 문장구성](https://youtu.be/SYQZRck-S8w)
- [동영상 - 2020_03_17, C언어, 문장을 출력하는 함수 구현](https://www.youtube.com/watch?v=8xNigqfr_5Q&feature=youtu.be)
- [동영상 - 2020_03_17, 개념, 문자열 상수, 문자열 변수](https://www.youtube.com/watch?v=8H50NXsw3JA&feature=youtu.be)
- [동영상 - 2020_03_17, 문제, 문자열 str1과 str2의 값 차이가 4인 이유 설명](https://www.youtube.com/watch?v=IpIXhrsSFLo&feature=youtu.be)
- [동영상 - 2020_03_17, 문제, 문자열 상수를 만들면 발생하는 일 설명](https://www.youtube.com/watch?v=g2_h3iQ_x08&feature=youtu.be)
- [동영상 - 2020_03_17, 문제, 문자열과 관련해서 각각의 부분이 몇 바이트를 소모하는지](https://www.youtube.com/watch?v=c1VLCP9wxus&feature=youtu.be)

# 12, 포인터(심화 : scanf, 문자열 다루기, 구조체)
## 문제
- [문제 - 변수 str1, str2, str4의 값이 같은 이유를 설명해주세요.](https://repl.it/@jangka512/JauntyFloralwhiteMacroinstruction)
- [문제 - 정답](https://repl.it/@jangka512/SupportiveOnerlookedControlpanel)
- [문제 - `*(name1 + 0)`을 수정하면 안되는 이유를 설명해주세요.](https://repl.it/@jangka512/AromaticRectangularForm)
- [문제 - 정답](https://repl.it/@jangka512/ThoughtfulBoringBudgetrange)
- [문제 4중 포인터를 사용해서 abcd라는 문장을 완성해주세요.](https://repl.it/@jangka512/FuchsiaIdenticalRelationalmodel)
- [문제 - 정답](https://repl.it/@jangka512/FinishedCarpalHertz)
- [문제 - 문장의 길이를 반환하는 함수를 만들어주세요.(get_str_len)](https://repl.it/@jangka512/SpecializedFavoriteVisitor)
- [문제 - 정답](https://repl.it/@jangka512/NewLuckyTests)
- [문제 - 문장에서 특정 문자의 위치를 반환하는 함수를 만들어주세요.(get_index_of_c)](https://repl.it/@jangka512/SkinnyCreamyInfinity)
- [문제 - 정답](https://repl.it/@jangka512/GloomyMonumentalPlanes)
- [문제 - 문장에서 특정 부분만 잘라서 출력하는 함수를 만들어주세요.(print_sub_str)](https://repl.it/@jangka512/ImpossibleModestLanservers)
- [문제 - 정답](https://repl.it/@jangka512/ShockingVioletredTrust)
- [문제 - starts_with 함수를 구현해주세요.](https://repl.it/@jangka512/MuddyUnimportantMotion)
- [문제 - 정답](https://repl.it/@jangka512/CavernousWorriedButton)
- [문제 - ends_with 함수를 구현해주세요.](https://repl.it/@jangka512/UnselfishFabulousTrees)
- [문제 - 정답](https://repl.it/@jangka512/VirtuousStaticDeletions)
- [문제 - 정답](https://repl.it/@jangka512/CelebratedShrillSolaris)
- [문제 - str_equals 함수를 구현해주세요.](https://repl.it/@jangka512/ScalyFriendlyTasks)
- [문제 - 정답](https://repl.it/@jangka512/SomeBiodegradableExtraction)
- [문제 - str_part_equals 함수를 구현해주세요.](https://repl.it/@jangka512/BriefLostBookmark)
- [문제 - 정답](https://repl.it/@jangka512/DualModernKnowledge)
- [문제 - 문장에서 특정 문장의 위치를 반환하는 함수를 만들어주세요.(get_in)](https://repl.it/@jangka512/BiodegradableElementaryIntegers)
- [문제 - 정답](https://repl.it/@jangka512/DistortedPepperyServer)
- [문제 - 정답 v2(starts_with 사용해서 쉽게 푼 버전)](https://repl.it/@jangka512/TurboRoastedDemand)
- [문제 - 정답 v3](https://repl.it/@jangka512/SubduedPungentOpensource)
- 구조체 기초 시작
- [문제 - 구조체를 도입해서 간단하게 바꿔주세요](https://repl.it/@jangka512/DarksalmonSparseSpheres)
- [문제 - 정답](https://repl.it/@jangka512/EverlastingMurkyWeblogsoftware)
- [문제 - 취미를 추가해주세요.](https://repl.it/@jangka512/SurprisedBothAngle)
- [문제 - 정답](https://repl.it/@jangka512/LightElectronicTree)
- [문제 - 취미를 추가해주세요.(구조체 버전)](https://repl.it/@jangka512/IllustriousOblongGlobalarrays)
- [문제 - 정답](https://repl.it/@jangka512/SociableMarriedBootstrapping)
- [문제 - 구조체 변수에 원본이 들어있는지 참조가 들어있는지 확인해주세요.](https://repl.it/@jangka512/ShadowyTediousTasks)
- [문제 - 정답](https://repl.it/@jangka512/GainsboroPoorBloatware)
- [문제 - 구조체 변수의 주소를 이용해서 원본값을 변경하는 함수를 구현](https://repl.it/@jangka512/FamiliarUnfoldedFunnel)
- [문제 - 정답](https://repl.it/@jangka512/UnknownImpishCallback)
- 구조체 기초 끝
- 동적할당 시작
- [문제 - 공간도 만들고 값도 세팅해 주는 함수 구현](https://repl.it/@jangka512/CalmSilverMosaic)
- [문제 - 정답](https://repl.it/@jangka512/CadetblueUnripeWifi)
- 동적할당 끝

## 동영상
- [동영상 - 2020 03 18, C언어, 문제, 문자열 상수의 문자를 수정하면 안되는 이유는?](https://www.youtube.com/watch?v=4jQOZ1cVwz8&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 문제, 변수 str1, str2, str4의 값이 같은 이유를 설명해주세요.](https://www.youtube.com/watch?v=hoT-0zpiqLQ&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 4중 포인터를 사용해서 `abcd` 라는 문장을 완성해주세요](https://www.youtube.com/watch?v=jZnhfHoI2Oo&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 문제, 문자열 길이 반환함수 구현, get str len](https://www.youtube.com/watch?v=Rv_DuR4RTl4&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 문제, 문자열에서, 특정 문자위치 반환](https://www.youtube.com/watch?v=A8iMQLub08g&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 문제, 문자열에서, 특정 부분만 출력](https://www.youtube.com/watch?v=0A8hl_z_n1E&feature=youtu.be)
- [동영상 - 2020 03 18, C언어, 문제, 문자열이 특정 문자열로 시작하는지 검사](https://www.youtube.com/watch?v=OU9g1HHi_mQ&feature=youtu.be)
- [동영상 - 2020 03 19, C언어, 문제, 문자열이 특정 문자열로 끝나는지 검사](https://www.youtube.com/watch?v=V0EcbYQmfpc&feature=youtu.be)
- [동영상 - 2020 03 19, C언어, 문제, 두 문자열이 서로 같은지 검사](https://www.youtube.com/watch?v=zaTeh2LQei4&feature=youtu.be)
- [동영상 - 2020 03 19, C언어, 문제, 두 문자열이 서로 같은지 검사, 특정부분만 비교](https://www.youtube.com/watch?v=Z4ESOpgjSWE&feature=youtu.be)
- [동영상 - 2020 03 19, C언어, 문제, 한 문자열이 다른 문자열 안에서 어디에 위치하는지 검사](https://www.youtube.com/watch?v=SYpbU_GKlQU&feature=youtu.be)
- [동영상 - 2020 03 20, C언어, 문제, 구조체를 도입해서 간단하게 바꿔주세요](https://www.youtube.com/watch?v=sQMI0CVw-78&feature=youtu.be)
- [동영상 - 2020 03 20, C언어, 문제, 사람의 구성요소로 취미를 추가해주세요](https://www.youtube.com/watch?v=VBvysckeGNM&feature=youtu.be)
- [동영상 - 2020 03 20, C언어, 문제, 구조체 변수에 원본이 들어있는지 참조가 들어있는지 확인해주세요](https://www.youtube.com/watch?v=N5xfiKv1EdA&feature=youtu.be)
- [동영상 - 2020 03 20, C언어, 문제, 구조체 변수의 주소를 이용해서 원본값을 변경하는 함수를 구현](https://www.youtube.com/watch?v=_0CjLnoNC_c&feature=youtu.be)
- [동영상 - 2020 03 20, C언어, 문제, 공간도 만들고 값도 세팅해 주는 함수 구현, 동적할당](https://www.youtube.com/watch?v=C58pCphamjY&feature=youtu.be)

# 13, 동적할당
## 문제
- [개념 - int 배열 정적할당](https://repl.it/@jangka512/QuizzicalSpryScripts)
  - 단점
    - 프로그램 실행도중 배열 사이즈 변경 불가능
  - 장점
    - 배열의 메모리는 자동할당, 자동반납
- [개념 - int 배열 동적할당](https://repl.it/@jangka512/WhoppingGiantMetadata)
  - 장점
    - 프로그램 실행도중 사이즈 변경 가능
  - 단점
    - 배열의 메모리는 수동할당(malloc), 수동반납(free)
  - 메모리관리가 수동인데도, malloc을 쓰는 이유 : 레이싱 선수들이 자동변속기가 아닌 수동변속기를 쓰는 이유를 생각해보세요.
- [문제 - 몇명의 사람의 나이를 받을지 입력받고 입력받은 수 만큼 나이를 입력받은 후 저장해주세요.](https://repl.it/@jangka512/FairSevereConversion)
- [문제 - 정답](https://repl.it/@jangka512/DisastrousZealousVertex)
- [문제 - 몇명의 사람의 나이를 받을지 입력받고 입력받은 수 만큼 나이를 입력받은 후 저장해주세요.(사람 수 제한 없는 버전)](https://repl.it/@jangka512/ShyAdeptSolidstatedrive)
- [문제 - 정답](https://repl.it/@jangka512/PhonyDarkredFraction)
- [문제 - 퀴즈](https://repl.it/@jangka512/WealthyCarelessQuark)
- [문제 - 정답](https://repl.it/@jangka512/HarmoniousWarmheartedManagement)
- [문제 - int 없이, int* 만으로 구구단 출력 함수를 만들어주세요.](https://repl.it/@jangka512/WillingMutedDecagons)
- [문제 - 정답](https://repl.it/@jangka512/KhakiOtherAnalyst)
- [문제 - int 없이, int* 만으로 구구단 출력 함수를 만들어주세요.(변수를 오직 하나만 만들어주세요.)](https://repl.it/@jangka512/OrangeGrandiosePcboard)
- [문제 - 정답](https://repl.it/@jangka512/ImperturbableSpotlessIntelligence)

## 동영상
- [동영상 - 2020_04_25, C언어, 개념, 정적할당, 동적할당, 1부](https://youtu.be/5gpNE9gngZY)
- [동영상 - 2020_04_25, C언어, 개념, 정적할당, 동적할당, 2부](https://youtu.be/ObXsy51XN2I)
- [동영상 - 2020_04_25, C언어, 문제, 사람들의 나이를 입력받아주세요, 정적할당](https://youtu.be/IjQxN6h2FS0)
- [동영상 - 2020_04_25, C언어, 문제, 사람들의 나이를 입력받아주세요, 동적할당](https://youtu.be/sYU59WVbwjk)
- [동영상 - 2020_04_25, C언어, 개념, 런타임, 컴파일타임, 동적할당, 정적할당](https://youtu.be/Kp0XtjLLGFo)
- [동영상 - 2020_04_25, C언어, 문제, 구구단을 동적할당으로](https://youtu.be/8VaPKL5gOFQ)
- [동영상 - 2020_04_25, C언어, 문제, 구구단을 동적할당으로, v2](https://youtu.be/ozOwcQIhI90)

# 14, 동적할당(배열, 링크드 리스트)
## 문제
- [문제 - 1차원 배열에 문장 10개를 입력해주세요.](https://repl.it/@jangka512/BoringIncompleteYottabyte)
- [문제 - 정답](https://repl.it/@jangka512/MajesticApprehensiveCloudcomputing)
- [개념 - 2차원 배열은 사실 1차원 배열이다.](https://repl.it/@jangka512/PalegreenHarmoniousCase)
- [문제 - 2차원 배열에 문장 10개를 입력해주세요.](https://repl.it/@jangka512/BestJauntyWireframes)
- [문제 - 정답](https://repl.it/@jangka512/AustereBrownNewsaggregator)
- [문제 - 3차원 배열을 사용하는 예를 만들어주세요.](https://repl.it/@jangka512/FractalJumboPatterns)
- [문제 - 정답](https://repl.it/@jangka512/InformalToughConfigfiles)
- [문제 - 몇명의 사람의 이름과 나이를 받을지 입력받고 입력받은 수 만큼 이름과 나이를 입력받은 후 저장해주세요.(구조체 사용 금지)](https://repl.it/@jangka512/JuvenilePortlyShareware)
- [문제 - 정답](https://repl.it/@jangka512/TealGrandioseInfinity)
- [문제 - 몇명의 사람의 이름과 나이를 받을지 입력받고 입력받은 수 만큼 이름과 나이를 입력받은 후 저장해주세요.(구조체 사용 가능)](https://repl.it/@jangka512/WavyAdventurousTags)
- [문제 - 정답](https://repl.it/@jangka512/KlutzyLeftDeeplearning)

## 동영상
- [동영상 - 2020_04_26, C언어, 문제, 1차원 배열에 문장 10개를 입력해주세요](https://www.youtube.com/watch?v=cMgbI5mIRTM)
- [동영상 - 2020_04_26, C언어, 개념, 2차원 배열](https://www.youtube.com/watch?v=1kKt9Vyx1hI)
- [동영상 - 2020_04_26, C언어, 문제, 2차원 배열에 문장 10개를 입력해주세요](https://www.youtube.com/watch?v=GZVoNvY2X-g)
- [동영상 - 2020_04_26, C언어, 문제, 사람들의 이름과 나이 입력 받기, 구조체 금지, malloc 금지](https://www.youtube.com/watch?v=xApCZ8V04tA)
- [동영상 - 2020_04_26, C언어, 문제, 사람들의 이름과 나이 입력 받기, 구조체 사용, malloc 금지](https://youtu.be/uzvC-28ENbw)

# 15, 동적할당(링크드 리스트 심화)

## 문제
### 일반
- [문제 - 몇명의 사람의 이름과 나이를 받을지 입력받고 입력받은 수 만큼 이름과 나이를 입력받은 후 저장해주세요.(사람 수 제한 없는 버전)](https://repl.it/@jangka512/SnarlingSuperiorAudit)
- [문제 - 정답](https://repl.it/@jangka512/IllustriousTallApplicationprogram)
- [문제 : 사람들을 서로 연결하여 하나의 리스트로 만들고 반복문으로 순회 v1(처음사람을 통해서 끝 사람까지 접근)](https://repl.it/@jangka512/DeepskyblueScientificWorkers)
- 문제 접근법
  - 1 단계 : 사람구조체 생성
    - 한 사람을 이루는 구성요소는 이름과 나이와 사람의 위치 이다.
  - 2 단계 : 사람 3명 생성
  - 3 단계 : 사람 3명을 세팅
  - 4 단계 : 1번째 사람이 2번째 사람의 위치를 기억, 2번째 사람이 3번째 사람의 위치를 기억
  - 5 단계 : 3번째 사람은 next 변수에 NULL 넣기
  - 6 단계 : 반복문 사용하지 않고 3명의 이름과 나이를 모두 출력(무식하게 나열하면 됨)
  - 7 단계 : for문으로 반복문 3번 돌아서 3명의 이름과 나이를 출력
  - 8 단계 : 현재 작업중인 사람의 next 변수에 NULL이 나올때 까지 반복하면서 다음사람으로 이동하는 버전
- [문제 - 정답(반복문 사용 안한 버전)](https://repl.it/@jangka512/SimpleDarkmagentaTwintext)
- [문제 - 정답](https://repl.it/@jangka512/CornyFavoriteWordprocessing)
- [문제 : 사람들을 서로 연결하여 하나의 리스트로 만들고 반복문으로 순회 v2(처음사람을 통해서 끝 사람까지 접근)](https://repl.it/@jangka512/ConcreteOlivePress)
- [문제 - 정답 v1](https://repl.it/@jangka512/SveltePriceyMegahertz)
- [문제 - 정답 v2](https://repl.it/@jangka512/UrbanIrritatingOutcomes)

## 동영상
- [동영상 - 2020 05 02, C언어, 개념, 동적할당, 정적할당](https://youtu.be/GSkuCXK3Loc)
- [동영상 - 2020 05 02, C언어, 문제, 사람의 정보를 무한히 많이 입력받기](https://youtu.be/OXuX85FUmGA)
- [동영상 - 2020 05 02, C언어, 문제, 사람 3명이 각자 다음사람을 가리킵니다](https://youtu.be/sgwgWfRJ2_I)

# 16, 동적할당(링크드 리스트 심화 2)

## 알고리즘 문제
- [두 정수 사이의 합](https://programmers.co.kr/learn/courses/30/lessons/12912?language=c)
- [약수의 합](https://programmers.co.kr/learn/courses/30/lessons/12928?language=c)
- [콜라츠 추측](https://programmers.co.kr/learn/courses/30/lessons/12943?language=c)
- [직사각형 별찍기](https://programmers.co.kr/learn/courses/30/lessons/12969?language=c)
- [2016년](https://programmers.co.kr/learn/courses/30/lessons/12901?language=c)
- [수박수박수박수박수박수?](https://programmers.co.kr/learn/courses/30/lessons/12922?language=c)
- [짝수와 홀수](https://programmers.co.kr/learn/courses/30/lessons/12937?language=c)
- [평균 구하기](https://programmers.co.kr/learn/courses/30/lessons/12944?language=c)
- [정수 제곱근 판별](https://programmers.co.kr/learn/courses/30/lessons/12934)
- [문자열 다루기 기본](https://programmers.co.kr/learn/courses/30/lessons/12918?language=c)
  - 힌트
    - 문장을 char의 배열이다.
    - 즉 문장을 이루는 개별 요소는 char 타입의 데이터이다.
    - 문장은 \0 문자로 끝난다.
    - 이것을 통해서 문장(문자배열)의 길이를 알 수 있다.
  - 정답
    - [v1](https://repl.it/@jangka512/PlasticAmpleCodeview)
    - [v2](https://repl.it/@jangka512/EasyWordySuperuser)

### 링크드 리스트
- 링크드 리스트 준비 시작
- [문제 - 몇명의 회원을 받을지 미리 알 수 없는 상태에서 회원 추가하기](https://repl.it/@jangka512/LongMajorDeprecatedsoftware)
- 문제조건, 힌트
  - 사람의 3명 만들어주세요.
  - 단 사람을 지역변수로 만들 수 없습니다.
  - 즉 malloc을 통해서 힙에 사람을 만들라는 뜻입니다.
  - 이는 나중에 free를 해야한다는 걸 의미 합니다.
  - 즉 사람을 구조체 포인터로만 다루라는 뜻 입니다.
  - 사람은 자신의 이름과 나이를 기억할 수 있습니다.
  - 사람은 자신 말고도 사람한명의 위치를 기억할 수 있습니다.
  - 즉 사람 구조체의 변수는 3개 입니다.
  - 처음 사람은 두번째 사람의 위치를 알아야 합니다.
  - 두번째 사람은 세번째사람의 위치를 알아야 합니다.
  - 마지막 사람의 next 변수에는 NULL이 들어가야 합니다.
  - 처음사람의 위치가 주어진 상황에서 반복문을 통해, 마지막 사람까지 접근하여주세요.
  - 즉 모든 사람의 이름과 나이가 출력되어야 합니다.
  - first 변수에는 처음사람의 위치가 들어가야 합니다.
  - 즉 first 변수에는 1등이 들어갑니다.
  - 그래서 first 변수에는 값이 1번만 들어갑니다.
  - current 변수에는 지금 막 태어난(malloc으로 만들어진) 사람의 위치가 들어가거나, 반복문에서 각각의 사람에게 접근할 때 사용됩니다.
  - last 변수에는 가장 마지막에 추가된(만들어진, malloc으로 생성된) 사람의 주소가 들어가야 합니다.
  - 즉 first 변수에는 꼴등이 들어갑니다.
  - 그래서 last변수에는 malloc 이 실행될 때 마다(즉 새로운 사람이 추가될 때 마다) 값이 바뀌어야 합니다.
- [문제 - 정답](https://repl.it/@jangka512/LikelyOlivedrabTrapezoid)
- [개념 : 링크드리스트를 위한 기초지식](https://repl.it/@jangka512/RubberyKeenConnection)

## 링크드 리스트 정식
- [문제 - 링크드 리스트, get_new_node](https://repl.it/@jangka512/LuminousElasticClicks)
- [문제 - 정답](https://repl.it/@jangka512/WiryDeepState)
- [문제 - 링크드 리스트, show_all_values](https://repl.it/@jangka512/KlutzyApprehensiveQuerylanguage)
- [문제 - 정답](https://repl.it/@jangka512/AgreeableSpotlessMultithreading)
- [문제 - 링크드 리스트, free_all](https://repl.it/@jangka512/SufficientImmaterialTrial)
- [문제 - 정답](https://repl.it/@jangka512/SuddenFatherlyStartups)
- [문제 - 링크드 리스트, add_value](https://repl.it/@jangka512/WelloffSlimyProjections)
- [문제 - 정답](https://repl.it/@jangka512/LavenderGrizzledEngineer)
- [문제 - 링크드 리스트, add_value_at](https://repl.it/@jangka512/UnwillingFlakyFirmware)
- [문제 - 정답](https://repl.it/@jangka512/OrneryBothBooleanalgebra)
- [문제 - 링크드 리스트, index_of](https://repl.it/@jangka512/HastyUnfoldedUsers)
- [문제 - 정답](https://repl.it/@jangka512/CanineNervousConditions)
- [문제 - 링크드 리스트, update_value_at](https://repl.it/@jangka512/FakeUnawareUpgrades)
- [문제 - 정답](https://repl.it/@jangka512/VisibleGrimProprietarysoftware)
- [문제 - 링크드 리스트, remove_value_at](https://repl.it/@jangka512/ExpensiveInconsequentialDaemons)
- [문제 - 정답](https://repl.it/@jangka512/FrayedRoyalDisk)
- [문제 - 링크드 리스트, remove_by_value](https://repl.it/@jangka512/OutrageousDraftyInteger)
- [문제 - 정답](https://repl.it/@jangka512/ImmediateDimwittedSite)

### 링크드 리스트 정렬
- [문제 - 1차원 int 배열의 오름차순 정렬 함수를 구현해주세요.](https://repl.it/@jangka512/CarefreeOlivedrabAmoebas)
- [문제 - 정답](https://repl.it/@jangka512/GiftedFrontLifecycle)
- [문제 - 1차원 int 배열의 내림차순 정렬 함수를 구현해주세요.](https://repl.it/@jangka512/ImmenseCoarseMicroscope)
- [문제 - 정답](https://repl.it/@jangka512/DarkgreyIndianredCopyrightinfringement)
- [문제 - 링크드 리스트, get_node_list_length](https://repl.it/@jangka512/LightgreenSuburbanProgrammers)
- [문제 - 정답](https://repl.it/@jangka512/KindMurkyBlock)
- [문제 - 링크드 리스트, sort_desc](https://repl.it/@jangka512/PaltryFlashyMultiprocessing)
- [문제 - 링크드 리스트, sort_desc - 정답](https://repl.it/@jangka512/DeadlyBlandCode)
- [문제 - 링크드 리스트, sort_asc](https://repl.it/@jangka512/StableCrimsonGigahertz)
- [문제 - 링크드 리스트, sort_asc - 정답](https://repl.it/@jangka512/FatalAverageRatio)

## 동영상
- [동영상 - 2020_05_03, C언어, 문제, 몇명의 회원을 받을지 미리 알 수 없는 상태에서 회원 추가하기](https://youtu.be/RG6KNBuZDZc)
- [동영상 - 2020_05_03, C언어, 문제, 몇명의 회원을 받을지 미리 알 수 없는 상태에서 회원 추가하기, 추가설명 1](https://youtu.be/V7eTzhkXFVQ)
- [동영상 - 2020_05_03, C언어, 문제, 몇명의 회원을 받을지 미리 알 수 없는 상태에서 회원 추가하기, 추가설명 2](https://youtu.be/MqKrZZeuagc)
- [동영상 - 2020_05_03, C언어, 문제, 몇명의 회원을 받을지 미리 알 수 없는 상태에서 회원 추가하기, 추가설명 3](https://youtu.be/7JjPsCmAySE)
