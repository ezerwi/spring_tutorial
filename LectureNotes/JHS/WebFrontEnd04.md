# 페이지 정보
- 주소 : to2.kr/bgd
- 클래스 : 2020년 04월 웹 수업

# 강사정보
- 이름 : 장희성
- 전화번호 : 010-8824-5558

# 수업관련링크
- [슬릭 슬라이더 한글](https://programmer93.tistory.com/34)
- [대전 앨리스 의원, 모작](https://codepen.io/jangka44/live/GRpJBQm)
- [2019년 09월 포폴반 학생들 포트폴리오](https://to2.kr/beR)
- [타이핑 정글](https://www.typingclub.com/sportal/program-3.game)
- [브라켓 다운로드](http://brackets.io/)
- [브라켓 에디터 세팅법](https://s.codepen.io/jangka44/debug/bjjPGx)
- [파일질라 다운로드](https://filezilla-project.org/download.php?type=client)
  - Pro가 아닌 일반 버전 다운로드
  - 광고 체크 해제
- [CSS DINER](https://flukeout.github.io/)
- [새 연습장(코드펜)](https://codepen.io/pen/)
  - 주소 : codepen.io/pen
- [code.org 코스 3](https://studio.code.org/s/course3)
- [code.org 코스 4](https://studio.code.org/s/course4)
- [기초 키보드 사용법](https://s.codepen.io/jangka44/debug/OzzVWM)
- [프로그래머스 자바스크립트 입문 강의](https://programmers.co.kr/learn/courses/3)
  - [풀이](https://codepen.io/jangka44/live/mddjOxo)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 30분이 넘으면 안됩니다.
- [플레이코드몽키](https://www.playcodemonkey.com)
  - 계정
    - 아이디 : dnehd1515@naver.com
    - 비번 : sbs1234
- [플레이코드몽키, 스테이지 쉽게 접근하기](https://codepen.io/jangka44/debug/ZEEWRzZ)

# 2020-02-14, 1일차
## 문제
- [개념 : HTML, CSS, JS 개요](https://codepen.io/jangka44/pen/vzYVYb)
- [개념 : HTML, CSS, JS 개요 2](https://codepen.io/jangka44/pen/YzXwLJP)
  - 책내용 : 23p
- [개념 : display 속성 정리](https://codepen.io/jangka44/live/ZjbpPR)
  - 책내용 : 238p
- [문제 : div, section, article 태그를 사용해서 3가지 색의 막대를 만들어주세요.](https://codepen.io/jangka44/pen/KBdaGd)
  - 책내용 : 92p
  - 제한시간 : 1분 30초
- [문제 - 정답](https://codepen.io/jangka44/pen/VBvPeM)
- [문제 : section(green 색 막대)과 article(blue 색 막대)을 한 줄에 보이게 해주세요.](https://codepen.io/jangka44/pen/qyOqrE)
  - 책내용 : 238p
  - 제한시간 : 1분 30초
- [문제 - 정답](https://codepen.io/jangka44/pen/WKQRGW)

## 동영상
- [동영상 - 2020 02 14, 프론트, 개념,CODEPN 세팅법](https://www.youtube.com/watch?v=4V6jglwjcvg&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS의 역할](https://www.youtube.com/watch?v=6nFdDSWj9JU)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS을 공부하는 효율적인 방법](https://www.youtube.com/watch?v=A_vHA7ALWRk&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, display 속성](https://www.youtube.com/watch?v=OjLElVktdos&feature=youtu.be)
- [동영상 - 2020 02 14 21, 프론트, 문제, 막대 3개 구현, 밑에 2개는 한 줄에](https://www.youtube.com/watch?v=ltAum2lXzNo&feature=youtu.be)
- [동영상 - 2020 02 14 21, 프론트, 문제, 막대 3개 구현](https://www.youtube.com/watch?v=U2F6Q2hfWKA&feature=youtu.be)

# 2020-02-17, 2일차
## 문제
- [개념 : 엘리먼트(배우)의 부모, 자식, 형제관계](https://codepen.io/jangka44/pen/bQqbpZ)
  - 책내용 : 274p
- [개념 : 글자](https://codepen.io/jangka44/pen/mjPYqy)
  - 책내용 : 92p
- [개념 : a와 br](https://codepen.io/jangka44/pen/zLqQam)
  - 책내용 : 84p
- [문제 : 각각의 사이트 명에 링크를 걸어주세요.(새창으로 떠야 합니다.)](https://codepen.io/jangka44/pen/ZEEMPpy)
  - 책내용 : 84p
  - 제한시간 : 1분 30초
- [문제 - 정답](https://codepen.io/jangka44/pen/mddGorY)
- [문제 : 젠코딩으로 네이버, 구글, 다음으로 가는 링크를 만들어주세요.](https://codepen.io/jangka44/pen/pBWYyR)
  - 책내용 : 84p
- [문제 - 정답](https://codepen.io/jangka44/pen/pBWYyR)
- [개념 : 후손 선택자](https://codepen.io/jangka44/pen/KBzjzB)
  - 책내용 : 274p
- [문제 : 각각의 크기와 색상이 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/JaoBNQ)
  - 책내용 : 274p
- [문제 - 정답](https://codepen.io/jangka44/pen/yxyqQQ)
- [개념 : text-align](https://codepen.io/jangka44/pen/JBXgNy)
  - 책내용 : 92p
- [개념 : text-align v2](https://codepen.io/jangka44/pen/JaLwpr)
  - 책내용 : 92p
- [문제 : 각각의 크기와 색상과 정렬상태가 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/XPJByw)
  - 책내용 : 92p
- [문제 - 정답](https://codepen.io/jangka44/pen/rZarQb)

## 오늘의 동영상
- [동영상 - 2020 02 17, 프론트, 개념, display, 엘리먼트, 선택자](https://www.youtube.com/watch?v=JGQKuf9uS3E)
- [동영상 - 2020 02 17, 프론트, 개념, 엘리먼트 관계, 부모자식 관계, 형제관계](https://www.youtube.com/watch?v=yE0EyLlnBdE)
- [동영상 - 2020 02 17, 프론트, 개념, font weight, letter spacing, font size](https://www.youtube.com/watch?v=yfgMvBiPzYc)
- [동영상 - 2020 02 17, 프론트, 개념, 앵커태그, br 태그](https://www.youtube.com/watch?v=tsb7ssDcd9k)
- [동영상 - 2020 02 17, 프론트, 개념, 젠코딩 emmet](https://www.youtube.com/watch?v=rRapBHkt6gU)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, 후손 선택자](https://www.youtube.com/watch?v=oVuSYCYjtaU)
- [동영상 - 2020 02 17, 프론트, 문제, 색상과 크기가 다른 3가지 링크를 만들어주세요](https://www.youtube.com/watch?v=AcIq02YTOdY)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, text align](https://www.youtube.com/watch?v=d2JjVUMkOoI)

# 2020-02-19, 3일차
## 문제
- [개념 : hover](https://codepen.io/jangka44/pen/KBzOKj)
  - 책내용 : 281p
- [문제 : 100칸 짜리 바둑판을 만들고 하나의 칸에 마우스를 올리면 해당 칸의 배경색이 바뀌도록 해주세요.](https://codepen.io/jangka44/pen/VGYGvp)
  - 책내용 : 281p
- [문제 - 정답](https://codepen.io/jangka44/pen/QVwVWZ)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/vYOKqNg)
- [개념 : short code(자식, 인접동생)](https://codepen.io/jangka44/pen/rreXjL)
  - 책내용 : 274p, 280p
- [개념 : `nbsp;`](https://codepen.io/jangka44/pen/yqOmxJ)
  - 책내용 : 59p
- [개념 : text-deocration:none](https://codepen.io/jangka44/pen/eLNreO)
- [문제 : bnx 사이트의 상단 메뉴까지 구현해주세요.](https://codepen.io/jangka44/pen/oMxKZp)
- [문제 - 정답 v1](https://codepen.io/jangka44/pen/wQdWEG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/ejZqvz)
- [문제 - 정답 v3](https://codepen.io/jangka44/pen/zLBYrQ)

## 오늘의 동영상
- [동영상 - 2020 02 19, 프론트, 문제, CSS, 100칸짜리 바둑판형태, hover](https://www.youtube.com/watch?v=tgj8dKLestk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, `nbsp;`](https://www.youtube.com/watch?v=5LeDdCf4Cso&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, hover](https://www.youtube.com/watch?v=N7BlAxCMFS4&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, text decoration](https://www.youtube.com/watch?v=JZtjOcZkIGg&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 선택자, 자식, 후손, text align, display](https://www.youtube.com/watch?v=cCaWklKPsLs&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 자식선택자, 인접동생선택자](https://www.youtube.com/watch?v=kRU98SwRrnQ&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 1차](https://www.youtube.com/watch?v=SJEhyNfSsD4&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 2차](https://www.youtube.com/watch?v=egc7Njqkddk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 3차](https://www.youtube.com/watch?v=KlwqR4rNv0g&feature=youtu.be)

# 2020-02-24, 4일차
## 문제
- [문제 : bnx 사이트의 상단 메뉴까지 구현해주세요.](https://codepen.io/jangka44/pen/oMxKZp)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/PxjWxY)
- [문제 - 정답 v5(10분완성)](https://codepen.io/jangka44/pen/qMdKbP)
- [개념 - 이미지](https://codepen.io/jangka44/pen/pZEYmx)
  - 책내용 : 76p
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/MWwJJYr)
  - 책내용 : 76p
- [문제 - 정답](https://codepen.io/jangka44/pen/KKpaNYm)
- [개념 - margin, padding](https://codepen.io/jangka44/pen/PBGgQa)
  - 책내용 : 98p
- [개념 - margin, padding v2](https://codepen.io/jangka44/pen/rZvRmQ)
  - 책내용 : 98p
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/Rwbrzgx)
  - 책내용 : 76p
- [문제 - 정답](https://codepen.io/jangka44/pen/pozgXaz)
- [문제 - 정답 v2(10분 완성)](https://codepen.io/jangka44/pen/QWbddOa)

## 오늘의 동영상
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 4차](https://www.youtube.com/watch?v=Kin-u_fjiNI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 5차](https://www.youtube.com/watch?v=kD0uXVLGy14&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 개념, CSS, margin, padding](https://www.youtube.com/watch?v=pDT-JgMIL-s&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 개념, img 엘리먼트](https://www.youtube.com/watch?v=cBF78x1SArI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS](https://www.youtube.com/watch?v=8-APhZJ12i8&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS, only padding](https://www.youtube.com/watch?v=83GwjeVlxtE)

# 2020-02-26, 5일차
## 문제
- [개념 - nth-child](https://codepen.io/jangka44/pen/xJEeag)
  - 책내용 : 282p
- [개념 - nth-child v2](https://codepen.io/jangka44/pen/wEXOJo)
  - 책내용 : 282p
- [문제 - 무지개를 만들어주세요.](https://codepen.io/jangka44/pen/OJLKjBX)
  - 책내용 : 282p
- [문제 - 정답](https://codepen.io/jangka44/pen/aboeyRQ)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다.](https://codepen.io/jangka44/pen/xJEoPe)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다. v2](https://codepen.io/jangka44/pen/ZMRPMM)
- [문제 - bnx 사이트의 상단 이미지 4개 까지만 구현해주세요.](https://codepen.io/jangka44/pen/EpgJPa)
- [문제 - 정답 v1, br과 nbsp](https://codepen.io/jangka44/pen/KBgYzb)
- [문제 - 정답 v2, margin](https://codepen.io/jangka44/pen/vaXMyZ)
- [문제 - 정답 v3, 엘리먼트 5개 이하, body](https://codepen.io/jangka44/pen/rZZqRW)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법](https://codepen.io/jangka44/pen/zJNKwN)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법 v2](https://codepen.io/jangka44/pen/EeGyKK)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/WggYOr)
- [개념 - border-radius](https://codepen.io/jangka44/pen/bjwyMd)
  - 책내용 : 177p
- [개념 - border-radius v2](https://codepen.io/jangka44/pen/KxeEqX)
  - 책내용 : 177p
- [개념 - border-radius v3](https://codepen.io/jangka44/pen/VGdRMZ)
  - 책내용 : 177p
- [개념 - border-radius v4(5분완성)](https://codepen.io/jangka44/pen/XyeREQ)
  - 책내용 : 177p

## 오늘의 동영상
- [동영상 - 2020 02 26, 프론트, 개념, CSS, nth-child, first-child, last-child](https://youtu.be/8udV6lGRgnI)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요.](https://youtu.be/xA51J05sRaU)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, inline 요소에는 width, height, padding, margin이 작동하지 않는다.](https://youtu.be/hLxOhBJzzl0)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, br, &nbsp;, margin 등을 사용](https://youtu.be/SbvwOTp1PaY)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 5개 배치, 오직 엘리먼트 5개만 사용, 영상의 끝은 2분 50초 까지 입니다.](https://youtu.be/iOJnado5Gds)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, block 요소 가운데 정렬하는 방법, margin:0 auto;](https://youtu.be/vKWVeRPmhKg)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/OtJvZo2ls5I)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, border-radius](https://youtu.be/Ddylg0YjouM)

# 2020-02-28, 6일차
## 개념
- HTML
  - 관계
    - 부모/자식
    - 형/동생
  - 태그
    - 인라인 계열
      - span(div에서 display만 inline인 태그, 인라인 계열의 기본 태그)
      - a : 링크
      - img : 이미지
    - 블록 계열
      - 기본
        - div(구분, 적절한 태그가 생각나지 않을 때, 모르면 div, 블록 계열의 기본태그)
        - nav(내비게이션, 보통 메뉴 감쌀 때)
        - section(섹션)
        - article(아티클, 게시물)
      - 제목
        - h1, h2, h3, h4, h5, h6
      - 목록
        - ul, li : 순서 없는 목록
        - ol, li : 순서 있는 목록
- CSS
  - 노말라이즈
    - 해당 엘리먼트에 기본적으로 적용되어 있는 디자인을 없애서 다시 평범하게 만든다.
    - a, body, ul, li, ol, li, h1, h2, h3, h4, h5, h6 은 사용하기전에 노말라이즈 해야 한다.
  - 선택자
    - 태그선택자 : `div { ... }`
    - 자식선택자 : `div > a { ... }`
    - 후손선태자 : `div a { ... }`
    - 클래스선택자 : `.menu-item { ... }`
  - 기본 속성
    - width, height, font-size, font-weight, letter-spacing, color, background-color, margin-top, margin-left, padding-top, padding-left, border, border-radius
  - 레이아웃 속성
    - display
      - none : 사라짐
      - inline-block, inline : 글자화
      - block : 블록화
    - float : ??
    - position : ???

## 문제
- [개념 - inherit](https://codepen.io/jangka44/pen/JBRgyK)
- [개념 - inherit v2](https://codepen.io/jangka44/pen/aaKMxg)
- [개념 - a 노말라이즈](https://codepen.io/jangka44/pen/BPLXrO)
- [문제 - 메뉴를 구현해주세요.(1차 메뉴까지만)](https://codepen.io/jangka44/pen/zLKQRP)
- [힌트](https://codepen.io/jangka44/pen/RwwbqJQ)
- [문제 - 정답 v1](https://codepen.io/jangka44/pen/EpgqKa)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/aaNBMp)
- [문제 - 정답 v3(10분 완성)](https://codepen.io/jangka44/pen/yxJaNb)
- [개념 - body 노말라이즈](https://codepen.io/jangka44/pen/djvqLM)
  - 책내용 : 98p
- [개념 - 클래스 선택자](https://codepen.io/jangka44/pen/oMZMyQ)
  - 책내용 : 273p
- [문제 - 모든 엘리먼트의 태그를 div로 바꿔주세요.](https://codepen.io/jangka44/pen/aQqGbO)
  - 책내용 : 273p
- [문제 - 정답](https://codepen.io/jangka44/pen/oQEqrL)
- [문제 - 최상위 엘리먼트의 클래스 속성을 제외한 나머지는 전부 지워주세요.](https://codepen.io/jangka44/pen/ZmroEN)
- [문제 - 정답(10분 완성)](https://codepen.io/jangka44/pen/xQYjbw)
- [개념 - ul, li](https://codepen.io/jangka44/pen/EpWOam)
  - 책내용 : 60p
- [개념 - ol, li](https://codepen.io/jangka44/pen/PxQeEB)
  - 책내용 : 60p
- [개념 - h1, h2, h3, h4, h5, h6](https://codepen.io/jangka44/pen/KrQRVJ)
  - 책내용 : 50p

## 오늘의 동영상
- [동영상 - 2020 02 28, 프론트, 개념, CSS, inherit, 상속](https://www.youtube.com/watch?v=bsgQpo8Ui1Y&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, a 엘리먼트 노말라이즈](https://www.youtube.com/watch?v=y6h8JlP-_hU&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, body 엘리먼트 노말라이즈](https://www.youtube.com/watch?v=jqvxkBLiZtw&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, CSS, 클래스](https://www.youtube.com/watch?v=GnPdH0GRaE8&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기](https://www.youtube.com/watch?v=ys1hu93CtkQ&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기, 오직 div 태그만 사용, 클래스 이용](https://www.youtube.com/watch?v=Q-PePcdbZ8s&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 문제, CSS, 1차 메뉴 만들기, 오직 div 태그만 사용, 1개의 엘리먼트에만 클래스 적용](https://www.youtube.com/watch?v=wxRqeP48kmw&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, HTML, h1, h2, h3](https://www.youtube.com/watch?v=1lMLjmOF52c&feature=youtu.be)
- [동영상 - 2020 02 28, 프론트, 개념, HTML, ul, ol, li](https://www.youtube.com/watch?v=Z5Z69G0XrZI&feature=youtu.be)

# 2020-03-13, 7일차
## 문제
- [문제 - 방탄소년단의 노래소개와 멤버구성을 ul, ol, li, h1, h2 태그만으로 표현해주세요.](https://codepen.io/jangka44/pen/LXQmzW)
  - 책내용 : 50p, 60p
- [문제 - 정답](https://codepen.io/jangka44/pen/BGrJqO)
- [개념 - ul, li 노말라이즈](https://codepen.io/jangka44/pen/ajJQzw)
  - 책내용 : 194p
- [개념 - h1, h2, h3, h4, h5, h6 노말라이즈](https://codepen.io/jangka44/pen/PxaKWb)
- [개념 - 상황에 맞는 태그를 알면 그걸 사용하고, 모르면 div](https://codepen.io/jangka44/pen/zLZmXo)
  - 최종버전을 5분안에 만들 수 있어야 합니다.
- 풀 다운 메뉴 만들기 v1 시작
- [풀 다운 메뉴 만들기 1](https://codepen.io/jangka44/pen/wmamMY)
- [풀 다운 메뉴 만들기 2](https://codepen.io/jangka44/pen/oqXqBe)
- [풀 다운 메뉴 만들기 3](https://codepen.io/jangka44/pen/JLdLVM)
- [풀 다운 메뉴 만들기 4](https://codepen.io/jangka44/pen/VXvQOK)
- [풀 다운 메뉴 만들기 5](https://codepen.io/jangka44/pen/NYGyZM)
- [풀 다운 메뉴 만들기 6](https://codepen.io/jangka44/pen/yKYKBZ)
- [풀 다운 메뉴 만들기 7](https://codepen.io/jangka44/pen/Zxbxbd)
- [풀 다운 메뉴 만들기 8](https://codepen.io/jangka44/pen/NYGYrL)
- [풀 다운 메뉴 만들기 9](https://codepen.io/jangka44/pen/QmjmvY)
- [풀 다운 메뉴 만들기 10](https://codepen.io/jangka44/pen/jzbzpj)
- [풀 다운 메뉴 만들기 11](https://codepen.io/jangka44/pen/JLGLpX)
- [풀 다운 메뉴 만들기 12](https://codepen.io/jangka44/pen/KoVoRE)
- [풀 다운 메뉴 만들기 13](https://codepen.io/jangka44/pen/eMJMPY)
- [풀 다운 메뉴 만들기 14](https://codepen.io/jangka44/pen/zWrWVB)
- [풀 다운 메뉴 만들기 15](https://codepen.io/jangka44/pen/JLGvXJ)
- [풀 다운 메뉴 만들기 16](https://codepen.io/jangka44/pen/eMJreg)
- [풀 다운 메뉴 만들기 17](https://codepen.io/jangka44/pen/mxVLXa)
- [풀 다운 메뉴 만들기 18](https://codepen.io/jangka44/pen/KoVRRz)
- [풀 다운 메뉴 만들기 19](https://codepen.io/jangka44/pen/KoVRez)
- [풀 다운 메뉴 만들기 20(5분 완성)](https://codepen.io/jangka44/pen/wmMjxw)
- 풀 다운 메뉴 만들기 v1 끝

## 오늘의 동영상
- [동영상 - 2020 03 13, 프론트, 개념, h1, h2, h3, h4, h5, h6 노말라이즈](https://www.youtube.com/watch?v=EhcqjWUOJM0&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 개념, 어떤 태그를 써야할지 모르겠다면 div](https://www.youtube.com/watch?v=SYAMjmslyFs&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 개념, ul, li 노말라이즈](https://www.youtube.com/watch?v=6Ozco_EpgPg&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 문제, 방탄소년단의 노래소개와 멤버구성을 ul, ol, li, h1, h2 태그만으로 표현해주세요](https://www.youtube.com/watch?v=39I5q9qW4bA&feature=youtu.be)
- [동영상 - 2020 03 13, 프론트, 문제, 풀 다운 메뉴 만들기 v1](https://www.youtube.com/watch?v=ewOyZAu1kso&feature=youtu.be)

# 2020-03-16, 8일차
## 문제
- [문제 - 3가지 메뉴를 구현해주세요.(1차 메뉴까지만)](https://codepen.io/jangka44/pen/EegZdW)
- [문제 - 정답](https://codepen.io/jangka44/pen/JaREmy)
- [문제 - a에 마우스를 올리면 인접동생인 div가 나타나게 해주세요.(즉 a의 인접동생인 div는 평소에 안나와야 합니다.)](https://codepen.io/jangka44/pen/PoorMYm)
- [문제 - 정답](https://codepen.io/jangka44/pen/LYYKwYE)
- position 개념 시작
  - 책내용 : 330p, 335p
- [개념 : position 속성 정리](https://codepen.io/jangka44/debug/aaXopM)
- [개념 : position 예제](https://codepen.io/jangka44/pen/eYYwqLO)
- [position 예제 v1 만들기 1](https://codepen.io/jangka44/pen/NYgaaw)
- [position 예제 v1 만들기 2](https://codepen.io/jangka44/pen/pLNWYy)
- [position 예제 v1 만들기 3](https://codepen.io/jangka44/pen/OvbxGe)
- [position 예제 v1 만들기 4](https://codepen.io/jangka44/pen/dmOVEj)
- [position 예제 v2 만들기 0](https://codepen.io/jangka44/pen/rNBmbER)
- [position 예제 v2 만들기 1](https://codepen.io/jangka44/pen/pLNdoE)
- [position 예제 v2 만들기 2](https://codepen.io/jangka44/pen/LdbOYB)
- [position 예제 v3 만들기 1](https://codepen.io/jangka44/pen/NYbwqG)
- [position 예제 v3 만들기 2](https://codepen.io/jangka44/pen/VXmrLV)

## 오늘의 동영상
- [동영상 - 2020 03 16, 프론트, 개념, position](https://www.youtube.com/watch?v=wt405dLbKyM)
- [동영상 - 2020 03 16, 프론트, 문제, 3가지 다른 글자색의 메뉴를 만들어주세요, 클래스](https://www.youtube.com/watch?v=GDpZMwFfcig)
- [동영상 - 2020 03 16, 프론트, 문제, a에 마우스를 올리면 인접동생인 div가 나타남](https://www.youtube.com/watch?v=3-IN1hFFh6c)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 4등분 해주세요](https://www.youtube.com/watch?v=BnGYQ9TDAVs)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 3등분 해주세요](https://www.youtube.com/watch?v=Tp5xfuXxo-o)
- [동영상 - 2020 03 16, 문제, absolute, width, height, top, left 로 화면을 9등분 해주세요](https://www.youtube.com/watch?v=OR0bNgpQgj8)
- [동영상 - 2020 03 16, 문제, absolute, top, left, right, bottom으로 화면을 4등분 해주세요](https://www.youtube.com/watch?v=m-5tK7hZtNs)

# 2020-03-18, 9일차
## 개념
- [에듀위드 웹 UI 개발 코스](https://www.edwith.org/boostcourse-ui)
  - 2. HTML 태그 까지 진행

## 문제
- [position 예제 v4 만들기 1](https://codepen.io/jangka44/pen/vRyeVz)
- [position 예제 v4 만들기 2](https://codepen.io/jangka44/pen/PRpJVB)
- [position 예제 v4 만들기 3](https://codepen.io/jangka44/pen/MVpEdY)
- [position 예제 v4 만들기 4](https://codepen.io/jangka44/pen/zWZPOK)
- [position 예제 v4 만들기 5](https://codepen.io/jangka44/pen/RMpjpe)
- [position 예제 v4 만들기 6(5분 완성)](https://codepen.io/jangka44/pen/bvqYab)
- [position 예제 v4-2 만들기 6](https://codepen.io/jangka44/pen/RvzwWL)
- [position 예제 v4-3 만들기 6](https://codepen.io/jangka44/pen/XOLWXJ)
- [position 예제 v4-4 만들기 6](https://codepen.io/jangka44/pen/BMgajV)
- [개념 : z-index 예제](https://codepen.io/jangka44/pen/rNaBavg)
- [문제 : 교재 339p 코드 24-6의 실행결과처럼 만들어주세요.(5분완성)](https://codepen.io/jangka44/pen/dyPbPQp)
- [문제 - 정답](https://codepen.io/jangka44/pen/yLyByqG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/LYVreep)
- position 개념 끝
- text, 숨김 시작
- [개념 : white-space:nowrap, 줄바꿈 금지](https://codepen.io/jangka44/pen/NLdRyK)
- [개념 : overflow-x:auto, 스크롤바 자동생성](https://codepen.io/jangka44/pen/vzgXjV)
- [개념 : overflow-x:scroll, 스크롤바 무조건 생성](https://codepen.io/jangka44/pen/ZMLpRB)
- [개념 : overflow-x:hidden, 넘치는 내용 숨김](https://codepen.io/jangka44/pen/vzgXrV)
- [개념 : text-overflow:ellipsis, 넘쳐서 숨겨지는 텍스트 ... 처리(5분완성)](https://codepen.io/jangka44/pen/ZMLpjL)
- text, 숨김 끝
- 풀 다운 메뉴 v2 시작
- [풀 다운 메뉴 v2 만들기 1](https://codepen.io/jangka44/pen/aYNGjw)
- [풀 다운 메뉴 v2 만들기 2](https://codepen.io/jangka44/pen/dmMKyq)
- [풀 다운 메뉴 v2 만들기 3](https://codepen.io/jangka44/pen/mxPKeg)
- [풀 다운 메뉴 v2 만들기 4](https://codepen.io/jangka44/pen/Kozezg)
- [풀 다운 메뉴 v2 만들기 5](https://codepen.io/jangka44/pen/WzwyGw)
- [풀 다운 메뉴 v2 만들기 6](https://codepen.io/jangka44/pen/jzrzwY)
- [풀 다운 메뉴 v2 만들기 7](https://codepen.io/jangka44/pen/EEyEbJ)
- [풀 다운 메뉴 v2 만들기 8](https://codepen.io/jangka44/pen/qoNowO)
- [풀 다운 메뉴 v2 만들기 9](https://codepen.io/jangka44/pen/OvXvYO)
- [풀 다운 메뉴 v2 만들기 10(5분 완성)](https://codepen.io/jangka44/pen/pLbVoq)
- 풀 다운 메뉴 v2 끝

## 오늘의 동영상
- [동영상 - 2020 03 18, 프론트, 개념, position absolute, 수직수평 중앙정렬](https://youtu.be/aROEkCwtIvw)
- [동영상 - 2020 03 18, 프론트, 개념, position absolute, 수직수평 중앙정렬, 유동너비버전](https://youtu.be/xm-1w-37NGo)
- [동영상 - 2020 03 18, 프론트, 문제, 사각형 6개를 배열해주세요, absolute, relative](https://youtu.be/OdZW5I0Hlrk)
- [동영상 - 2020 03 18, 프론트, 개념, position, fixed, relative](https://youtu.be/9W2KzkXHu-s)
- [동영상 - 2020 03 18, 프론트, 개념, overflow, text-overflow, white-space, 텍스트 말줄임 효과](https://youtu.be/I9e8BknCvKs)
- [동영상 - 2020 03 18, 프론트, 문제, 풀 다운 메뉴 만들기 v2](https://youtu.be/jLW47KUS0n8)

# 2020-03-23, 10일차
## 개념
- 속성선택자
  - 책내용 : 276p
- 테이블
  - 책내용 : 68p
- 브라켓
  - 책내용 : 100p

## 문제
- [문제 : 선택자 연습](https://codepen.io/jangka44/pen/ZxLXMb)
- [문제 - 정답](https://codepen.io/jangka44/pen/pLeWWY)
- [문제 - 시간표를 구현해주세요.](https://codepen.io/jangka44/pen/NWPPLWq)
- [문제 - 정답](https://codepen.io/jangka44/pen/PowwdYr)
- [문제 - 시간표를 가운데 정렬 해주세요.](https://codepen.io/jangka44/pen/JjooaPL)
- [문제 - 정답](https://codepen.io/jangka44/pen/dyPPjQK)
- [개념 - 테이블, border-collapse](https://codepen.io/jangka44/pen/gObbjRB)
- [개념 - 테이블, 예제 1](https://codepen.io/jangka44/pen/gOpdQwB)
- 풀 다운 메뉴 v3 시작
- [풀 다운 메뉴 v3 만들기 1](https://codepen.io/jangka44/pen/eMzrWY)
- [풀 다운 메뉴 v3 만들기 2](https://codepen.io/jangka44/pen/PRbJzd)
- [풀 다운 메뉴 v3 만들기 3](https://codepen.io/jangka44/pen/vVGgvX)
- [풀 다운 메뉴 v3 만들기 4](https://codepen.io/jangka44/pen/YaprpY)

## CSS DINER
- [CSS DINER](https://flukeout.github.io/)

## 오늘의 동영상
- [동영상 - 2020 03 23, 프론트, 문제, 각각의 선택자가 몇개의 엘리먼트를 선택하나요](https://www.youtube.com/watch?v=Fiw1ANPFv6o&feature=youtu.be)
- [동영상 - 2020 03 23, 프론트, 개념, 테이블](https://www.youtube.com/watch?v=Cbb0cnDEfT4&feature=youtu.be)
- [동영상 - 2019 03 04, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=dNWfYAwLEMk&list=PLE0hRBClSk5J2xT3zbtrYyYV1gsDhGJd1&index=9)
- [동영상 - 2020 03 23, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=Cbb0cnDEfT4&feature=youtu.be)

# 2020-04-06, 11일차
## 문제
- [풀 다운 메뉴 v3 만들기 5](https://codepen.io/jangka44/pen/zmqNeV)
- [풀 다운 메뉴 v3 만들기 6](https://codepen.io/jangka44/pen/oaxBOb)
- [풀 다운 메뉴 v3 만들기 7](https://codepen.io/jangka44/pen/rqejbg)

## 오늘의 동영상
- [동영상 - 2020 04 06, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 2차까지](https://www.youtube.com/watch?v=DO5sIbvIPsg&feature=youtu.be)
- [동영상 - 2019 03 04, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 완성](https://www.youtube.com/watch?v=18PbGTxuJkA&list=PLE0hRBClSk5J2xT3zbtrYyYV1gsDhGJd1&index=10)
- [동영상 - 2020 04 06, 프론트, 문제, 풀 다운 메뉴 만들기, v3, 완성](https://www.youtube.com/watch?v=SBELxy--0Fc&feature=youtu.be)
- [동영상 - 2020 04 06, 프론트, 개념, CSS 디너 하는 방법](https://www.youtube.com/watch?v=jJs07q_y0eE&feature=youtu.be)
- [동영상 - 2020 04 06, 프론트, 개념, 코드펜, 자바스크립트 콘솔 사용법](https://www.youtube.com/watch?v=AZrwUAc6BB8&feature=youtu.be)

## 숙제
- 프로그래머스 자바스크립트 입문 파트 5까지 풀기, 정답보기 가능
- CSS DINER 12단계까지 풀기, 정답동영상 보기 가능
- 7일차, 풀다운 메뉴 만들기 v1 20단계, 엄청나게 확실하게 마스터!!

# 2020-04-08, 12일차
## 문제
- [문제 - 풀 다운 메뉴 v3, 엘리먼트 개수를 맞춰주세요.](https://codepen.io/jangka44/pen/KKwVaLL)
- [문제 - 정답](https://codepen.io/jangka44/pen/VRwyep)
- 풀 다운 메뉴 v3 끝
- position:fixed 시작
- [문제 - 스크롤바를 따라다니는 유령을 만들어주세요.](https://codepen.io/jangka44/pen/PxXwWX)
- [문제 - 정답](https://codepen.io/jangka44/pen/GwPgWY)
- position:fixed 끝
- before, after 시작
- [::before, ::after v1 1](https://codepen.io/jangka44/pen/WzXpQY)
- [::before, ::after v1 2](https://codepen.io/jangka44/pen/wmPJMx)
- [::before, ::after v1 3](https://codepen.io/jangka44/pen/pLdeyB)
- [::before, ::after v1 4](https://codepen.io/jangka44/pen/JLOWKR)
- before, after 끝
- 트랜지션 시작
- [트랜지션 예제 v1 만들기 1](https://codepen.io/jangka44/pen/VXbrrX)
- [트랜지션 예제 v1 만들기 2](https://codepen.io/jangka44/pen/RMVjxB)
- [트랜지션 예제 v1 만들기 3](https://codepen.io/jangka44/pen/PRmOQX)
- [트랜지션 예제 v1 만들기 4](https://codepen.io/jangka44/pen/geWXeZ)
- [문제 - 너비가 늘어나는데 3초, 줄어드는데 1초가 걸리게 해주세요.](https://codepen.io/jangka44/pen/RwwRpJr)
- [문제 - 정답](https://codepen.io/jangka44/pen/RwwRpJr)
- 트랜지션 끝
- 토클 사이드 바 시작
- [문제 - 좌측 토글 사이드 바를 만들어주세요](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://codepen.io/jangka44/debug/LYENJqO)
- [문제 - 정답](https://codepen.io/jangka44/pen/QJzbyG)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/gOaYeeQ)
- [문제 - 좌측 토글 사이드 바를 만들어주세요, 2차 메뉴 버전](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://codepen.io/jangka44/debug/LYENJqO)
- [정답](https://codepen.io/jangka44/pen/LYENJqO)
- [문제 - 서브메뉴를 가지고 있는 메뉴 아이템은 표시를 해주세요.](https://codepen.io/jangka44/pen/MWWjgVR)
- [문제 - 정답](https://codepen.io/jangka44/pen/OJJRLZQ)
- [문제 - 좌측 토글 사이드 바를 만들어주세요, 2차 메뉴 버전](https://codepen.io/jangka44/pen/oRvgyZ)
- [정답(소스코드 없음)](https://cdpn.io/jangka44/debug/VOZZxE)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/VOZZxE)
- [문제 - 정답 v3](https://codepen.io/jangka44/pen/MWgOmmr)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/ZEzaKKr)
- [예제 - 좌측 토글 사이드바 구현(아이콘 회전, 3차 메뉴)](https://codepen.io/jangka44/pen/xxbONej)
- [문제 - 정답 v4(2020-04-08)](https://codepen.io/jangka44/pen/vYNBjNb)

## 오늘의 동영상
- [동영상 - 2020 04 08, 프론트, 개념, 3차 메뉴 쉽게 만드는 전략](https://www.youtube.com/watch?v=GMFsMvpDejs&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, position, fixed](https://www.youtube.com/watch?v=UGKOitl8uLw&feature=youtu.be)
- [동영상 - 2019 12 11, 프론트, 개념, before, after](https://www.youtube.com/watch?v=-fgbyOjp4BA&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, before, after](https://www.youtube.com/watch?v=tBz-p6Zi4zM&feature=youtu.be)
- [동영상 - 2019 12 09, 프론트, 개념, 트랜지션](https://www.youtube.com/watch?v=VhiLkt6vTb0&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, 트랜지션](https://www.youtube.com/watch?v=x_FwmPj1Rrg&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 개념, 트랜지션으로 프로그래시브 바 구현](https://www.youtube.com/watch?v=3rW-ysNgM9c&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 트랜지션, 너비가 늘어나는데 3초, 줄어드는데 1초가 걸리게 해주세요](https://www.youtube.com/watch?v=WhyfTPzii_Y&feature=youtu.be)
- [동영상 - 2019 12 11, 프론트, 문제, 좌측 토글 사이드 바 만들기 v2, 작업 1](https://www.youtube.com/watch?v=A2OWRqqg6_A&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 좌측 토글 사이드 바 만들기, 작업 2](https://www.youtube.com/watch?v=45UpAz0eyis&feature=youtu.be)
- [동영상 - 2020 04 08, 프론트, 문제, 좌측 토글 사이드 바 만들기 v2, 작업 2](https://www.youtube.com/watch?v=9cAl4Fzc7F4&feature=youtu.be)

# 2020-04-13, 13일차

## 개념
- 웹 디자이너가 하는 일
  - 디자인 시안 만들기
    - 사용 기술 : 포토샵, 스케치 등
  - 퍼블리싱
    - 사용 기술 : HTML, CSS, JS
- HTML 핵심
  - 태그
    - div 계열(block 계열)
      - EX : section, article, nav, ul, li
    - span 계열(inline 계열)
      - EX : a, img, span
  - 관계
    - 부모자식, 형제
- CSS 핵심
  - display
    - inline, inline-block
    - block
    - none
  - position
    - static
    - relative
    - absolute, fixed
  - float
    - none
    - left
    - right

## 문제
- height:auto 시작
- [개념 - height:auto 의 고마움](https://codepen.io/jangka44/pen/MEdqKR)
- [문제 - height 속성을 사용하여 2개의 엘리먼트를 겹치게 하기](https://codepen.io/jangka44/pen/QmOpEY)
- [문제 - 정답](https://codepen.io/jangka44/pen/jzaBBR)
- [문제 - 최소한의 작업으로 모든 엘리먼트의 높이를 변경해주세요.](https://codepen.io/jangka44/pen/NOWbPy)
- [문제 - 정답](https://codepen.io/jangka44/pen/qJBqdE)
- height:auto 끝
- float 시작
- [개념 - display:inline-block 으로 그리드를 만들기 어려운 이유](https://codepen.io/jangka44/pen/GxOWBX)
- [float 예제 v1 1](https://codepen.io/jangka44/pen/MVoOXY)
- [float 예제 v1 2](https://codepen.io/jangka44/pen/OvgOwa)
- [float 예제 v1 3](https://codepen.io/jangka44/pen/qojVJX)
- [float 예제 v1 4](https://codepen.io/jangka44/pen/BrmpXr)
- [float을 사용하는 이유 : display:inline-block 이 채워주지 못하는 2%를 채워준다.](https://codepen.io/jangka44/pen/eGooJY)
- [개념 - block이 float을 무시한다.](https://codepen.io/jangka44/pen/mjLPYQ)
- [개념 - display:block 인 요소는 float 요소를 자식으로 인정하지 않는다. 즉 없는 자식 취급한다.](https://codepen.io/jangka44/pen/OxYoWZ)
- [개념 - nth-of-type](https://codepen.io/jangka44/pen/oMdxmE)
- [개념 - clear:both 와 nth-of-type(2n+1)](https://codepen.io/jangka44/pen/GMaXzB)
- [개념 - 희생적인 막내를 추가하여 가정(section)을 구원한다.](https://codepen.io/jangka44/pen/jGoeEE)
- [개념 : float 속성정리](https://s.codepen.io/jangka44/debug/bQZeEB)
- [개념 - float으로 만든 간단한 이미지 갤러리](https://codepen.io/jangka44/pen/MWawXrE)
- [float 설명용 소스코드, 시작버전](https://codepen.io/jangka44/pen/yWNOEW)
- [float 설명용 소스코드, 완료버전](https://codepen.io/jangka44/pen/VOLadb)
- [float 예제](https://codepen.io/jangka44/pen/dyPXBqy)
- 종합문제 1. 아래의 요구조건을 만족하는 메뉴를 만들어주세요.
  - [시작소스코드](https://codepen.io/jangka44/pen/xNZoVb)
  - 쇼핑몰의 메뉴를 만들어주세요.
  - 3차 메뉴까지 있습니다.
  - 1차 메뉴 아이템은 홈, 상품리스트, QNA, 회사소개, 소셜 입니다.
  - 사이트의 전체 너비는 1221.320px 입니다.
  - 1차 메뉴의 너비는 사이트 전체 너비에 맞춰야 합니다.
  - 1차 메뉴 아이템들 사이에 약간이라도 여백은 고객이 싫어합니다.
  - 1차 메뉴 아이템의 너비는 모두 같아야 합니다.
  - 메뉴 아이템 텍스트에는 text-align:center가 적용 되어야 합니다.
  - 사이트 전체 너비는 하루에 한번씩 바뀝니다.
  - 메뉴(.menu-1 > ul)에 padding과 border-radius를 빼주세요.
- [정답](https://codepen.io/jangka44/pen/QRyXyB)
- [정답 v2](https://codepen.io/jangka44/pen/YzPGKWa)
- [정답 v3](https://codepen.io/jangka44/pen/yLYNEvo)

## 자바스크립트 개념
- JS 개념
  - 2대 구성요소
    - 변수
    - 값
      - 숫자 : 1, 1.5
      - 문장 : "안녕", "a", "잘가세요."
      - 논리 : true, false
      - 객체 : {이름: "홍길동", 나이: 22}
      - 함수 : function() {}      
  - 2대 제어요소
    - 조건문
      - if, else if, else
    - 반복문
      - while, for
    
## 자바스크립트 문제

### 기초
- [개념 - console.log, 프로그램은 위에서 아래로 실행된다.](https://codepen.io/jangka44/pen/JJERBz)
- [개념 - CSS 보다 우선순위가 높은 자바스크립트, 자바스크립트는 감독입니다.](https://codepen.io/jangka44/pen/awpmER)
- [개념 - 자바스크립트와 CSS가 협력하는 바람직한 방법](https://codepen.io/jangka44/pen/VWPKdo)

### 콘솔
- [문제 - `안녕하세요.`를 10번 출력해주세요.](https://codepen.io/jangka44/pen/PBxLMR)
- [문제 - 정답](https://codepen.io/jangka44/pen/mjQgdp)

### 변수
- [문제 - 변수 a와 b의 값을 서로 교체해주세요.(변수 없이)](https://codepen.io/jangka44/pen/KBrYXP)
- [문제 - 정답](https://codepen.io/jangka44/pen/pZQBWo)
- [문제 - 변수 a와 b의 값을 서로 교체해주세요.(temp 변수 활용)](https://codepen.io/jangka44/pen/MBzROw)
- [문제 - 정답](https://codepen.io/jangka44/pen/MBzROa)

### 조건문
- [문제 - age의 값에 따라서 성년인지 미성년인지 출력해주세요.](https://codepen.io/jangka44/pen/OBzmVw)
- [문제 - 정답](https://codepen.io/jangka44/pen/NOXjqe)
- [문제 - if문 개념문제 - 실행되는 출력문에는 참 그렇지 않으면 거짓 이라고 적어주세요.](https://codepen.io/jangka44/pen/rNamKJP)
- [문제 - 정답](https://codepen.io/jangka44/pen/dyPWKQM)
- [문제 - 할인 대상인지 아닌지 출력(&& 사용)](https://codepen.io/jangka44/pen/zYxwaaQ)
- [문제 - 정답](https://codepen.io/jangka44/pen/wvBdXOy)
- [문제 - 할인 대상인지 아닌지 출력(|| 사용)](https://codepen.io/jangka44/pen/PowmaBj)
- [문제 - 정답](https://codepen.io/jangka44/pen/bGNWKZv)
- [문제 - 할인 대상인지 아닌지 출력(중첩 if문 사용, &&와 ||는 사용 금지)](https://codepen.io/jangka44/pen/BayRVPw)
- [문제 - 정답](https://codepen.io/jangka44/pen/NWPjzJM)
- [문제 - 할인 대상인지 아닌지 출력(if, else if, else 만 사용, &&와 ||는 사용 금지)](https://codepen.io/jangka44/pen/wvBdXxP)
- [문제 - 정답](https://codepen.io/jangka44/pen/jOEmKJv)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/dyPWjPN)

# 2020-04-14, 14일차
## 대전 앨리스 의원, 모작
- [탑바 작업](https://codepen.io/jangka44/live/GRpJBQm)

## 오늘의 동영상
- [동영상 - 2019 12 13, 웹 퍼블리싱, float 1부(height auto가 중요한 이유)](https://www.youtube.com/watch?v=LKFHwKCPqGo&feature=youtu.be)
- [동영상 - 2019 12 13, 웹 퍼블리싱, float 2부(float의 의미와 사용하는 이유)
](https://www.youtube.com/watch?v=19l1TIlTDe8&feature=youtu.be)
- [동영상 - 2019 12 13, 웹 퍼블리싱, float 3부(float의 부작용이 발생하는 원인과 해결방법)](https://www.youtube.com/watch?v=KP07OqZgdHQ&feature=youtu.be)
- [동영상 - 2019 12 09, 프론트, 개념, 브라켓 세팅 및 사용법](https://www.youtube.com/watch?v=LLviMYJ7b-Y&feature=youtu.be)
- [동영상 - 2019 12 13, 프론트, 문제, 1차메뉴 너비 고정, float을 사용한 3차 메뉴](https://www.youtube.com/watch?v=B2svUGZkeLU&feature=youtu.be)
- [동영상 - 2020_04_13, 프론트, 개념, float을 쓴 후 해야하는 작업, 1부](https://youtu.be/qcE0n3mf20Y)
- [동영상 - 2020_04_13, 프론트, 개념, float을 쓴 후 해야하는 작업, 2부](https://youtu.be/QfI33mymSAE)
- [동영상 - 2020_04_13, 프론트, 개념, float을 이용하여 이미지 갤러리 만들기](https://youtu.be/UOkXjh6Oi0w)
- [동영상 - 2020_04_13, 프론트, 개념, height auto](https://youtu.be/uy7DrSUoRD4)
- [동영상 - 2020_04_13, 프론트, 개념, inline-block 으로 정교한 디자인 못하는 이유](https://youtu.be/vgMJIQpRmjc)
- [동영상 - 2020_04_13, 프론트, 문제, display, position, float을 사용하여 3차 풀다운 메뉴 만들기](https://youtu.be/OWH_CImovWo)
- [동영상 - 2020_04_13, 프론트, 문제, height를 이용해서 2개의 엘리먼트 겹치기](https://youtu.be/Ld8c23hD8v4)
- [동영상 - 2020_04_13, 프론트, 문제, 최소한의 작업으로 모든 엘리먼트의 높이 변경](https://youtu.be/e6u_5mk0tgA)
- [동영상 - 2019 12 23, 프론트, 자바스크립트 기초, 변수, 조건문](https://www.youtube.com/watch?v=JE7vmrQ5m44&feature=youtu.be)
- [동영상 - 2020_04_14, 자바스크립트, 개념, console.log, codepen 에서 js 실행](https://www.youtube.com/watch?v=MBeW06gtX1U)
- [동영상 - 2020_04_14, 자바스크립트, 개념, 변수](https://www.youtube.com/watch?v=2F4bMende7I)
- [동영상 - 2020_04_14, 자바스크립트, 개념, 조건문](https://www.youtube.com/watch?v=2i4yjuKLAdE)
- [동영상 - 2020_04_14, 프론트, 개념, 시작소스코드, 1부](https://www.youtube.com/watch?v=_iun0OXl26Q)
- [동영상 - 2020_04_14, 프론트, 개념, 시작소스코드, 2부](https://www.youtube.com/watch?v=ORZcQ-NKOfQ)
- [동영상 - 2020_04_14, 프론트, 개념, 시작소스코드, 3부](https://www.youtube.com/watch?v=rif2e5d24Ks)
- [동영상 - 2020_04_14, 프론트, 개념, con-min-width를 해야하는 이유](https://www.youtube.com/watch?v=Sy7tOmarNVQ)
- [동영상 - 2020_04_14, 앨리스의원, 상단 탑바 만들기, 1차](https://www.youtube.com/watch?v=_2VyfZdStI0)
- [동영상 - 2020_04_14, 앨리스의원, 상단 탑바 만들기, 2차](https://www.youtube.com/watch?v=3pExWFmC3HE)
- [동영상 - 2020_04_14, 앨리스의원, 상단 탑바 만들기, 3차](https://www.youtube.com/watch?v=V-itMXoX8Cc)
- [동영상 - 2020_04_14, 앨리스의원, 상단 탑바 만들기, 4차](https://www.youtube.com/watch?v=XHyelXeBkxQ)

# 2020-04-16, 15일차

## 대전 앨리스 의원, 모작
- [탑바 작업](https://to2.kr/bf9)

# 2020-04-17, 16일차

## 대전 앨리스 의원, 모작
- [중단 배너박스 작업](https://to2.kr/bf9)

## 문제
- [개념 - 테이블](https://codepen.io/jangka44/pen/WNQwgOM)

# 2020-04-21, 17일차

## 대전 앨리스 의원, 모작
- [중단 배너박스 4 작업](https://to2.kr/bf9)

## BNX 사이트 모작
- [상단 로고바](https://to2.kr/bgV)

# 실무도움예제
- [시작소스코드](https://codepen.io/jangka44/pen/zYvvZdr)

# 2020-04-23, 18일차

## 대전 앨리스 의원, 모작
- [중단 배너박스](https://to2.kr/bf9)

## BNX 사이트 모작
- [모바일 사이드바](https://to2.kr/bgV)

# 2020-04-24, 19일차

## 대전 앨리스 의원, 모작
- [중단 배너박스](https://to2.kr/bf9)

# 2020-04-28, 20일차

## 문제
### 제이쿼리
- [문제 - 버튼을 누르면 팝업이 보이고, 팝업의 닫기 버튼을 누르면 팝업이 사라지도록 해주세요.](https://codepen.io/jangka44/pen/NVyppz)
- [문제 - 정답](https://codepen.io/jangka44/pen/QRQMXP)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/Bayjgdw)

## 대전 앨리스 의원, 모작
- [중단 배너박스](https://to2.kr/bf9)

## 오늘의 동영상
- [동영상 - 2019 12 09, 프론트, 문제, 팝업 만들기](https://www.youtube.com/watch?v=gP22qr1kENQ&feature=youtu.be)

# 2020-05-07, 21일차

## 대전 앨리스 의원, 모작
- [팝업](https://to2.kr/bf9)

# 2020-05-08, 22일차

## 대전 앨리스 의원, 모작
- [팝업](https://to2.kr/bf9)

# 기타 페이지
- [19년도 웹국기반(하단에 중요자료 엄청 많음)](https://to2.kr/a6r)
- [20년도 웹국기반](https://to2.kr/bfB)
- [20년도 04월 웹2,웹3](https://to2.kr/bc9)