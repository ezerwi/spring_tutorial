# 페이지 정보
- 주소 : to2.kr/bqq
- 클래스 : 2020년 06월 10일, 웹 수업

# 강사정보
- 이름 : 장희성
- 전화번호 : 010-8824-5558

# 수업관련링크
- [대전 앨리스 의원, 모작](https://to2.kr/bf9)
- [BNX, 모작](https://to2.kr/bgV)
- [최근 학생들 웹/프로그램 포트폴리오](https://to2.kr/bgH)
- [2019년 09월 웹 포폴반 학생들 포트폴리오](https://to2.kr/beR)
- [타이핑 정글](https://www.typingclub.com/sportal/program-3.game)
  - 하루에 3개 클리어
- [브라켓 다운로드](http://brackets.io/)
- [브라켓 에디터 세팅법](https://s.codepen.io/jangka44/debug/bjjPGx)
- [파일질라 다운로드](https://filezilla-project.org/download.php?type=client)
  - Pro가 아닌 일반 버전 다운로드
  - 광고 체크 해제
- [CSS DINER](https://flukeout.github.io/)
- [새 연습장(코드펜)](https://codepen.io/pen/)
  - 주소 : codepen.io/pen
- [code.org 코스 3](https://studio.code.org/s/course3)
- [code.org 코스 4](https://studio.code.org/s/course4)
- [기초 키보드 사용법](https://s.codepen.io/jangka44/debug/OzzVWM)
- [프로그래머스 자바스크립트 입문 강의](https://programmers.co.kr/learn/courses/3)
  - [풀이](https://codepen.io/jangka44/live/mddjOxo)
  - 풀이에 나온 정답을 기준으로 처음부터 끝까지, 힌트없이 문제를 혼자힘으로 푸는데, 30분이 넘으면 안됩니다.
- [플레이코드몽키](https://www.playcodemonkey.com)
  - 유료
  - 한달에 2만원
  - 한달만 하고 결제해지 하시면 됩니다.
- [플레이코드몽키, 스테이지 쉽게 접근하기](https://codepen.io/jangka44/debug/ZEEWRzZ)

# 2020-06-10, 1일차
## 문제
### 일반
- [개념 - HTML, CSS, JS 개요](https://codepen.io/jangka44/pen/vzYVYb)
- [개념 - HTML, CSS, JS 개요 2](https://codepen.io/jangka44/pen/YzXwLJP)
  - 책내용 : 23p
- [개념 - display 속성 정리](https://codepen.io/jangka44/live/ZjbpPR)
  - 책내용 : 238p
- [문제 - div, section, article 태그를 사용해서 3가지 색의 막대를 만들어주세요.](https://codepen.io/jangka44/pen/KBdaGd)
  - 책내용 : 92p
  - 제한시간 : 1분 30초
- [문제 - 정답예시](https://codepen.io/jangka44/debug/VBvPeM)
- [문제 - 정답](https://codepen.io/jangka44/pen/VBvPeM)
- [문제 - section(green 색 막대)과 article(blue 색 막대)을 한 줄에 보이게 해주세요.](https://codepen.io/jangka44/pen/qyOqrE)
  - 책내용 : 238p
  - 제한시간 : 1분 30초
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/WKQRGW)
- [문제 - 정답](https://codepen.io/jangka44/pen/WKQRGW)
- [개념 - 엘리먼트(배우)의 부모, 자식, 형제관계](https://codepen.io/jangka44/pen/bQqbpZ)
  - 책내용 : 274p
- [개념 - 글자](https://codepen.io/jangka44/pen/mjPYqy)
  - 책내용 : 92p
- [문제 - 다음 글자를 똑같은 스타일로 만들어주세요.](https://codepen.io/jangka44/debug/mjPYqy)
- [문제 - 정답](https://codepen.io/jangka44/pen/mjPYqy)
- [개념 - a와 br](https://codepen.io/jangka44/pen/zLqQam)
  - 책내용 : 84p

## 동영상
### 일반
- [동영상 - 2020 02 14, 프론트, 개념,CODEPN 세팅법](https://www.youtube.com/watch?v=4V6jglwjcvg&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS의 역할](https://www.youtube.com/watch?v=6nFdDSWj9JU)
- [동영상 - 2020 02 14, 프론트, 개념, HTML, CSS, JS을 공부하는 효율적인 방법](https://www.youtube.com/watch?v=A_vHA7ALWRk&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 개념, display 속성](https://www.youtube.com/watch?v=OjLElVktdos&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 문제, 막대 3개 구현, 밑에 2개는 한 줄에](https://www.youtube.com/watch?v=ltAum2lXzNo&feature=youtu.be)
- [동영상 - 2020 02 14, 프론트, 문제, 막대 3개 구현](https://www.youtube.com/watch?v=U2F6Q2hfWKA&feature=youtu.be)
- [동영상 - 2020 02 17, 프론트, 개념, display, 엘리먼트, 선택자](https://www.youtube.com/watch?v=JGQKuf9uS3E)
- [동영상 - 2020 02 17, 프론트, 개념, 엘리먼트 관계, 부모자식 관계, 형제관계](https://www.youtube.com/watch?v=yE0EyLlnBdE)
- [동영상 - 2020 02 17, 프론트, 개념, font weight, letter spacing, font size](https://www.youtube.com/watch?v=yfgMvBiPzYc)

# 2020-06-12, 2일차
## 문제
### 일반
- [문제 - 각각의 사이트 명에 링크를 걸어주세요.(새창으로 떠야 합니다.)](https://codepen.io/jangka44/pen/ZEEMPpy)
  - 책내용 : 84p
  - 제한시간 : 1분 30초
- [문제 - 정답예시](https://codepen.io/jangka44/debug/mddGorY)
- [문제 - 정답](https://codepen.io/jangka44/pen/mddGorY)
- [문제 - 젠코딩으로 네이버, 구글, 다음으로 가는 링크를 만들어주세요.](https://codepen.io/jangka44/pen/pBWYyR)
  - 책내용 : 84p
- [문제 - 정답](https://codepen.io/jangka44/pen/pBWYyR)
- [개념 - 후손 선택자](https://codepen.io/jangka44/pen/KBzjzB)
  - 책내용 : 274p
- [문제 - 각각의 크기와 색상이 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/JaoBNQ)
  - 책내용 : 274p
- [문제 - 정답예시](https://codepen.io/jangka44/debug/yxyqQQ)
- [문제 - 정답](https://codepen.io/jangka44/pen/yxyqQQ)
- [개념 - text-align](https://codepen.io/jangka44/pen/JBXgNy)
  - 책내용 : 92p
- [개념 - text-align v2](https://codepen.io/jangka44/pen/JaLwpr)
  - 책내용 : 92p
- [문제 - 각각의 크기와 색상과 정렬상태가 다른 링크 버튼 3개 만들어주세요.](https://codepen.io/jangka44/pen/XPJByw)
  - 책내용 : 92p
- [문제 - 정답예시](https://codepen.io/jangka44/debug/rZarQb)
- [문제 - 정답](https://codepen.io/jangka44/pen/rZarQb)
- [개념 - hover](https://codepen.io/jangka44/pen/KBzOKj)
  - 책내용 : 281p
- [문제 - 100칸 짜리 바둑판을 만들고 하나의 칸에 마우스를 올리면 해당 칸의 배경색이 바뀌도록 해주세요.](https://codepen.io/jangka44/pen/VGYGvp)
  - 책내용 : 281p
- [문제 - 예시정답](https://codepen.io/jangka44/debug/QVwVWZ)
- [문제 - 정답](https://codepen.io/jangka44/pen/QVwVWZ)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/vYOKqNg)
- [개념 - short code(자식, 인접동생)](https://codepen.io/jangka44/pen/rreXjL)
  - 책내용 : 274p, 280p
- [개념 - `nbsp;`](https://codepen.io/jangka44/pen/yqOmxJ)
  - 책내용 : 59p
- [개념 - text-deocration:none](https://codepen.io/jangka44/pen/eLNreO)
- [문제 - div 태그만 사용하여 사과의 색상을 red로 변경해주세요.](https://codepen.io/jangka44/pen/jOWqvPR)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/XWXdPXX)
- [문제 - 정답](https://codepen.io/jangka44/pen/XWXdPXX)
- [문제 - bnx 사이트의 상단 메뉴까지 구현해주세요.](https://codepen.io/jangka44/pen/oMxKZp)
- [문제 - 정답예시 v1](https://codepen.io/jangka44/debug/wQdWEG)
- [문제 - 정답 v1](https://codepen.io/jangka44/pen/wQdWEG)
- [개념 - margin, padding](https://codepen.io/jangka44/pen/PBGgQa)
  - 책내용 : 98p
- [개념 - margin, padding v2](https://codepen.io/jangka44/pen/rZvRmQ)
  - 책내용 : 98p
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/Rwbrzgx)
  - 책내용 : 76p
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/pozgXaz)

## 오늘의 동영상
### 일반
- [동영상 - 2020 02 17, 프론트, 개념, 앵커태그, br 태그](https://www.youtube.com/watch?v=tsb7ssDcd9k)
- [동영상 - 2020 02 17, 프론트, 개념, 젠코딩 emmet](https://www.youtube.com/watch?v=rRapBHkt6gU)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, 후손 선택자](https://www.youtube.com/watch?v=oVuSYCYjtaU)
- [동영상 - 2020 02 17, 프론트, 문제, 색상과 크기가 다른 3가지 링크를 만들어주세요](https://www.youtube.com/watch?v=AcIq02YTOdY)
- [동영상 - 2020 02 17, 프론트, 개념, CSS, text align](https://www.youtube.com/watch?v=d2JjVUMkOoI)
- [동영상 - 2020 05 14, 프론트, 문제, 젠코딩으로 네이버, 구글, 다음으로 가는 링크를 만들어주세요](https://youtu.be/uhkfnFYd-8Y)
- [동영상 - 2020 05 14, 프론트, 개념, 후손선택자](https://youtu.be/EwW9XWqFDkw)
- [동영상 - 2020 05 14, 프론트, 문제, 각각의 크기와 색상이 다른 링크 버튼 3개 만들어주세요](https://youtu.be/ePCg9DBpoeQ)
- [동영상 - 2020 05 14, 프론트, 개념, text-align](https://youtu.be/elypw0w9SsU)
- [동영상 - 2020 05 14, 프론트, 문제, 각각의 크기와 색상과 정렬상태가 다른 링크 버튼 3개 만들어주세요](https://youtu.be/iP3jqv5vs1Y)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, hover](https://www.youtube.com/watch?v=N7BlAxCMFS4&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, CSS, 100칸짜리 바둑판형태, hover](https://www.youtube.com/watch?v=tgj8dKLestk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, `nbsp;`](https://www.youtube.com/watch?v=5LeDdCf4Cso&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, text decoration](https://www.youtube.com/watch?v=JZtjOcZkIGg&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 선택자, 자식, 후손, text align, display](https://www.youtube.com/watch?v=cCaWklKPsLs&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 개념, CSS, 자식선택자, 인접동생선택자](https://www.youtube.com/watch?v=kRU98SwRrnQ&feature=youtu.be)
- [동영상 - 2020 06 12, 프론트, 문제, div 태그만 사용하여 사과의 색상을 red로 변경해주세요](https://youtu.be/bqVgPspw73E)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 1차](https://www.youtube.com/watch?v=SJEhyNfSsD4&feature=youtu.be)
- [동영상 - 2020_05_19, 프론트, 개념, short code, 자식, 인접동생](https://youtu.be/gRdXL18le-s)
- [동영상 - 2020_05_19, 프론트, 개념, nbsp, 빈칸](https://youtu.be/YarVgrbCppQ)
- [동영상 - 2020_05_19, 프론트, 개념, text-decoration](https://youtu.be/9hfaqbXPPlo)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v1](https://youtu.be/ZWxQETnqsoE)

# 2020-06-15, 3일차
## 문제
### 일반
- [문제 - 정답예시 v2](https://codepen.io/jangka44/debug/ejZqvz)
- [문제 - 정답 v2](https://codepen.io/jangka44/pen/ejZqvz)
- [문제 - 정답예시 v3](https://codepen.io/jangka44/debug/zLBYrQ)
- [문제 - 정답 v3](https://codepen.io/jangka44/pen/zLBYrQ)
- [문제 - 정답예시 v4](https://codepen.io/jangka44/debug/PxjWxY)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/PxjWxY)
- [문제 - 정답예시 v5](https://codepen.io/jangka44/debug/qMdKbP)
- [문제 - 정답 v5](https://codepen.io/jangka44/pen/qMdKbP)
- [개념 - 이미지](https://codepen.io/jangka44/pen/pZEYmx)
  - 책내용 : 76p
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/MWwJJYr)
- [문제 - 힌트](https://codepen.io/jangka44/pen/WNrGbQg)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/KKpaNYm)
  - 책내용 : 76p
- [문제 - 정답](https://codepen.io/jangka44/pen/KKpaNYm)
- [개념 - margin, padding](https://codepen.io/jangka44/pen/PBGgQa)
  - 책내용 : 98p
- [개념 - margin, padding v2](https://codepen.io/jangka44/pen/rZvRmQ)
  - 책내용 : 98p
- [문제 - 이미지 6개를 3x2 가운데 정렬로 배열해주세요.](https://codepen.io/jangka44/pen/Rwbrzgx)
  - 책내용 : 76p
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/pozgXaz)
- [문제 - 정답](https://codepen.io/jangka44/pen/pozgXaz)
- [문제 - 정답 v2(10분 완성)](https://codepen.io/jangka44/pen/QWbddOa)
- [개념 - nth-child](https://codepen.io/jangka44/pen/xJEeag)
  - 책내용 : 282p
- [개념 - nth-child v2](https://codepen.io/jangka44/pen/wEXOJo)
  - 책내용 : 282p
- [문제 - 무지개를 만들어주세요.](https://codepen.io/jangka44/pen/OJLKjBX)
  - 책내용 : 282p
- [문제 - 정답예시](https://codepen.io/jangka44/debug/aboeyRQ)
- [문제 - 정답](https://codepen.io/jangka44/pen/aboeyRQ)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다.](https://codepen.io/jangka44/pen/xJEoPe)
- [개념 - display:inline 에는 width, height, padding, margin이 제대로 작동하지 않는다. v2](https://codepen.io/jangka44/pen/ZMRPMM)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법](https://codepen.io/jangka44/pen/zJNKwN)
- [개념 : block 요소 좌측,가운데,우측 정렬하는 방법 v2](https://codepen.io/jangka44/pen/EeGyKK)
- [개념 : 모든 보이는 엘리먼트의 부모는 body이고, 그의 부모는 html 이다.](https://codepen.io/jangka44/pen/RwWvRMe)

## 오늘의 동영상
### 일반
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 2차](https://www.youtube.com/watch?v=egc7Njqkddk&feature=youtu.be)
- [동영상 - 2020 02 19, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 3차](https://www.youtube.com/watch?v=KlwqR4rNv0g&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 4차](https://www.youtube.com/watch?v=Kin-u_fjiNI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, bnx 사이트의 상단 메뉴까지 구현해주세요, 5차](https://www.youtube.com/watch?v=kD0uXVLGy14&feature=youtu.be)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v2, 힌트](https://youtu.be/YkwIcWLf_X8)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v2, v3](https://youtu.be/uU09WXWHavs)
- [동영상 - 2020_05_19, 프론트, 문제, bnx 사이트의 상단 메뉴를 구현해주세요, v4, v5](https://youtu.be/9wFQFGWepnM)
- [동영상 - 2020_05_12, 프론트, 개념, 코드팬을 티스토리에 포스팅하기](https://youtu.be/kdS1wHq1irY)
- [동영상 - 2020 02 24, 프론트, 개념, CSS, margin, padding](https://www.youtube.com/watch?v=pDT-JgMIL-s&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 개념, img 엘리먼트](https://www.youtube.com/watch?v=cBF78x1SArI&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS](https://www.youtube.com/watch?v=8-APhZJ12i8&feature=youtu.be)
- [동영상 - 2020 02 24, 프론트, 문제, 이미지 6개를 배치, CSS, only padding](https://www.youtube.com/watch?v=83GwjeVlxtE)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, nth-child, first-child, last-child](https://youtu.be/8udV6lGRgnI)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요.](https://youtu.be/xA51J05sRaU)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, inline 요소에는 width, height, padding, margin이 작동하지 않는다.](https://youtu.be/hLxOhBJzzl0)
- [동영상 - 2020 05 21, 프론트, 개념, img 엘리먼트](https://youtu.be/M-wu6_tve1Q)
- [동영상 - 2020 05 21, 프론트, 개념, CSS, nth-child, nth-last-child, first-child, last-child](https://youtu.be/KxJ37cQokAg)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, nth-child와 7n + x를 사용해서 무지개를 만들어주세요.](https://youtu.be/r0jKqPgFGLw)
- [동영상 - 2020 02 26, 프론트, 개념, CSS, block 요소 가운데 정렬하는 방법, margin:0 auto;](https://youtu.be/vKWVeRPmhKg)

# 2020-06-17, 4일차
## 개념
### 일반
- HTML
  - 관계
    - 부모/자식
    - 형/동생
  - 태그
    - 인라인 계열
      - span(div에서 display만 inline인 태그, 인라인 계열의 기본 태그)
      - a : 링크
      - img : 이미지
    - 블록 계열
      - 기본
        - div(구분, 적절한 태그가 생각나지 않을 때, 모르면 div, 블록 계열의 기본태그)
        - nav(내비게이션, 보통 메뉴 감쌀 때)
        - section(섹션)
        - article(아티클, 게시물)
      - 제목
        - h1, h2, h3, h4, h5, h6
      - 목록
        - ul, li : 순서 없는 목록
        - ol, li : 순서 있는 목록
- CSS
  - 노말라이즈
    - 해당 엘리먼트에 기본적으로 적용되어 있는 디자인을 없애서 다시 평범하게 만든다.
    - a, body, ul, li, ol, li, h1, h2, h3, h4, h5, h6 은 사용하기전에 노말라이즈 해야 한다.
  - 선택자
    - 태그선택자 : `div { ... }`
    - 자식선택자 : `div > a { ... }`
    - 후손선태자 : `div a { ... }`
    - 클래스선택자 : `.menu-item { ... }`
  - 기본 속성
    - width, height, font-size, font-weight, letter-spacing, color, background-color, margin-top, margin-left, padding-top, padding-left, border, border-radius
  - 레이아웃 속성
    - display
      - none : 사라짐
      - inline-block, inline : 글자화
      - block : 블록화
    - float : ??
    - position : ???

## 문제
### 일반
- [문제 - bnx 사이트의 상단 이미지 4개 까지만 구현해주세요.](https://codepen.io/jangka44/pen/EpgJPa)
- [문제 - 정답예시 v1, 조건 : br과 nbsp 사용](https://codepen.io/jangka44/debug/KBgYzb)
- [문제 - 정답예시 v2, 조건 : margin 사용](https://codepen.io/jangka44/debug/vaXMyZ)
- [문제 - 정답예시 v3, 조건 : 엘리먼트 5개 사용, 힌트 : body](https://codepen.io/jangka44/debug/rZZqRW)
- [문제 - 정답예시 v4, 조건 : 엘리먼트 4개 사용, 힌트 : body, block](https://codepen.io/jangka44/debug/WggYOr)
- [문제 - 정답 v1, br과 nbsp](https://codepen.io/jangka44/pen/KBgYzb)
- [문제 - 정답 v2, margin](https://codepen.io/jangka44/pen/vaXMyZ)
- [문제 - 정답 v3, 엘리먼트 5개 이하, body](https://codepen.io/jangka44/pen/rZZqRW)
- [문제 - 정답 v4](https://codepen.io/jangka44/pen/WggYOr)
- [개념 - border-radius](https://codepen.io/jangka44/pen/bjwyMd)
  - 책내용 : 177p
- [개념 - border-radius v2](https://codepen.io/jangka44/pen/KxeEqX)
  - 책내용 : 177p
- [개념 - border-radius v3](https://codepen.io/jangka44/pen/VGdRMZ)
  - 책내용 : 177p
- [개념 - border-radius v4(5분완성)](https://codepen.io/jangka44/pen/XyeREQ)
  - 책내용 : 177p
- [개념 - inherit](https://codepen.io/jangka44/pen/JBRgyK)
- [개념 - inherit v2](https://codepen.io/jangka44/pen/aaKMxg)
- [개념 - a 노말라이즈](https://codepen.io/jangka44/pen/BPLXrO)
- [문제 - 메뉴를 구현해주세요.(1차 메뉴까지만)](https://codepen.io/jangka44/pen/zLKQRP)
- [문제 - 힌트](https://codepen.io/jangka44/pen/RwwbqJQ)
- [문제 - 정답 예시](https://codepen.io/jangka44/debug/EpgqKa)

## 오늘의 동영상
### 일반
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, br, &nbsp;, margin 등을 사용](https://youtu.be/SbvwOTp1PaY)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 5개만 사용](https://youtu.be/iOJnado5Gds)
- [동영상 - 2020 02 26, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/OtJvZo2ls5I)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 5개만 사용](https://youtu.be/ZCT4IDQbEW4)
- [동영상 - 2020 05 21, 프론트, 문제, CSS, 이미지 4개 배치, 오직 엘리먼트 4개만 사용](https://youtu.be/39MMNmsr9zs)
