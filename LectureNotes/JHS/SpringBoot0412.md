# 페이지 정보
- 주소 : https://to2.kr/bf6

## 관련링크
### 일반
- [부트스트랩 기본](https://to2.kr/bn8)

### 포트폴리오
- [블로그 작성](https://codepen.io/jangka44/live/PoPPPej)
- [뉴렉처 2020 서블릿/JSP](https://www.youtube.com/playlist?list=PLq8wAnVUcTFVOtENMsujSgtv2TOsMy8zd)
- [뉴렉처 2020 서블릿/JSP의 공부기록, 유튜브](https://www.youtube.com/playlist?list=PLDtTzRqvUA4hsVli4rFDyJuaEOB6xnyVQ)
- [뉴렉처 2020 서블릿/JSP의 공부기록, 블로그](https://jangka44.tistory.com/category/뉴렉처%20강의%20공부,%202020%20서블릿,%20JSP)
- [유튜브 계정인증(대용량 업로드를 하려면 필요)](https://www.youtube.com/verify)

### 스프링
- [스프링 부트 설치 메뉴얼](https://codepen.io/jangka44/debug/rNaORbO)
- [실전용 pom.xml, application.yml](https://gist.github.com/jhs512/167101701fdb3c07fb65f4405b0c82a0)

## 명령어
### 최초에 가져오기(꼭 워크스페이스 폴더에서 실행)
- git clone https://github.com/jhs512/cuni.git cuni

### repository 내 모든 수정 되돌리기
- git checkout .

### 내 소스코드를 강사 리포지터리의 최신 버전으로 만들기
- git checkout .
- git pull origin master

### 특정 폴더 아래의 모든 수정 되돌리기
- git checkout {dir}

### 특정 파일의 수정 되돌리기
- git checkout {file_name}

### git add 명령으로 stage에 올린 경우 되돌리기
- git reset

### 과거로 돌아가기
- git checkout {커밋번호}

### 현재로 돌아오기
- git checkout .
- git checkout master
- git checkout .

## 작업1, GITHUB 세팅, 게시물 리스트, 공통 jsp
- [커밋](https://github.com/jhs512/cuni/commit/776fcba6aef8ead9a37a6622cbca23039cefc158)
- [동영상 - 2020 04 12, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 리스팅](https://www.youtube.com/watch?v=Q9d_PRjy6Zk)
- [동영상 - 2020 04 12, 스프링부트 실전, 커뮤니티 사이트, 개념, git clone 한 프로젝트를 STS에서 열기](https://www.youtube.com/watch?v=ADZPB1MAtzU&feature=youtu.be)

## 작업2, 게시물 상세페이지 기능
- [커밋](https://github.com/jhs512/cuni/commit/4c74a24758230c548ed96cbf7756cdc0066b41be)
- [동영상 - 2020 04 12, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 상세페이지 구현](https://www.youtube.com/watch?v=hjUqIqcnoas)

## 작업3, 기초 css 작업
- [커밋](https://github.com/jhs512/cuni/commit/64753c680f50709874694e90bc08f97b14cb909b)
- [동영상 - 2020 04 12, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 리스트/상세페이지 꾸미기](https://www.youtube.com/watch?v=HbH_VS-ShQQ)
- [동영상 - 2020 04 12, 스프링부트 실전, 커뮤니티 사이트, 개념, git으로 시간여행하기](https://www.youtube.com/watch?v=iveKGjyYw40)

## 작업4, 게시물 삭제기능
- [커밋](https://github.com/jhs512/cuni/commit/8c180b7f4b503febe3898b23b58fd24f7ef3e3bd)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 삭제, MySQL 사용자 추가](https://www.youtube.com/watch?v=DvWer5ERcEM)

## 작업5, 게시판 추가, 게시물 리스팅 분류
- [커밋](https://github.com/jhs512/cuni/commit/ff3f6a620fd6fe9230ef486cd6dd1f2a9d9b2275)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시판 추가, 1부, 소리 없음](https://www.youtube.com/watch?v=wq5Y8pwr-QY&feature=youtu.be)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시판 추가, 2부, 소리 없음](https://www.youtube.com/watch?v=G9h-NfoUEok&feature=youtu.be)

## 작업6, 게시물 리스트의 페이지 명에 게시판 이름
- [커밋](https://github.com/jhs512/cuni/commit/047cf85f073cea6ee90a4f62e998a948a26903f4)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시판 이름이 리스트에 나오게 처리, 소리 없음](https://www.youtube.com/watch?v=7CzmBpEFMLw&feature=youtu.be)

## 개념
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 개념, 튜토리얼 공부법, 소리 없음](https://www.youtube.com/watch?v=ZJ9Nhk9LufI&feature=youtu.be)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 개념, 튜토리얼 공부법, 소리 있음](https://www.youtube.com/watch?v=b5wmIc-UKTg)

## 작업7, 게시글 추가
- [커밋](https://github.com/jhs512/cuni/commit/d1aacbdb91c301891f2671500c68a814a65086c9)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시글 추가, 1부, 소리 없음](https://www.youtube.com/watch?v=nEk1oUnQNEk&feature=youtu.be)
- [동영상 - 2020 04 18, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시글 추가, 2부, 소리 없음](https://www.youtube.com/watch?v=j-3m7pGyzCo&feature=youtu.be)

## 작업8, 게시글 수정
- [커밋](https://github.com/jhs512/cuni/commit/54732a46245ca9d9dfd8276449f05ef30618b015)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시글 수정, 1부](https://www.youtube.com/watch?v=Yyj9a240L9I)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시글 수정, 2부](https://www.youtube.com/watch?v=8HcxyyQkBYY)

## 작업9, 회원가입, 회원로그인 폼
- [커밋](https://github.com/jhs512/cuni/commit/a303ac37f14e322dfaca744927654b6f45e99e1b)
- [커밋 2](https://github.com/jhs512/cuni/commit/0f24ecfa32f4f2a6c525e2bcf7e1465fc29b31ff)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 작업, 회원가입, 회원로그인 폼](https://www.youtube.com/watch?v=kK5YHyyHeXk&feature=youtu.be)

## 작업10, 로그인, 로그아웃
- [커밋](https://github.com/jhs512/cuni/commit/0b5100de984cdc7de0a7537a175ad1634c3c8129)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그인, 로그아웃](https://www.youtube.com/watch?v=Pb5AFq1GC70)
- [동영상 - 2020 04 19, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그인, 로그아웃, 2부](https://www.youtube.com/watch?v=eTBITFuXzFQ&feature=youtu.be)

## 작업11, 게시물 수정 ,삭제에 대한 권한체크
- [커밋](https://github.com/jhs512/cuni/commit/b32cac8100150e2fa952127e9ab2ce6d75c2ca99)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 수정 ,삭제에 대한 로그인 체크](https://www.youtube.com/watch?v=UfNNJY17odk)

## 작업12, 게시물 수정, 삭제에 대한 권한체크를 인터셉터로 처리
- [커밋](https://github.com/jhs512/cuni/commit/9807e17eaa6a7e772b9503921bab0f4b158f4df6)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 인터셉터 도입](https://www.youtube.com/watch?v=GZUW7WBsVM4&feature=youtu.be)

## 작업13, 게시물 작성 시 회원정보 저장
- [커밋](https://github.com/jhs512/cuni/commit/9b24425f6b04f8b2fdebcfe608fa4904809a76b2)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 작성 시 회원정보 저장, 1부](https://www.youtube.com/watch?v=eJZjTB8UvDw)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 작성 시 회원정보 저장, 2부](https://www.youtube.com/watch?v=5_AhixDGIig)

## 작업14, 게시물 수정, 삭제에 대한 권한체크에 소유권 체크 추가
- [커밋](https://github.com/jhs512/cuni/commit/df0ed527daf42628b93adb41ae89c3e8c9402d1c)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 수정, 삭제에 대한 권한체크에 소유권 체크 추가, 1부](https://www.youtube.com/watch?v=TCMzU5DhzFQ)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 수정, 삭제에 대한 권한체크에 소유권 체크 추가, 2부](https://www.youtube.com/watch?v=QfnXSvPq0lc)

## 작업15, 게시물 조회수 기능 추가
- [커밋](https://github.com/jhs512/cuni/commit/5266a11ec358a6a1c5b6715b10bd8042ee0840cd)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 조회수 기능 추가](https://youtu.be/eFdtKwjJMyA)

## 작업16, 게시물 리스트에서 작성자 이름 보이게 하기
- [커밋](https://github.com/jhs512/cuni/commit/1a08f427b1576d0abe50112b3a4e200d0700af6b)
- [동영상 - 2020 04 25, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 리스트에서 작성자 이름 보이게 하기](https://youtu.be/axCb7d595tQ)

## 작업17, 게시물 정보 가져올 때, 필요할 때만, 회원정보 같이 가져오기
- [커밋](https://github.com/jhs512/cuni/commit/ecba12c8a210952d39e2119baa88c88673646cad)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 정보 가져올 때, 필요할 때만, 회원정보 같이 가져오기, 1부](https://youtu.be/XowTVCGqqQs)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 정보 가져올 때, 필요할 때만, 회원정보 같이 가져오기, 2부](https://youtu.be/83CKYanBJ_8)

## 작업18, 좋아요 기능
- [커밋](https://github.com/jhs512/cuni/commit/a54632c762aaf1c346717d275baeb51968aadc48)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, 좋아요 기능](https://www.youtube.com/watch?v=iFkDjo14zqs)

## 작업19, 좋아요 취소 기능, 좋아요 관련 버튼 노출 최적화
- [커밋](https://github.com/jhs512/cuni/commit/86ecfdbc18d87cc70561513b2170cf1b06283923)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, 좋아요 취소 기능, 좋아요 관련 버튼 노출 최적화, 1부](https://www.youtube.com/watch?v=ln3MBl9eEgs&feature=youtu.be)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, 좋아요 취소 기능, 좋아요 관련 버튼 노출 최적화, 2부](https://www.youtube.com/watch?v=ofhJr5qEXu4&feature=youtu.be)

## 작업20, ajax 예제
- [커밋](https://github.com/jhs512/cuni/commit/010604145bfc0fc7c90466d2bc136794dfe1b0fd)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, ajax 예제, 1부](https://youtu.be/FEngay-fcqY)
- [동영상 - 2020 05 02, 스프링부트 실전, 커뮤니티 사이트, 작업, ajax 예제, 2부](https://www.youtube.com/watch?v=xK-qAOQQQCM)

## 작업21, 게시물 상세페이지의 추천기능 ajax화
- [커밋](https://github.com/jhs512/cuni/commit/17a83e81f1162b5d89d74926149ba26b6c8d8949)
- [동영상 - 2020 05 09, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 상세페이지의 추천기능 ajax화, 힌트 1](https://youtu.be/MBzmpuFaPo0)
- [동영상 - 2020 05 09, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 상세페이지의 추천기능 ajax화, 힌트 2](https://youtu.be/qRFwNMTMhpM)
- [동영상 - 2020 05 09, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 상세페이지의 추천기능 ajax화](https://youtu.be/-UL-fOsI0a8)

## 작업22, 게시물 상세페이지의 추천취소기능 ajax화
- [커밋](https://github.com/jhs512/cuni/commit/d311d05e703852534b16540b3ea9ff409ef05f6a)
- [동영상 - 2020 05 10, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 상세페이지의 추천취소기능 ajax화](https://www.youtube.com/watch?v=gjHf23CQ64A&feature=youtu.be)

## 작업23, 로그아웃 상태에서, 로그인이 필요한 ajax 요청에 대한 예외처리
- [커밋](https://github.com/jhs512/cuni/commit/2805923c884062b471d7b3d147cd811ab4a8d7be)
- [동영상 - 2020 05 10, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그아웃 상태에서, 로그인이 필요한 ajax 요청에 대한 예외처리, 1부](https://www.youtube.com/watch?v=xhVA0hWSpjw&feature=youtu.be)
- [동영상 - 2020 05 10, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그아웃 상태에서, 로그인이 필요한 ajax 요청에 대한 예외처리, 2부](https://www.youtube.com/watch?v=gCvJOtTu7KI&feature=youtu.be)
- [동영상 - 2020 05 10, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그아웃 상태에서, 로그인이 필요한 ajax 요청에 대한 예외처리, 3부](https://youtu.be/OjEUirTKPVw)

## 작업24, BeforeAction 인터셉터에서 requesturiQueryString 등 유용한 정보를 만들어서 Request 객체에 첨부
- [커밋](https://github.com/jhs512/cuni/commit/d38d5d105ff07cd4409a69ba77238472c62a4520)
- [동영상 - 2020 05 10, 스프링부트 실전, 커뮤니티 사이트, 작업, BeforeAction 인터셉터에서 requesturiQueryString 등 유용한 정보를 만들어서 Request 객체에 첨부](https://www.youtube.com/watch?v=WQElu4B2eOc&feature=youtu.be)

## 작업25, 댓글 작성기능
- [커밋](https://github.com/jhs512/cuni/commit/a4f9fbb1388998156ab9f03e38b56baa35fea9f8)
- [동영상 - 2020 05 16, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 작성기능](https://youtu.be/7U3EEEhPssM)

## 작업26, 댓글 리스팅
- [커밋](https://github.com/jhs512/cuni/commit/83ad8e30ae785f83ad6603d6fce430c7fb561630)
- [동영상 - 2020 05 16, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 리스팅](https://youtu.be/2VhQtQqsSY4)

## 작업27, 댓글 삭제
- [커밋](https://github.com/jhs512/cuni/commit/079f3130993f2314e3de16ddd81b948cd0a13725)
- [동영상 - 2020 05 16, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 삭제](https://youtu.be/Tc0eanjxER8)

### 팁
- [동영상 - 2020 05 17, 스프링부트 실전, 커뮤니티 사이트, 개념, 메이븐 의존성 다운로드가 느릴때 대처법](https://youtu.be/BbmRZqGqiXI)

## 작업28, 댓글 수정
- [커밋](https://github.com/jhs512/cuni/commit/a1a122b9073302d4886dba666f7fcbc4e63cad2f)
- [동영상 - 2020 05 17, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 수정](https://youtu.be/7CwBbkx3pmI)

### 팁
- [제이쿼리 : addClass, removeClass, parent, closest](https://codepen.io/jangka44/pen/YbYmyp)
- [댓글 jquery, 작업 1](https://codepen.io/jangka44/pen/rgpbox)
- [댓글 jquery, 작업 2](https://codepen.io/jangka44/pen/xNpeoq)
- [댓글 jquery, 작업 3](https://codepen.io/jangka44/pen/gJoJaQ)
- [댓글 jquery, 작업 4](https://codepen.io/jangka44/pen/Rmxmpq)
- [댓글 jquery, 작업 다시시작, 1](https://codepen.io/jangka44/pen/OJPeYop)
- [댓글 jquery, 작업 다시시작, 2](https://codepen.io/jangka44/pen/bGNPyZo)
- [댓글 jquery, 작업 다시시작, 3](https://codepen.io/jangka44/pen/povXXvx)
- [댓글 jquery, 작업 다시시작, 4](https://codepen.io/jangka44/pen/VwYJJjK)
- [댓글 jquery, 작업 다시시작, 5](https://codepen.io/jangka44/pen/XWJLLeW)
- [댓글 jquery, 작업 다시시작, 6](https://codepen.io/jangka44/pen/WNbVxmP)
- [댓글 jquery, 작업 다시시작, 7](https://codepen.io/jangka44/pen/gObVwYj)
  - [실험](https://codepen.io/jangka44/pen/JjogRoN)
- [댓글 jquery, 작업 다시시작, 8](https://codepen.io/jangka44/pen/mdyNrPj)

### 팁 동영상
- [2020 02 01, 스프링, 프론트, 댓글 작업 1](https://www.youtube.com/watch?v=kkZbTiCby7w&feature=youtu.be)
- [2020 02 01, 스프링, 프론트, 댓글 작업 2](https://www.youtube.com/watch?v=DY0TUHCjRT8&feature=youtu.be)
- [2020 02 01, 스프링, 프론트, 댓글 작업 3](https://www.youtube.com/watch?v=uqXuyAytOAc&feature=youtu.be)
- [2020 02 01, 스프링, 프론트, 댓글 작업 4](https://www.youtube.com/watch?v=vLpr7eSWY0s&feature=youtu.be)
- [2020 02 01, 스프링, 프론트, 댓글 작업 5](https://www.youtube.com/watch?v=HDQByIfYFpU&feature=youtu.be)
- [2020 02 02, 스프링, 프론트, 댓글 작업 6](https://www.youtube.com/watch?v=mYZFqh4H_MM&feature=youtu.be)
- [2020 02 02, 스프링, 프론트, 댓글 작업 7](https://www.youtube.com/watch?v=fYzook0g-Jk)
- [2020 02 02, 스프링, 프론트, 댓글 작업 8](https://www.youtube.com/watch?v=Q55vdpeMhus&feature=youtu.be)

## 작업29, 댓글 작성 ajax 화
- [커밋](https://github.com/jhs512/cuni/commit/d559f3dcf33cb49beed77a26613307c6b980c6f2)
- [동영상 - 2020 05 22, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 작성 ajax 화](https://youtu.be/MSyulawQ8s0)

## 작업30, 댓글 리스팅 ajax화, 1부
- [커밋](https://github.com/jhs512/cuni/commit/387966462bfefded7d569ca42f79b7a707ff2657)
- [동영상 - 2020 05 23, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 리스팅 ajax 화, 1부](https://youtu.be/aFWYx5wFkmI)

## 작업30, 댓글 리스팅 ajax화, 2부
- [커밋](https://github.com/jhs512/cuni/commit/b4da4405a93f7c64c0e4bfd889fe2813dcb85f32)
- [동영상 - 2020 05 23, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 리스팅 ajax 화, 2부-1](https://youtu.be/WE4C_aBMcPI)
- [동영상 - 2020 05 23, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 리스팅 ajax 화, 2부-2](https://youtu.be/n5jzrhMFcDg)

## 작업31, 댓글 리스팅 ajax화, 3부, 댓글 리스트 아이템 템플릿 화, 리스팅 로직 합리적
- [커밋](https://github.com/jhs512/cuni/commit/8be76392a97d16168b1de6ec4a882463fea90f9c)
- [동영상 - 2020 05 24, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 리스팅 ajax 화, 3부](https://youtu.be/oYExS0h-0sI)

## 작업32, 댓글 삭제 ajax화
- [커밋](https://github.com/jhs512/cuni/commit/0b6e9177e7d6286aa1b47cb59f1ee12d3d4d7192)
- [동영상 - 2020 05 24, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 삭제 ajax화](https://youtu.be/ls9FSsO5U5I)

## 작업33, 댓글 수정 ajax화
- [커밋](https://github.com/jhs512/cuni/commit/16ff494e1d5b767e964a817135847e98a53f1e68)
- [동영상 - 2020 05 24, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 수정 ajax화, 1부](https://youtu.be/ZULc3gmaHH4)
- [동영상 - 2020 05 24, 스프링부트 실전, 커뮤니티 사이트, 작업, 댓글 수정 ajax화, 2부](https://youtu.be/u9R--KwmgIo)

## 작업34, 로그인 안하면 댓글 리스트 안나오는 버그 수정
- [커밋](https://github.com/jhs512/cuni/commit/3ecd71e361ec860d0883e65a9b3e34520dc09a98)
- [동영상 - 2020 05 30, 스프링부트 실전, 커뮤니티 사이트, 작업, 로그인 안하면 댓글 리스트 안나오는 버그 수정](https://youtu.be/wCp8LbbTuww)

## 작업35, 게시물리스트에서 게시물 개수 제한
- [커밋](https://github.com/jhs512/cuni/commit/62fc2ab39445d94badb9d78255010cb94f489b86)
- [동영상 - 2020 05 30, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물리스트에서 게시물 개수 제한](https://youtu.be/DiQZXNYsZTM)

## 작업36, 게시물리스트에서 게시물 페이징 마무리
- [커밋](https://github.com/jhs512/cuni/commit/2a8d57c2563af359688a7416f79e527121a0cbcb)
- [동영상 - 2020 05 30, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 페이징 마무리, 1부](https://youtu.be/0WfMCnh5FSs)
- [동영상 - 2020 05 30, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 페이징 마무리, 2부](https://youtu.be/DaSevneHuOc)

## 작업37, 게시물리스트에서 게시물 페이지가 너무 많을 때 처리
- [커밋](https://github.com/jhs512/cuni/commit/19e49a45b71f0286eca3605f3c574d9abd9a0eab)
- [동영상 - 2020 05 31, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 페이지가 너무 많을 때 처리](https://youtu.be/-FIn8iR0LF0)
- 관련링크
  - https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css
  - [https://cdnjs.com](https://cdnjs.com)
  - [https://fontawesome.com/icons?d=gallery&m=free](https://fontawesome.com/icons?d=gallery&m=free)
  
## 작업38, 게시물 검색
- [커밋](https://github.com/jhs512/cuni/commit/103e020578d13b7b2978f5fdce667f23a2c7435f)
- [동영상 - 2020 05 31, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 검색](https://youtu.be/i2YhW5I3kxU)
- [동영상 - 2020 05 31, 스프링부트 실전, 커뮤니티 사이트, 작업, 게시물 검색, 2부](https://youtu.be/5A-0y6Wojjo)

## 유용한 링크
- [IT 상식](https://cdpn.io/jangka44/debug/MWaBoVz)
- [학생들 포폴](https://to2.kr/bgH)
- [예전 앱개발](https://to2.kr/a9r)